package conf

import (
	"fmt"
)

// Database configuration
type Db struct {
	file       string
	host       string
	user       string
	pwd        string
	port       uint
	database   string
	schema     string
	driverUrl  string
	driver     string
	generator  string
	pluginConf *PluginConf
	actions    string
}

// Optional database configuration option function signature
type DbOption func(conf *Db) error

// Option to use schema generator
func WithGenerator(generator string) DbOption {
	return func(conf *Db) error {
		conf.generator = generator
		return nil
	}
}

func WithActionsProvider(actions string) DbOption {
	return func(conf *Db) error {
		conf.actions = actions
		return nil
	}
}

// Option to override default driver
func WithDriver(driver string) DbOption {
	return func(conf *Db) error {
		conf.driver = driver
		if conf.generator == "" {
			conf.generator = driver
		}
		if conf.actions == "" {
			conf.actions = driver
		}
		return nil
	}
}

// Option to create database configuration with specific driver url
func WithUrl(driverUrl string) DbOption {
	return func(conf *Db) error {
		conf.driverUrl = driverUrl
		return nil
	}
}

// Option to create database configuration with specific port
func WithPort(port uint) DbOption {
	return func(conf *Db) error {
		if port == 0 {
			return nil
		}
		if IsFileConf(conf) {
			return fmt.Errorf("Cannot set port for a file conf!")
		}
		if port > 65535 {
			return fmt.Errorf("Invalid port %#v", port)
		}
		conf.port = port
		return nil
	}
}

// Option to create database configuration with specific host
func WithHost(host string) DbOption {
	return func(conf *Db) error {
		if host == "" {
			return nil
		}
		if IsFileConf(conf) {
			return fmt.Errorf("Cannot set host for a file conf!")
		}
		conf.host = host
		return nil
	}
}

// Option to create database configuration with specific user
func WithUser(user string) DbOption {
	return func(conf *Db) error {
		if user == "" {
			return nil
		}
		conf.user = user
		return nil
	}
}

// Option to create database configuration with specific password
func WithPwd(pwd string) DbOption {
	return func(conf *Db) error {
		if pwd == "" {
			return nil
		}
		conf.pwd = pwd
		return nil
	}
}

// Option to create database configuration with specific schema
func WithSchema(schema string) DbOption {
	return func(conf *Db) error {
		if schema == "" {
			return nil
		}
		conf.schema = schema
		return nil
	}
}

// Option to create database configuration with specific database
func WithDb(db string) DbOption {
	return func(conf *Db) error {
		if db == "" {
			return nil
		}
		conf.database = db
		return nil
	}
}

// Option to create database configuration with specific local file
func WithFile(file string) DbOption {
	return func(conf *Db) error {
		if file == "" {
			return nil
		}
		if IsRemoteConf(conf) {
			return fmt.Errorf("Cannot set file for remote DB")
		}
		conf.file = file
		return nil
	}
}

// Option to create database configuration using another db conf as a starting point
func WithPlugin(plugin *PluginConf) DbOption {
	return func(nc *Db) error {
		if plugin == nil {
			return fmt.Errorf("Cannot use a nil plugin conf")
		}
		nc.pluginConf = plugin
		return nil
	}
}

// Option to create database configuration using another db conf as a starting point
func WithBase(conf *Db) DbOption {
	return func(nc *Db) error {
		if conf == nil {
			return fmt.Errorf("Cannot use a nil conf")
		}
		*nc = *conf
		return nil
	}
}

// Determines if a conf is a database conf
func IsFileConf(conf *Db) bool {
	return conf.file != ""
}

// Determines if a conf is a remote conf
func IsRemoteConf(conf *Db) bool {
	return conf.host != "" || conf.port != 0
}

// Makes a new database configuration
func MakeDb(options ...DbOption) (*Db, error) {
	res := &Db{}
	for _, option := range options {
		if err := option(res); err != nil {
			return nil, err
		}
	}

	return res, nil
}

// Gets the file from the db conf
func (cnf *Db) File() string { return cnf.file }

// Gets the database driver from the db conf
func (cnf *Db) Driver() string { return cnf.driver }

// Gets the database generator from the db conf
func (cnf *Db) Generator() string { return cnf.generator }

// Gets the database driver url from the conf
func (cnf *Db) DriverUrl() string { return cnf.driverUrl }

// Gets the database host from the conf
func (cnf *Db) Host() string { return cnf.host }

// Gets the database user from the conf
func (cnf *Db) User() string { return cnf.user }

// Gets the database password from the conf
func (cnf *Db) Pwd() string { return cnf.pwd }

// Gets the database port from the conf
func (cnf *Db) Port() uint { return cnf.port }

// Gets the database "database" from the conf
func (cnf *Db) Database() string { return cnf.database }

// Gets the database schema from the conf
func (cnf *Db) Schema() string { return cnf.schema }

// Gets the database schema from the conf
func (cnf *Db) Actions() string { return cnf.actions }
