package conf_test

import (
	"rove_migrate/conf"
	"testing"
)

func TestWithPortErr(t *testing.T) {
	cnf := &conf.Db{}

	err := conf.WithPort(65536)(cnf)
	if err == nil {
		t.Errorf("Expected error but did not get one. %#v", cnf)
	}

	err = conf.WithFile("test")(cnf)
	if err != nil {
		t.Fatalf("Unexpected error %v", err)
	}

	err = conf.WithPort(8080)(cnf)
	if err == nil {
		t.Errorf("Expected error but did not get one. %#v", cnf)
	}

	err = conf.WithPort(0)(cnf)
	if err != nil {
		t.Fatalf("Unexpected error %v", err)
	}
}

func TestWithPort(t *testing.T) {
	cnf := &conf.Db{}
	err := conf.WithPort(8080)(cnf)
	if err != nil {
		t.Fatalf("Unexpected error %v", err)
	}
	if cnf.Port() != 8080 {
		t.Errorf("Unable to set port")
	}
}

func TestWithHostErr(t *testing.T) {
	cnf := &conf.Db{}

	err := conf.WithFile("test")(cnf)
	if err != nil {
		t.Fatalf("Unexpected error %v", err)
	}
	err = conf.WithHost("local")(cnf)
	if err == nil {
		t.Errorf("Expected error but did not get one. %#v", cnf)
	}

	err = conf.WithHost("")(cnf)
	if err != nil {
		t.Fatalf("Unexpected error %v", err)
	}
}

func TestWithUrl(t *testing.T) {
	cnf := &conf.Db{}
	err := conf.WithPort(1230)(cnf)
	if err != nil {
		t.Fatalf("Unexpected error %v", err)
	}

	err = conf.WithUrl("file:local")(cnf)
	if err != nil {
		t.Fatalf("Unexpected error %v", err)
	}
	if cnf.DriverUrl() != "file:local" {
		t.Errorf("Unable to set host. %#v", cnf)
	}
}

func TestWithHost(t *testing.T) {
	cnf := &conf.Db{}
	err := conf.WithHost("local")(cnf)
	if err != nil {
		t.Fatalf("Unexpected error %v", err)
	}
	if cnf.Host() != "local" {
		t.Errorf("Unable to set host. %#v", cnf)
	}
}

func TestWithUser(t *testing.T) {
	cnf := &conf.Db{}
	err := conf.WithUser("test")(cnf)
	if err != nil {
		t.Fatalf("Unexpected error %v", err)
	}
}

func TestWithPwd(t *testing.T) {
	cnf := &conf.Db{}
	err := conf.WithPwd("test")(cnf)
	if err != nil {
		t.Fatalf("Unexpected error %v", err)
	}
}

func TestWithDb(t *testing.T) {
	cnf := &conf.Db{}
	err := conf.WithDb("test")(cnf)
	if err != nil {
		t.Fatalf("Unexpected error %v", err)
	}
}

func TestWithFile(t *testing.T) {
	cnf := &conf.Db{}
	err := conf.WithFile("test")(cnf)
	if err != nil {
		t.Fatalf("Unexpected error %v", err)
	}
}

func TestWithFileErr(t *testing.T) {
	cnf := &conf.Db{}

	err := conf.WithHost("test")(cnf)
	if err != nil {
		t.Fatalf("Unexpected error %v", err)
	}

	err = conf.WithFile("local")(cnf)
	if err == nil {
		t.Errorf("Expected error but did not get one. %#v", cnf)
	}

	err = conf.WithFile("")(cnf)
	if err != nil {
		t.Fatalf("Unexpected error %v", err)
	}

	cnf = &conf.Db{}
	err = conf.WithPort(8080)(cnf)
	if err != nil {
		t.Fatalf("Unexpected error %v", err)
	}

	err = conf.WithFile("local")(cnf)
	if err == nil {
		t.Errorf("Expected error but did not get one. %#v", cnf)
	}

	err = conf.WithFile("")(cnf)
	if err != nil {
		t.Fatalf("Unexpected error %v", err)
	}
}

func TestIsFileConf(t *testing.T) {
	cnf := &conf.Db{}

	if conf.IsFileConf(cnf) {
		t.Errorf("Unexpected file conf!")
	}
	if conf.IsRemoteConf(cnf) {
		t.Errorf("Unexpected remote conf!")
	}

	err := conf.WithFile("local.db")(cnf)
	if err != nil {
		t.Fatalf("Unexpected error %v", err)
	}

	if !conf.IsFileConf(cnf) {
		t.Errorf("Expected file conf!")
	}
	if conf.IsRemoteConf(cnf) {
		t.Errorf("Unexpected remote conf!")
	}

	cnf = &conf.Db{}
	err = conf.WithHost("localhost")(cnf)
	if err != nil {
		t.Fatalf("Unexpected error %v", err)
	}

	if conf.IsFileConf(cnf) {
		t.Errorf("Expected file conf!")
	}
	if !conf.IsRemoteConf(cnf) {
		t.Errorf("Expected remote conf!")
	}

	cnf = &conf.Db{}
	err = conf.WithPort(8080)(cnf)
	if err != nil {
		t.Fatalf("Unexpected error %v", err)
	}

	if conf.IsFileConf(cnf) {
		t.Errorf("Expected file conf!")
	}
	if !conf.IsRemoteConf(cnf) {
		t.Errorf("Expected remote conf!")
	}
}
