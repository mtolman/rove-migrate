package conf

import (
	"fmt"
	"os/exec"
	"runtime"
	log "github.com/sirupsen/logrus"
)

type PluginConf struct {
	Id           string      `json:"id" toml:"id"`
	DbDrivers    []string    `json:"drivers" toml:"drivers"`
	DbGenerators []string    `json:"generators,omitempty" toml:"generators"`
	Dir          string      `json:"workingDir,omitempty" toml:"workingDir"`
	Envs         []PluginEnv `json:"envs" toml:"envs"`
}

type PluginEnv struct {
	Os     string               `json:"os,omitempty" toml:"os"`
	Arch   string               `json:"arch,omitempty" toml:"arch"`
	OnPath []string             `json:"onpath,omitempty" toml:"onpath"`
	Setup  []PluginSetupCommand `json:"setup,omitempty" toml:"setup"`
	Exe    []string             `json:"exe" toml:"exe"`
}

type PluginSetupCommand struct {
	Command []string `json:"command" toml:"command"`
	Dir     string   `json:"workingDir,omitempty" toml:"workingDir"`
}

func (c *PluginConf) EnvFor() (*PluginEnv, error) {
	var env *PluginEnv
	ENVLOOP:
	for _, e := range c.Envs {
		if e.Os != "" && e.Os != runtime.GOOS {
			log.Warnf("Skipping environment for plugin %s because of OS mismatch", c.Id)
			continue ENVLOOP
		}
		if e.Arch != "" && e.Arch != runtime.GOARCH {
			log.Warnf("Skipping environment for plugin %s because of ARCH mismatch", c.Id)
			continue ENVLOOP
		}
		for _, op := range e.OnPath {
			if _, err := exec.LookPath(op); err != nil {
				log.Errorf("Skipping environment load for plugin %s because could not find %s on path. %v", c.Id, op, err)
				continue ENVLOOP
			}
		}
		env = &e
		break ENVLOOP
	}

	if env == nil {
		return nil, fmt.Errorf("Could not find usable ENV for plugin %s", c.Id)
	}

	return env, nil
}

