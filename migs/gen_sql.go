package migs

import (
	"database/sql"
	"errors"
	"fmt"
	"rove_migrate/common"

	"github.com/jmoiron/sqlx"
	log "github.com/sirupsen/logrus"
)

// Generic SQL method to get hashes
func SqlGetHashes(s *sqlx.DB, cnf *common.DbConf) ([]common.MigrationHashCheck, error) {
	migRes, err := s.Query("SELECT id, hash FROM rove_migrate__migs ORDER BY id ASC")
	if err != nil {
		return nil, fmt.Errorf("Unable to load migration state, %v", err)
	}
	defer migRes.Close()

	res := []common.MigrationHashCheck{}

	for migRes.Next() {
		var info common.MigrationHashCheck

		if err := migRes.Scan(&info.Name, &info.Hash); err != nil {
			return nil, fmt.Errorf("Unable to load migration hashes, %v", err)
		}

		res = append(res, info)
	}

	if err := migRes.Err(); err != nil {
		return nil, fmt.Errorf("Unable to load migration hashes, %v", err)
	}

	return res, nil
}

// Generic SQL method to get information about migrations
func SqlMigrationInfo(s *sqlx.DB, cnf *common.DbConf) (*common.MigrationOverview, error) {
	runsQuery := "SELECT id, migration_id, ctime, is_rollback, error, success FROM rove_migrate__runs ORDER BY id ASC"
	runsCount := "SELECT COUNT(*) FROM rove_migrate__runs WHERE migration_id = $1"
	cmdQuery := "SELECT id, run_id, migration_id, sql_cmd, ctime, utime FROM rove_migrate__cmd_log ORDER BY id ASC"

	migRes, err := s.Query("SELECT id, state, error, ctime, utime, hash FROM rove_migrate__migs ORDER BY id ASC")
	if err != nil {
		return nil, fmt.Errorf("Unable to load migration state, %v", err)
	}
	defer migRes.Close()

	var migrations common.MigrationOverview = common.MigrationOverview{Migrations: make(map[string]common.MigrationInfo)}

	for migRes.Next() {
		var info common.MigrationInfo

		if err := migRes.Scan(&info.Id, &info.State, &info.Err, &info.Ctime, &info.Utime, &info.Hash); err != nil {
			return nil, fmt.Errorf("Unable to load migration state, %v", err)
		}

		countRes := s.QueryRow(runsCount, info.Id)

		if err := countRes.Scan(&info.RunCount); err != nil {
			return nil, fmt.Errorf("Unable to load migration state, %v", err)
		}

		migrations.Migrations[info.Id] = info
	}

	if err := migRes.Err(); err != nil {
		return nil, fmt.Errorf("Unable to load migration state, %v", err)
	}

	runsRes, err := s.Query(runsQuery)
	if err != nil {
		return nil, fmt.Errorf("Unable to load migration run info, %v", err)
	}
	defer runsRes.Close()

	for runsRes.Next() {
		var info common.MigrationRunInfo

		if err := runsRes.Scan(&info.Id, &info.MigId, &info.Ctime, &info.IsRollback, &info.Error, &info.Success); err != nil {
			return nil, fmt.Errorf("Unable to load migration run info, %v", err)
		}

		migrations.Runs = append(migrations.Runs, info)
	}

	if err := runsRes.Err(); err != nil {
		return nil, fmt.Errorf("Unable to load migration run info, %v", err)
	}

	cmdRes, err := s.Query(cmdQuery)
	if err != nil {
		return nil, fmt.Errorf("Unable to load ran migration commands info, %v", err)
	}
	defer cmdRes.Close()

	for cmdRes.Next() {
		var info common.MigrationCmdInfo

		if err := cmdRes.Scan(&info.Id, &info.RunId, &info.Migration, &info.SqlCmd, &info.Ctime, &info.Utime); err != nil {
			return nil, fmt.Errorf("Unable to load ran migration commands info, %v", err)
		}

		migrations.Cmds = append(migrations.Cmds, info)
	}

	if err := cmdRes.Err(); err != nil {
		return nil, fmt.Errorf("Unable to load ran migration commands info, %v", err)
	}

	return &migrations, nil
}

// Generic SQL method to update run information
func SqlUpdateRunInfo(runId int64, run *common.Run, tx *sqlx.Tx) error {
	execQuery := `UPDATE rove_migrate__runs
SET success = $2, error = $3 WHERE id = $1`
	_, err := tx.Exec(execQuery, runId, run.Success, run.Error)
	if err != nil {
		return fmt.Errorf("Failed updating migration run information for run %v. Attempted to save values %#v", runId, *run)
	}
	return nil
}

// Generic SQL method to save command information log
func SqlSaveCmdInfo(cmd string, migration *common.Migration, tx *sqlx.Tx, runId int64) error {
	execQuery := `INSERT INTO rove_migrate__cmd_log
	(run_id, migration_id, sql_cmd)
VALUES ($1, $2, $3)`
	_, err := tx.Exec(execQuery, runId, migration.Name, cmd)
	if err != nil {
		return fmt.Errorf("Failed saving migration command log for migration run (%+v, %v). Command: %#v", migration.Name, runId, cmd)
	}
	return nil
}

// Gets migration state enum from migration run information
func migStateFromRun(run *common.Run) int {
	state := common.MIG_STATE_UNKNOWN
	switch {
	case !run.IsRollback && run.Success:
		state = common.MIG_STATE_SUCCESS
	case !run.IsRollback && !run.Success:
		state = common.MIG_STATE_ERROR
	case run.IsRollback && run.Success:
		state = common.MIG_STATE_ROLLBACK
	case run.IsRollback && !run.Success:
		state = common.MIG_STATE_ROLLBACK_ERROR
	default:
		log.Warnf("Migration %v in unknown state!", run.Migration)
	}
	return state
}

// Generic SQL method to save migration information summary
func SqlSaveMigInfo(mig *common.Migration, migState int, tx *sqlx.Tx, cnf *common.DbConf, err error) error {
	selQuery := `SELECT COUNT(*) FROM rove_migrate__migs WHERE id = $1`
	res := tx.QueryRow(selQuery, mig.Name)
	var count int64
	if err := res.Scan(&count); err != nil {
		return fmt.Errorf("Failed getting migration information for migration %+v", mig.Name)
	}

	var errStr *string
	if err != nil {
		log.Errorf("Migration %v failed with %v", mig.Name, err)
		s := err.Error()
		errStr = &s
	} else {
		errStr = nil
	}

	var execQuery string

	hash, err := mig.IntegrityHash()
	if err != nil {
		return err
	}

	if count > 0 {
		if migState == common.MIG_STATE_ROLLBACK {
			// Turn off hash verification for migrations that were rolled back
			// Rolled back migrations are usually rolled back due to a script issue
			// Often, they will be changed and remigrated in the future
			hash = "0:" + hash[2:]
			execQuery = `UPDATE rove_migrate__migs SET state = $2, error = $3, hash = $4 WHERE id = $1`
		} else {
			// Record hash for successful or errored migrations
			// We want users to manually indicate that an error is corrected
			execQuery = `UPDATE rove_migrate__migs SET state = $2, error = $3, hash = $4 WHERE id = $1`
		}
	} else {
		execQuery = `INSERT INTO rove_migrate__migs
	(id, state, error, hash)
VALUES ($1, $2, $3, $4)`
	}

	_, errSave := tx.Exec(execQuery, mig.Name, migState, errStr, hash)
	if errSave != nil {
		return fmt.Errorf("Unable to save migration state, reason: %+v", errSave)
	}
	return nil
}

// Generic SQL command to run a migration. Will use database actions for specific insertions/updates/queries which may differ between database
func SqlRunMigration(actions common.DbActions, migration *common.Migration, cmds []string, s *sqlx.DB, cnf *common.DbConf) (err error) {
	tx, err := s.Beginx()
	if err != nil {
		return fmt.Errorf("Unable to start transaction, cannot perform migration. Reason: %+v", err)
	}
	log.Info("Beginning transaction")

	run := &common.Run{Migration: migration.Name, IsRollback: false, Success: false, Error: nil}

	runState := migStateFromRun(run)
	saveMigErr := actions.SaveMigInfo(migration, runState, err, tx, cnf)
	if saveMigErr != nil {
		err = saveMigErr
		log.Errorf("Save migration failed. Attempting transaction rollback. Error: %v", err)
		if err := tx.Rollback(); err != nil {
			log.Panicf("Transaction rollback failed! Unknown DB state. Reason: %+v", err)
		}
		return err
	}

	runId, saveRunErr := actions.CreateRunInfo(run, tx, cnf)
	if saveRunErr != nil {
		err = saveRunErr
		log.Errorf("Save migration run error: %v. Marking migration as failed.", err)
		return err
	}

	defer func() {
		if err != nil {
			run.Success = false
			errStr := err.Error()
			run.Error = &errStr

			log.Errorf("Migration %v failed with error %v", migration.Name, err)
		} else {
			run.Success = true
		}

		saveRunErr := actions.UpdateRunInfo(runId, run, tx)
		if saveRunErr != nil {
			err = saveRunErr
			log.Errorf("Migration %v run info save failed with error %v", migration.Name, err)
		}

		runState := migStateFromRun(run)
		saveMigErr := actions.SaveMigInfo(migration, runState, err, tx, cnf)
		if saveMigErr != nil {
			err = saveMigErr
			log.Errorf("Save migration failed. Attempting transaction rollback. Error: %v", err)
			if err := tx.Rollback(); err != nil {
				log.Panicf("Transaction rollback failed! Unknown DB state. Reason: %+v", err)
			}
		} else if err := tx.Commit(); err != nil {
			log.Panicf("Transaction commit failed! Unknown DB state. Reason: %+v", err)
		}

		if err != nil {
			log.Infof("Migration %v succeeded", migration.Name)
		}
	}()

	for _, cmd := range cmds {
		if _, err := tx.Exec(cmd); err != nil {
			log.Errorf("Migration %v failed with error %v. Command which failed: %#v", migration.Name, err, cmd)
			return err
		}
		if cmdErr := actions.SaveCmdInfo(cmd, tx, runId, migration); cmdErr != nil {
			err = cmdErr
			log.Errorf("Failed to save migration command record for migration %v with error %v. Command which failed: %#v", migration.Name, err, cmd)
			return err
		}
	}
	return nil
}

// Runs assertion for a migration and makes sure that the database is in a good state
func SqlAssert(cmd string, tx *sqlx.Tx, cnf *common.DbConf) error {
	_, err := tx.Exec(cmd)
	if err != nil {
		return err
	}
	return nil
}

// Generic SQL command to rollback a migration. Will use database actions for specific insertions/updates/queries which may differ between database
func SqlRollbackMigration(actions common.DbActions, migration *common.Migration, cmds []string, s *sqlx.DB, cnf *common.DbConf) error {
	tx, err := s.Beginx()
	if err != nil {
		return fmt.Errorf("Unable to start transaction, cannot perform rollback. Reason: %+v", err)
	}
	log.Info("Begin transaction")

	run := &common.Run{Migration: migration.Name, IsRollback: true, Success: false, Error: nil}
	runId, saveRunErr := actions.CreateRunInfo(run, tx, cnf)
	if saveRunErr != nil {
		err = saveRunErr
		log.Errorf("Unable to create migration run info for rollback of %v. Error: %v", migration.Name, err)
		return err
	}

	defer func() {
		if err != nil {
			run.Success = false
			errStr := err.Error()
			run.Error = &errStr
			log.Errorf("Rollback of migration %v failed with error %v", migration.Name, err)
		} else {
			run.Success = true
		}

		saveRunErr := actions.UpdateRunInfo(runId, run, tx)
		if saveRunErr != nil {
			err = saveRunErr
			log.Errorf("Unable to update run info of migration %v. Error %v. Attempting transaction rollback", migration.Name, err)
			if err := tx.Rollback(); err != nil {
				log.Panicf("Unable to rollback transaction! Unknown DB state for rollback of %v. Error: %v", migration.Name, err)
			}
			return
		}

		runState := migStateFromRun(run)
		saveMigErr := actions.SaveMigInfo(migration, runState, err, tx, cnf)
		if saveMigErr != nil {
			err = saveMigErr
			log.Errorf("Unable to save migration state info for rollback of %v. Error: %v. Attempting transaction rollback", migration.Name, err)
			if err := tx.Rollback(); err != nil {
				log.Panicf("Unable to rollback transaction! Unknown DB state for rollback of %v. Error: %v", migration.Name, err)
			}
		} else if err := tx.Commit(); err != nil {
			log.Panicf("Unable to commit transaction! Unknown DB state for rollback of %v. Error: %v", migration.Name, err)
		}
	}()

	for _, cmd := range cmds {
		if _, err := tx.Exec(cmd); err != nil {
			log.Errorf("Failed to rollback %v. Command failed with error %v. Command which failed: %v", migration.Name, err, cmd)
			return err
		}
		if cmdErr := actions.SaveCmdInfo(cmd, tx, runId, migration); cmdErr != nil {
			err = cmdErr
			log.Errorf("Failed to save command log for rollback of migration %v. Error: %v. Command which failed to log: %v", migration.Name, err, cmd)
			return err
		}
	}
	return nil
}

// Generic SQL to get migration information with a specific state
func SqlGetMigrated(s *sqlx.DB, cnf *common.DbConf, state int) ([]string, error) {
	var migsQuery string

	if state != common.MIG_STATE_UNKNOWN {
		migsQuery = "SELECT id FROM rove_migrate__migs WHERE state = $1"
	} else {
		migsQuery = "SELECT id FROM rove_migrate__migs WHERE state <= $1 OR state > 4"
	}

	migRes, err := s.Query(migsQuery, state)
	if err != nil {
		log.Errorf("Unable to get which migrations have completed successfully! Error: %v", err)
		return nil, fmt.Errorf("Unable to get migration records, %+v", err)
	}
	defer migRes.Close()

	var res []string

	for migRes.Next() {
		var migName string

		if err := migRes.Scan(&migName); err != nil {
			log.Errorf("Unable to parse SQL response for successful migration records! Error: %v", err)
			return nil, fmt.Errorf("Unable to get migration records, %+v", err)
		}
		res = append(res, migName)
	}

	if err := migRes.Err(); err != nil {
		log.Errorf("Unable to parse SQL response for successful migration records! Error: %v", err)
		return nil, fmt.Errorf("Unable to get migration records, %+v", err)
	}

	return res, nil
}

func SqlGetLastSessionInfo(s *sqlx.DB, cnf *common.DbConf) (*common.SessionInfo, error) {
	sessionRow := s.QueryRow("SELECT id, ctime, is_rollback FROM rove_migrate__sessions ORDER BY id DESC LIMIT 1")
	res := &common.SessionInfo{}

	if err := sessionRow.Scan(&res.Id, &res.Ctime, &res.IsRollback); err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, fmt.Errorf("No migration sessions exist!")
		}
		return nil, fmt.Errorf("Cannot get session information. %v", err)
	}

	migrationRows, err := s.Query("SELECT migration_id FROM rove_migrate__sessions_migs WHERE session_id = $1 ORDER BY migration_id ASC", res.Id)
	if err != nil {
		return nil, fmt.Errorf("Cannot get session information. %v", err)
	}

	for migrationRows.Next() {
		var migName string
		if err := migrationRows.Scan(&migName); err != nil {
			return nil, fmt.Errorf("Cannot get session information. %v", err)
		}
		res.Migrations = append(res.Migrations, migName)
	}

	if err := migrationRows.Err(); err != nil {
		return nil, fmt.Errorf("Cannot get session information. %v", err)
	}

	return res, nil
}

func SqlGetSessionInfo(sessionId int64, s *sqlx.DB, cnf *common.DbConf) (*common.SessionInfo, error) {
	sessionRow := s.QueryRow("SELECT id, ctime, is_rollback FROM rove_migrate__sessions WHERE id = $1 LIMIT 1", sessionId)
	res := &common.SessionInfo{}

	if err := sessionRow.Scan(&res.Id, &res.Ctime, &res.IsRollback); err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, fmt.Errorf("No migration session exists with id %v", sessionId)
		}
		return nil, fmt.Errorf("Cannot get session information. %v", err)
	}

	migrationRows, err := s.Query("SELECT migration_id FROM rove_migrate__sessions_migs WHERE session_id = $1 ORDER BY migration_id ASC", sessionId)
	if err != nil {
		return nil, fmt.Errorf("Cannot get session information. %v", err)
	}

	for migrationRows.Next() {
		var migName string
		if err := migrationRows.Scan(&migName); err != nil {
			return nil, fmt.Errorf("Cannot get session information. %v", err)
		}
		res.Migrations = append(res.Migrations, migName)
	}

	if err := migrationRows.Err(); err != nil {
		return nil, fmt.Errorf("Cannot get session information. %v", err)
	}

	return res, nil
}
