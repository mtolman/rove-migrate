package migs

import (
	"encoding/json"
	"fmt"
	"os"
	"path/filepath"
	"regexp"
	"rove_migrate/common"
	"strconv"
	"strings"

	"github.com/BurntSushi/toml"
	log "github.com/sirupsen/logrus"
)

// Parse migrations from a JSON string
func ParseMigration(migration string, configType string) (*common.Migration, error) {
	var mig *common.Migration

	if configType == "json" {
		err := json.Unmarshal([]byte(migration), &mig)
		if err != nil {
			log.Errorf("Could not parse JSON for migration %v. Error: %v", migration, err)
			return nil, fmt.Errorf("Migration JSON parsing failed with %v", err)
		}
	} else if configType == "toml" {
		err := toml.Unmarshal([]byte(migration), &mig)
		if err != nil {
			log.Errorf("Could not parse TOML for migration %v. Error: %v", migration, err)
			return nil, fmt.Errorf("Migration TOML parsing failed with %v", err)
		}
	} else {
		return nil, fmt.Errorf("Invalid migration type %v", configType)
	}
	if len(mig.Name) > 500 {
		log.Errorf("Migration id must be 500 characters or less. ID: %+v", mig.Name)
		return nil, fmt.Errorf("Migration id exceeds 500 character limit. ID: %+v", mig.Name)
	}
	return mig, nil
}

// Loads migrations from a file directory
func LoadFrom(dir string) ([]*common.Migration, error) {
	migrationFiles := []string{}
	filepath.Walk(dir, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			log.Errorf("Could not load migration %v. Error: %v", path, err)
			return fmt.Errorf("Unable to load migration %#v. Error %v", path, err)
		}

		if info.IsDir() {
			return nil
		}

		if filepath.Ext(path) != ".json" {
			return nil
		}

		migrationFiles = append(migrationFiles, path)
		return nil
	})

	migrations := []*common.Migration{}
	for _, file := range migrationFiles {
		contents, err := os.ReadFile(file)
		if err != nil {
			log.Errorf("Could not load migration %v. Error: %v", file, err)
			return nil, fmt.Errorf("Error in migration file %#v. Error: %v", file, err)
		}

		configType := ""
		if strings.ToLower(filepath.Ext(file)) == ".json" {
			configType = "json"
		} else if strings.ToLower(filepath.Ext(file)) == ".toml" {
			configType = "toml"
		}

		mig, err := ParseMigration(string(contents), configType)
		if err != nil {
			log.Errorf("Could not load migration %v. Error: %v", file, err)
			return nil, fmt.Errorf("Error in migration file %#v. Error: %v", file, err)
		}
		if mig.FileDir == "" {
			mig.FileDir = filepath.Dir(file)
		} else if !filepath.IsAbs(mig.FileDir) {
			dir, err := filepath.Abs(filepath.Join(filepath.Dir(file), mig.FileDir))
			if err == nil {
				mig.FileDir = dir
			}
		}
		migrations = append(migrations, mig)
	}

	return migrations, nil
}

var (
	intRegex             = "^\\s*int(eger)?(\\(\\s*\\d+\\s*\\))?$"
	bigIntRegex          = "^\\s*bigint(\\s*\\(\\d+\\)\\s*)?$"
	tinyIntRegex         = "^\\s*(tinyint|smallint)(\\(\\s*\\d+\\s*\\))?$"
	bitRegex             = "^\\s*bit$"
	boolRegex            = "^\\s*bool(ean)?$"
	mediumIntRegex       = "^\\s*mediumint(\\(\\s*\\d+\\s*\\))?$"
	floatRegex           = "^\\s*float(\\(\\s*(\\d+)(,\\s*(\\d+))?\\s*\\))?$"
	doubleRegex          = "^\\s*double(\\(\\s*(\\d+)(,\\s*(\\d+))?\\s*\\))?$"
	doublePrecisionRegex = "^\\s*double precision(\\(\\s*(\\d+)(,\\s*(\\d+))?\\s*\\))?$"
	decimalRegex         = "^\\s*dec(imal)?(\\(\\s*(\\d+)\\s*(,\\s*(\\d+))?\\))?$"
	numericRegex         = "^\\s*numeric(\\(\\s*(\\d+)(,\\s*(\\d+))?\\s*\\))?$"
	charRegex            = "^\\s*char(\\(\\s*(\\d+)\\s*\\))?"
	varcharRegex         = "^\\s*varchar(\\(\\s*(\\d+)\\s*\\))?"
	binaryRegex          = "^\\s*binary(\\(\\s*(\\d+)\\s*\\))?"
	varbinaryRegex       = "^\\s*varbinary(\\(\\s*(\\d+)\\s*\\))?"
	textRegex            = "^\\s*text(\\(\\s*(\\d+)\\s*\\))?"
	blobRegex            = "^\\s*blob(\\(\\s*(\\d+)\\s*\\))?"
	tinyBlobRegex        = "^tinyblob$"
	tinyTextRegex        = "^tinytext$"
	mediumTextRegex      = "^mediumtext$"
	mediumBlobRegex      = "^mediumblob$"
	longTextRegex        = "^longtext$"
	longBlobRegex        = "^longblob$"
	dateRegex            = "^date$"
	dateTimeRegex        = "^datetime$"
	timestampRegex       = "^timestamp$"
	timeRegex            = "^time$"
	yearRegex            = "^year$"
	moneyRegex           = "^money$"
)

var cIntRegex, cBigIntRegex, cTinyIntRegex, cBitRegex, cBoolRegex, cMoneyRegex *regexp.Regexp
var cMediumIntRegex, cFloatRegex, cDoubleRegex, cDoublePrecisionRegex, cDecimalRegex, cNumericRegex *regexp.Regexp
var cCharRegex, cVarcharRegex, cBinaryRegex, cVarbinaryRegex, cTextRegex, cBlobRegex *regexp.Regexp
var cTinyBlobRegex, cTinyTextRegex, cMediumTextRegex, cMediumBlobRegex, cLongTextRegex, cLongBlobRegex *regexp.Regexp
var cDateRegex, cDateTimeRegex, cTimestampRegex, cTimeRegex, cYearRegex *regexp.Regexp

// Precompiles all of our regexes for determing the type of columns
func init() {
	var err error
	if cIntRegex, err = regexp.Compile(intRegex); err != nil {
		panic(err)
	}
	if cBigIntRegex, err = regexp.Compile(bigIntRegex); err != nil {
		panic(err)
	}
	if cTinyIntRegex, err = regexp.Compile(tinyIntRegex); err != nil {
		panic(err)
	}
	if cBitRegex, err = regexp.Compile(bitRegex); err != nil {
		panic(err)
	}
	if cBoolRegex, err = regexp.Compile(boolRegex); err != nil {
		panic(err)
	}
	if cMediumIntRegex, err = regexp.Compile(mediumIntRegex); err != nil {
		panic(err)
	}
	if cFloatRegex, err = regexp.Compile(floatRegex); err != nil {
		panic(err)
	}
	if cDoubleRegex, err = regexp.Compile(doubleRegex); err != nil {
		panic(err)
	}
	if cDoublePrecisionRegex, err = regexp.Compile(doublePrecisionRegex); err != nil {
		panic(err)
	}
	if cDecimalRegex, err = regexp.Compile(decimalRegex); err != nil {
		panic(err)
	}
	if cCharRegex, err = regexp.Compile(charRegex); err != nil {
		panic(err)
	}
	if cVarcharRegex, err = regexp.Compile(varcharRegex); err != nil {
		panic(err)
	}
	if cBinaryRegex, err = regexp.Compile(binaryRegex); err != nil {
		panic(err)
	}
	if cVarbinaryRegex, err = regexp.Compile(varbinaryRegex); err != nil {
		panic(err)
	}
	if cTextRegex, err = regexp.Compile(textRegex); err != nil {
		panic(err)
	}
	if cBlobRegex, err = regexp.Compile(blobRegex); err != nil {
		panic(err)
	}
	if cTinyBlobRegex, err = regexp.Compile(tinyBlobRegex); err != nil {
		panic(err)
	}
	if cTinyTextRegex, err = regexp.Compile(tinyTextRegex); err != nil {
		panic(err)
	}
	if cTinyBlobRegex, err = regexp.Compile(tinyBlobRegex); err != nil {
		panic(err)
	}
	if cTinyTextRegex, err = regexp.Compile(tinyTextRegex); err != nil {
		panic(err)
	}
	if cMediumBlobRegex, err = regexp.Compile(mediumBlobRegex); err != nil {
		panic(err)
	}
	if cMediumTextRegex, err = regexp.Compile(mediumTextRegex); err != nil {
		panic(err)
	}
	if cLongBlobRegex, err = regexp.Compile(longBlobRegex); err != nil {
		panic(err)
	}
	if cLongTextRegex, err = regexp.Compile(longTextRegex); err != nil {
		panic(err)
	}
	if cDateRegex, err = regexp.Compile(dateRegex); err != nil {
		panic(err)
	}
	if cDateTimeRegex, err = regexp.Compile(dateTimeRegex); err != nil {
		panic(err)
	}
	if cTimestampRegex, err = regexp.Compile(timestampRegex); err != nil {
		panic(err)
	}
	if cTimeRegex, err = regexp.Compile(timeRegex); err != nil {
		panic(err)
	}
	if cYearRegex, err = regexp.Compile(yearRegex); err != nil {
		panic(err)
	}
	if cNumericRegex, err = regexp.Compile(numericRegex); err != nil {
		panic(err)
	}
	if cMoneyRegex, err = regexp.Compile(moneyRegex); err != nil {
		panic(err)
	}
}

// Enums of supported SQL types. Using strings for readability, debugging, and easy reuse for fork/pipe protocols
const (
	SQL_INT              = "SQL_INT"
	SQL_BIGINT           = "SQL_BIGINT"
	SQL_TINYINT          = "SQL_TINYINT"
	SQL_MEDIUMINT        = "SQL_MEDIUMINT"
	SQL_BIT              = "SQL_BIT"
	SQL_BOOL             = "SQL_BOOL"
	SQL_FLOAT            = "SQL_FLOAT"
	SQL_DOUBLE           = "SQL_DOUBLE"
	SQL_DOUBLE_PRECISION = "SQL_DOUBLE_PRECISION"
	SQL_DECIMAL          = "SQL_DECIMAL"
	SQL_NUMERIC          = "SQL_NUMERIC"
	SQL_MONEY            = "SQL_MONEY"
	SQL_CHAR             = "SQL_CHAR"
	SQL_VARCHAR          = "SQL_VARCHAR"
	SQL_BINARY           = "SQL_BINARY"
	SQL_VARBINARY        = "SQL_VARBINARY"
	SQL_TINYBLOB         = "SQL_TINYBLOB"
	SQL_TINYTEXT         = "SQL_TINYTEXT"
	SQL_TEXT             = "SQL_TEXT"
	SQL_BLOB             = "SQL_BLOB"
	SQL_MEDIUMTEXT       = "SQL_MEDIUMTEXT"
	SQL_MEDIUMBLOB       = "SQL_MEDIUMBLOB"
	SQL_LONGTEXT         = "SQL_LONGTEXT"
	SQL_LONGBLOB         = "SQL_LONGBLOB"
	SQL_DATE             = "SQL_DATE"
	SQL_DATETIME         = "SQL_DATETIME"
	SQL_TIMESTAMP        = "SQL_TIMESTAMP"
	SQL_TIME             = "SQL_TIME"
	SQL_YEAR             = "SQL_YEAR"
)

// Describes the type of an SQL field
type SqlFieldType struct {
	SqlType  string
	Size     int
	DecPoint int
	Opts     []string
}

// Parses a string into an SQL field type description
func ToSqlFieldType(sqlType string) (*SqlFieldType, error) {
	sqlType = strings.ToLower(sqlType)
	if m := cIntRegex.FindAllStringSubmatch(sqlType, -1); m != nil {
		log.Debug("Found INT type")
		sizeStr := ""
		if len(m[0]) >= 3 {
			sizeStr = strings.TrimSpace(m[0][2])
		}
		size := 0

		if sizeStr != "" {
			end := len(sizeStr) - 1
			i, err := strconv.Atoi(sizeStr[1:end])
			if err != nil {
				return nil, fmt.Errorf("Unable to parse SQL display size for INT. %v", err)
			}
			size = i
		}
		log.Debugf("INT(%v)", size)
		return &SqlFieldType{SQL_INT, size, 0, nil}, nil
	}
	if m := cBigIntRegex.FindAllStringSubmatch(sqlType, -1); m != nil {
		log.Debug("Found BIGINT type")
		sizeStr := ""
		if len(m[0]) >= 2 {
			sizeStr = strings.TrimSpace(m[0][1])
		}
		size := 0

		if sizeStr != "" {
			end := len(sizeStr) - 1
			i, err := strconv.Atoi(sizeStr[1:end])
			if err != nil {
				return nil, fmt.Errorf("Unable to parse SQL display size for BIGINT. %v", err)
			}
			size = i
		}
		log.Debugf("BIGINT(%v)", size)
		return &SqlFieldType{SQL_BIGINT, size, 0, nil}, nil
	}
	if m := cTinyIntRegex.FindAllStringSubmatch(sqlType, -1); m != nil {
		log.Debug("Found TINYINT type")
		sizeStr := ""
		if len(m[0]) >= 3 {
			sizeStr = strings.TrimSpace(m[0][2])
		}
		size := 0

		if sizeStr != "" {
			end := len(sizeStr) - 1
			i, err := strconv.Atoi(sizeStr[1:end])
			if err != nil {
				return nil, fmt.Errorf("Unable to parse SQL display size for TINYINT. %v", err)
			}
			size = i
		}
		log.Debugf("TINYINT(%v)", size)
		return &SqlFieldType{SQL_TINYINT, size, 0, nil}, nil
	}
	if m := cMediumIntRegex.FindAllStringSubmatch(sqlType, -1); m != nil {
		log.Debug("Found MEDIUMINT type")
		sizeStr := ""
		if len(m[0]) >= 2 {
			sizeStr = strings.TrimSpace(m[0][1])
		}
		size := 0

		if sizeStr != "" {
			end := len(sizeStr) - 1
			i, err := strconv.Atoi(sizeStr[1:end])
			if err != nil {
				return nil, fmt.Errorf("Unable to parse SQL display size for MEDIUMINT. %v", err)
			}
			size = i
		}
		log.Debugf("MEDIUMINT(%v)", size)
		return &SqlFieldType{SQL_MEDIUMINT, size, 0, nil}, nil
	}
	if m := cBitRegex.FindAllStringSubmatch(sqlType, -1); m != nil {
		log.Debug("Found BIT type")
		return &SqlFieldType{SQL_BIT, 0, 0, nil}, nil
	}
	if m := cBoolRegex.FindAllStringSubmatch(sqlType, -1); m != nil {
		log.Debug("Found BOOL type")
		return &SqlFieldType{SQL_BOOL, 0, 0, nil}, nil
	}
	if m := cFloatRegex.FindAllStringSubmatch(sqlType, -1); m != nil {
		log.Debug("Found FLOAT type")
		sizeStr := ""
		precisionStr := ""
		if len(m[0]) >= 3 {
			sizeStr = strings.TrimSpace(m[0][2])
		}
		if len(m[0]) >= 5 {
			precisionStr = strings.TrimSpace(m[0][4])
		}
		size := 0
		precision := 0

		if sizeStr != "" {
			i, err := strconv.Atoi(sizeStr)
			if err != nil {
				return nil, fmt.Errorf("Unable to parse first SQL size param for FLOAT. %v", err)
			}
			size = i
		}

		if precisionStr != "" {
			i, err := strconv.Atoi(precisionStr)
			if err != nil {
				return nil, fmt.Errorf("Unable to parse second SQL size param for FLOAT. %v", err)
			}
			precision = i
		}
		log.Debugf("FLOAT(%v,%v)", size, precision)

		return &SqlFieldType{SQL_FLOAT, size, precision, nil}, nil
	}
	if m := cDoubleRegex.FindAllStringSubmatch(sqlType, -1); m != nil {
		log.Debugf("Found DOUBLE type")
		sizeStr := ""
		precisionStr := ""
		if len(m[0]) >= 3 {
			sizeStr = strings.TrimSpace(m[0][2])
		}
		if len(m[0]) >= 5 {
			precisionStr = strings.TrimSpace(m[0][4])
		}
		size := 0
		precision := 0

		if sizeStr != "" {
			i, err := strconv.Atoi(sizeStr)
			if err != nil {
				return nil, fmt.Errorf("Unable to parse first SQL size param for DOUBLE. %v", err)
			}
			size = i
		}

		if precisionStr != "" {
			i, err := strconv.Atoi(precisionStr)
			if err != nil {
				return nil, fmt.Errorf("Unable to parse second SQL size param for DOUBLE. %v", err)
			}
			precision = i
		}

		log.Debugf("DOUBLE(%v,%v)", size, precision)
		return &SqlFieldType{SQL_DOUBLE, size, precision, nil}, nil
	}
	if m := cDoublePrecisionRegex.FindAllStringSubmatch(sqlType, -1); m != nil {
		log.Debug("Found DOUBLE PRECISION type")
		sizeStr := ""
		precisionStr := ""
		if len(m[0]) >= 3 {
			sizeStr = strings.TrimSpace(m[0][2])
		}
		if len(m[0]) >= 5 {
			precisionStr = strings.TrimSpace(m[0][4])
		}
		size := 0
		precision := 0

		if sizeStr != "" {
			i, err := strconv.Atoi(sizeStr)
			if err != nil {
				return nil, fmt.Errorf("Unable to parse first SQL size param for DOUBLE PRECISION. %v", err)
			}
			size = i
		}

		if precisionStr != "" {
			i, err := strconv.Atoi(precisionStr)
			if err != nil {
				return nil, fmt.Errorf("Unable to parse second SQL size param for DOUBLE PRECISION. %v", err)
			}
			precision = i
		}

		log.Debugf("DOUBLE PRECISION(%v,%v)", size, precision)
		return &SqlFieldType{SQL_DOUBLE_PRECISION, size, precision, nil}, nil
	}
	if m := cMoneyRegex.FindAllStringSubmatch(sqlType, -1); m != nil {
		log.Debug("Found MONEY type")
		return &SqlFieldType{SqlType: SQL_MONEY, Size: 0, DecPoint: 0, Opts: nil}, nil
	}
	if m := cNumericRegex.FindAllStringSubmatch(sqlType, -1); m != nil {
		log.Debug("Found NUMERIC type")
		sizeStr := ""
		scaleStr := ""

		if len(m[0]) >= 3 {
			sizeStr = strings.TrimSpace(m[0][2])
		}
		if len(m[0]) >= 5 {
			scaleStr = strings.TrimSpace(m[0][4])
		}
		size := 0
		scale := 0

		if sizeStr != "" {
			i, err := strconv.Atoi(sizeStr)
			if err != nil {
				return nil, fmt.Errorf("Unable to parse first SQL size param for NUMERIC. %v", err)
			}
			size = i
		}

		if scaleStr != "" {
			i, err := strconv.Atoi(scaleStr)
			if err != nil {
				return nil, fmt.Errorf("Unable to parse second SQL size param for NUMERIC. %v", err)
			}
			scale = i
		}

		log.Debugf("NUMERIC(%v,%v)", size, scale)
		return &SqlFieldType{SqlType: SQL_NUMERIC, Size: size, DecPoint: scale, Opts: nil}, nil
	}
	if m := cDecimalRegex.FindAllStringSubmatch(sqlType, -1); m != nil {
		log.Debug("Found NUMERIC type")
		sizeStr := ""
		scaleStr := ""

		if len(m[0]) >= 4 {
			sizeStr = strings.TrimSpace(m[0][3])
		}
		if len(m[0]) >= 6 {
			scaleStr = strings.TrimSpace(m[0][5])
		}
		size := 0
		scale := 0

		if sizeStr != "" {
			i, err := strconv.Atoi(sizeStr)
			if err != nil {
				return nil, fmt.Errorf("Unable to parse first SQL size param for DECIMAL. %v", err)
			}
			size = i
		}

		if scaleStr != "" {
			i, err := strconv.Atoi(scaleStr)
			if err != nil {
				return nil, fmt.Errorf("Unable to parse second SQL size param for DECIMAL. %v", err)
			}
			scale = i
		}

		log.Debugf("NUMERIC(%v, %v)", size, scale)
		return &SqlFieldType{SqlType: SQL_DECIMAL, Size: size, DecPoint: scale, Opts: nil}, nil
	}
	if m := cCharRegex.FindAllStringSubmatch(sqlType, -1); m != nil {
		log.Debug("Found CHAR type")
		sizeStr := ""
		if len(m[0]) >= 3 {
			sizeStr = strings.TrimSpace(m[0][2])
		}
		size := 0

		if sizeStr != "" {
			i, err := strconv.Atoi(sizeStr)
			if err != nil {
				return nil, fmt.Errorf("Unable to parse SQL size param for CHAR. %v", err)
			}
			size = i
		}
		log.Debugf("CHAR(%v)", size)
		return &SqlFieldType{SQL_CHAR, size, 0, nil}, nil
	}
	if m := cVarcharRegex.FindAllStringSubmatch(sqlType, -1); m != nil {
		log.Debug("Found VARCHAR type")
		sizeStr := ""
		if len(m[0]) >= 3 {
			sizeStr = strings.TrimSpace(m[0][2])
		}
		size := 0

		if sizeStr != "" {
			i, err := strconv.Atoi(sizeStr)
			if err != nil {
				return nil, fmt.Errorf("Unable to parse SQL size param for VARCHAR. %v", err)
			}
			size = i
		}
		log.Debugf("VARCHAR(%v)", size)
		return &SqlFieldType{SQL_VARCHAR, size, 0, nil}, nil
	}
	if m := cBinaryRegex.FindAllStringSubmatch(sqlType, -1); m != nil {
		log.Debug("Found BINARY type")
		sizeStr := ""
		if len(m[0]) >= 3 {
			sizeStr = strings.TrimSpace(m[0][2])
		}
		size := 0

		if sizeStr != "" {
			i, err := strconv.Atoi(sizeStr)
			if err != nil {
				return nil, fmt.Errorf("Unable to parse SQL size param for BINARY. %v", err)
			}
			size = i
		}
		log.Debugf("BINARY(%v)", size)
		return &SqlFieldType{SQL_BINARY, size, 0, nil}, nil
	}
	if m := cVarbinaryRegex.FindAllStringSubmatch(sqlType, -1); m != nil {
		log.Debug("Found VARBINARY type")
		sizeStr := ""
		if len(m[0]) >= 3 {
			sizeStr = strings.TrimSpace(m[0][2])
		}
		size := 0

		if sizeStr != "" {
			i, err := strconv.Atoi(sizeStr)
			if err != nil {
				return nil, fmt.Errorf("Unable to parse SQL size param for VARBINARY. %v", err)
			}
			size = i
		}
		log.Debugf("VARBINARY(%v)", size)
		return &SqlFieldType{SQL_VARBINARY, size, 0, nil}, nil
	}
	if m := cTextRegex.FindAllStringSubmatch(sqlType, -1); m != nil {
		log.Debug("Found TEXT type")
		sizeStr := ""
		if len(m[0]) >= 3 {
			sizeStr = strings.TrimSpace(m[0][2])
		}
		size := 0

		if sizeStr != "" {
			i, err := strconv.Atoi(sizeStr)
			if err != nil {
				return nil, fmt.Errorf("Unable to parse SQL size param for TEXT. %v", err)
			}
			size = i
		}
		log.Debugf("TEXT(%v)", size)
		return &SqlFieldType{SQL_TEXT, size, 0, nil}, nil
	}
	if m := cBlobRegex.FindAllStringSubmatch(sqlType, -1); m != nil {
		log.Debug("Found BLOB type")
		sizeStr := ""
		if len(m[0]) >= 3 {
			sizeStr = strings.TrimSpace(m[0][2])
		}
		size := 0

		if sizeStr != "" {
			i, err := strconv.Atoi(sizeStr)
			if err != nil {
				return nil, fmt.Errorf("Unable to parse SQL size param for BLOB. %v", err)
			}
			size = i
		}
		log.Debugf("BLOB(%v)", size)
		return &SqlFieldType{SQL_BLOB, size, 0, nil}, nil
	}
	if m := cTinyBlobRegex.FindAllStringSubmatch(sqlType, -1); m != nil {
		log.Debug("Found TINYBLOB type")
		return &SqlFieldType{SQL_TINYBLOB, 0, 0, nil}, nil
	}
	if m := cTinyTextRegex.FindAllStringSubmatch(sqlType, -1); m != nil {
		log.Debug("Found TINYTEXT type")
		return &SqlFieldType{SQL_TINYTEXT, 0, 0, nil}, nil
	}
	if m := cMediumTextRegex.FindAllStringSubmatch(sqlType, -1); m != nil {
		log.Debug("Found MEDIUMTEXT type")
		return &SqlFieldType{SQL_MEDIUMTEXT, 0, 0, nil}, nil
	}
	if m := cMediumBlobRegex.FindAllStringSubmatch(sqlType, -1); m != nil {
		log.Debug("Found MEDIUMBLOB type")
		return &SqlFieldType{SQL_MEDIUMBLOB, 0, 0, nil}, nil
	}
	if m := cLongTextRegex.FindAllStringSubmatch(sqlType, -1); m != nil {
		log.Debug("Found LONGTEXT type")
		return &SqlFieldType{SQL_LONGTEXT, 0, 0, nil}, nil
	}
	if m := cLongBlobRegex.FindAllStringSubmatch(sqlType, -1); m != nil {
		log.Debug("Found LONGBLOB type")
		return &SqlFieldType{SQL_LONGBLOB, 0, 0, nil}, nil
	}
	if m := cDateRegex.FindAllStringSubmatch(sqlType, -1); m != nil {
		log.Debug("Found DATE type")
		return &SqlFieldType{SQL_DATE, 0, 0, nil}, nil
	}
	if m := cDateTimeRegex.FindAllStringSubmatch(sqlType, -1); m != nil {
		log.Debug("Found DATETIME type")
		return &SqlFieldType{SQL_DATETIME, 0, 0, nil}, nil
	}
	if m := cTimestampRegex.FindAllStringSubmatch(sqlType, -1); m != nil {
		log.Debug("Found TIMESTAMP type")
		return &SqlFieldType{SQL_TIMESTAMP, 0, 0, nil}, nil
	}
	if m := cTimeRegex.FindAllStringSubmatch(sqlType, -1); m != nil {
		log.Debug("Found TIME type")
		return &SqlFieldType{SQL_TIME, 0, 0, nil}, nil
	}
	if m := cYearRegex.FindAllStringSubmatch(sqlType, -1); m != nil {
		log.Debug("Found YEAR type")
		return &SqlFieldType{SQL_YEAR, 0, 0, nil}, nil
	}

	log.Errorf("Unknown SQL type %v", sqlType)
	return nil, fmt.Errorf("Unsupported SQL type %#v", sqlType)
}
