package migs

import (
	"errors"
	"fmt"
	"reflect"
	"testing"
)

func TestToSqlFieldType(t *testing.T) {
	tests := []struct {
		input string
		out   *SqlFieldType
		err   error
	}{
		{"int", &SqlFieldType{SQL_INT, 0, 0, nil}, nil},
		{"int(10)", &SqlFieldType{SQL_INT, 10, 0, nil}, nil},
		{"bigint", &SqlFieldType{SQL_BIGINT, 0, 0, nil}, nil},
		{"bigint(10)", &SqlFieldType{SQL_BIGINT, 10, 0, nil}, nil},
		{"tinyint", &SqlFieldType{SQL_TINYINT, 0, 0, nil}, nil},
		{"tinyint(10)", &SqlFieldType{SQL_TINYINT, 10, 0, nil}, nil},
		{"smallint", &SqlFieldType{SQL_TINYINT, 0, 0, nil}, nil},
		{"smallint(10)", &SqlFieldType{SQL_TINYINT, 10, 0, nil}, nil},
		{"mediumint", &SqlFieldType{SQL_MEDIUMINT, 0, 0, nil}, nil},
		{"mediumint(10)", &SqlFieldType{SQL_MEDIUMINT, 10, 0, nil}, nil},
		{"bit", &SqlFieldType{SQL_BIT, 0, 0, nil}, nil},
		{"bool", &SqlFieldType{SQL_BOOL, 0, 0, nil}, nil},
		{"float", &SqlFieldType{SQL_FLOAT, 0, 0, nil}, nil},
		{"float(10)", &SqlFieldType{SQL_FLOAT, 10, 0, nil}, nil},
		{"float(10, 10)", &SqlFieldType{SQL_FLOAT, 10, 10, nil}, nil},
		{"double", &SqlFieldType{SQL_DOUBLE, 0, 0, nil}, nil},
		{"double(10)", &SqlFieldType{SQL_DOUBLE, 10, 0, nil}, nil},
		{"double(10, 10)", &SqlFieldType{SQL_DOUBLE, 10, 10, nil}, nil},
		{"double precision", &SqlFieldType{SQL_DOUBLE_PRECISION, 0, 0, nil}, nil},
		{"double precision(10)", &SqlFieldType{SQL_DOUBLE_PRECISION, 10, 0, nil}, nil},
		{"double precision(10, 10)", &SqlFieldType{SQL_DOUBLE_PRECISION, 10, 10, nil}, nil},
		{"decimal", &SqlFieldType{SQL_DECIMAL, 0, 0, nil}, nil},
		{"decimal(10)", &SqlFieldType{SQL_DECIMAL, 10, 0, nil}, nil},
		{"decimal(10, 5)", &SqlFieldType{SQL_DECIMAL, 10, 5, nil}, nil},
		{"numeric", &SqlFieldType{SQL_NUMERIC, 0, 0, nil}, nil},
		{"numeric(10)", &SqlFieldType{SQL_NUMERIC, 10, 0, nil}, nil},
		{"numeric(10, 5)", &SqlFieldType{SQL_NUMERIC, 10, 5, nil}, nil},
		{"dec", &SqlFieldType{SQL_DECIMAL, 0, 0, nil}, nil},
		{"dec(10)", &SqlFieldType{SQL_DECIMAL, 10, 0, nil}, nil},
		{"dec(10, 5)", &SqlFieldType{SQL_DECIMAL, 10, 5, nil}, nil},
		{"binary", &SqlFieldType{SQL_BINARY, 0, 0, nil}, nil},
		{"binary(10)", &SqlFieldType{SQL_BINARY, 10, 0, nil}, nil},
		{"varbinary", &SqlFieldType{SQL_VARBINARY, 0, 0, nil}, nil},
		{"varbinary(10)", &SqlFieldType{SQL_VARBINARY, 10, 0, nil}, nil},
		{"char", &SqlFieldType{SQL_CHAR, 0, 0, nil}, nil},
		{"char(10)", &SqlFieldType{SQL_CHAR, 10, 0, nil}, nil},
		{"varchar", &SqlFieldType{SQL_VARCHAR, 0, 0, nil}, nil},
		{"varchar(10)", &SqlFieldType{SQL_VARCHAR, 10, 0, nil}, nil},
		{"text", &SqlFieldType{SQL_TEXT, 0, 0, nil}, nil},
		{"text(10)", &SqlFieldType{SQL_TEXT, 10, 0, nil}, nil},
		{"blob", &SqlFieldType{SQL_BLOB, 0, 0, nil}, nil},
		{"blob(10)", &SqlFieldType{SQL_BLOB, 10, 0, nil}, nil},
		{"tinyblob", &SqlFieldType{SQL_TINYBLOB, 0, 0, nil}, nil},
		{"tinytext", &SqlFieldType{SQL_TINYTEXT, 0, 0, nil}, nil},
		{"mediumblob", &SqlFieldType{SQL_MEDIUMBLOB, 0, 0, nil}, nil},
		{"mediumtext", &SqlFieldType{SQL_MEDIUMTEXT, 0, 0, nil}, nil},
		{"longblob", &SqlFieldType{SQL_LONGBLOB, 0, 0, nil}, nil},
		{"longtext", &SqlFieldType{SQL_LONGTEXT, 0, 0, nil}, nil},
		{"date", &SqlFieldType{SQL_DATE, 0, 0, nil}, nil},
		{"datetime", &SqlFieldType{SQL_DATETIME, 0, 0, nil}, nil},
		{"timestamp", &SqlFieldType{SQL_TIMESTAMP, 0, 0, nil}, nil},
		{"time", &SqlFieldType{SQL_TIME, 0, 0, nil}, nil},
		{"year", &SqlFieldType{SQL_YEAR, 0, 0, nil}, nil},
		{"money", &SqlFieldType{SQL_MONEY, 0, 0, nil}, nil},
	}

	for _, test := range tests {
		t.Run(fmt.Sprintf("Input %s", test.input), func(t *testing.T) {
			actual, err := ToSqlFieldType(test.input)
			if test.err != nil {
				if err == nil {
					t.Fatalf("Expected error %#v", err)
				} else if !errors.Is(err, test.err) {
					t.Fatalf("Expected error %#v, received %#v", test.err, err)
				}
				return
			}

			if err != nil {
				t.Fatalf("Unexpected error %#v", err)
			}

			if actual.SqlType != test.out.SqlType || actual.Size != test.out.Size || actual.DecPoint != test.out.DecPoint {
				t.Errorf("Expected %#v, received %#v", *test.out, *actual)
			}
			if !reflect.DeepEqual(actual.Opts, test.out.Opts) {
				t.Errorf("Expected options %#v, received %#v", test.out.Opts, actual.Opts)
			}
		})
	}
}
