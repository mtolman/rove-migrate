package common_test

import (
	"rove_migrate/common"
	"rove_migrate/conf"
	"testing"

	"github.com/jmoiron/sqlx"
)

type testActions struct{}
type testSqlGen struct{}

func (tsg testSqlGen) GenAlterTable(tc common.TableAlter) (string, error) {
	return "ALTER TEST", nil
}

func (tsg testSqlGen) RevAlterTable(tc common.TableAlter) (string, error) {
	return "REVERT ALTER TEST", nil
}

func (tsg testSqlGen) GenCreateTable(tc common.TableCreate) (string, error) {
	return "CREATE TEST", nil
}

func (tsg testSqlGen) RevCreateTable(tc common.TableCreate) (string, error) {
	return "DROP TEST", nil
}

func (tsg testSqlGen) GenCreateIndex(ic common.IndexCreate) (string, error) {
	return "CREATE INDEX", nil
}

func (tsg testSqlGen) RevCreateIndex(ic common.IndexCreate) (string, error) {
	return "DROP INDEX", nil
}

func (tsg testSqlGen) Generate(generation, variance string, args any) (string, error) {
	return common.GenerateNative(tsg, generation, variance, args)
}

func (ta testActions) GetInfo(s *sqlx.DB, cnf *common.DbConf) (*common.Info, error) {
	return &common.Info{}, nil
}

func (ta testActions) GetHashes(s *sqlx.DB, cnf *common.DbConf) ([]common.MigrationHashCheck, error) {
	return nil, nil
}

func (ta testActions) EnsureDatabase(database string, s *sqlx.DB) error {
	return nil
}

func (ta testActions) EnsureSchema(schemaName string, s *sqlx.DB) error {
	return nil
}

func (ta testActions) EnsureSetup(s *sqlx.DB, cnf *common.DbConf) error {
	return nil
}

func (ta testActions) Assert(cmd string, s *sqlx.Tx, cnf *common.DbConf) error {
	return nil
}

func (ta testActions) CreateSession(isRollback bool, s *sqlx.DB, cnf *common.DbConf) (int64, error) {
	return 0, nil
}

func (ta testActions) SaveMigrationToSession(sessionId int64, mig *common.Migration, s *sqlx.DB, cnf *common.DbConf) error {
	return nil
}

func (ta testActions) GetLastSessionInfo(s *sqlx.DB, cnf *common.DbConf) (*common.SessionInfo, error) {
	return nil, nil
}

func (ta testActions) GetSessionInfo(sessionId int64, s *sqlx.DB, cnf *common.DbConf) (*common.SessionInfo, error) {
	return nil, nil
}

func (ta testActions) GetMigrationInfo(s *sqlx.DB, cnf *common.DbConf) (*common.MigrationOverview, error) {
	return &common.MigrationOverview{}, nil
}

func (ta testActions) RunMigration(m *common.Migration, cmds []string, s *sqlx.DB, cnf *common.DbConf) error {
	return nil
}

func (ta testActions) RollbackMigration(m *common.Migration, cmds []string, s *sqlx.DB, cnf *common.DbConf) error {
	return nil
}

func (ta testActions) UpdateRunInfo(runId int64, run *common.Run, tx *sqlx.Tx) error {
	return nil
}

func (ta testActions) CreateRunInfo(run *common.Run, tx *sqlx.Tx, cnf *common.DbConf) (int64, error) {
	return 0, nil
}

func (ta testActions) SaveCmdInfo(cmd string, tx *sqlx.Tx, runId int64, migration *common.Migration) error {
	return nil
}

func (ta testActions) SaveMigInfo(mig *common.Migration, migState int, err error, tx *sqlx.Tx, cnf *common.DbConf) error {
	return nil
}

func (ta testActions) GetMigrated(s *sqlx.DB, cnf *common.DbConf, state int) ([]string, error) {
	return []string{}, nil
}

func testUrlBuilder(cnf *common.DbConf) (string, error) {
	driver, err := cnf.SqlDriver()
	if err != nil {
		return "", err
	}
	return "test:" + driver, nil
}

func TestDrivers(t *testing.T) {
	t.Run("Empty driver list", func(t *testing.T) {
		drivers := common.DbDrivers()
		if len(drivers) != 0 {
			t.Fatalf("Unexpected drivers found! Expected 0 drivers! Received: %#v", drivers)
		}
	})

	t.Run("Register new remote driver", func(t *testing.T) {
		common.RegisterRemoteDriver("test_remote", "remote", testActions{}, testUrlBuilder, 8080)
		drivers := common.DbDrivers()
		if len(drivers) != 1 {
			t.Fatalf("Unexpected drivers found! Expected 1 driver! Received: %#v", drivers)
		}
	})

	t.Run("Register new file driver", func(t *testing.T) {
		common.RegisterFileDriver("test_file", "file", testActions{}, testUrlBuilder)
		drivers := common.DbDrivers()
		if len(drivers) != 2 {
			t.Fatalf("Unexpected drivers found! Expected 2 drivers! Received: %#v", drivers)
		}
	})

	t.Run("Default port for", func(t *testing.T) {
		if port, err := common.PortFor("test_remote"); port != 8080 || err != nil {
			t.Fatalf("Unexpected port for test_remote. Expected 8080, recieved %v, err %v", port, err)
		}
		if port, err := common.PortFor("test_file"); port != 0 || err != nil {
			t.Fatalf("Unexpected port for test_file. Expected 0, recieved %v, err %v", port, err)
		}
		if port, err := common.PortFor("unknown"); port != 0 || err == nil {
			t.Fatalf("Unexpected port for test_file. Expected error, recieved %v, err %v", port, err)
		}
	})

	t.Run("Default port conf for remote conf", func(t *testing.T) {
		cnf, err := common.NewConf(conf.WithDriver("test_remote"), conf.WithHost("localhost"), conf.WithUser("user"))
		if err != nil {
			t.Fatalf("Unexpected error %#v", err)
		}
		if cnf.Port() != 8080 {
			t.Errorf("Expected port 8080, received %#v", cnf.Port())
		}
		if cnf.Host() != "localhost" {
			t.Errorf("Expected host \"localhost\", received %#v", cnf.Host())
		}
		if cnf.User() != "user" {
			t.Errorf("Expected user \"user\", received %#v", cnf.Host())
		}
		driver, err := cnf.SqlDriver()
		if err != nil {
			t.Fatalf("Unexpected error %#v", err)
		}
		if driver != "remote" {
			t.Errorf("Expected driver \"remote\", received %#v", driver)
		}
		url, err := cnf.Url()
		if err != nil {
			t.Fatalf("Unexpected error %#v", err)
		}
		if url != "test:remote" {
			t.Errorf("Expected url \"test:remote\", recieved %#v", url)
		}
		actions, err := cnf.MigrationActions()
		if err != nil {
			t.Fatalf("Unexpected error %#v", err)
		}

		switch actions.(type) {
		default:
			t.Fatalf("Unexpected actions %#v", actions)
		case testActions:
		}
	})

	t.Run("Specified port conf for remote conf", func(t *testing.T) {
		cnf, err := common.NewConf(conf.WithDriver("test_remote"), conf.WithPort(9090), conf.WithHost("localhost"), conf.WithUser("user"))
		if err != nil {
			t.Fatalf("Unexpected error %#v", err)
		}
		if cnf.Port() != 9090 {
			t.Errorf("Expected port 9090, received %#v", cnf.Port())
		}
		if cnf.Host() != "localhost" {
			t.Errorf("Expected host \"localhost\", received %#v", cnf.Host())
		}
		if cnf.User() != "user" {
			t.Errorf("Expected user \"user\", received %#v", cnf.Host())
		}
		driver, err := cnf.SqlDriver()
		if err != nil {
			t.Fatalf("Unexpected error %#v", err)
		}
		if driver != "remote" {
			t.Errorf("Expected driver \"remote\", received %#v", driver)
		}
		url, err := cnf.Url()
		if err != nil {
			t.Fatalf("Unexpected error %#v", err)
		}
		if url != "test:remote" {
			t.Errorf("Expected url \"test:remote\", recieved %#v", url)
		}
		actions, err := cnf.MigrationActions()
		if err != nil {
			t.Fatalf("Unexpected error %#v", err)
		}

		switch actions.(type) {
		default:
			t.Fatalf("Unexpected actions %#v", actions)
		case testActions:
		}
	})

	t.Run("Conf for file conf", func(t *testing.T) {
		cnf, err := common.NewConf(conf.WithDriver("test_file"), conf.WithFile("test.db"))
		if err != nil {
			t.Fatalf("Unexpected error %#v", err)
		}
		if cnf.Port() != 0 {
			t.Errorf("Expected port 0, received %#v", cnf.Port())
		}
		if cnf.Host() != "" {
			t.Errorf("Expected host \"localhost\", received %#v", cnf.Host())
		}
		if cnf.File() != "test.db" {
			t.Errorf("Expected file \"test.db\", received %#v", cnf.Host())
		}
		driver, err := cnf.SqlDriver()
		if err != nil {
			t.Fatalf("Unexpected error %#v", err)
		}
		if driver != "file" {
			t.Errorf("Expected driver \"file\", received %#v", driver)
		}
		url, err := cnf.Url()
		if err != nil {
			t.Fatalf("Unexpected error %#v", err)
		}
		if url != "test:file" {
			t.Errorf("Expected url \"test:file\", recieved %#v", url)
		}
		actions, err := cnf.MigrationActions()
		if err != nil {
			t.Fatalf("Unexpected error %#v", err)
		}

		switch actions.(type) {
		default:
			t.Fatalf("Unexpected actions %#v", actions)
		case testActions:
		}
	})

	t.Run("Conf for invalid conf", func(t *testing.T) {
		_, err := common.NewConf(conf.WithDriver("invalid"), conf.WithFile("test.db"))
		if err == nil {
			t.Fatalf("Expected error")
		}
	})
}
