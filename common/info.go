package common

import (
	"fmt"
	"slices"
	"strconv"
	"strings"
	"time"

	"github.com/charmbracelet/lipgloss"
	"github.com/charmbracelet/lipgloss/table"
)

// Information for running migration hash checks
type MigrationHashCheck struct {
	// Name/ID of migration
	Name string
	// Hash of migration
	Hash string
}

// Overview of migration data
type MigrationOverview struct {
	// Data for all migrations
	Migrations map[string]MigrationInfo
	// Data for migration runs
	Runs []MigrationRunInfo
	// Data for migration commands
	Cmds []MigrationCmdInfo
}

// Overview of migration run information
type MigrationRunInfo struct {
	// Run ID
	Id int64
	// Associated migration id
	MigId string
	// Run information create time (usually start time)
	Ctime time.Time
	// Run information update time (usually end time)
	Utime time.Time
	// Whether the run was a rollback
	IsRollback bool
	// Any error messages tied to the run
	Error *string
	// Whether the run was successful
	Success bool
}

// Command information for a migration
type MigrationCmdInfo struct {
	// Command entry id (basically order id)
	Id int64
	// Run id
	RunId int64
	// migration id
	Migration string
	// SQL command that was ran
	SqlCmd string
	// Create time of record
	Ctime time.Time
	// Update time of record
	Utime time.Time
	// Errors when the command ran
	Error *string
}

// Information related to database state and information
type Info struct {
	// Information about database tables
	Tables []TableInfo
	// Information about migrations and migration runs
	Migrations *MigrationOverview
	// Information about database indexes
	Indexes []IndexInfo
	// Information about database triggers
	Triggers []TriggerInfo
	// Version information about the database
	VersionInfo map[string]string
	// Migration session informatino
	Sessions []SessionInfo
}

// Information related to a migration session
type SessionInfo struct {
	// Session ID
	Id int64
	// Migrations tied to session
	Migrations []string
	// Whether session was a rollback
	IsRollback bool
	// When session started
	Ctime time.Time
}

// Information about a database indesx
type IndexInfo struct {
	// Index name
	Name string
	// Database table tied to the index
	Table string
}

// Information about a database trigger
type TriggerInfo struct {
	// Trigger name
	Name string
	// Database table tied to the trigger
	Table string
}

// Migration state enums
const (
	MIG_STATE_UNKNOWN = iota
	MIG_STATE_SUCCESS
	MIG_STATE_ERROR
	MIG_STATE_ROLLBACK
	MIG_STATE_ROLLBACK_ERROR
)

// Information about a migration
type MigrationInfo struct {
	// ID of a migration
	Id string
	// Migration state (see MIG_STATE_* consts)
	State int
	// Errors that happened while performing the migration
	Err *string
	// Time migration record was created
	Ctime time.Time
	// Time record was last updated
	Utime time.Time
	// Number of runs (migrations, rollbacks, errors) tied to migration
	RunCount int
	// Integrity hash tied to migration. If it starts with '1:' then integrity checks will be ran. If it starts with '0:' then integrity checks will be skipped
	Hash string
}

// Information about a database table
type TableInfo struct {
	// Name of the database table
	Name string
	// Schema tied to the database table
	Schema string
	// Columns information for the database table
	Cols []ColumnInfo
}

// Database column information
type ColumnInfo struct {
	// Name of the database column
	Name string
	// Type of the database column
	ColType string
}

// Simplified table view of table to column information as raw strings
type colTable struct {
	Table string
	Cols  [][]string
}

var (
	headerStyle = lipgloss.NewStyle().
			Bold(true).
			Foreground(lipgloss.CompleteAdaptiveColor{
			Light: lipgloss.CompleteColor{TrueColor: "#3C3C3C", ANSI256: "235", ANSI: "0"},
			Dark:  lipgloss.CompleteColor{TrueColor: "#BCBCBC", ANSI256: "253", ANSI: "7"},
		})

	evenRowStyle = lipgloss.NewStyle().
			Bold(false).
			Background(lipgloss.CompleteAdaptiveColor{
			Light: lipgloss.CompleteColor{TrueColor: "#ECECEC", ANSI256: "235", ANSI: "7"},
			Dark:  lipgloss.CompleteColor{TrueColor: "#3C3C3C", ANSI256: "253", ANSI: "0"},
		}).
		Foreground(lipgloss.CompleteAdaptiveColor{
			Light: lipgloss.CompleteColor{TrueColor: "#3C3C3C", ANSI256: "235", ANSI: "7"},
			Dark:  lipgloss.CompleteColor{TrueColor: "#BCBCBC", ANSI256: "253", ANSI: "0"},
		})
	oddRowStyle = lipgloss.NewStyle().
			Bold(false).
			Foreground(lipgloss.CompleteAdaptiveColor{
			Light: lipgloss.CompleteColor{TrueColor: "#3C3C3C", ANSI256: "235", ANSI: "0"},
			Dark:  lipgloss.CompleteColor{TrueColor: "#BCBCBC", ANSI256: "253", ANSI: "7"},
		})
	// Lipglos base style function for tables
	styleFunc = func(row, col int) lipgloss.Style {
		switch {
		case row == 0:
			return headerStyle
		case row%2 == 0:
			return evenRowStyle
		default:
			return oddRowStyle
		}
	}
)

// Prints database table information to a string builder
func (i *Info) tableStr(builder *strings.Builder) {
	titleStyle := lipgloss.NewStyle().Bold(true)
	builder.WriteString("\n")

	builder.WriteString(
		titleStyle.SetString("Tables:").Render(),
	)
	builder.WriteString("\n")
	builder.WriteString(
		titleStyle.SetString("=======").Render(),
	)
	builder.WriteString("\n\n")

	tableData := [][]string{}
	tableHeaders := []string{"Schema", "Table", "# Cols"}

	colTables := []colTable{}
	colHeaders := []string{"Column", "Type"}

	slices.SortStableFunc(i.Tables, func(a, b TableInfo) int {
		switch {
		case a.Name < b.Name:
			return -1
		case a.Name > b.Name:
			return 1
		default:
			return 0
		}
	})

	slices.SortStableFunc(i.Tables, func(a, b TableInfo) int {
		switch {
		case a.Schema < b.Schema:
			return -1
		case a.Schema > b.Schema:
			return 1
		default:
			return 0
		}
	})

	for _, table := range i.Tables {
		tableData = append(tableData, []string{table.Schema, table.Name, strconv.Itoa(int(len(table.Cols)))})

		colData := [][]string{}
		cols := table.Cols
		slices.SortStableFunc(cols, func(a, b ColumnInfo) int {
			switch {
			case a.Name < b.Name:
				return -1
			case a.Name > b.Name:
				return 1
			default:
				return 0
			}
		})
		for _, col := range cols {
			colData = append(colData, []string{col.Name, col.ColType})
		}
		colTables = append(colTables, colTable{Table: table.Schema + "." + table.Name, Cols: colData})
	}

	tablesTable := table.New().
		Border(lipgloss.RoundedBorder()).
		StyleFunc(styleFunc).
		Headers(tableHeaders...).
		Rows(tableData...)
	builder.WriteString(tablesTable.Render())
	builder.WriteString("\n")

	for _, colTableData := range colTables {
		builder.WriteString("\n")
		builder.WriteString(colTableData.Table)
		builder.WriteString("\n")
		colTable := table.New().
			Border(lipgloss.RoundedBorder()).
			StyleFunc(styleFunc).
			Headers(colHeaders...).
			Rows(colTableData.Cols...)
		builder.WriteString(colTable.Render())
		builder.WriteString("\n")
	}
}

// Gets the string visualization of database information
func (i *Info) String() string {
	builder := strings.Builder{}

	i.tableStr(&builder)
	i.indexStr(&builder)
	i.triggerStr(&builder)
	i.sessionsStr(&builder)
	i.migrationsStr(&builder)
	i.runsStr(&builder)
	i.cmdsStr(&builder)

	return builder.String()
}

func (i *Info) SessionsStr() string {
	builder := strings.Builder{}
	i.sessionsStr(&builder)
	return builder.String()
}

// Prints the run information to a string builder
func (i *Info) sessionsStr(builder *strings.Builder) {
	titleStyle := lipgloss.NewStyle().Bold(true)
	builder.WriteString("\n")

	builder.WriteString(
		titleStyle.SetString("Migration Sessions:").Render(),
	)
	builder.WriteString("\n")
	builder.WriteString(
		titleStyle.SetString("===================").Render(),
	)
	builder.WriteString("\n\n")

	if i.Sessions == nil || len(i.Sessions) == 0 {
		builder.WriteString("No information found\n")
		return
	}

	sessionsHeader := []string{
		"Session ID",
		"Time",
		"Rollback?",
		"Migrations",
	}
	sessionsInfo := [][]string{}

	for _, session := range i.Sessions {
		cols := []string{
			fmt.Sprintf("%d", session.Id),
			session.Ctime.Format(time.RFC822Z),
			boolStr(session.IsRollback),
			strings.Join(session.Migrations[:], ", "),
		}
		sessionsInfo = append(sessionsInfo, cols)
	}

	runsTable := table.New().
		Border(lipgloss.RoundedBorder()).
		StyleFunc(styleFunc).
		Headers(sessionsHeader...).
		Rows(sessionsInfo...)

	builder.WriteString(runsTable.Render())
	builder.WriteString("\n")
}

// Prints the trigger information to a string builder
func (i *Info) triggerStr(builder *strings.Builder) {
	titleStyle := lipgloss.NewStyle().Bold(true)
	builder.WriteString("\n")

	builder.WriteString(
		titleStyle.SetString("Triggers:").Render(),
	)
	builder.WriteString("\n")
	builder.WriteString(
		titleStyle.SetString("=========").Render(),
	)
	builder.WriteString("\n\n")

	if len(i.Triggers) == 0 {
		builder.WriteString("No triggers found\n")
		return
	}

	headers := []string{
		"Index",
		"Table",
	}
	rows := [][]string{}

	triggers := i.Triggers
	slices.SortStableFunc(triggers, func(a, b TriggerInfo) int {
		switch {
		case a.Table < b.Table:
			return -1
		case a.Table > b.Table:
			return 1
		default:
			return 0
		}
	})
	slices.SortStableFunc(triggers, func(a, b TriggerInfo) int {
		switch {
		case a.Name < b.Name:
			return -1
		case a.Name > b.Name:
			return 1
		default:
			return 0
		}
	})
	for _, row := range triggers {
		rows = append(rows, []string{row.Name, row.Table})
	}

	infoTable := table.New().
		Border(lipgloss.RoundedBorder()).
		StyleFunc(styleFunc).
		Headers(headers...).
		Rows(rows...)

	builder.WriteString(infoTable.Render())
	builder.WriteString("\n")
}

// Prints the index information to a string builder
func (i *Info) indexStr(builder *strings.Builder) {
	titleStyle := lipgloss.NewStyle().Bold(true)
	builder.WriteString("\n")

	builder.WriteString(
		titleStyle.SetString("Indexes:").Render(),
	)
	builder.WriteString("\n")
	builder.WriteString(
		titleStyle.SetString("========").Render(),
	)
	builder.WriteString("\n\n")

	if len(i.Indexes) == 0 {
		builder.WriteString("No indexes found\n")
		return
	}

	headers := []string{
		"Index",
		"Table",
	}
	rows := [][]string{}

	indexes := i.Indexes
	slices.SortStableFunc(indexes, func(a, b IndexInfo) int {
		switch {
		case a.Table < b.Table:
			return -1
		case a.Table > b.Table:
			return 1
		default:
			return 0
		}
	})
	slices.SortStableFunc(indexes, func(a, b IndexInfo) int {
		switch {
		case a.Name < b.Name:
			return -1
		case a.Name > b.Name:
			return 1
		default:
			return 0
		}
	})
	for _, index := range indexes {
		rows = append(rows, []string{index.Name, index.Table})
	}

	infoTable := table.New().
		Border(lipgloss.RoundedBorder()).
		StyleFunc(styleFunc).
		Headers(headers...).
		Rows(rows...)

	builder.WriteString(infoTable.Render())
	builder.WriteString("\n")
}

// Prints the commands information to a string builder
func (i *Info) cmdsStr(builder *strings.Builder) {
	titleStyle := lipgloss.NewStyle().Bold(true)
	builder.WriteString("\n")

	builder.WriteString(
		titleStyle.SetString("Migration Commands:").Render(),
	)
	builder.WriteString("\n")
	builder.WriteString(
		titleStyle.SetString("====================").Render(),
	)
	builder.WriteString("\n\n")

	if i.Migrations == nil || len(i.Migrations.Cmds) == 0 {
		builder.WriteString("No information found\n")
		return
	}

	runsHeader := []string{
		"Cmd ID",
		"Run ID",
		"Migration",
		"Time",
		"Error",
		"SQL",
	}
	runsInfo := [][]string{}

	for _, runs := range i.Migrations.Cmds {
		const limit = 45
		cmd := runs.SqlCmd
		if len(runs.SqlCmd) > limit {
			cmd = runs.SqlCmd[:(limit-3)] + "..."
		}
		cols := []string{
			fmt.Sprintf("%d", runs.Id),
			fmt.Sprintf("%d", runs.RunId),
			runs.Migration,
			runs.Ctime.Format(time.RFC822Z),
			errStr(runs.Error),
			cmd,
		}
		runsInfo = append(runsInfo, cols)
	}

	runsTable := table.New().
		Border(lipgloss.RoundedBorder()).
		StyleFunc(styleFunc).
		Headers(runsHeader...).
		Rows(runsInfo...)

	builder.WriteString(runsTable.Render())
	builder.WriteString("\n")
}

// Prints the run information to a string builder
func (i *Info) runsStr(builder *strings.Builder) {
	titleStyle := lipgloss.NewStyle().Bold(true)
	builder.WriteString("\n")

	builder.WriteString(
		titleStyle.SetString("Migration Runs:").Render(),
	)
	builder.WriteString("\n")
	builder.WriteString(
		titleStyle.SetString("================").Render(),
	)
	builder.WriteString("\n\n")

	if i.Migrations == nil || len(i.Migrations.Runs) == 0 {
		builder.WriteString("No information found\n")
		return
	}

	runsHeader := []string{
		"Run ID",
		"Migration",
		"Time",
		"Rollback?",
		"Success?",
		"Error",
	}
	runsInfo := [][]string{}

	for _, runs := range i.Migrations.Runs {
		cols := []string{
			fmt.Sprintf("%d", runs.Id),
			runs.MigId,
			runs.Ctime.Format(time.RFC822Z),
			boolStr(runs.IsRollback),
			boolStr(runs.Success),
			errStr(runs.Error),
		}
		runsInfo = append(runsInfo, cols)
	}

	runsTable := table.New().
		Border(lipgloss.RoundedBorder()).
		StyleFunc(styleFunc).
		Headers(runsHeader...).
		Rows(runsInfo...)

	builder.WriteString(runsTable.Render())
	builder.WriteString("\n")
}

// converts a boolean to a string
func boolStr(b bool) string {
	if b == true {
		return "YES"
	}
	return "NO"
}

// Prints the migration information to a string builder
func (i *Info) migrationsStr(builder *strings.Builder) {
	titleStyle := lipgloss.NewStyle().Bold(true)
	builder.WriteString("\n")

	builder.WriteString(
		titleStyle.SetString("Migrations:").Render(),
	)
	builder.WriteString("\n")
	builder.WriteString(
		titleStyle.SetString("===========").Render(),
	)
	builder.WriteString("\n\n")

	if i.Migrations == nil || len(i.Migrations.Migrations) == 0 {
		builder.WriteString("No information found\n")
		return
	}

	migsHeader := []string{
		"Migration",
		"State",
		"Error",
		"# Runs",
		"Integrity Hash",
	}
	migsInfos := [][]string{}

	for _, info := range i.Migrations.Migrations {
		cols := []string{
			info.Id,
			stateStr(info.State),
			errStr(info.Err),
			fmt.Sprintf("%d", info.RunCount),
			info.Hash,
		}
		migsInfos = append(migsInfos, cols)
	}

	slices.SortFunc(migsInfos, func(a, b []string) int {
		if a[0] < b[0] {
			return -1
		} else if a[0] > b[0] {
			return 1
		} else {
			return 0
		}
	})

	migTable := table.New().
		Border(lipgloss.RoundedBorder()).
		StyleFunc(styleFunc).
		Headers(migsHeader...).
		Rows(migsInfos...)

	builder.WriteString(migTable.Render())
	builder.WriteString("\n")
}

var (
	errStyle = lipgloss.NewStyle().Bold(true).
			Foreground(lipgloss.CompleteColor{
			TrueColor: "#FF3C3C", ANSI256: "160", ANSI: "9",
		})
	unknownStyle = lipgloss.NewStyle().Bold(true).
			Foreground(lipgloss.CompleteColor{
			TrueColor: "#8C8C8C", ANSI256: "245", ANSI: "8",
		})
	successStyle = lipgloss.NewStyle().Bold(true).
			Foreground(lipgloss.CompleteColor{
			TrueColor: "#3CFF3C", ANSI256: "70", ANSI: "2",
		})
	warnStyle = lipgloss.NewStyle().Bold(true).
			Foreground(lipgloss.CompleteColor{
			TrueColor: "#BCBC1C", ANSI256: "220", ANSI: "11",
		})
)

// Converts migration state enum to a string
func stateStr(state int) string {
	switch state {
	case MIG_STATE_ERROR:
		eStyle := errStyle
		return eStyle.SetString("FAILED").Render()
	case MIG_STATE_SUCCESS:
		style := successStyle
		return style.SetString("SUCCESS").Render()
	case MIG_STATE_ROLLBACK:
		style := successStyle
		return style.SetString("ROLLBACK").Render()
	case MIG_STATE_ROLLBACK_ERROR:
		eStyle := errStyle
		return eStyle.SetString("FAILED [ROLLBACK]").Render()
	default:
		style := unknownStyle
		return style.SetString("UKNOWN").Render()
	}
}

// Converts database error information to a string
func errStr(err *string) string {
	if err == nil {
		return ""
	}
	return *err
}
