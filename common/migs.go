package common

import (
	"crypto/sha512"
	"encoding/base64"
	"fmt"
	"os"
	"path/filepath"
	"slices"
	"strings"

	"github.com/jmoiron/sqlx"
	log "github.com/sirupsen/logrus"
)

// Sample ignroed hash, used often in tests
const IGNORED_HASH = "0:0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"

type SqlGen interface {
	Generate(generation, variance string, args any) (string, error)
}

// SQL Generator interface
type NativeSqlGen interface {
	GenCreateTable(tc TableCreate) (string, error)
	RevCreateTable(tc TableCreate) (string, error)
	GenAlterTable(ta TableAlter) (string, error)
	RevAlterTable(ta TableAlter) (string, error)
	GenCreateIndex(ic IndexCreate) (string, error)
	RevCreateIndex(ic IndexCreate) (string, error)
}

func GenerateNative(ng NativeSqlGen, generation, variance string, args any) (string, error) {
	v := strings.ToUpper(variance)
	if v != "UP" && v != "DOWN" {
		return "", fmt.Errorf("Expected variance to be either UP or DOWN")
	}

	switch a := args.(type) {
	case TableCreate:
		if generation != "createTable" {
			return "", fmt.Errorf("Expected createTable for arg of TableCreate")
		}
		switch v {
		case "UP":
			return ng.GenCreateTable(a)
		case "DOWN":
			return ng.RevCreateTable(a)
		}
	case TableAlter:
		if generation != "alterTable" {
			return "", fmt.Errorf("Expected alterTable for arg of TableAlter")
		}
		switch v {
		case "UP":
			return ng.GenAlterTable(a)
		case "DOWN":
			return ng.RevAlterTable(a)
		}
	case IndexCreate:
		if generation != "createIndex" {
			return "", fmt.Errorf("Expected createIndex for arg of IndexCreate")
		}
		switch v {
		case "UP":
			return ng.GenCreateIndex(a)
		case "DOWN":
			return ng.RevCreateIndex(a)
		}
	}
	return "", fmt.Errorf("Invalid args, generation, and variance combination")
}

// Database actions interface for running commands against a database
type DbActions interface {
	GetInfo(s *sqlx.DB, cnf *DbConf) (*Info, error)
	EnsureSetup(s *sqlx.DB, cnf *DbConf) error
	GetLastSessionInfo(s *sqlx.DB, cnf *DbConf) (*SessionInfo, error)
	GetSessionInfo(sessionId int64, s *sqlx.DB, cnf *DbConf) (*SessionInfo, error)
	GetMigrationInfo(s *sqlx.DB, cnf *DbConf) (*MigrationOverview, error)
	RunMigration(m *Migration, cmds []string, s *sqlx.DB, cnf *DbConf) error
	RollbackMigration(m *Migration, cmds []string, s *sqlx.DB, cnf *DbConf) error
	GetMigrated(s *sqlx.DB, cnf *DbConf, state int) ([]string, error)
	GetHashes(s *sqlx.DB, cnf *DbConf) ([]MigrationHashCheck, error)
	EnsureDatabase(dbname string, s *sqlx.DB) error
	EnsureSchema(schemaName string, s *sqlx.DB) error
	CreateSession(isRollback bool, s *sqlx.DB, cnf *DbConf) (int64, error)
	SaveMigrationToSession(sessionId int64, mig *Migration, s *sqlx.DB, cnf *DbConf) error

	UpdateRunInfo(runId int64, run *Run, tx *sqlx.Tx) error
	CreateRunInfo(run *Run, tx *sqlx.Tx, cnf *DbConf) (int64, error)
	SaveCmdInfo(cmd string, tx *sqlx.Tx, runId int64, migration *Migration) error
	SaveMigInfo(mig *Migration, migState int, err error, tx *sqlx.Tx, cnf *DbConf) error
	Assert(cmd string, tx *sqlx.Tx, cnf *DbConf) error
}

// Migration information from migration configuration files
type Migration struct {
	Name    string   `json:"name,omitempty" toml:"name"`
	Deps    []Dep    `json:"depsOn,omitempty" toml:"depsOn"`
	Actions []Action `json:"actions,omitempty" toml:"actions"`
	FileDir string   `json:"fileDir,omitempty" toml:"fileDir"`
	Asserts []Assert `json:"asserts,omitempty" toml:"asserts"`
}

type Assert struct {
	SqlFile *string `json:"file,omitempty" toml:"file"`
	Sql     *string `json:"raw,omitempty" toml:"raw"`
}

// Migration dependency
type Dep string

// Migration action
type Action struct {
	SqlFile     *SqlFileMig  `json:"file,omitempty" toml:"file"`
	Sql         *SqlMig      `json:"raw,omitempty" toml:"raw"`
	CreateTable *TableCreate `json:"createTable,omitempty" toml:"createTable"`
	AlterTable  *TableAlter  `json:"alterTable,omitempty" toml:"alterTable"`
	CreateIndex *IndexCreate `json:"createIndex,omitempty" toml:"createIndex"`
}

// Represents creating a database index
type IndexCreate struct {
	Schema      string   `json:"schema,omitempty" toml:"schema" protoVal:"schema"`
	Name        string   `json:"name" toml:"name" protoVal:"name"`
	Unique      bool     `json:"unique,omitempty" toml:"unique" protoVal:"unique"`
	Table       string   `json:"table" toml:"table" protoVal:"table"`
	ErrIfExists bool     `json:"errIfExists,omitempty" toml:"errIfExists" protoVal:"errIfExists"`
	Columns     []string `json:"columns" toml:"columns" protoVal:"columns"`
	Where       string   `json:"where,omitempty" toml:"where" protoVal:"where"`
}

// Represents loading SQL scripts from SQL files
type SqlFileMig struct {
	Up   string `json:"up" toml:"up"`
	Down string `json:"down" toml:"down"`
}

// Represents raw SQL scripts
type SqlMig struct {
	Up   string `json:"up" toml:"up"`
	Down string `json:"down"`
}

// Represents creating a new database table
type TableCreate struct {
	Schema      string       `json:"schema,omitempty" toml:"schema" protoVal:"schema"`
	Name        string       `json:"name" toml:"name" protoVal:"name"`
	Columns     []Column     `json:"columns,omitempty" toml:"columns" protoVal:"columns"`
	ErrIfExists bool         `json:"errIfExists,omitempty" toml:"errIfExists" protoVal:"errIfExists"`
	Timestamps  bool         `json:"timestamps,omitempty" toml:"timestamps" protoVal:"timestamps"`
	PrimaryKey  []string     `json:"primaryKey,omitempty" toml:"primaryKey" protoVal:"primaryKey"`
	ForeignKeys []ForeignKey `json:"foreignKeys,omitempty" toml:"foreignKeys" protoVal:"foreignKeys"`
}

// Represents renaming a column
type ColRename struct {
	From string `json:"from" toml:"from" protoVal:"from"`
	To   string `json:"to" toml:"to" protoVal:"to"`
}

// Represents altering a table
type TableAlter struct {
	Schema        string      `json:"schema,omitempty" toml:"schema" protoVal:"schema"`
	Name          string      `json:"name" toml:"name" protoVal:"name"`
	RenameTo      string      `json:"renameTo,omitempty" toml:"renameTo" protoVal:"renameTo"`
	AddColumns    []Column    `json:"addColumns,omitempty" toml:"addColumns" protoVal:"addColumns"`
	RenameColumns []ColRename `json:"renameColumns,omitempty" toml:"renameColumns" protoVal:"renameColumns"`
}

// Represents information around new columns
type Column struct {
	Name          string `json:"name" toml:"name" protoVal:"name"`
	AutoIncrement bool   `json:"autoincrement,omitempty" toml:"autoincrement" protoVal:"autoincrement"`
	Type          string `json:"type" toml:"type" protoVal:"type"`
	DefaultValue  any    `json:"default,omitempty" toml:"default" protoVal:"default"`
	NotNull       bool   `json:"notNull,omitempty" toml:"notNull" protoVal:"notNull"`
}

// Represents creating a new foreign key
type ForeignKey struct {
	Name           string   `json:"name" toml:"name" protoVal:"name"`
	Keys           []string `json:"keys" toml:"keys" protoVal:"keys"`
	ReferenceTable string   `json:"table" toml:"table" protoVal:"table"`
	ReferenceKeys  []string `json:"references" toml:"references" protoVal:"references"`
	OnDelete       *string  `json:"onDelete,omitempty" toml:"onDelete" protoVal:"onDelete"`
	OnUpdate       *string  `json:"onUpdate,omitempty" toml:"onUpdate" protoVal:"onUpdate"`
}

// Represents a migration run
type Run struct {
	Migration  string
	IsRollback bool
	Success    bool
	Error      *string
}

// Gets the database assertions tied to a migration
func (m *Migration) Assertions() ([]string, error) {
	res := []string{}

	for _, assert := range m.Asserts {
		if err := validateAssert(&assert); err != nil {
			log.Errorf("Invalid migration ASSERT %#v. Reason: %v", assert, err)
			return nil, fmt.Errorf("Invalid migration ASSERT. %v", err)
		}
		switch {
		case assert.SqlFile != nil:
			path := filepath.Join(m.FileDir, *assert.SqlFile)
			contents, err := os.ReadFile(path)
			if err != nil {
				log.Errorf("Unable to load SQL migration file %v. Reason: %v", path, err)
				return nil, fmt.Errorf("Cannot get DOWN migrations from SQL file. %v", err)
			}
			contentsStr := string(contents)
			if contentsStr != "" {
				res = append(res, contentsStr)
			}
		case assert.Sql != nil:
			if *assert.Sql != "" {
				res = append(res, *assert.Sql)
			}
		}
	}

	return res, nil
}

// Gets the commands to rollback (spin DOWN) a migration
// Takes the database engine type for SQL generation (if needed)
func (m *Migration) Down(cnf *DbConf) ([]string, error) {
	res := []string{}

	// We revert each command backwards
	actions := slices.Clone(m.Actions)
	slices.Reverse(actions)

	for _, action := range actions {
		if err := validateAction(&action); err != nil {
			log.Errorf("Invalid DOWN migration %#v. Reason: %v", action, err)
			return nil, fmt.Errorf("Invalid DOWN migration. %v", err)
		}
		switch {
		case action.SqlFile != nil:
			path := filepath.Join(m.FileDir, action.SqlFile.Down)
			contents, err := os.ReadFile(path)
			if err != nil {
				log.Errorf("Unable to load SQL migration file %v. Reason: %v", path, err)
				return nil, fmt.Errorf("Cannot get DOWN migrations from SQL file. %v", err)
			}
			contentsStr := string(contents)
			if contentsStr != "" {
				res = append(res, contentsStr)
			}
		case action.Sql != nil:
			if action.Sql.Down != "" {
				res = append(res, action.Sql.Down)
			}
		case action.CreateTable != nil:
			generator, err := cnf.sqlGen()
			if err != nil {
				log.Errorf("Could not get generator. Reason: %v", err)
				return nil, err
			}
			sql, err := generator.Generate("createTable", "DOWN", *action.CreateTable)
			if err != nil {
				return nil, fmt.Errorf("Cannot get DOWN migrations from SQL generator. %v", err)
			}
			if sql != "" {
				res = append(res, sql)
			}
		case action.AlterTable != nil:
			generator, err := cnf.sqlGen()
			if err != nil {
				log.Errorf("Could not get generator. Reason: %v", err)
				return nil, err
			}
			sql, err := generator.Generate("alterTable", "DOWN", *action.AlterTable)
			if err != nil {
				return nil, fmt.Errorf("Cannot get DOWN migrations from SQL generator. %v", err)
			}
			if sql != "" {
				res = append(res, sql)
			}
		case action.CreateIndex != nil:
			generator, err := cnf.sqlGen()
			if err != nil {
				log.Errorf("Could not get generator. Reason: %v", err)
				return nil, err
			}
			sql, err := generator.Generate("createIndex", "DOWN", *action.CreateIndex)
			if err != nil {
				return nil, fmt.Errorf("Cannot get DOWN migrations from SQL generator. %v", err)
			}
			if sql != "" {
				res = append(res, sql)
			}
		default:
			return nil, fmt.Errorf("Unknown DOWN action %#v", action)
		}
	}
	return res, nil
}

// Gets the SQL for migrating forward (spinning UP)
// Takes the DB engine for SQL generation (if needed)
func (m *Migration) Up(cnf *DbConf) ([]string, error) {
	res := []string{}

	for _, action := range m.Actions {
		if err := validateAction(&action); err != nil {
			log.Errorf("Invalid UP migration %#v. Reason: %v", action, err)
			return nil, fmt.Errorf("Invalid UP migration. %v", err)
		}
		switch {
		case action.SqlFile != nil:
			path := filepath.Join(m.FileDir, action.SqlFile.Up)
			contents, err := os.ReadFile(path)
			if err != nil {
				log.Errorf("Unable to load SQL migration file %v. Reason: %v", path, err)
				return nil, fmt.Errorf("Cannot get UP migrations from SQL file. %v", err)
			}
			contentsStr := string(contents)
			if contentsStr != "" {
				res = append(res, contentsStr)
			}
		case action.Sql != nil:
			if action.Sql.Up != "" {
				res = append(res, action.Sql.Up)
			}
		case action.CreateTable != nil:
			generator, err := cnf.sqlGen()
			if err != nil {
				log.Errorf("Could not get generator. Reason: %v", err)
				return nil, err
			}
			sql, err := generator.Generate("createTable", "UP", *action.CreateTable)
			if err != nil {
				return nil, fmt.Errorf("Cannot get UP migrations from SQL generator. %v", err)
			}
			fmt.Println(sql)
			if sql != "" {
				res = append(res, sql)
			}
		case action.AlterTable != nil:
			generator, err := cnf.sqlGen()
			if err != nil {
				log.Errorf("Could not get generator. Reason: %v", err)
				return nil, err
			}
			sql, err := generator.Generate("alterTable", "UP", *action.AlterTable)
			if err != nil {
				return nil, fmt.Errorf("Cannot get UP migrations from SQL generator. %v", err)
			}
			fmt.Println(sql)
			if sql != "" {
				res = append(res, sql)
			}
		case action.CreateIndex != nil:
			generator, err := cnf.sqlGen()
			if err != nil {
				log.Errorf("Could not get generator. Reason: %v", err)
				return nil, err
			}
			sql, err := generator.Generate("createIndex", "UP", *action.CreateIndex)
			if err != nil {
				return nil, fmt.Errorf("Cannot get UP migrations from SQL generator. %v", err)
			}
			fmt.Println(sql)
			if sql != "" {
				res = append(res, sql)
			}
		default:
			return nil, fmt.Errorf("Unknown UP action %#v", action)
		}
	}
	return res, nil
}

// Gets the integrity hash for a migration loaded from disk
// Used for integrity checking against the database
func (m *Migration) IntegrityHash() (string, error) {
	segmentsToHash := strings.Builder{}
	writeVal := func(id, val string) {
		segmentsToHash.WriteString(id)
		segmentsToHash.WriteString("::")
		segmentsToHash.WriteString(val)
		segmentsToHash.WriteString("\n")
	}

	writeVal("NAME", m.Name)

	for _, dep := range m.Deps {
		writeVal("DEPENDENCY", string(dep))
	}

	for _, action := range m.Actions {
		switch {
		case action.SqlFile != nil:
			path := filepath.Join(m.FileDir, action.SqlFile.Up)
			contents, err := os.ReadFile(path)
			if err != nil {
				log.Errorf("Unable to load SQL migration file %v. Reason: %v", path, err)
				return "", fmt.Errorf("Cannot get UP migrations from SQL file. %v", err)
			}
			writeVal("ACTION_SQL_UP", string(contents))

			path = filepath.Join(m.FileDir, action.SqlFile.Down)
			contents, err = os.ReadFile(path)
			if err != nil {
				log.Errorf("Unable to load SQL migration file %v. Reason: %v", path, err)
				return "", fmt.Errorf("Cannot get UP migrations from SQL file. %v", err)
			}
			writeVal("ACTION_SQL_DOWN", string(contents))
		case action.Sql != nil:
			writeVal("ACTION_SQL_UP", action.Sql.Up)
			writeVal("ACTION_SQL_DOWN", action.Sql.Down)
		case action.CreateTable != nil:
			writeVal("ACTION_JSON", fmt.Sprintf("%#v", action.CreateTable))
		case action.AlterTable != nil:
			writeVal("ACTION_JSON", fmt.Sprintf("%#v", action.AlterTable))
		case action.CreateIndex != nil:
			writeVal("ACTION_JSON", fmt.Sprintf("%#v", action.CreateIndex))
		}
	}

	toHash := segmentsToHash.String()
	hasher := sha512.New()
	hasher.Write([]byte(toHash))
	res := "1:" + base64.StdEncoding.EncodeToString(hasher.Sum(nil))
	if len(res) > 90 {
		panic("hash is not the right size!")
	}
	return res, nil
}

// Validates that a migration action is correct
func validateAction(action *Action) error {
	if action == nil {
		return fmt.Errorf("Cannot operate on a null action")
	}
	optypes := []string{}
	if action.SqlFile != nil {
		optypes = append(optypes, "file")
	}
	if action.Sql != nil {
		optypes = append(optypes, "raw")
	}
	if action.CreateTable != nil {
		optypes = append(optypes, "createTable")
	}
	if action.AlterTable != nil {
		optypes = append(optypes, "alterTable")
	}
	if action.CreateIndex != nil {
		optypes = append(optypes, "createIndex")
	}
	if len(optypes) == 0 {
		return fmt.Errorf("Unknown or empty action found. Expected one of: file, raw, createTable, alterTable, createIndex")
	}
	if len(optypes) > 1 {
		return fmt.Errorf("Too many entries in a single action! Cannot determine execution order, please split each entry into a separate object. Found %+v", optypes)
	}
	return nil
}

// Validates that a migration assertion is correct
func validateAssert(assert *Assert) error {
	if assert == nil {
		return fmt.Errorf("Cannot operate on a null assert")
	}
	optypes := []string{}
	if assert.SqlFile != nil {
		optypes = append(optypes, "file")
	}
	if assert.Sql != nil {
		optypes = append(optypes, "raw")
	}
	if len(optypes) == 0 {
		return fmt.Errorf("Unknown or empty assert found. Expected one of: file, raw")
	}
	if len(optypes) > 1 {
		return fmt.Errorf("Too many entries in a single assert! Cannot determine execution order, please split each entry into a separate object. Found %+v", optypes)
	}
	return nil
}
