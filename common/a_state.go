package common

import (
	"fmt"
	"rove_migrate/conf"

	log "github.com/sirupsen/logrus"
)

var registeredDrivers map[string]DbDriver
var registeredGenerators map[string]DbGenerator
var registeredMigrationActions map[string]MigrationActions

// Initializes database registry
func init() {
	registeredDrivers = make(map[string]DbDriver)
	registeredGenerators = make(map[string]DbGenerator)
	registeredMigrationActions = make(map[string]MigrationActions)

	registeredMigrationActions["nil"] = MigrationActions{Actions: nil}
}

// Returns a list of registered database drivers
func MigActions() []string {
	res := make([]string, 0, len(registeredMigrationActions))
	for k := range registeredMigrationActions {
		res = append(res, k)
	}
	return res
}

// Returns a list of registered database drivers
func DbDrivers() []string {
	res := make([]string, 0, len(registeredDrivers))
	for k := range registeredDrivers {
		res = append(res, k)
	}
	return res
}

// Returns a list of registered database generators
func DbGenerators() []string {
	res := make([]string, 0, len(registeredGenerators))
	for k := range registeredGenerators {
		res = append(res, k)
	}
	return res
}

// DB engine type id (REMOTE vs FILE vs PLUGIN)
type DbDriverType int

const (
	DB_REMOTE DbDriverType = iota
	DB_FILE
	DB_PLUGIN
)

type DbGeneratorType int

const (
	GEN_NATIVE DbGeneratorType = iota
	GEN_PLUGIN
)

// Type for a database URL builder
type DbUrlBuilder func(conf *DbConf) (string, error)

// Database engine information
type DbDriver struct {
	SqlDriver   string
	Type        DbDriverType
	UrlBuilder  DbUrlBuilder
	DefaultPort uint
	Limitations string
	PluginConf  *conf.PluginConf
}

type MigrationActions struct{
	Actions     DbActions
}

type DbGenerator struct {
	Engine      string
	Type        DbGeneratorType
	Generator   SqlGen
	Limitations string
	PluginConf  *conf.PluginConf
}

// Database conf wrapper
type DbConf struct {
	*conf.Db
}

// Represents options for setting up a new database engine
type DriverOption func(*DbDriver) error
type GeneratorOption func(*DbGenerator) error

// Sets limitations for a database engine
func WithDriverLimits(limitations string) DriverOption {
	return func(driver *DbDriver) error {
		driver.Limitations = limitations
		return nil
	}
}

// Sets limitations for a database engine
func WithGeneratorLimits(limitations string) GeneratorOption {
	return func(gen *DbGenerator) error {
		gen.Limitations = limitations
		return nil
	}
}

// Gets limitations related to a database engine
func DriverLimitations(driver string) (string, error) {
	v, ok := registeredDrivers[driver]
	if !ok {
		log.Errorf("Unable to get limitations for driver %#v. Reason: driver does not exist", driver)
		return "", fmt.Errorf("Unknown driver %#v", driver)
	}

	return v.Limitations, nil
}

// Gets limitations related to a database engine
func GeneratorLimitations(generator string) (string, error) {
	v, ok := registeredGenerators[generator]
	if !ok {
		log.Errorf("Unable to get limitations for generator %#v. Reason: generator does not exist", generator)
		return "", fmt.Errorf("Unknown driver %#v", generator)
	}

	return v.Limitations, nil
}

// Creates and validates a new database configuration
func NewConf(options ...conf.DbOption) (*DbConf, error) {
	options = append([]conf.DbOption{}, options...)
	dbConf, err := conf.MakeDb(options...)
	if err != nil {
		return nil, fmt.Errorf("Cannot initalize database configuration. Reason: %v", err)
	}

	driver := dbConf.Driver()

	if dbConf.Port() == 0 && dbConf.Host() != "" && dbConf.DriverUrl() == "" {
		port, err := PortFor(driver)
		if err != nil {
			return nil, fmt.Errorf("Cannot initalize database configuration. Reason: %v", err)
		}
		dbConf, err = conf.MakeDb(conf.WithBase(dbConf), conf.WithPort(port))
		if err != nil {
			return nil, fmt.Errorf("Cannot initalize database configuration. Reason: %v", err)
		}
	}

	if isValidPluginDbType(driver) {
		pluginConf, err := PluginConfFor(driver)
		if err != nil {
			return nil, err
		}

		dbConf, err = conf.MakeDb(conf.WithBase(dbConf), conf.WithPlugin(pluginConf))
		if err != nil {
			return nil, fmt.Errorf("Cannot initialize database plugin configuration. Reason: %v", err)
		}
		return &DbConf{dbConf}, nil
		// do nothing
	} else if dbConf.DriverUrl() == "" {
		if conf.IsRemoteConf(dbConf) {
			if !isValidRemoteDbType(driver) {
				return nil, fmt.Errorf("Cannot initialize database configuration. Reason: Given host/port, but driver %#v is not a remote database.", driver)
			}
		} else if conf.IsFileConf(dbConf) {
			if !isValidFileDbType(driver) {
				return nil, fmt.Errorf("Cannot initialize database configuration. Reason: Given file, but driver %#v is not a local filesystem database.", driver)
			}
		} else {
			return nil, fmt.Errorf("Cannot initialize database configuration. Reason: Cannot match conf %#v to needs of driver %#v", dbConf, driver)
		}
	} else {
		log.Info("Driver url provided. Will use that for database connections")
	}

	return &DbConf{dbConf}, nil
}

// Registers remote database to the database registry
// id is how the user will select the database with CLI options
// driver is the go driver to use
// generator is the implementation of the SqlGen interface to generate SQL commands
// actions is the implementation of DbActions
// urlBuilder builds the connection url used by the driver
// port is the TCP/UDP port to connect to
// options are optional engine options
func RegisterRemoteDriver(id string, sqlDriver string, actions DbActions, urlBuilder DbUrlBuilder, port uint, options ...DriverOption) error {
	if port > 65535 {
		log.Errorf("Invalid port %v specified when registering db %v. Port must be between 0-65535", port, id)
		return fmt.Errorf("Cannot register database %#v. Invalid default port %#v", id, port)
	}

	d := DbDriver{sqlDriver, DB_REMOTE, urlBuilder, port, "", nil}
	for _, o := range options {
		if err := o(&d); err != nil {
			return fmt.Errorf("Cannot register driver %#v. %v", id, err)
		}
	}

	registeredDrivers[id] = d
	registeredMigrationActions[id] = MigrationActions{actions}
	return nil
}

func RegisterNativeGenerator(id string, gen SqlGen, options ...GeneratorOption) error {
	g := DbGenerator{id, GEN_NATIVE, gen, "", nil}
	for _, o := range options {
		if err := o(&g); err != nil {
			return fmt.Errorf("Cannot register generator %#v. %v", id, err)
		}
	}

	registeredGenerators[id] = g
	return nil
}

// Register plugin database to the database registry
// You should not have to change this manually
func RegisterPluginGenerator(id string, generator SqlGen, conf *conf.PluginConf, options ...GeneratorOption) error {
	g := DbGenerator{"plugin", GEN_PLUGIN, generator, "", conf}

	for _, o := range options {
		if err := o(&g); err != nil {
			return fmt.Errorf("Cannot register generator %#v. %v", id, err)
		}
	}

	registeredGenerators[id] = g
	return nil
}

// Registers filesystem database to the database registry
// id is how the user will select the database with CLI options
// driver is the go driver to use
// generator is the implementation of the SqlGen interface to generate SQL commands
// actions is the implementation of DbActions
// urlBuilder builds the connection url used by the driver
// port is the TCP/UDP port to connect to
// options are optional engine options
func RegisterFileDriver(id string, driver string, actions DbActions, urlBuilder DbUrlBuilder, options ...DriverOption) error {
	d := DbDriver{driver, DB_FILE, urlBuilder, 0, "", nil}
	for _, o := range options {
		if err := o(&d); err != nil {
			return fmt.Errorf("Cannot register driver %#v. %v", id, err)
		}
	}

	registeredDrivers[id] = d
	registeredMigrationActions[id] = MigrationActions{actions}
	return nil
}

// Register plugin database to the database registry
// You should not have to change this manually
func RegisterPluginDriver(id string, urlBuilder DbUrlBuilder, conf *conf.PluginConf, options ...DriverOption) error {
	d := DbDriver{"plugin", DB_PLUGIN, urlBuilder, 0, "", conf}

	for _, o := range options {
		if err := o(&d); err != nil {
			return fmt.Errorf("Cannot register driver %#v. %v", id, err)
		}
	}

	registeredDrivers[id] = d
	return nil
}

func RegisterPluginActions(id string, actions DbActions) error {
	d := MigrationActions{actions}
	registeredMigrationActions[id] = d
	return nil
}

// Gets the default port for a database engine
func PortFor(driver string) (uint, error) {
	v, ok := registeredDrivers[driver]
	if !ok {
		return 0, fmt.Errorf("Cannot get default database port. Invalid driver %#v", driver)
	}
	return v.DefaultPort, nil
}

// Is a database engine a valid remote database engine
func isValidRemoteDbType(driver string) bool {
	v, ok := registeredDrivers[driver]
	if !ok {
		return false
	}
	return v.Type == DB_REMOTE
}

// Is a database engine a valid filesystem database engine
func isValidFileDbType(driver string) bool {
	v, ok := registeredDrivers[driver]
	if !ok {
		return false
	}
	return v.Type == DB_FILE
}

func isValidPluginDbType(driver string) bool {
	v, ok := registeredDrivers[driver]
	if !ok {
		return false
	}
	return v.Type == DB_PLUGIN
}

func PluginConfFor(driver string) (*conf.PluginConf, error) {
	v, ok := registeredDrivers[driver]
	if !ok || v.Type != DB_PLUGIN {
		return nil, fmt.Errorf("Invalid plugin %s", driver)
	}
	return v.PluginConf, nil
}

// Gets the SQL generator to use by id
func sqlGen(generator string) (SqlGen, error) {
	v, ok := registeredGenerators[generator]
	if !ok {
		return nil, fmt.Errorf("Cannot get SQL generator. Invalid driver %#v", generator)
	}

	return v.Generator, nil
}

// Gets the GoLang generator engine for DB conf
func (cnf *DbConf) GeneratorEngine() (string, error) {
	v, ok := registeredGenerators[cnf.Generator()]
	if !ok {
		return "", fmt.Errorf("Cannot get database generator engine. Invalid generator %#v", cnf.Generator())
	}

	return v.Engine, nil
}

// Gets the GoLang driver engine for DB conf
func (cnf *DbConf) SqlDriver() (string, error) {
	v, ok := registeredDrivers[cnf.Driver()]
	if !ok {
		return "", fmt.Errorf("Cannot get database driver engine. Invalid driver %#v", cnf.Driver())
	}

	return v.SqlDriver, nil
}

// Gets the URL for DB conf
func (cnf *DbConf) Url() (string, error) {
	v, ok := registeredDrivers[cnf.Driver()]
	if !ok {
		return "", fmt.Errorf("Cannot get database URL builder. Invalid driver %#v", cnf.Driver())
	}

	if cnf.DriverUrl() != "" {
		return cnf.DriverUrl(), nil
	}

	return v.UrlBuilder(cnf)
}

// Gets the database actions tied to DB conf
func (cnf *DbConf) MigrationActions() (DbActions, error) {
	v, ok := registeredMigrationActions[cnf.Actions()]
	if !ok {
		return nil, fmt.Errorf("Cannot get migration actions. Invalid driver %#v", cnf.Actions())
	}

	return v.Actions, nil
}

func (cnf *DbConf) sqlGen() (SqlGen, error) {
	generator, err := sqlGen(cnf.Generator())

	if err != nil {
		return nil, err
	}
	return generator, nil
}
