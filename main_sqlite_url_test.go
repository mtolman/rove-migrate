package main

import (
	"os"
	"os/exec"
	"path/filepath"
	"rove_migrate/common"
	"rove_migrate/conf"
	"rove_migrate/db"
	_ "rove_migrate/sqlite3"
	"strings"
	"testing"
)

func sqliteUrlConf() (*common.DbConf, func()) {
	file, err := os.CreateTemp(".", "test_sqlite_*.db")
	if err != nil {
		panic(nil)
	}
	file.Close()

	url := "file:" + file.Name()

	conf, err := common.NewConf(
		conf.WithDriver("sqlite3"),
		conf.WithGenerator("sqlite3"),
		conf.WithUrl(url),
	)
	if err != nil {
		panic(err)
	}
	cleanup := func() {
		os.Remove(file.Name())
	}
	return conf, cleanup
}

func TestSqliteUrlBasic(t *testing.T) {
	cnf, clean := sqliteUrlConf()
	defer clean()

	dir, err := os.Getwd()
	if err != nil {
		t.Fatal(err)
	}

	cmdPath := filepath.Join(dir, binName)

	t.Run("Init", func(t *testing.T) {
		cmd := exec.Command(cmdPath, "--driver", cnf.Driver(), "--generator", cnf.Generator(), "--url", cnf.DriverUrl(), "init")
		out, err := cmd.CombinedOutput()
		if err != nil {
			t.Fatalf("Unexpected error: %+v\nOutput: %s", err, out)
		}

		outStr := string(out)
		expectedStr := "INIT SUCCESS!\n"

		if outStr != expectedStr {
			t.Errorf("Expected output %#v, received %#v", expectedStr, outStr)
		}
	})

	t.Run("Test Conn", func(t *testing.T) {
		cmd := exec.Command(cmdPath, "--driver", cnf.Driver(), "--generator", cnf.Generator(), "--url", cnf.DriverUrl(), "check-conn")
		out, err := cmd.CombinedOutput()
		if err != nil {
			t.Fatalf("Unexpected error: %+v\nOutput: %s", err, out)
		}

		outStr := string(out)
		expectedStr := "CONNECTION SUCCESS!\n"

		if outStr != expectedStr {
			t.Errorf("Expected output %#v, received %#v", expectedStr, outStr)
		}
	})

	t.Run("Test Blank Info", func(t *testing.T) {
		cmd := exec.Command(cmdPath, "--driver", cnf.Driver(), "--generator", cnf.Generator(), "--url", cnf.DriverUrl(), "info")
		out, err := cmd.CombinedOutput()
		if err != nil {
			t.Fatalf("Unexpected error: %+v\nOutput: %s", err, out)
		}

		outStr := string(out)
		expectedSequences := []string{
			"Tables:",
			`test_sqlite_`, `.db"│rove_migrate__cmd_log`, `│6 `,
			`test_sqlite_`, `.db"│rove_migrate__migs`, `│6 `,
			`test_sqlite_`, `.db"│rove_migrate__runs`, `│7 `,
			`test_sqlite_`, `.db"│rove_migrate__version_info`, `│2 `,

			`test_sqlite_`, `.db".rove_migrate__cmd_log`,
			"ctime",
			"id",
			"migration_id",
			"run_id",
			"sql_cmd",
			"utime",

			`test_sqlite_`, `.db".rove_migrate__migs`,
			"ctime",
			"error",
			"id",
			"state",
			"utime",

			`test_sqlite_`, `.db".rove_migrate__runs`,
			"ctime",
			"error",
			"id",
			"is_rollback",
			"migration_id",
			"success",
			"utime",

			"Indexes:",
			"rove_migrate__migs",

			"Triggers:",
			"rove_migrate__update_utime_cmd",
			"rove_migrate__update_utime_migs",
			"rove_migrate__update_utime_runs",

			"Migrations:",
			"No information found",

			"Migration Runs:",
			"No information found",

			"Migration Commands:",
			"No information found",
		}

		assertHasOrderedSequence(t, expectedSequences, outStr)
	})

	t.Run("Test Migration", func(t *testing.T) {
		migDir, err := os.MkdirTemp(dir, "test_migrations")
		if err != nil {
			t.Fatal(err)
		}
		defer os.RemoveAll(migDir)

		if err := writeSqlMigFiles(migDir); err != nil {
			t.Fatal(err)
		}

		cmd := exec.Command(cmdPath, "--driver", cnf.Driver(), "--generator", cnf.Generator(), "--url", cnf.DriverUrl(), "migrate", "--migs", migDir)
		out, err := cmd.CombinedOutput()
		if err != nil {
			t.Fatalf("Unexpected error: %+v\nOutput: %s", err, out)
		}

		expectedSequences := []string{
			"Migrations Ran:",
			"test_sql_mig",
		}

		outStr := string(out)

		assertHasOrderedSequence(t, expectedSequences, outStr)
	})

	t.Run("Test Post Basic Migration Info", func(t *testing.T) {
		cmd := exec.Command(cmdPath, "--driver", cnf.Driver(), "--generator", cnf.Generator(), "--url", cnf.DriverUrl(), "info")
		out, err := cmd.CombinedOutput()
		if err != nil {
			t.Fatalf("Unexpected error: %+v\nOutput: %s", err, out)
		}

		outStr := string(out)
		expectedSequences := []string{
			"Tables:",
			`test_sqlite_`, `.db"│rove_migrate__cmd_log`, `│6 `,
			`test_sqlite_`, `.db"│rove_migrate__migs`, `│6 `,
			`test_sqlite_`, `.db"│rove_migrate__runs`, `│7 `,
			`test_sqlite_`, `.db"│user`, `│2 `,

			`test_sqlite_`, `.db".rove_migrate__cmd_log`,
			"ctime",
			"id",
			"migration_id",
			"run_id",
			"sql_cmd",
			"utime",

			`test_sqlite_`, `.db".rove_migrate__migs`,
			"ctime",
			"error",
			"id",
			"state",
			"utime",

			`test_sqlite_`, `.db".rove_migrate__runs`,
			"ctime",
			"error",
			"id",
			"is_rollback",
			"migration_id",
			"success",
			"utime",

			`test_sqlite_`, `.db".user`,
			"id",
			"name",

			"Indexes:",
			"rove_migrate__migs",

			"Triggers:",
			"rove_migrate__update_utime_cmd",
			"rove_migrate__update_utime_migs",
			"rove_migrate__update_utime_runs",

			"Migration Sessions:",
			"1", "NO", "test_sql_mig",

			"Migrations:",
			"test_sql_mig│SUCCESS│     │1 ",

			"Migration Runs:",
			"1 ", "test_sql_mig", "NO", "YES",

			"Migration Commands:",
			"CREATE TABLE IF NOT EXISTS user",
		}

		assertHasOrderedSequence(t, expectedSequences, outStr)
	})

	t.Run("Test Rollback", func(t *testing.T) {
		migDir, err := os.MkdirTemp(dir, "test_migrations")
		if err != nil {
			t.Fatal(err)
		}
		defer os.RemoveAll(migDir)

		if err := writeSqlMigFiles(migDir); err != nil {
			t.Fatal(err)
		}

		cmd := exec.Command(cmdPath, "--driver", cnf.Driver(), "--generator", cnf.Generator(), "--url", cnf.DriverUrl(), "rollback", "--migs", migDir)
		out, err := cmd.CombinedOutput()
		if err != nil {
			t.Fatalf("Unexpected error: %+v\nOutput: %s", err, out)
		}

		expectedSequences := []string{
			"Migrations Rolled Back:",
			"test_sql_mig",
		}

		outStr := string(out)

		assertHasOrderedSequence(t, expectedSequences, outStr)
	})

	t.Run("Test Post Basic Rollback Info", func(t *testing.T) {
		cmd := exec.Command(cmdPath, "--driver", cnf.Driver(), "--generator", cnf.Generator(), "--url", cnf.DriverUrl(), "info")
		out, err := cmd.CombinedOutput()
		if err != nil {
			t.Fatalf("Unexpected error: %+v\nOutput: %s", err, out)
		}

		outStr := string(out)
		expectedSequences := []string{
			"Tables:",
			`test_sqlite_`, `.db"│rove_migrate__cmd_log`, `│6 `,
			`test_sqlite_`, `.db"│rove_migrate__migs`, `│6 `,
			`test_sqlite_`, `.db"│rove_migrate__runs`, `│7 `,
			`test_sqlite_`, `.db"│rove_migrate__sessions`, `│4 `,
			`test_sqlite_`, `.db"│rove_migrate__sessions_migs`, `│2 `,
			`test_sqlite_`, `.db"│rove_migrate__version_info`, `│2 `,

			`test_sqlite_`, `.db".rove_migrate__cmd_log`,
			"ctime",
			"id",
			"migration_id",
			"run_id",
			"sql_cmd",
			"utime",

			`test_sqlite_`, `.db".rove_migrate__migs`,
			"ctime",
			"error",
			"id",
			"state",
			"utime",

			`test_sqlite_`, `.db".rove_migrate__runs`,
			"ctime",
			"error",
			"id",
			"is_rollback",
			"migration_id",
			"success",
			"utime",

			`test_sqlite_`, `.db".rove_migrate__sessions`,
			"ctime",
			"id",
			"is_rollback",
			"utime",

			`test_sqlite_`, `.db".rove_migrate__sessions_migs`,
			"migration_id",
			"session_id",

			`test_sqlite_`, `.db".rove_migrate__version_info`,
			"id",
			"version",

			"Indexes:",
			"rove_migrate__cmd_log_indx_migration_id", "rove_migrate__cmd_log",
			"rove_migrate__cmd_log_indx_runid", "rove_migrate__cmd_log",
			"rove_migrate__runs_indx_migration_id", "rove_migrate__runs",
			"rove_migrate__sessions_migs_indx_migration", "rove_migrate__sessions_migs",
			"rove_migrate__sessions_migs_indx_session", "rove_migrate__sessions_migs",
			"rove_migrate__migs",
			"rove_migrate__version_info",

			"Triggers:",
			"rove_migrate__update_utime_cmd",
			"rove_migrate__update_utime_migs",
			"rove_migrate__update_utime_runs",
			"rove_migrate__update_utime_sessions",

			"Migration Sessions:",
			"1", "NO", "test_sql_mig",
			"2", "YES", "test_sql_mig",

			"Migrations:",
			"test_sql_mig│ROLLBACK│     │2 ",

			"Migration Runs:",
			"1 ", "test_sql_mig", "NO", "YES",
			"2 ", "test_sql_mig", "YES", "YES",

			"Migration Commands:",
			"CREATE TABLE IF NOT EXISTS user",
			"DROP TABLE IF EXISTS user",
		}

		assertHasOrderedSequence(t, expectedSequences, outStr)
	})
}

func TestSqliteUrlInvalidTarget(t *testing.T) {
	cnf, clean := sqliteUrlConf()
	defer clean()

	dir, err := os.Getwd()
	if err != nil {
		t.Fatal(err)
	}

	cmdPath := filepath.Join(dir, binName)

	t.Run("Init", func(t *testing.T) {
		cmd := exec.Command(cmdPath, "--driver", cnf.Driver(), "--generator", cnf.Generator(), "--url", cnf.DriverUrl(), "init")
		out, err := cmd.CombinedOutput()
		if err != nil {
			t.Fatalf("Unexpected error: %+v\nOutput: %s", err, out)
		}

		outStr := string(out)
		expectedStr := "INIT SUCCESS!\n"

		if outStr != expectedStr {
			t.Errorf("Expected output %#v, received %#v", expectedStr, outStr)
		}
	})

	t.Run("Test Migration Plan", func(t *testing.T) {
		migDir, err := os.MkdirTemp(dir, "test_migrations")
		if err != nil {
			t.Fatal(err)
		}
		defer os.RemoveAll(migDir)

		if err := writeDepMigFiles(migDir); err != nil {
			t.Fatal(err)
		}

		cmd := exec.Command(cmdPath, "--driver", cnf.Driver(), "--generator", cnf.Generator(), "--url", cnf.DriverUrl(), "migrate", "--migs", migDir, "--dry", "--target", "invalid")
		out, err := cmd.CombinedOutput()
		if err == nil {
			t.Fatalf("Expected error: \nOutput: %s", out)
		}

		expectedSequences := []string{
			"Invalid target! Migration \"invalid\" does not exist!",
		}

		outStr := string(out)

		assertHasOrderedSequence(t, expectedSequences, outStr)
	})

	t.Run("Test Migration", func(t *testing.T) {
		migDir, err := os.MkdirTemp(dir, "test_migrations")
		if err != nil {
			t.Fatal(err)
		}
		defer os.RemoveAll(migDir)

		if err := writeDepMigFiles(migDir); err != nil {
			t.Fatal(err)
		}

		cmd := exec.Command(cmdPath, "--driver", cnf.Driver(), "--generator", cnf.Generator(), "--url", cnf.DriverUrl(), "migrate", "--migs", migDir, "--target", "invalid")
		out, err := cmd.CombinedOutput()
		if err == nil {
			t.Fatalf("Expected error: \nOutput: %s", out)
		}

		expectedSequences := []string{
			"Invalid target! Migration \"invalid\" does not exist!",
		}

		outStr := string(out)

		assertHasOrderedSequence(t, expectedSequences, outStr)
	})
}

func TestSqliteUrlValidate(t *testing.T) {
	cnf, clean := sqliteUrlConf()
	defer clean()

	dir, err := os.Getwd()
	if err != nil {
		t.Fatal(err)
	}

	cmdPath := filepath.Join(dir, binName)

	t.Run("Init", func(t *testing.T) {
		cmd := exec.Command(cmdPath, "--driver", cnf.Driver(), "--generator", cnf.Generator(), "--url", cnf.DriverUrl(), "init")
		out, err := cmd.CombinedOutput()
		if err != nil {
			t.Fatalf("Unexpected error: %+v\nOutput: %s", err, out)
		}

		outStr := string(out)
		expectedStr := "INIT SUCCESS!\n"

		if outStr != expectedStr {
			t.Errorf("Expected output %#v, received %#v", expectedStr, outStr)
		}
	})

	t.Run("Test unknown state", func(t *testing.T) {
		migDir, err := os.MkdirTemp(dir, "test_migrations")
		if err != nil {
			t.Fatal(err)
		}
		defer os.RemoveAll(migDir)

		if err := writeDepMigFiles(migDir); err != nil {
			t.Fatal(err)
		}

		conn, err := db.ConnectTo(cnf)
		if err != nil {
			t.Fatal(err)
		}
		defer func() {
			conn.Exec("DELETE FROM rove_migrate__migs")
			conn.Close()
		}()

		execQuery := `INSERT INTO rove_migrate__migs
	(id, state, error, hash)
VALUES ($1, $2, $3, $4)`
		if _, err := conn.Exec(execQuery, "test_dep_mig_3", common.MIG_STATE_UNKNOWN, nil, common.IGNORED_HASH); err != nil {
			t.Fatal(err)
		}
		if _, err := conn.Exec(execQuery, "test_dep_mig_1", 928913, nil, common.IGNORED_HASH); err != nil {
			t.Fatal(err)
		}

		cmd := exec.Command(cmdPath, "--logLevel", "info", "--driver", cnf.Driver(), "--generator", cnf.Generator(), "--url", cnf.DriverUrl(), "validate", "--migs", migDir)
		out, err := cmd.CombinedOutput()

		// Should not error in non-strict mode
		if err != nil {
			t.Fatalf("Unexpected error: %+v\nOutput: %s", err, out)
		}

		// Should error in strict mode
		cmd = exec.Command(cmdPath, "--logLevel", "info", "--driver", cnf.Driver(), "--generator", cnf.Generator(), "--url", cnf.DriverUrl(), "validate", "--strict", "--migs", migDir)
		out, err = cmd.CombinedOutput()
		if err == nil {
			t.Fatalf("Expected error: %+v\nOutput: %s", err, out)
		}

		expectedSequences := []string{
			"Several migrations in unknown state",
			"Migrations: [test_dep_mig_3 test_dep_mig_1]",
			"VALIDATION FAILED!",
		}

		outStr := string(out)

		assertHasOrderedSequence(t, expectedSequences, outStr)
	})

	t.Run("Test errored rollback", func(t *testing.T) {
		migDir, err := os.MkdirTemp(dir, "test_migrations")
		if err != nil {
			t.Fatal(err)
		}
		defer os.RemoveAll(migDir)

		if err := writeDepMigFiles(migDir); err != nil {
			t.Fatal(err)
		}

		conn, err := db.ConnectTo(cnf)
		if err != nil {
			t.Fatal(err)
		}
		defer func() {
			conn.Exec("DELETE FROM rove_migrate__migs")
			conn.Close()
		}()

		execQuery := `INSERT INTO rove_migrate__migs
	(id, state, error, hash)
VALUES ($1, $2, $3, $4)`
		if _, err := conn.Exec(execQuery, "test_dep_mig_3", common.MIG_STATE_ROLLBACK_ERROR, nil, common.IGNORED_HASH); err != nil {
			t.Fatal(err)
		}

		cmd := exec.Command(cmdPath, "--logLevel", "info", "--driver", cnf.Driver(), "--generator", cnf.Generator(), "--url", cnf.DriverUrl(), "validate", "--migs", migDir)
		out, err := cmd.CombinedOutput()
		if err == nil {
			t.Fatalf("Expected error: %+v\nOutput: %s", err, out)
		}

		expectedSequences := []string{
			"Failed database integrity checks",
			"Failed rollbacks detected",
			"Failed: [test_dep_mig_3]",
			"VALIDATION FAILED!",
		}

		outStr := string(out)

		assertHasOrderedSequence(t, expectedSequences, outStr)
	})

	t.Run("Test errored migration", func(t *testing.T) {
		migDir, err := os.MkdirTemp(dir, "test_migrations")
		if err != nil {
			t.Fatal(err)
		}
		defer os.RemoveAll(migDir)

		if err := writeDepMigFiles(migDir); err != nil {
			t.Fatal(err)
		}

		conn, err := db.ConnectTo(cnf)
		if err != nil {
			t.Fatal(err)
		}
		defer func() {
			conn.Exec("DELETE FROM rove_migrate__migs")
			conn.Close()
		}()

		execQuery := `INSERT INTO rove_migrate__migs
	(id, state, error, hash)
VALUES ($1, $2, $3, $4)`
		if _, err := conn.Exec(execQuery, "test_dep_mig_3", common.MIG_STATE_ERROR, nil, common.IGNORED_HASH); err != nil {
			t.Fatal(err)
		}

		cmd := exec.Command(cmdPath, "--logLevel", "info", "--driver", cnf.Driver(), "--generator", cnf.Generator(), "--url", cnf.DriverUrl(), "validate", "--migs", migDir)
		out, err := cmd.CombinedOutput()
		if err == nil {
			t.Fatalf("Expected error: %+v\nOutput: %s", err, out)
		}

		expectedSequences := []string{
			"Failed database integrity checks",
			"Failed migrations detected",
			"Failed: [test_dep_mig_3]",
			"VALIDATION FAILED!",
		}

		outStr := string(out)

		assertHasOrderedSequence(t, expectedSequences, outStr)
	})

	t.Run("Test dependency tree mismatch", func(t *testing.T) {
		migDir, err := os.MkdirTemp(dir, "test_migrations")
		if err != nil {
			t.Fatal(err)
		}
		defer os.RemoveAll(migDir)

		if err := writeDepMigFiles(migDir); err != nil {
			t.Fatal(err)
		}

		conn, err := db.ConnectTo(cnf)
		if err != nil {
			t.Fatal(err)
		}
		defer func() {
			conn.Exec("DELETE FROM rove_migrate__migs")
			conn.Close()
		}()

		execQuery := `INSERT INTO rove_migrate__migs
	(id, state, error, hash)
VALUES ($1, $2, $3, $4)`
		if _, err := conn.Exec(execQuery, "test_dep_mig_3", common.MIG_STATE_SUCCESS, nil, common.IGNORED_HASH); err != nil {
			t.Fatal(err)
		}

		cmd := exec.Command(cmdPath, "--logLevel", "info", "--driver", cnf.Driver(), "--generator", cnf.Generator(), "--url", cnf.DriverUrl(), "validate", "--migs", migDir)
		out, err := cmd.CombinedOutput()
		if err == nil {
			t.Fatalf("Expected error: %+v\nOutput: %s", err, out)
		}

		expectedSequences := []string{
			"Migration test_dep_mig_3 is missing run information",
			"Database in invalid state.",
			"Not all dependencies are satisfied.",
			"Expected migration test_dep_mig_2 to have been migrated!",
			"Depended on by: [test_dep_mig_3]",
			"VALIDATION FAILED!",
		}

		outStr := string(out)

		assertHasOrderedSequence(t, expectedSequences, outStr)
	})

	t.Run("Test missing run information", func(t *testing.T) {
		migDir, err := os.MkdirTemp(dir, "test_migrations")
		if err != nil {
			t.Fatal(err)
		}
		defer os.RemoveAll(migDir)

		if err := writeDepMigFiles(migDir); err != nil {
			t.Fatal(err)
		}

		conn, err := db.ConnectTo(cnf)
		if err != nil {
			t.Fatal(err)
		}
		defer func() {
			conn.Exec("DELETE FROM rove_migrate__migs")
			conn.Close()
		}()

		execQuery := `INSERT INTO rove_migrate__migs
	(id, state, error, hash)
VALUES ($1, $2, $3, $4)`
		if _, err := conn.Exec(execQuery, "test_dep_mig_1", common.MIG_STATE_SUCCESS, nil, common.IGNORED_HASH); err != nil {
			t.Fatal(err)
		}

		cmd := exec.Command(cmdPath, "--logLevel", "info", "--driver", cnf.Driver(), "--generator", cnf.Generator(), "--url", cnf.DriverUrl(), "validate", "--strict", "--migs", migDir)
		out, err := cmd.CombinedOutput()
		if err == nil {
			t.Fatalf("Expected error: %+v\nOutput: %s", err, out)
		}

		expectedSequences := []string{
			"Database in invalid state.",
			"Migration", "is a no-op!",
			"Migration test_dep_mig_1 is missing run information",
			"VALIDATION FAILED!",
		}

		outStr := string(out)

		assertHasOrderedSequence(t, expectedSequences, outStr)
	})

	t.Run("Test hash mismatch migration", func(t *testing.T) {
		migDir, err := os.MkdirTemp(dir, "test_migrations")
		if err != nil {
			t.Fatal(err)
		}
		defer os.RemoveAll(migDir)

		if err := writeDepMigFiles(migDir); err != nil {
			t.Fatal(err)
		}

		conn, err := db.ConnectTo(cnf)
		if err != nil {
			t.Fatal(err)
		}
		defer func() {
			conn.Exec("DELETE FROM rove_migrate__migs")
			conn.Close()
		}()

		execQuery := `INSERT INTO rove_migrate__migs
	(id, state, error, hash)
VALUES ($1, $2, $3, $4)`

		invalidHash := "1:0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
		if _, err := conn.Exec(execQuery, "test_dep_mig_1", common.MIG_STATE_SUCCESS, nil, invalidHash); err != nil {
			t.Fatal(err)
		}
		if _, err := conn.Exec(execQuery, "test_dep_mig_2", common.MIG_STATE_SUCCESS, nil, invalidHash); err != nil {
			t.Fatal(err)
		}

		cmd := exec.Command(cmdPath, "--logLevel", "info", "--driver", cnf.Driver(), "--generator", cnf.Generator(), "--url", cnf.DriverUrl(), "migrate", "--migs", migDir)
		out, err := cmd.CombinedOutput()
		if err == nil {
			t.Fatalf("Expected error: %+v\nOutput: %s", err, out)
		}

		expectedSequences := []string{
			"Mismatched integrity hashes found between database and migration scripts",
			"To ignore this error, change the database hash to start with '0:' instead of '1:'",
			"Mismatched hash for migration test_dep_mig_1",
			"Mismatched hash for migration test_dep_mig_2",
		}

		outStr := string(out)

		assertHasOrderedSequence(t, expectedSequences, outStr)
	})

	t.Run("Test hash mismatch", func(t *testing.T) {
		migDir, err := os.MkdirTemp(dir, "test_migrations")
		if err != nil {
			t.Fatal(err)
		}
		defer os.RemoveAll(migDir)

		if err := writeDepMigFiles(migDir); err != nil {
			t.Fatal(err)
		}

		conn, err := db.ConnectTo(cnf)
		if err != nil {
			t.Fatal(err)
		}
		defer func() {
			conn.Exec("DELETE FROM rove_migrate__migs")
			conn.Close()
		}()

		execQuery := `INSERT INTO rove_migrate__migs
	(id, state, error, hash)
VALUES ($1, $2, $3, $4)`

		invalidHash := "1:0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
		if _, err := conn.Exec(execQuery, "test_dep_mig_1", common.MIG_STATE_SUCCESS, nil, invalidHash); err != nil {
			t.Fatal(err)
		}
		if _, err := conn.Exec(execQuery, "test_dep_mig_2", common.MIG_STATE_SUCCESS, nil, invalidHash); err != nil {
			t.Fatal(err)
		}

		cmd := exec.Command(cmdPath, "--logLevel", "info", "--driver", cnf.Driver(), "--generator", cnf.Generator(), "--url", cnf.DriverUrl(), "validate", "--migs", migDir)
		out, err := cmd.CombinedOutput()
		if err == nil {
			t.Fatalf("Expected error: %+v\nOutput: %s", err, out)
		}

		expectedSequences := []string{
			"Mismatched hash for migration test_dep_mig_1",
			"Mismatched hash for migration test_dep_mig_2",
			"VALIDATION FAILED!",
		}

		outStr := string(out)

		assertHasOrderedSequence(t, expectedSequences, outStr)
	})

	t.Run("Test Clean Validate", func(t *testing.T) {
		migDir, err := os.MkdirTemp(dir, "test_migrations")
		if err != nil {
			t.Fatal(err)
		}
		defer os.RemoveAll(migDir)

		if err := writeDepMigFiles(migDir); err != nil {
			t.Fatal(err)
		}

		cmd := exec.Command(cmdPath, "--logLevel", "info", "--driver", cnf.Driver(), "--generator", cnf.Generator(), "--url", cnf.DriverUrl(), "validate", "--migs", migDir)
		out, err := cmd.CombinedOutput()
		if err != nil {
			t.Fatalf("Unexpected error: %+v\nOutput: %s", err, out)
		}

		expectedSequences := []string{
			"Validating migrations",
			"Migration", "is a no-op!",
			"Validating database integrity",
			"Verifying database state",
			"Building dependency tree",
			"Checking database migrations match expected dependency tree",
			"Finished validation checks",
			"VALIDATION PASSED!",
		}

		outStr := string(out)

		assertHasOrderedSequence(t, expectedSequences, outStr)
	})

	t.Run("Test Strict Mode", func(t *testing.T) {
		migDir, err := os.MkdirTemp(dir, "test_migrations")
		if err != nil {
			t.Fatal(err)
		}
		defer os.RemoveAll(migDir)

		if err := writeDepMigFiles(migDir); err != nil {
			t.Fatal(err)
		}

		cmd := exec.Command(cmdPath, "--logLevel", "info", "--driver", cnf.Driver(), "--generator", cnf.Generator(), "--url", cnf.DriverUrl(), "validate", "--strict", "--migs", migDir)
		out, err := cmd.CombinedOutput()
		if err == nil {
			t.Fatalf("Expected error: %+v\nOutput: %s", err, out)
		}

		expectedSequences := []string{
			"Invalid migrations",
			"Migration test_dep_mig_3 is a no-op!",
			"Migration test_dep_mig_4 is a no-op!",
			"VALIDATION FAILED!",
		}

		outStr := string(out)

		assertHasOrderedSequence(t, expectedSequences, outStr)
	})
}

func TestSqliteUrlDeps(t *testing.T) {
	cnf, clean := sqliteUrlConf()
	defer clean()

	dir, err := os.Getwd()
	if err != nil {
		t.Fatal(err)
	}

	cmdPath := filepath.Join(dir, binName)

	t.Run("Init", func(t *testing.T) {
		cmd := exec.Command(cmdPath, "--driver", cnf.Driver(), "--generator", cnf.Generator(), "--url", cnf.DriverUrl(), "init")
		out, err := cmd.CombinedOutput()
		if err != nil {
			t.Fatalf("Unexpected error: %+v\nOutput: %s", err, out)
		}

		outStr := string(out)
		expectedStr := "INIT SUCCESS!\n"

		if outStr != expectedStr {
			t.Errorf("Expected output %#v, received %#v", expectedStr, outStr)
		}
	})

	t.Run("Test Dependency Tree", func(t *testing.T) {
		migDir, err := os.MkdirTemp(dir, "test_migrations")
		if err != nil {
			t.Fatal(err)
		}
		defer os.RemoveAll(migDir)

		if err := writeDepMigFiles(migDir); err != nil {
			t.Fatal(err)
		}

		cmd := exec.Command(cmdPath, "--driver", cnf.Driver(), "--generator", cnf.Generator(), "--url", cnf.DriverUrl(), "dep-tree", "--migs", migDir)
		out, err := cmd.CombinedOutput()
		if err != nil {
			t.Fatalf("Unexpected error: %+v\nOutput: %s", err, out)
		}

		expectedSequences := []string{
			"Dependency Tree",
			"test_dep_mig_4",
			"test_dep_mig_3",
			"test_dep_mig_2",
			"test_dep_mig_1",
		}

		outStr := string(out)

		assertHasOrderedSequence(t, expectedSequences, outStr)
	})

	t.Run("Test Dependency Tree 2", func(t *testing.T) {
		migDir, err := os.MkdirTemp(dir, "test_migrations")
		if err != nil {
			t.Fatal(err)
		}
		defer os.RemoveAll(migDir)

		if err := writeDepMigFiles2(migDir); err != nil {
			t.Fatal(err)
		}

		cmd := exec.Command(cmdPath, "--driver", cnf.Driver(), "--generator", cnf.Generator(), "--url", cnf.DriverUrl(), "dep-tree", "--migs", migDir)
		out, err := cmd.CombinedOutput()
		if err != nil {
			t.Fatalf("Unexpected error: %+v\nOutput: %s", err, out)
		}

		expectedSequences := []string{
			"Dependency Tree",
			"test_dep_mig_4",
			"test_dep_mig_3",
			"test_dep_mig_2",
			"test_dep_mig_1",
			"test_dep_mig_2",
			"test_dep_mig_1",
			"test_dep_mig_5",
			"test_dep_mig_7",
			"test_dep_mig_3",
			"test_dep_mig_2",
			"test_dep_mig_1",
			"test_dep_mig_6",
			"test_dep_mig_1",
		}

		outStr := string(out)

		assertHasOrderedSequence(t, expectedSequences, outStr)
	})

	t.Run("Test Migration Plan", func(t *testing.T) {
		migDir, err := os.MkdirTemp(dir, "test_migrations")
		if err != nil {
			t.Fatal(err)
		}
		defer os.RemoveAll(migDir)

		if err := writeDepMigFiles(migDir); err != nil {
			t.Fatal(err)
		}

		cmd := exec.Command(cmdPath, "--driver", cnf.Driver(), "--generator", cnf.Generator(), "--url", cnf.DriverUrl(), "migrate", "--migs", migDir, "--dry")
		out, err := cmd.CombinedOutput()
		if err != nil {
			t.Fatalf("Unexpected error: %+v\nOutput: %s", err, out)
		}

		expectedSequences := []string{
			"Migration Plan:",
			"test_dep_mig_1",
			"test_dep_mig_2",
		}

		outStr := string(out)

		assertHasOrderedSequence(t, expectedSequences, outStr)
	})

	t.Run("Test Migration", func(t *testing.T) {
		migDir, err := os.MkdirTemp(dir, "test_migrations")
		if err != nil {
			t.Fatal(err)
		}
		defer os.RemoveAll(migDir)

		if err := writeDepMigFiles(migDir); err != nil {
			t.Fatal(err)
		}

		cmd := exec.Command(cmdPath, "--driver", cnf.Driver(), "--generator", cnf.Generator(), "--url", cnf.DriverUrl(), "migrate", "--migs", migDir)
		out, err := cmd.CombinedOutput()
		if err != nil {
			t.Fatalf("Unexpected error: %+v\nOutput: %s", err, out)
		}

		expectedSequences := []string{
			"Migrations Ran:",
			"test_dep_mig_1",
			"test_dep_mig_2",
		}

		outStr := string(out)

		assertHasOrderedSequence(t, expectedSequences, outStr)
	})

	t.Run("Test Post Deps Migration Info", func(t *testing.T) {
		cmd := exec.Command(cmdPath, "--driver", cnf.Driver(), "--generator", cnf.Generator(), "--url", cnf.DriverUrl(), "info")
		out, err := cmd.CombinedOutput()
		if err != nil {
			t.Fatalf("Unexpected error: %+v\nOutput: %s", err, out)
		}

		outStr := string(out)
		expectedSequences := []string{
			"Tables:",
			`test_sqlite_`, `.db"│rove_migrate__cmd_log`, `│6 `,
			`test_sqlite_`, `.db"│rove_migrate__migs`, `│6 `,
			`test_sqlite_`, `.db"│rove_migrate__runs`, `│7 `,
			`test_sqlite_`, `.db"│user`, `│3 `,

			`test_sqlite_`, `.db".rove_migrate__cmd_log`,
			"ctime",
			"id",
			"migration_id",
			"run_id",
			"sql_cmd",
			"utime",

			`test_sqlite_`, `.db".rove_migrate__migs`,
			"ctime",
			"error",
			"id",
			"state",
			"utime",

			`test_sqlite_`, `.db".rove_migrate__runs`,
			"ctime",
			"error",
			"id",
			"is_rollback",
			"migration_id",
			"success",
			"utime",

			`test_sqlite_`, `.db".user`,
			"id",
			"name",

			"Indexes:",
			"rove_migrate__migs",

			"Triggers:",
			"rove_migrate__update_utime_cmd",
			"rove_migrate__update_utime_migs",
			"rove_migrate__update_utime_runs",

			"Migration Sessions:",
			"1", "NO", "test_dep_mig_1, test_dep_mig_2, test_dep_mig_3, test_dep_mig_4",

			"Migrations:",
			"test_dep_mig_1│SUCCESS│     │1 ",
			"test_dep_mig_2│SUCCESS│     │1 ",
			"test_dep_mig_3│SUCCESS│     │1 ",
			"test_dep_mig_4│SUCCESS│     │1 ",

			"Migration Runs:",
			"1 ", "test_dep_mig_1", "NO", "YES",
			"2 ", "test_dep_mig_2", "NO", "YES",
			"3 ", "test_dep_mig_3", "NO", "YES",
			"4 ", "test_dep_mig_4", "NO", "YES",

			"Migration Commands:",
			"CREATE TABLE IF NOT EXISTS user",
			"ALTER TABLE user ADD email TEXT",
		}

		assertHasOrderedSequence(t, expectedSequences, outStr)
	})

	t.Run("Test Rollback Plan", func(t *testing.T) {
		migDir, err := os.MkdirTemp(dir, "test_migrations")
		if err != nil {
			t.Fatal(err)
		}
		defer os.RemoveAll(migDir)

		if err := writeDepMigFiles(migDir); err != nil {
			t.Fatal(err)
		}

		cmd := exec.Command(cmdPath, "--driver", cnf.Driver(), "--generator", cnf.Generator(), "--url", cnf.DriverUrl(), "rollback", "--migs", migDir, "--dry")
		out, err := cmd.CombinedOutput()
		if err != nil {
			t.Fatalf("Unexpected error: %+v\nOutput: %s", err, out)
		}

		expectedSequences := []string{
			"Rollback Plan:",
			"test_dep_mig_2",
			"test_dep_mig_1",
		}

		outStr := string(out)

		assertHasOrderedSequence(t, expectedSequences, outStr)
	})

	t.Run("Test Rollback", func(t *testing.T) {
		migDir, err := os.MkdirTemp(dir, "test_migrations")
		if err != nil {
			t.Fatal(err)
		}
		defer os.RemoveAll(migDir)

		if err := writeDepMigFiles(migDir); err != nil {
			t.Fatal(err)
		}

		cmd := exec.Command(cmdPath, "--driver", cnf.Driver(), "--generator", cnf.Generator(), "--url", cnf.DriverUrl(), "rollback", "--migs", migDir)
		out, err := cmd.CombinedOutput()
		if err != nil {
			t.Fatalf("Unexpected error: %+v\nOutput: %s", err, out)
		}

		expectedSequences := []string{
			"Migrations Rolled Back:",
			"test_dep_mig_2",
			"test_dep_mig_1",
		}

		outStr := string(out)

		assertHasOrderedSequence(t, expectedSequences, outStr)
	})

	t.Run("Test Post Deps Rollback Info", func(t *testing.T) {
		cmd := exec.Command(cmdPath, "--driver", cnf.Driver(), "--generator", cnf.Generator(), "--url", cnf.DriverUrl(), "info")
		out, err := cmd.CombinedOutput()
		if err != nil {
			t.Fatalf("Unexpected error: %+v\nOutput: %s", err, out)
		}

		outStr := string(out)
		expectedSequences := []string{
			"Tables:",
			`test_sqlite_`, `.db"│rove_migrate__cmd_log`, `│6 `,
			`test_sqlite_`, `.db"│rove_migrate__migs`, `│6 `,
			`test_sqlite_`, `.db"│rove_migrate__runs`, `│7 `,

			`test_sqlite_`, `.db".rove_migrate__cmd_log`,
			"ctime",
			"id",
			"migration_id",
			"run_id",
			"sql_cmd",
			"utime",

			`test_sqlite_`, `.db".rove_migrate__migs`,
			"ctime",
			"error",
			"id",
			"state",
			"utime",

			`test_sqlite_`, `.db".rove_migrate__runs`,
			"ctime",
			"error",
			"id",
			"is_rollback",
			"migration_id",
			"success",
			"utime",

			"Indexes:",
			"rove_migrate__migs",

			"Triggers:",
			"rove_migrate__update_utime_cmd",
			"rove_migrate__update_utime_migs",
			"rove_migrate__update_utime_runs",

			"Migrations:",
			"test_dep_mig_1│ROLLBACK│     │2 ",
			"test_dep_mig_2│ROLLBACK│     │2 ",
			"test_dep_mig_3│ROLLBACK│     │2 ",
			"test_dep_mig_4│ROLLBACK│     │2 ",

			"Migration Runs:",
			"1 ", "test_dep_mig_1", "NO", "YES",
			"2 ", "test_dep_mig_2", "NO", "YES",
			"3 ", "test_dep_mig_3", "NO", "YES",
			"4 ", "test_dep_mig_4", "NO", "YES",
			"5 ", "test_dep_mig_4", "YES", "YES",
			"6 ", "test_dep_mig_3", "YES", "YES",
			"7 ", "test_dep_mig_2", "YES", "YES",
			"8 ", "test_dep_mig_1", "YES", "YES",

			"Migration Commands:",
			"CREATE TABLE IF NOT EXISTS user",
			"ALTER TABLE user ADD email TEXT",
			"ALTER TABLE user DROP COLUMN email",
			"DROP TABLE IF EXISTS user",
		}

		assertHasOrderedSequence(t, expectedSequences, outStr)
	})
}

func TestSqliteUrlDeps2(t *testing.T) {
	cnf, clean := sqliteUrlConf()
	defer clean()

	dir, err := os.Getwd()
	if err != nil {
		t.Fatal(err)
	}

	cmdPath := filepath.Join(dir, binName)

	t.Run("Init", func(t *testing.T) {
		cmd := exec.Command(cmdPath, "--driver", cnf.Driver(), "--generator", cnf.Generator(), "--url", cnf.DriverUrl(), "init")
		out, err := cmd.CombinedOutput()
		if err != nil {
			t.Fatalf("Unexpected error: %+v\nOutput: %s", err, out)
		}

		outStr := string(out)
		expectedStr := "INIT SUCCESS!\n"

		if outStr != expectedStr {
			t.Errorf("Expected output %#v, received %#v", expectedStr, outStr)
		}
	})

	t.Run("Test Dependency Tree 2", func(t *testing.T) {
		migDir, err := os.MkdirTemp(dir, "test_migrations")
		if err != nil {
			t.Fatal(err)
		}
		defer os.RemoveAll(migDir)

		if err := writeDepMigFiles2(migDir); err != nil {
			t.Fatal(err)
		}

		cmd := exec.Command(cmdPath, "--driver", cnf.Driver(), "--generator", cnf.Generator(), "--url", cnf.DriverUrl(), "dep-tree", "--migs", migDir)
		out, err := cmd.CombinedOutput()
		if err != nil {
			t.Fatalf("Unexpected error: %+v\nOutput: %s", err, out)
		}

		expectedSequences := []string{
			"Dependency Tree",
			"test_dep_mig_4",
			"test_dep_mig_3",
			"test_dep_mig_2",
			"test_dep_mig_1",
			"test_dep_mig_2",
			"test_dep_mig_1",
			"test_dep_mig_5",
			"test_dep_mig_7",
			"test_dep_mig_3",
			"test_dep_mig_2",
			"test_dep_mig_1",
			"test_dep_mig_6",
			"test_dep_mig_1",
		}

		outStr := string(out)

		assertHasOrderedSequence(t, expectedSequences, outStr)
	})

	t.Run("Test Migration Plan", func(t *testing.T) {
		migDir, err := os.MkdirTemp(dir, "test_migrations")
		if err != nil {
			t.Fatal(err)
		}
		defer os.RemoveAll(migDir)

		if err := writeDepMigFiles2(migDir); err != nil {
			t.Fatal(err)
		}

		cmd := exec.Command(cmdPath, "--driver", cnf.Driver(), "--generator", cnf.Generator(), "--url", cnf.DriverUrl(), "migrate", "--migs", migDir, "--dry", "--target", "test_dep_mig_5", "--min")
		out, err := cmd.CombinedOutput()
		if err != nil {
			t.Fatalf("Unexpected error: %+v\nOutput: %s", err, out)
		}

		expectedSequences := []string{
			"test_dep_mig_5",
		}

		outStr := string(out)

		assertHasOrderedSequence(t, expectedSequences, outStr)

		if strings.Contains(outStr, "test_dep_mig_1") {
			t.Errorf("Expected minimal migration without test_dep_mig_1. Received: %v", outStr)
		}
	})

	t.Run("Test Migration Plan 2", func(t *testing.T) {
		migDir, err := os.MkdirTemp(dir, "test_migrations")
		if err != nil {
			t.Fatal(err)
		}
		defer os.RemoveAll(migDir)

		if err := writeDepMigFiles2(migDir); err != nil {
			t.Fatal(err)
		}

		cmd := exec.Command(cmdPath, "--driver", cnf.Driver(), "--generator", cnf.Generator(), "--url", cnf.DriverUrl(), "migrate", "--migs", migDir, "--dry", "--target", "test_dep_mig_7", "--min")
		out, err := cmd.CombinedOutput()
		if err != nil {
			t.Fatalf("Unexpected error: %+v\nOutput: %s", err, out)
		}

		expectedSequences := []string{
			"test_dep_mig_1",
			"test_dep_mig_6",
			"test_dep_mig_2",
			"test_dep_mig_3",
			"test_dep_mig_7",
		}

		outStr := string(out)

		assertHasOrderedSequence(t, expectedSequences, outStr)

		if strings.Contains(outStr, "test_dep_mig_4") {
			t.Errorf("Expected minimal migration without test_dep_mig_4. Received: %v", outStr)
		}
	})

	t.Run("Test Migration", func(t *testing.T) {
		migDir, err := os.MkdirTemp(dir, "test_migrations")
		if err != nil {
			t.Fatal(err)
		}
		defer os.RemoveAll(migDir)

		if err := writeDepMigFiles2(migDir); err != nil {
			t.Fatal(err)
		}

		cmd := exec.Command(cmdPath, "--driver", cnf.Driver(), "--generator", cnf.Generator(), "--url", cnf.DriverUrl(), "migrate", "--migs", migDir)
		out, err := cmd.CombinedOutput()
		if err != nil {
			t.Fatalf("Unexpected error: %+v\nOutput: %s", err, out)
		}
	})

	t.Run("Test Rollback Plan", func(t *testing.T) {
		migDir, err := os.MkdirTemp(dir, "test_migrations")
		if err != nil {
			t.Fatal(err)
		}
		defer os.RemoveAll(migDir)

		if err := writeDepMigFiles2(migDir); err != nil {
			t.Fatal(err)
		}

		cmd := exec.Command(cmdPath, "--driver", cnf.Driver(), "--generator", cnf.Generator(), "--url", cnf.DriverUrl(), "rollback", "--migs", migDir, "--dry", "--target", "test_dep_mig_5", "--min")
		out, err := cmd.CombinedOutput()
		if err != nil {
			t.Fatalf("Unexpected error: %+v\nOutput: %s", err, out)
		}

		expectedSequences := []string{
			"test_dep_mig_5",
		}

		outStr := string(out)

		assertHasOrderedSequence(t, expectedSequences, outStr)

		if strings.Contains(outStr, "test_dep_mig_4") {
			t.Errorf("Expected minimal migration without test_dep_mig_1. Received: %v", outStr)
		}
	})

	t.Run("Test Migration Plan 2", func(t *testing.T) {
		migDir, err := os.MkdirTemp(dir, "test_migrations")
		if err != nil {
			t.Fatal(err)
		}
		defer os.RemoveAll(migDir)

		if err := writeDepMigFiles2(migDir); err != nil {
			t.Fatal(err)
		}

		cmd := exec.Command(cmdPath, "--driver", cnf.Driver(), "--generator", cnf.Generator(), "--url", cnf.DriverUrl(), "rollback", "--migs", migDir, "--dry", "--target", "test_dep_mig_3", "--min")
		out, err := cmd.CombinedOutput()
		if err != nil {
			t.Fatalf("Unexpected error: %+v\nOutput: %s", err, out)
		}

		expectedSequences := []string{
			"test_dep_mig_4",
			"test_dep_mig_7",
			"test_dep_mig_3",
		}

		outStr := string(out)

		assertHasOrderedSequence(t, expectedSequences, outStr)

		if strings.Contains(outStr, "test_dep_mig_5") {
			t.Errorf("Expected minimal migration without test_dep_mig_4. Received: %v", outStr)
		}
	})
}

func TestSqliteUrlDepsTarget(t *testing.T) {
	cnf, clean := sqliteUrlConf()
	defer clean()

	dir, err := os.Getwd()
	if err != nil {
		t.Fatal(err)
	}

	cmdPath := filepath.Join(dir, binName)

	t.Run("Init", func(t *testing.T) {
		cmd := exec.Command(cmdPath, "--driver", cnf.Driver(), "--generator", cnf.Generator(), "--url", cnf.DriverUrl(), "init")
		out, err := cmd.CombinedOutput()
		if err != nil {
			t.Fatalf("Unexpected error: %+v\nOutput: %s", err, out)
		}

		outStr := string(out)
		expectedStr := "INIT SUCCESS!\n"

		if outStr != expectedStr {
			t.Errorf("Expected output %#v, received %#v", expectedStr, outStr)
		}
	})

	t.Run("Test Migration Plan", func(t *testing.T) {
		migDir, err := os.MkdirTemp(dir, "test_migrations")
		if err != nil {
			t.Fatal(err)
		}
		defer os.RemoveAll(migDir)

		if err := writeDepMigFiles(migDir); err != nil {
			t.Fatal(err)
		}

		cmd := exec.Command(cmdPath, "--driver", cnf.Driver(), "--generator", cnf.Generator(), "--url", cnf.DriverUrl(), "migrate", "--migs", migDir, "--dry", "--target", "test_dep_mig_2")
		out, err := cmd.CombinedOutput()
		if err != nil {
			t.Fatalf("Unexpected error: %+v\nOutput: %s", err, out)
		}

		expectedSequences := []string{
			"Migration Plan:",
			"test_dep_mig_1",
		}

		outStr := string(out)

		assertHasOrderedSequence(t, expectedSequences, outStr)
	})

	t.Run("Test Validate Post Target", func(t *testing.T) {
		migDir, err := os.MkdirTemp(dir, "test_migrations")
		if err != nil {
			t.Fatal(err)
		}
		defer os.RemoveAll(migDir)

		if err := writeDepMigFiles(migDir); err != nil {
			t.Fatal(err)
		}

		cmd := exec.Command(cmdPath, "--driver", cnf.Driver(), "--generator", cnf.Generator(), "--url", cnf.DriverUrl(), "validate", "--migs", migDir)
		out, err := cmd.CombinedOutput()
		if err != nil {
			t.Fatalf("Unexpected error: %+v\nOutput: %s", err, out)
		}

		expectedSequences := []string{
			"VALIDATION PASSED!",
		}

		outStr := string(out)

		assertHasOrderedSequence(t, expectedSequences, outStr)
	})

	t.Run("Test Migration", func(t *testing.T) {
		migDir, err := os.MkdirTemp(dir, "test_migrations")
		if err != nil {
			t.Fatal(err)
		}
		defer os.RemoveAll(migDir)

		if err := writeDepMigFiles(migDir); err != nil {
			t.Fatal(err)
		}

		cmd := exec.Command(cmdPath, "--driver", cnf.Driver(), "--generator", cnf.Generator(), "--url", cnf.DriverUrl(), "migrate", "--migs", migDir, "--target", "test_dep_mig_1")
		out, err := cmd.CombinedOutput()
		if err != nil {
			t.Fatalf("Unexpected error: %+v\nOutput: %s", err, out)
		}

		expectedSequences := []string{
			"Migrations Ran:",
			"test_dep_mig_1",
		}

		outStr := string(out)

		assertHasOrderedSequence(t, expectedSequences, outStr)
	})

	t.Run("Test Post Basic Migration Info", func(t *testing.T) {
		cmd := exec.Command(cmdPath, "--driver", cnf.Driver(), "--generator", cnf.Generator(), "--url", cnf.DriverUrl(), "info")
		out, err := cmd.CombinedOutput()
		if err != nil {
			t.Fatalf("Unexpected error: %+v\nOutput: %s", err, out)
		}

		outStr := string(out)
		expectedSequences := []string{
			"Tables:",
			`test_sqlite_`, `.db"│rove_migrate__cmd_log`, `│6 `,
			`test_sqlite_`, `.db"│rove_migrate__migs`, `│6 `,
			`test_sqlite_`, `.db"│rove_migrate__runs`, `│7 `,
			`test_sqlite_`, `.db"│user`, `│2 `,

			`test_sqlite_`, `.db".rove_migrate__cmd_log`,
			"ctime",
			"id",
			"migration_id",
			"run_id",
			"sql_cmd",
			"utime",

			`test_sqlite_`, `.db".rove_migrate__migs`,
			"ctime",
			"error",
			"id",
			"state",
			"utime",

			`test_sqlite_`, `.db".rove_migrate__runs`,
			"ctime",
			"error",
			"id",
			"is_rollback",
			"migration_id",
			"success",
			"utime",

			`test_sqlite_`, `.db".user`,
			"id",
			"name",

			"Indexes:",
			"rove_migrate__migs",

			"Triggers:",
			"rove_migrate__update_utime_cmd",
			"rove_migrate__update_utime_migs",
			"rove_migrate__update_utime_runs",

			"Migrations:",
			"test_dep_mig_1│SUCCESS│     │1 ",

			"Migration Runs:",
			"1 ", "test_dep_mig_1", "NO", "YES",

			"Migration Commands:",
			"CREATE TABLE IF NOT EXISTS user",
		}

		assertHasOrderedSequence(t, expectedSequences, outStr)
	})

	t.Run("Test Migration Plan Full", func(t *testing.T) {
		migDir, err := os.MkdirTemp(dir, "test_migrations")
		if err != nil {
			t.Fatal(err)
		}
		defer os.RemoveAll(migDir)

		if err := writeDepMigFiles(migDir); err != nil {
			t.Fatal(err)
		}

		cmd := exec.Command(cmdPath, "--driver", cnf.Driver(), "--generator", cnf.Generator(), "--url", cnf.DriverUrl(), "migrate", "--migs", migDir, "--dry")
		out, err := cmd.CombinedOutput()
		if err != nil {
			t.Fatalf("Unexpected error: %+v\nOutput: %s", err, out)
		}

		expectedSequences := []string{
			"Migration Plan:",
			"test_dep_mig_2",
			"test_dep_mig_3",
		}

		outStr := string(out)

		assertHasOrderedSequence(t, expectedSequences, outStr)
	})

	t.Run("Test Migration Full", func(t *testing.T) {
		migDir, err := os.MkdirTemp(dir, "test_migrations")
		if err != nil {
			t.Fatal(err)
		}
		defer os.RemoveAll(migDir)

		if err := writeDepMigFiles(migDir); err != nil {
			t.Fatal(err)
		}

		cmd := exec.Command(cmdPath, "--driver", cnf.Driver(), "--generator", cnf.Generator(), "--url", cnf.DriverUrl(), "migrate", "--migs", migDir)
		out, err := cmd.CombinedOutput()
		if err != nil {
			t.Fatalf("Unexpected error: %+v\nOutput: %s", err, out)
		}

		expectedSequences := []string{
			"Migrations Ran:",
			"test_dep_mig_2",
			"test_dep_mig_3",
		}

		outStr := string(out)

		assertHasOrderedSequence(t, expectedSequences, outStr)
	})

	t.Run("Test Rollback Plan", func(t *testing.T) {
		migDir, err := os.MkdirTemp(dir, "test_migrations")
		if err != nil {
			t.Fatal(err)
		}
		defer os.RemoveAll(migDir)

		if err := writeDepMigFiles(migDir); err != nil {
			t.Fatal(err)
		}

		cmd := exec.Command(cmdPath, "--driver", cnf.Driver(), "--generator", cnf.Generator(), "--url", cnf.DriverUrl(), "rollback", "--migs", migDir, "--dry", "--target", "test_dep_mig_2")
		out, err := cmd.CombinedOutput()
		if err != nil {
			t.Fatalf("Unexpected error: %+v\nOutput: %s", err, out)
		}

		expectedSequences := []string{
			"Rollback Plan:",
			"test_dep_mig_2",
		}

		outStr := string(out)

		assertHasOrderedSequence(t, expectedSequences, outStr)
	})

	t.Run("Test Rollback", func(t *testing.T) {
		migDir, err := os.MkdirTemp(dir, "test_migrations")
		if err != nil {
			t.Fatal(err)
		}
		defer os.RemoveAll(migDir)

		if err := writeDepMigFiles(migDir); err != nil {
			t.Fatal(err)
		}

		cmd := exec.Command(cmdPath, "--driver", cnf.Driver(), "--generator", cnf.Generator(), "--url", cnf.DriverUrl(), "rollback", "--migs", migDir, "--target", "test_dep_mig_2")
		out, err := cmd.CombinedOutput()
		if err != nil {
			t.Fatalf("Unexpected error: %+v\nOutput: %s", err, out)
		}

		expectedSequences := []string{
			"Migrations Rolled Back:",
			"test_dep_mig_2",
		}

		outStr := string(out)

		assertHasOrderedSequence(t, expectedSequences, outStr)
	})

	t.Run("Test Post Basic Rollback Info", func(t *testing.T) {
		cmd := exec.Command(cmdPath, "--driver", cnf.Driver(), "--generator", cnf.Generator(), "--url", cnf.DriverUrl(), "info")
		out, err := cmd.CombinedOutput()
		if err != nil {
			t.Fatalf("Unexpected error: %+v\nOutput: %s", err, out)
		}

		outStr := string(out)
		expectedSequences := []string{
			"Tables:",
			`test_sqlite_`, `.db"│rove_migrate__cmd_log`, `│6 `,
			`test_sqlite_`, `.db"│rove_migrate__migs`, `│6 `,
			`test_sqlite_`, `.db"│rove_migrate__runs`, `│7 `,
			`test_sqlite_`, `.db"│user`, `│2 `,

			`test_sqlite_`, `.db".rove_migrate__cmd_log`,
			"ctime",
			"id",
			"migration_id",
			"run_id",
			"sql_cmd",
			"utime",

			`test_sqlite_`, `.db".rove_migrate__migs`,
			"ctime",
			"error",
			"id",
			"state",
			"utime",

			`test_sqlite_`, `.db".rove_migrate__runs`,
			"ctime",
			"error",
			"id",
			"is_rollback",
			"migration_id",
			"success",
			"utime",

			`test_sqlite_`, `.db".user`,
			"id",
			"name",

			"Indexes:",
			"rove_migrate__migs",

			"Triggers:",
			"rove_migrate__update_utime_cmd",
			"rove_migrate__update_utime_migs",
			"rove_migrate__update_utime_runs",

			"Migrations:",
			"test_dep_mig_1│SUCCESS │     │1 ",
			"test_dep_mig_2│ROLLBACK│     │2 ",

			"Migration Runs:",
			"test_dep_mig_1", "NO", "YES",
			"test_dep_mig_2", "NO", "YES",
			"test_dep_mig_2", "YES", "YES",

			"Migration Commands:",
			"CREATE TABLE IF NOT EXISTS user",
			"ALTER TABLE user ADD email TEXT",
			"ALTER TABLE user DROP COLUMN email",
		}

		assertHasOrderedSequence(t, expectedSequences, outStr)
	})
}

func TestSqliteUrlJson(t *testing.T) {
	cnf, clean := sqliteUrlConf()
	defer clean()

	dir, err := os.Getwd()
	if err != nil {
		t.Fatal(err)
	}

	cmdPath := filepath.Join(dir, binName)

	t.Run("Init", func(t *testing.T) {
		cmd := exec.Command(cmdPath, "--driver", cnf.Driver(), "--generator", cnf.Generator(), "--url", cnf.DriverUrl(), "init")
		out, err := cmd.CombinedOutput()
		if err != nil {
			t.Fatalf("Unexpected error: %+v\nOutput: %s", err, out)
		}

		outStr := string(out)
		expectedStr := "INIT SUCCESS!\n"

		if outStr != expectedStr {
			t.Errorf("Expected output %#v, received %#v", expectedStr, outStr)
		}
	})

	t.Run("Test Migration", func(t *testing.T) {
		migDir, err := os.MkdirTemp(dir, "test_migrations")
		if err != nil {
			t.Fatal(err)
		}
		defer os.RemoveAll(migDir)

		if err := writeJsonMigFiles(migDir); err != nil {
			t.Fatal(err)
		}

		cmd := exec.Command(cmdPath, "--driver", cnf.Driver(), "--generator", cnf.Generator(), "--url", cnf.DriverUrl(), "migrate", "--migs", migDir)
		out, err := cmd.CombinedOutput()
		if err != nil {
			t.Fatalf("Unexpected error: %+v\nOutput: %s", err, out)
		}

		expectedSequences := []string{
			"Migrations Ran:",
			"test_json_mig_1",
		}

		outStr := string(out)

		assertHasOrderedSequence(t, expectedSequences, outStr)
	})

	t.Run("Test Post Basic Migration Info", func(t *testing.T) {
		cmd := exec.Command(cmdPath, "--driver", cnf.Driver(), "--generator", cnf.Generator(), "--url", cnf.DriverUrl(), "info")
		out, err := cmd.CombinedOutput()
		if err != nil {
			t.Fatalf("Unexpected error: %+v\nOutput: %s", err, out)
		}

		outStr := string(out)
		expectedSequences := []string{
			"Tables:",
			`test_sqlite_`, `.db"│rove_migrate__cmd_log`, `│6 `,
			`test_sqlite_`, `.db"│rove_migrate__migs`, `│6 `,
			`test_sqlite_`, `.db"│rove_migrate__runs`, `│7 `,
			`test_sqlite_`, `.db"│users `, `│4 `,

			`test_sqlite_`, `.db".rove_migrate__cmd_log`,
			"ctime",
			"id",
			"migration_id",
			"run_id",
			"sql_cmd",
			"utime",

			`test_sqlite_`, `.db".rove_migrate__migs`,
			"ctime",
			"error",
			"id",
			"state",
			"utime",

			`test_sqlite_`, `.db".rove_migrate__runs`,
			"ctime",
			"error",
			"id",
			"is_rollback",
			"migration_id",
			"success",
			"utime",

			`test_sqlite_`, `.db".users`,
			"active",
			"email",
			"full_name",
			"id",

			"Indexes:",
			"rove_migrate__migs",
			"user_index", "users",

			"Triggers:",
			"rove_migrate__update_utime_cmd",
			"rove_migrate__update_utime_migs",
			"rove_migrate__update_utime_runs",

			"Migrations:",
			"test_json_mig_1│SUCCESS│     │1 ",

			"Migration Runs:",
			"1 ", "test_json_mig_1", "NO", "YES",

			"Migration Commands:",
			"CREATE TABLE IF NOT EXISTS user",
			"ALTER TABLE user RENAME COLUMN name ",
			"CREATE UNIQUE INDEX IF NOT EXISTS user_ind",
		}

		assertHasOrderedSequence(t, expectedSequences, outStr)
	})

	t.Run("Test Rollback", func(t *testing.T) {
		migDir, err := os.MkdirTemp(dir, "test_migrations")
		if err != nil {
			t.Fatal(err)
		}
		defer os.RemoveAll(migDir)

		if err := writeJsonMigFiles(migDir); err != nil {
			t.Fatal(err)
		}

		cmd := exec.Command(cmdPath, "--driver", cnf.Driver(), "--generator", cnf.Generator(), "--url", cnf.DriverUrl(), "rollback", "--migs", migDir)
		out, err := cmd.CombinedOutput()
		if err != nil {
			t.Fatalf("Unexpected error: %+v\nOutput: %s", err, out)
		}

		expectedSequences := []string{
			"Migrations Rolled Back:",
			"test_json_mig_1",
		}

		outStr := string(out)

		assertHasOrderedSequence(t, expectedSequences, outStr)
	})

	t.Run("Test Post Basic Rollback Info", func(t *testing.T) {
		cmd := exec.Command(cmdPath, "--driver", cnf.Driver(), "--generator", cnf.Generator(), "--url", cnf.DriverUrl(), "info")
		out, err := cmd.CombinedOutput()
		if err != nil {
			t.Fatalf("Unexpected error: %+v\nOutput: %s", err, out)
		}

		outStr := string(out)
		expectedSequences := []string{
			"Tables:",
			`test_sqlite_`, `.db"│rove_migrate__cmd_log`, `│6 `,
			`test_sqlite_`, `.db"│rove_migrate__migs`, `│6 `,
			`test_sqlite_`, `.db"│rove_migrate__runs`, `│7 `,

			`test_sqlite_`, `.db".rove_migrate__cmd_log`,
			"ctime",
			"id",
			"migration_id",
			"run_id",
			"sql_cmd",
			"utime",

			`test_sqlite_`, `.db".rove_migrate__migs`,
			"ctime",
			"error",
			"id",
			"state",
			"utime",

			`test_sqlite_`, `.db".rove_migrate__runs`,
			"ctime",
			"error",
			"id",
			"is_rollback",
			"migration_id",
			"success",
			"utime",

			"Indexes:",
			"rove_migrate__migs",

			"Triggers:",
			"rove_migrate__update_utime_cmd",
			"rove_migrate__update_utime_migs",
			"rove_migrate__update_utime_runs",

			"Migrations:",
			"test_json_mig_1│ROLLBACK│     │2 ",

			"Migration Runs:",
			"1 ", "test_json_mig_1", "NO", "YES",
			"2 ", "test_json_mig_1", "YES", "YES",

			"Migration Commands:",
			"CREATE TABLE IF NOT EXISTS user",
			"ALTER TABLE user RENAME COLUMN name ",
			"CREATE UNIQUE INDEX IF NOT EXISTS user_ind",
			"DROP INDEX IF EXISTS user_index",
			"ALTER TABLE users RENAME TO user",
			"DROP TABLE IF EXISTS user",
		}

		assertHasOrderedSequence(t, expectedSequences, outStr)
	})
}

func TestSqliteUrlSqlFiles(t *testing.T) {
	cnf, clean := sqliteUrlConf()
	defer clean()

	dir, err := os.Getwd()
	if err != nil {
		t.Fatal(err)
	}

	cmdPath := filepath.Join(dir, binName)

	t.Run("Init", func(t *testing.T) {
		cmd := exec.Command(cmdPath, "--driver", cnf.Driver(), "--generator", cnf.Generator(), "--url", cnf.DriverUrl(), "init")
		out, err := cmd.CombinedOutput()
		if err != nil {
			t.Fatalf("Unexpected error: %+v\nOutput: %s", err, out)
		}

		outStr := string(out)
		expectedStr := "INIT SUCCESS!\n"

		if outStr != expectedStr {
			t.Errorf("Expected output %#v, received %#v", expectedStr, outStr)
		}
	})

	t.Run("Test Conn", func(t *testing.T) {
		cmd := exec.Command(cmdPath, "--driver", cnf.Driver(), "--generator", cnf.Generator(), "--url", cnf.DriverUrl(), "check-conn")
		out, err := cmd.CombinedOutput()
		if err != nil {
			t.Fatalf("Unexpected error: %+v\nOutput: %s", err, out)
		}

		outStr := string(out)
		expectedStr := "CONNECTION SUCCESS!\n"

		if outStr != expectedStr {
			t.Errorf("Expected output %#v, received %#v", expectedStr, outStr)
		}
	})

	t.Run("Test Blank Info", func(t *testing.T) {
		cmd := exec.Command(cmdPath, "--driver", cnf.Driver(), "--generator", cnf.Generator(), "--url", cnf.DriverUrl(), "info")
		out, err := cmd.CombinedOutput()
		if err != nil {
			t.Fatalf("Unexpected error: %+v\nOutput: %s", err, out)
		}

		outStr := string(out)
		expectedSequences := []string{
			"Tables:",
			`test_sqlite_`, `.db"│rove_migrate__cmd_log`, `│6 `,
			`test_sqlite_`, `.db"│rove_migrate__migs`, `│6 `,
			`test_sqlite_`, `.db"│rove_migrate__runs`, `│7 `,

			`test_sqlite_`, `.db".rove_migrate__cmd_log`,
			"ctime",
			"id",
			"migration_id",
			"run_id",
			"sql_cmd",
			"utime",

			`test_sqlite_`, `.db".rove_migrate__migs`,
			"ctime",
			"error",
			"id",
			"state",
			"utime",

			`test_sqlite_`, `.db".rove_migrate__runs`,
			"ctime",
			"error",
			"id",
			"is_rollback",
			"migration_id",
			"success",
			"utime",

			"Indexes:",
			"rove_migrate__migs",

			"Triggers:",
			"rove_migrate__update_utime_cmd",
			"rove_migrate__update_utime_migs",
			"rove_migrate__update_utime_runs",

			"Migrations:",
			"No information found",

			"Migration Runs:",
			"No information found",

			"Migration Commands:",
			"No information found",
		}

		assertHasOrderedSequence(t, expectedSequences, outStr)
	})

	t.Run("Test Migration", func(t *testing.T) {
		migDir, err := os.MkdirTemp(dir, "test_migrations")
		if err != nil {
			t.Fatal(err)
		}
		defer os.RemoveAll(migDir)

		if err := writeSqliteFileMigFiles(migDir); err != nil {
			t.Fatal(err)
		}

		cmd := exec.Command(cmdPath, "--driver", cnf.Driver(), "--generator", cnf.Generator(), "--url", cnf.DriverUrl(), "migrate", "--migs", migDir)
		out, err := cmd.CombinedOutput()
		if err != nil {
			t.Fatalf("Unexpected error: %+v\nOutput: %s", err, out)
		}

		expectedSequences := []string{
			"Migrations Ran:",
			"test_sql_mig",
		}

		outStr := string(out)

		assertHasOrderedSequence(t, expectedSequences, outStr)
	})

	t.Run("Test Post Basic Migration Info", func(t *testing.T) {
		cmd := exec.Command(cmdPath, "--driver", cnf.Driver(), "--generator", cnf.Generator(), "--url", cnf.DriverUrl(), "info")
		out, err := cmd.CombinedOutput()
		if err != nil {
			t.Fatalf("Unexpected error: %+v\nOutput: %s", err, out)
		}

		outStr := string(out)
		expectedSequences := []string{
			"Tables:",
			`test_sqlite_`, `.db"│rove_migrate__cmd_log`, `│6 `,
			`test_sqlite_`, `.db"│rove_migrate__migs`, `│6 `,
			`test_sqlite_`, `.db"│rove_migrate__runs`, `│7 `,
			`test_sqlite_`, `.db"│user`, `│2 `,

			`test_sqlite_`, `.db".rove_migrate__cmd_log`,
			"ctime",
			"id",
			"migration_id",
			"run_id",
			"sql_cmd",
			"utime",

			`test_sqlite_`, `.db".rove_migrate__migs`,
			"ctime",
			"error",
			"id",
			"state",
			"utime",

			`test_sqlite_`, `.db".rove_migrate__runs`,
			"ctime",
			"error",
			"id",
			"is_rollback",
			"migration_id",
			"success",
			"utime",

			`test_sqlite_`, `.db".user`,
			"id",
			"name",

			"Indexes:",
			"rove_migrate__migs",

			"Triggers:",
			"rove_migrate__update_utime_cmd",
			"rove_migrate__update_utime_migs",
			"rove_migrate__update_utime_runs",

			"Migrations:",
			"test_sql_mig│SUCCESS│     │1 ",

			"Migration Runs:",
			"1 ", "test_sql_mig", "NO", "YES",

			"Migration Commands:",
			"CREATE TABLE IF NOT EXISTS user",
		}

		assertHasOrderedSequence(t, expectedSequences, outStr)
	})

	t.Run("Test Rollback", func(t *testing.T) {
		migDir, err := os.MkdirTemp(dir, "test_migrations")
		if err != nil {
			t.Fatal(err)
		}
		defer os.RemoveAll(migDir)

		if err := writeSqliteFileMigFiles(migDir); err != nil {
			t.Fatal(err)
		}

		cmd := exec.Command(cmdPath, "--driver", cnf.Driver(), "--generator", cnf.Generator(), "--url", cnf.DriverUrl(), "rollback", "--migs", migDir)
		out, err := cmd.CombinedOutput()
		if err != nil {
			t.Fatalf("Unexpected error: %+v\nOutput: %s", err, out)
		}

		expectedSequences := []string{
			"Migrations Rolled Back:",
			"test_sql_mig",
		}

		outStr := string(out)

		assertHasOrderedSequence(t, expectedSequences, outStr)
	})

	t.Run("Test Post Basic Rollback Info", func(t *testing.T) {
		cmd := exec.Command(cmdPath, "--driver", cnf.Driver(), "--generator", cnf.Generator(), "--url", cnf.DriverUrl(), "info")
		out, err := cmd.CombinedOutput()
		if err != nil {
			t.Fatalf("Unexpected error: %+v\nOutput: %s", err, out)
		}

		outStr := string(out)
		expectedSequences := []string{
			"Tables:",
			`test_sqlite_`, `.db"│rove_migrate__cmd_log`, `│6 `,
			`test_sqlite_`, `.db"│rove_migrate__migs`, `│6 `,
			`test_sqlite_`, `.db"│rove_migrate__runs`, `│7 `,

			`test_sqlite_`, `.db".rove_migrate__cmd_log`,
			"ctime",
			"id",
			"migration_id",
			"run_id",
			"sql_cmd",
			"utime",

			`test_sqlite_`, `.db".rove_migrate__migs`,
			"ctime",
			"error",
			"id",
			"state",
			"utime",

			`test_sqlite_`, `.db".rove_migrate__runs`,
			"ctime",
			"error",
			"id",
			"is_rollback",
			"migration_id",
			"success",
			"utime",

			"Indexes:",
			"rove_migrate__migs",

			"Triggers:",
			"rove_migrate__update_utime_cmd",
			"rove_migrate__update_utime_migs",
			"rove_migrate__update_utime_runs",

			"Migrations:",
			"test_sql_mig│ROLLBACK│     │2 ",

			"Migration Runs:",
			"1 ", "test_sql_mig", "NO", "YES",
			"2 ", "test_sql_mig", "YES", "YES",

			"Migration Commands:",
			"CREATE TABLE IF NOT EXISTS user",
			"DROP TABLE IF EXISTS user",
		}

		assertHasOrderedSequence(t, expectedSequences, outStr)
	})
}

func TestSqliteUrlRevertSession(t *testing.T) {
	cnf, clean := sqliteUrlConf()
	defer clean()

	dir, err := os.Getwd()
	if err != nil {
		t.Fatal(err)
	}

	cmdPath := filepath.Join(dir, binName)

	t.Run("Init", func(t *testing.T) {
		cmd := exec.Command(cmdPath, "--driver", cnf.Driver(), "--generator", cnf.Generator(), "--url", cnf.DriverUrl(), "init")
		out, err := cmd.CombinedOutput()
		if err != nil {
			t.Fatalf("Unexpected error: %+v\nOutput: %s", err, out)
		}

		outStr := string(out)
		expectedStr := "INIT SUCCESS!\n"

		if outStr != expectedStr {
			t.Errorf("Expected output %#v, received %#v", expectedStr, outStr)
		}
	})

	t.Run("Test Conn", func(t *testing.T) {
		cmd := exec.Command(cmdPath, "--driver", cnf.Driver(), "--generator", cnf.Generator(), "--url", cnf.DriverUrl(), "check-conn")
		out, err := cmd.CombinedOutput()
		if err != nil {
			t.Fatalf("Unexpected error: %+v\nOutput: %s", err, out)
		}

		outStr := string(out)
		expectedStr := "CONNECTION SUCCESS!\n"

		if outStr != expectedStr {
			t.Errorf("Expected output %#v, received %#v", expectedStr, outStr)
		}
	})

	t.Run("Test Blank Info", func(t *testing.T) {
		cmd := exec.Command(cmdPath, "--driver", cnf.Driver(), "--generator", cnf.Generator(), "--url", cnf.DriverUrl(), "info")
		out, err := cmd.CombinedOutput()
		if err != nil {
			t.Fatalf("Unexpected error: %+v\nOutput: %s", err, out)
		}

		outStr := string(out)
		expectedSequences := []string{
			"Tables:",
			`test_sqlite_`, `.db"│rove_migrate__cmd_log`, `│6 `,
			`test_sqlite_`, `.db"│rove_migrate__migs`, `│6 `,
			`test_sqlite_`, `.db"│rove_migrate__runs`, `│7 `,
			`test_sqlite_`, `.db"│rove_migrate__sessions`, `│4 `,
			`test_sqlite_`, `.db"│rove_migrate__sessions_migs`, `│2 `,
			`test_sqlite_`, `.db"│rove_migrate__version_info`, `│2 `,

			`test_sqlite_`, `.db".rove_migrate__cmd_log`,
			"ctime",
			"id",
			"migration_id",
			"run_id",
			"sql_cmd",
			"utime",

			`test_sqlite_`, `.db".rove_migrate__migs`,
			"ctime",
			"error",
			"id",
			"state",
			"utime",

			`test_sqlite_`, `.db".rove_migrate__runs`,
			"ctime",
			"error",
			"id",
			"is_rollback",
			"migration_id",
			"success",
			"utime",

			"Indexes:",
			"rove_migrate__migs",

			"Triggers:",
			"rove_migrate__update_utime_cmd",
			"rove_migrate__update_utime_migs",
			"rove_migrate__update_utime_runs",

			"Migrations:",
			"No information found",

			"Migration Runs:",
			"No information found",

			"Migration Commands:",
			"No information found",
		}

		assertHasOrderedSequence(t, expectedSequences, outStr)
	})

	t.Run("Test Migration", func(t *testing.T) {
		migDir, err := os.MkdirTemp(dir, "test_migrations")
		if err != nil {
			t.Fatal(err)
		}
		defer os.RemoveAll(migDir)

		if err := writeDepMigFiles(migDir); err != nil {
			t.Fatal(err)
		}

		cmd := exec.Command(cmdPath, "--driver", cnf.Driver(), "--generator", cnf.Generator(), "--url", cnf.DriverUrl(), "migrate", "--migs", migDir, "--target", "test_dep_mig_2")
		out, err := cmd.CombinedOutput()
		if err != nil {
			t.Fatalf("Unexpected error: %+v\nOutput: %s", err, out)
		}

		expectedSequences := []string{
			"Migrations Ran:",
			"test_dep_mig_1",
			"test_dep_mig_2",
		}

		outStr := string(out)

		assertHasOrderedSequence(t, expectedSequences, outStr)
	})

	t.Run("Test Sessions Info Last", func(t *testing.T) {
		cmd := exec.Command(cmdPath, "--driver", cnf.Driver(), "--generator", cnf.Generator(), "--url", cnf.DriverUrl(), "sessions", "--session", "last")
		out, err := cmd.CombinedOutput()
		if err != nil {
			t.Fatalf("Unexpected error: %+v\nOutput: %s", err, out)
		}

		outStr := string(out)
		expectedSequences := []string{
			"Migration Sessions:",
			"1", "NO", "test_dep_mig_1, test_dep_mig_2",
		}

		assertHasOrderedSequence(t, expectedSequences, outStr)
	})

	t.Run("Test Migration Part 2", func(t *testing.T) {
		migDir, err := os.MkdirTemp(dir, "test_migrations")
		if err != nil {
			t.Fatal(err)
		}
		defer os.RemoveAll(migDir)

		if err := writeDepMigFiles(migDir); err != nil {
			t.Fatal(err)
		}

		cmd := exec.Command(cmdPath, "--driver", cnf.Driver(), "--generator", cnf.Generator(), "--url", cnf.DriverUrl(), "migrate", "--migs", migDir)
		out, err := cmd.CombinedOutput()
		if err != nil {
			t.Fatalf("Unexpected error: %+v\nOutput: %s", err, out)
		}

		expectedSequences := []string{
			"Migrations Ran:",
			"test_dep_mig_3",
			"test_dep_mig_4",
		}

		outStr := string(out)

		assertHasOrderedSequence(t, expectedSequences, outStr)
	})

	t.Run("Test Post Basic Migration Info", func(t *testing.T) {
		cmd := exec.Command(cmdPath, "--driver", cnf.Driver(), "--generator", cnf.Generator(), "--url", cnf.DriverUrl(), "info")
		out, err := cmd.CombinedOutput()
		if err != nil {
			t.Fatalf("Unexpected error: %+v\nOutput: %s", err, out)
		}

		outStr := string(out)
		expectedSequences := []string{
			"Tables:",
			`test_sqlite_`, `.db"│rove_migrate__cmd_log`, `│6 `,
			`test_sqlite_`, `.db"│rove_migrate__migs`, `│6 `,
			`test_sqlite_`, `.db"│rove_migrate__runs`, `│7 `,
			`test_sqlite_`, `.db"│rove_migrate__sessions`, `│4 `,
			`test_sqlite_`, `.db"│rove_migrate__sessions_migs`, `│2 `,
			`test_sqlite_`, `.db"│rove_migrate__version_info`, `│2 `,
			`test_sqlite_`, `.db"│user`, `│3 `,

			`test_sqlite_`, `.db".rove_migrate__cmd_log`,
			"ctime",
			"id",
			"migration_id",
			"run_id",
			"sql_cmd",
			"utime",

			`test_sqlite_`, `.db".rove_migrate__migs`,
			"ctime",
			"error",
			"id",
			"state",
			"utime",

			`test_sqlite_`, `.db".rove_migrate__runs`,
			"ctime",
			"error",
			"id",
			"is_rollback",
			"migration_id",
			"success",
			"utime",

			`test_sqlite_`, `.db".user`,
			"id",
			"name",

			"Indexes:",
			"rove_migrate__migs",

			"Triggers:",
			"rove_migrate__update_utime_cmd",
			"rove_migrate__update_utime_migs",
			"rove_migrate__update_utime_runs",
			"rove_migrate__update_utime_sessions",

			"Migration Sessions:",
			"1", "NO",
			"2", "NO",

			"Migrations:",
			"test_dep_mig_1│SUCCESS│     │1 ",
			"test_dep_mig_2│SUCCESS│     │1 ",
			"test_dep_mig_3│SUCCESS│     │1 ",
			"test_dep_mig_4│SUCCESS│     │1 ",

			"Migration Runs:",
			"1 ", "test_dep_mig_1", "NO", "YES",
			"2 ", "test_dep_mig_2", "NO", "YES",
			"3 ", "test_dep_mig_3", "NO", "YES",
			"4 ", "test_dep_mig_4", "NO", "YES",

			"Migration Commands:",
			"CREATE TABLE IF NOT EXISTS user",
			"ALTER TABLE user ADD email TEXT",
		}

		assertHasOrderedSequence(t, expectedSequences, outStr)
	})

	t.Run("Test All Sessions Info", func(t *testing.T) {
		cmd := exec.Command(cmdPath, "--driver", cnf.Driver(), "--generator", cnf.Generator(), "--url", cnf.DriverUrl(), "sessions")
		out, err := cmd.CombinedOutput()
		if err != nil {
			t.Fatalf("Unexpected error: %+v\nOutput: %s", err, out)
		}

		outStr := string(out)
		expectedSequences := []string{
			"Migration Sessions:",
			"1", "NO",
			"2", "NO",
		}

		assertHasOrderedSequence(t, expectedSequences, outStr)
	})

	t.Run("Test Sessions Info Last", func(t *testing.T) {
		cmd := exec.Command(cmdPath, "--driver", cnf.Driver(), "--generator", cnf.Generator(), "--url", cnf.DriverUrl(), "sessions", "--session", "last")
		out, err := cmd.CombinedOutput()
		if err != nil {
			t.Fatalf("Unexpected error: %+v\nOutput: %s", err, out)
		}

		outStr := string(out)
		expectedSequences := []string{
			"Migration Sessions:",
			"2", "NO",
		}

		assertHasOrderedSequence(t, expectedSequences, outStr)
	})

	t.Run("Test Revert Session", func(t *testing.T) {
		migDir, err := os.MkdirTemp(dir, "test_migrations")
		if err != nil {
			t.Fatal(err)
		}
		defer os.RemoveAll(migDir)

		if err := writeDepMigFiles(migDir); err != nil {
			t.Fatal(err)
		}

		cmd := exec.Command(cmdPath, "--driver", cnf.Driver(), "--generator", cnf.Generator(), "--url", cnf.DriverUrl(), "revert-session", "--session", "last", "--migs", migDir)
		out, err := cmd.CombinedOutput()
		if err != nil {
			t.Fatalf("Unexpected error: %+v\nOutput: %s", err, out)
		}

		expectedSequences := []string{
			"Migrations Rolled Back:",
			"test_dep_mig_4",
			"test_dep_mig_3",
		}

		outStr := string(out)

		assertHasOrderedSequence(t, expectedSequences, outStr)
	})

	t.Run("Test All Sessions Info Revert", func(t *testing.T) {
		cmd := exec.Command(cmdPath, "--driver", cnf.Driver(), "--generator", cnf.Generator(), "--url", cnf.DriverUrl(), "sessions")
		out, err := cmd.CombinedOutput()
		if err != nil {
			t.Fatalf("Unexpected error: %+v\nOutput: %s", err, out)
		}

		outStr := string(out)
		expectedSequences := []string{
			"Migration Sessions:",
			"1", "NO",
			"2", "NO",
			"3", "YES",
		}

		assertHasOrderedSequence(t, expectedSequences, outStr)
	})

	t.Run("Test Sessions Info 1", func(t *testing.T) {
		cmd := exec.Command(cmdPath, "--driver", cnf.Driver(), "--generator", cnf.Generator(), "--url", cnf.DriverUrl(), "sessions", "--session", "1")
		out, err := cmd.CombinedOutput()
		if err != nil {
			t.Fatalf("Unexpected error: %+v\nOutput: %s", err, out)
		}

		outStr := string(out)
		expectedSequences := []string{
			"Migration Sessions:",
			"1", "NO", "test_dep_mig_1, test_dep_mig_2",
		}

		assertHasOrderedSequence(t, expectedSequences, outStr)
	})

	t.Run("Test Sessions Info 2", func(t *testing.T) {
		cmd := exec.Command(cmdPath, "--driver", cnf.Driver(), "--generator", cnf.Generator(), "--url", cnf.DriverUrl(), "sessions", "--session", "2")
		out, err := cmd.CombinedOutput()
		if err != nil {
			t.Fatalf("Unexpected error: %+v\nOutput: %s", err, out)
		}

		outStr := string(out)
		expectedSequences := []string{
			"Migration Sessions:",
			"2", "NO",
		}

		assertHasOrderedSequence(t, expectedSequences, outStr)
	})

	t.Run("Test Sessions Info Last Revert", func(t *testing.T) {
		cmd := exec.Command(cmdPath, "--driver", cnf.Driver(), "--generator", cnf.Generator(), "--url", cnf.DriverUrl(), "sessions", "--session", "last")
		out, err := cmd.CombinedOutput()
		if err != nil {
			t.Fatalf("Unexpected error: %+v\nOutput: %s", err, out)
		}

		outStr := string(out)
		expectedSequences := []string{
			"Migration Sessions:",
			"3", "YES",
		}

		assertHasOrderedSequence(t, expectedSequences, outStr)
	})

	t.Run("Test Post Basic Rollback Info", func(t *testing.T) {
		cmd := exec.Command(cmdPath, "--driver", cnf.Driver(), "--generator", cnf.Generator(), "--url", cnf.DriverUrl(), "info")
		out, err := cmd.CombinedOutput()
		if err != nil {
			t.Fatalf("Unexpected error: %+v\nOutput: %s", err, out)
		}

		outStr := string(out)
		expectedSequences := []string{
			"Tables:",
			`test_sqlite_`, `.db"│rove_migrate__cmd_log`, `│6 `,
			`test_sqlite_`, `.db"│rove_migrate__migs`, `│6 `,
			`test_sqlite_`, `.db"│rove_migrate__runs`, `│7 `,
			`test_sqlite_`, `.db"│rove_migrate__sessions`, `│4 `,
			`test_sqlite_`, `.db"│rove_migrate__sessions_migs`, `│2 `,
			`test_sqlite_`, `.db"│rove_migrate__version_info`, `│2 `,

			`test_sqlite_`, `.db".rove_migrate__cmd_log`,
			"ctime",
			"id",
			"migration_id",
			"run_id",
			"sql_cmd",
			"utime",

			`test_sqlite_`, `.db".rove_migrate__migs`,
			"ctime",
			"error",
			"id",
			"state",
			"utime",

			`test_sqlite_`, `.db".rove_migrate__runs`,
			"ctime",
			"error",
			"id",
			"is_rollback",
			"migration_id",
			"success",
			"utime",

			`test_sqlite_`, `.db".user`,
			"id",
			"name",

			"Indexes:",
			"rove_migrate__migs",

			"Triggers:",
			"rove_migrate__update_utime_cmd",
			"rove_migrate__update_utime_migs",
			"rove_migrate__update_utime_runs",
			"rove_migrate__update_utime_sessions",

			"Migration Sessions:",
			"1", "NO", "test_dep_mig_1, test_dep_mig_2",
			"2", "NO", "test_dep_mig_3, test_dep_mig_4",
			"3", "YES", "test_dep_mig_3, test_dep_mig_4",

			"Migrations:",
			"test_dep_mig_1│SUCCESS │     │1 ",
			"test_dep_mig_2│SUCCESS │     │1 ",
			"test_dep_mig_3│ROLLBACK│     │2 ",
			"test_dep_mig_4│ROLLBACK│     │2 ",

			"Migration Runs:",
			"1 ", "test_dep_mig_1", "NO", "YES",
			"2 ", "test_dep_mig_2", "NO", "YES",
			"3 ", "test_dep_mig_3", "NO", "YES",
			"4 ", "test_dep_mig_4", "NO", "YES",
			"5 ", "test_dep_mig_4", "YES", "YES",
			"6 ", "test_dep_mig_3", "YES", "YES",

			"Migration Commands:",
			"CREATE TABLE IF NOT EXISTS user",
			"ALTER TABLE user ADD email TEXT",
		}

		assertHasOrderedSequence(t, expectedSequences, outStr)
	})

	t.Run("Test Revert Session 2", func(t *testing.T) {
		// We can even revert revertted sessions
		migDir, err := os.MkdirTemp(dir, "test_migrations")
		if err != nil {
			t.Fatal(err)
		}
		defer os.RemoveAll(migDir)

		if err := writeDepMigFiles(migDir); err != nil {
			t.Fatal(err)
		}

		cmd := exec.Command(cmdPath, "--driver", cnf.Driver(), "--generator", cnf.Generator(), "--url", cnf.DriverUrl(), "revert-session", "--session", "last", "--migs", migDir)
		out, err := cmd.CombinedOutput()
		if err != nil {
			t.Fatalf("Unexpected error: %+v\nOutput: %s", err, out)
		}

		expectedSequences := []string{
			"Migrations Ran:",
			"test_dep_mig_3",
			"test_dep_mig_4",
		}

		outStr := string(out)

		assertHasOrderedSequence(t, expectedSequences, outStr)
	})

	t.Run("Test Sessions Info Last Revert", func(t *testing.T) {
		cmd := exec.Command(cmdPath, "--driver", cnf.Driver(), "--generator", cnf.Generator(), "--url", cnf.DriverUrl(), "sessions", "--session", "last")
		out, err := cmd.CombinedOutput()
		if err != nil {
			t.Fatalf("Unexpected error: %+v\nOutput: %s", err, out)
		}

		outStr := string(out)
		expectedSequences := []string{
			"Migration Sessions:",
			"4", "NO", "test_dep_mig_3, test_dep_mig_4",
		}

		assertHasOrderedSequence(t, expectedSequences, outStr)
	})
}
