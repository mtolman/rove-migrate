package postgres

import (
	"rove_migrate/common"
	"rove_migrate/conf"
	"rove_migrate/migs"
	"testing"
)

func TestParseSqlToml(t *testing.T) {
	toml := `
name = "test"
depsOn = [ "init" ]

[[actions]]
raw = { up = "CREATE TABLE users (id INT)", down = "DROP TABLE users" }

[[actions]]
raw = { up = "ALTER TABLE users ADD COLUMN name TEXT", down = "ALTER TABLE users DROP COLUMN name" }
`
	mig, err := migs.ParseMigration(toml, "toml")
	if err != nil {
		t.Fatalf("Received unexpected error %v", err)
	}

	cnf, err := common.NewConf(conf.WithDriver("postgres"), conf.WithGenerator("postgres"), conf.WithUrl("postgres://localhost"))
	if err != nil {
		t.Fatalf("Received unexpected error %v", err)
	}

	ups, err := mig.Up(cnf)
	if err != nil {
		t.Fatalf("Received unexpected error %v", err)
	}

	if len(ups) != 2 {
		t.Errorf("Expected len 2, received %d", len(ups))
	}

	expected := "CREATE TABLE users (id INT)"
	if ups[0] != expected {
		t.Errorf("Expected %#v, received %#v", expected, ups[0])
	}

	expected = "ALTER TABLE users ADD COLUMN name TEXT"
	if ups[1] != expected {
		t.Errorf("Expected %#v, received %#v", expected, ups[0])
	}
}

func TestParseSql(t *testing.T) {
	json := `{
	"name": "test",
	"depsOn": ["init"],
	"actions": [
		{
			"raw": {
				"up": "CREATE TABLE users (id INT)",
				"down": "DROP TABLE users"
			}
		}
	]
}`
	mig, err := migs.ParseMigration(json, "json")
	if err != nil {
		t.Fatalf("Received unexpected error %v", err)
	}

	cnf, err := common.NewConf(conf.WithDriver("postgres"), conf.WithGenerator("postgres"), conf.WithUrl("postgres://localhost"))
	if err != nil {
		t.Fatalf("Received unexpected error %v", err)
	}

	ups, err := mig.Up(cnf)
	if err != nil {
		t.Fatalf("Received unexpected error %v", err)
	}

	if len(ups) != 1 {
		t.Errorf("Expected len 1, received %d", len(ups))
	}

	expected := "CREATE TABLE users (id INT)"
	if ups[0] != expected {
		t.Errorf("Expected %#v, received %#v", expected, ups[0])
	}
}

func TestSqlUp(t *testing.T) {
	mig := &common.Migration{
		Name: "test",
		Deps: []common.Dep{},
		Actions: []common.Action{
			common.Action{
				Sql: &common.SqlMig{
					Up:   "CREATE TABLE IF NOT EXISTS test (id INT)",
					Down: "DROP TABLE IF EXISTS test CASCADE",
				},
			},
		},
	}
	cnf, err := common.NewConf(conf.WithDriver("postgres"), conf.WithGenerator("postgres"), conf.WithUrl("postgres://localhost"))
	if err != nil {
		t.Fatalf("Received unexpected error %v", err)
	}

	ups, err := mig.Up(cnf)

	if err != nil {
		t.Fatalf("Received unexpected error %v", err)
	}

	if len(ups) != 1 {
		t.Errorf("Expected len 1, received %d", len(ups))
	}

	expected := mig.Actions[0].Sql.Up
	if ups[0] != expected {
		t.Errorf("Expected %#v, received %#v", ups[0], expected)
	}
}

func TestCreateTable(t *testing.T) {
	gen := postgresGen{}
	tests := []struct {
		name string
		tc   common.TableCreate
		ex   string
	}{
		{
			"Basic Create",
			common.TableCreate{
				Name: "user",
				Columns: []common.Column{
					{Name: "id", AutoIncrement: true, Type: "bigint", NotNull: true},
					{Name: "name", Type: "text", NotNull: false},
				},
				ErrIfExists: false,
				Timestamps:  false,
				PrimaryKey:  []string{"id"},
				ForeignKeys: []common.ForeignKey{},
			},
			`CREATE TABLE IF NOT EXISTS "user" ("id" BIGSERIAL NOT NULL, "name" TEXT NULL, PRIMARY KEY ("id"));`,
		},
		{
			"Timestamp Create",
			common.TableCreate{
				Name: "user",
				Columns: []common.Column{
					{Name: "id", AutoIncrement: true, Type: "bigint", NotNull: true},
					{Name: "name", Type: "text", NotNull: false},
				},
				ErrIfExists: true,
				Timestamps:  true,
				PrimaryKey:  []string{"id"},
				ForeignKeys: []common.ForeignKey{},
			},
			`CREATE TABLE "user" ("id" BIGSERIAL NOT NULL, "name" TEXT NULL, "ctime" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, "utime" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, PRIMARY KEY ("id"));
CREATE OR REPLACE FUNCTION rove_migrate__helpers_update_modified_column()
RETURNS TRIGGER AS $$
BEGIN
	NEW.utime = now();
	RETURN NEW;
END;
$$ LANGUAGE 'plpgsql';
CREATE TRIGGER "user__update_utime_trigger"
AFTER UPDATE ON "user"
FOR EACH ROW EXECUTE PROCEDURE rove_migrate__helpers_update_modified_column();`,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			sql, err := gen.GenCreateTable(test.tc)
			if err != nil {
				t.Fatalf("Unexpected error %#v", err)
			}
			if sql != test.ex {
				t.Errorf("Unexpected SQL.\nExpected %#v\nReceived %#v", test.ex, sql)
			}
		})
	}
}

func TestRevertCreateTable(t *testing.T) {
	gen := postgresGen{}
	tests := []struct {
		name string
		tc   common.TableCreate
		ex   string
	}{
		{
			"Basic Create",
			common.TableCreate{
				Name: "user",
				Columns: []common.Column{
					{Name: "id", AutoIncrement: true, Type: "bigint", NotNull: true},
					{Name: "name", Type: "text", NotNull: false},
				},
				ErrIfExists: false,
				Timestamps:  false,
				PrimaryKey:  []string{"id"},
				ForeignKeys: []common.ForeignKey{},
			},
			`DROP TABLE IF EXISTS "user" CASCADE;`,
		},
		{
			"Timestamp Create",
			common.TableCreate{
				Name: "user",
				Columns: []common.Column{
					{Name: "id", AutoIncrement: true, Type: "bigint", NotNull: true},
					{Name: "name", Type: "text", NotNull: false},
				},
				ErrIfExists: true,
				Timestamps:  true,
				PrimaryKey:  []string{"id"},
				ForeignKeys: []common.ForeignKey{},
			},
			`DROP TRIGGER IF EXISTS "user__update_utime_trigger";
DROP TABLE "user" CASCADE;`,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			sql, err := gen.RevCreateTable(test.tc)
			if err != nil {
				t.Fatalf("Unexpected error %#v", err)
			}
			if sql != test.ex {
				t.Errorf("Unexpected SQL. Expected %#v, received %#v", test.ex, sql)
			}
		})
	}
}

func TestAlterTable(t *testing.T) {
	gen := postgresGen{}
	tests := []struct {
		name string
		tc   common.TableAlter
		ex   string
	}{
		{
			"Basic Alter",
			common.TableAlter{
				Name: "user",
				AddColumns: []common.Column{
					{Name: "id", AutoIncrement: true, Type: "bigint", NotNull: true},
					{Name: "name", Type: "text", NotNull: false},
				},
				RenameColumns: []common.ColRename{
					{From: "email", To: "emailAddress"},
				},
				RenameTo: "users",
			},
			`ALTER TABLE "user" RENAME COLUMN "email" TO "emailAddress";
ALTER TABLE "user" ADD COLUMN "id" BIGSERIAL NOT NULL;
ALTER TABLE "user" ADD COLUMN "name" TEXT NULL;
ALTER TABLE "user" RENAME TO "users";
`,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			sql, err := gen.GenAlterTable(test.tc)
			if err != nil {
				t.Fatalf("Unexpected error %#v", err)
			}
			if sql != test.ex {
				t.Errorf("Unexpected SQL. Expected %#v\nReceived %#v", test.ex, sql)
			}
		})
	}
}

func TestRevertAlterTable(t *testing.T) {
	gen := postgresGen{}
	tests := []struct {
		name string
		tc   common.TableAlter
		ex   string
	}{
		{
			"Basic Alter",
			common.TableAlter{
				Name: "user",
				AddColumns: []common.Column{
					{Name: "id", AutoIncrement: true, Type: "bigint", NotNull: true},
					{Name: "name", Type: "text", NotNull: false},
				},
				RenameColumns: []common.ColRename{
					{From: "email", To: "emailAddress"},
				},
				RenameTo: "users",
			},
			`ALTER TABLE "users" RENAME TO "user";
ALTER TABLE "user" DROP COLUMN "id";
ALTER TABLE "user" DROP COLUMN "name";
ALTER TABLE "user" RENAME COLUMN "emailAddress" TO "email";
`,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			sql, err := gen.RevAlterTable(test.tc)
			if err != nil {
				t.Fatalf("Unexpected error %#v", err)
			}
			if sql != test.ex {
				t.Errorf("Unexpected SQL. Expected %#v\nReceived %#v", test.ex, sql)
			}
		})
	}
}

func TestCreateIndex(t *testing.T) {
	gen := postgresGen{}
	tests := []struct {
		name string
		tc   common.IndexCreate
		ex   string
	}{
		{
			"Unique Create",
			common.IndexCreate{
				Name:  "user_index",
				Table: "user",
				Columns: []string{
					"id",
					"name",
				},
				ErrIfExists: false,
				Unique:      true,
				Where:       "",
			},
			`CREATE UNIQUE INDEX IF NOT EXISTS "user_index" ON "user" ("id", "name")`,
		},
		{
			"Constrained Create",
			common.IndexCreate{
				Name:  "user_index",
				Table: "user",
				Columns: []string{
					"id",
					"name",
				},
				ErrIfExists: true,
				Unique:      false,
				Where:       "(id > 20)",
			},
			`CREATE INDEX "user_index" ON "user" ("id", "name") WHERE (id > 20)`,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			sql, err := gen.GenCreateIndex(test.tc)
			if err != nil {
				t.Fatalf("Unexpected error %#v", err)
			}
			if sql != test.ex {
				t.Errorf("Unexpected SQL. Expected %#v, received %#v", test.ex, sql)
			}
		})
	}
}

func TestRevertCreateIndex(t *testing.T) {
	gen := postgresGen{}
	tests := []struct {
		name string
		tc   common.IndexCreate
		ex   string
	}{
		{
			"Unique Create",
			common.IndexCreate{
				Name:  "user_index",
				Table: "user",
				Columns: []string{
					"id",
					"name",
				},
				ErrIfExists: false,
				Unique:      true,
				Where:       "",
			},
			`DROP INDEX IF EXISTS "user_index"`,
		},
		{
			"Constrained Create",
			common.IndexCreate{
				Name:  "user_index",
				Table: "user",
				Columns: []string{
					"id",
					"name",
				},
				ErrIfExists: true,
				Unique:      false,
				Where:       "(id > 20)",
			},
			`DROP INDEX "user_index"`,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			sql, err := gen.RevCreateIndex(test.tc)
			if err != nil {
				t.Fatalf("Unexpected error %#v", err)
			}
			if sql != test.ex {
				t.Errorf("Unexpected SQL. Expected %#v, received %#v", test.ex, sql)
			}
		})
	}
}
