package postgres

import (
	"fmt"
	"net/url"
	"rove_migrate/common"
	"rove_migrate/migs"
	"slices"
	"strconv"
	"strings"
	"time"

	_ "github.com/jackc/pgx/v5/stdlib"
	"github.com/jmoiron/sqlx"
	log "github.com/sirupsen/logrus"
)

type postgresGen struct{}

// Registers postgres to the database registry
func init() {
	log.Debug("Registering postgres...")
	common.RegisterRemoteDriver(
		"postgres",
		"pgx",
		postgresActions{},
		PostgresUrlBuilder,
		5432,
		common.WithDriverLimits(` - No local file support (host/port only)
 - Creating database/schema with init requires permissions for user
 - Creating database with init requires access permissions to default database for user (usually with same name as user)
 - Advanced connection options (e.g. libpq pool options) are not exposed
`),
	)
	common.RegisterNativeGenerator(
		"postgres",
		postgresGen{},
		common.WithGeneratorLimits(` - Creates additional functions and triggers for tables with Timestamps
 - Integer printing parameters (e.g. INT(11)) are ignored
 - Some size modifiers (e.g. SMALLTEXT, LONGTEXT) are ignored
 - Binary and Blob types map to bytea
 - Bit and Boolean types map to Boolean
 - DATETIME maps to Timestamp
 - Timezones for TIMESTAMP and TIME are not supported
 - Year type maps to INT`),
	)
}

func (p postgresGen) Generate(generation, variance string, args any) (string, error) {
	return common.GenerateNative(p, generation, variance, args)
}

// SQL generation for creating an index
func (p postgresGen) GenCreateIndex(ic common.IndexCreate) (string, error) {
	builder := strings.Builder{}
	if len(ic.Columns) == 0 {
		return "", fmt.Errorf("Cannot create index. Must specify columns to be indexed")
	}

	builder.WriteString("CREATE ")
	if ic.Unique {
		builder.WriteString("UNIQUE ")
	}
	builder.WriteString("INDEX ")

	if !ic.ErrIfExists {
		builder.WriteString("IF NOT EXISTS ")
	}

	if ic.Schema != "" {
		builder.WriteString("\"")
		builder.WriteString(ic.Schema)
		builder.WriteString("\".")
	}

	builder.WriteString("\"")
	builder.WriteString(ic.Name)
	builder.WriteString("\" ON \"")
	builder.WriteString(ic.Table)
	builder.WriteString("\" (")

	for i, index := range ic.Columns {
		if i != 0 {
			builder.WriteString(", ")
		}
		builder.WriteString("\"")
		builder.WriteString(index)
		builder.WriteString("\"")
	}
	builder.WriteString(")")

	if ic.Where != "" {
		builder.WriteString(" WHERE ")
		builder.WriteString(ic.Where)
	}

	return builder.String(), nil
}

// SQL generation for rolling back a create index
func (p postgresGen) RevCreateIndex(ic common.IndexCreate) (string, error) {
	builder := strings.Builder{}
	if len(ic.Columns) == 0 {
		return "", fmt.Errorf("Cannot generate rollback. Invalid index. Must specify columns to be indexed")
	}

	builder.WriteString("DROP INDEX ")
	if !ic.ErrIfExists {
		builder.WriteString("IF EXISTS ")
	}
	builder.WriteString("\"")
	builder.WriteString(ic.Name)
	builder.WriteString("\"")

	return builder.String(), nil
}

// SQL generation for rolling back a table create
func (p postgresGen) RevCreateTable(tc common.TableCreate) (string, error) {
	builder := strings.Builder{}

	if tc.Timestamps {
		builder.WriteString("DROP TRIGGER IF EXISTS \"")
		builder.WriteString(tc.Name)
		builder.WriteString("__update_utime_trigger\";\n")
	}

	builder.WriteString("DROP TABLE ")
	if !tc.ErrIfExists {
		builder.WriteString("IF EXISTS ")
	}
	if tc.Schema != "" {
		builder.WriteString("\"")
		builder.WriteString(tc.Schema)
		builder.WriteString("\".")
	}

	builder.WriteString("\"")
	builder.WriteString(tc.Name)
	builder.WriteString("\" CASCADE;")
	return builder.String(), nil
}

// SQL generation for creating a table
func (p postgresGen) GenCreateTable(tc common.TableCreate) (string, error) {
	builder := strings.Builder{}
	builder.WriteString("CREATE TABLE ")
	if !tc.ErrIfExists {
		builder.WriteString("IF NOT EXISTS ")
	}

	if tc.Schema != "" {
		builder.WriteString("\"")
		builder.WriteString(tc.Schema)
		builder.WriteString("\".")
	}

	builder.WriteString("\"")
	builder.WriteString(tc.Name)
	builder.WriteString("\" (")

	for i, col := range tc.Columns {
		t, err := migs.ToSqlFieldType(col.Type)
		if err != nil {
			return "", fmt.Errorf("Cannot generate migration SQL. %v", err)
		}
		if i != 0 {
			builder.WriteString(", ")
		}
		builder.WriteString("\"")
		builder.WriteString(col.Name)
		builder.WriteString("\" ")
		ts, err := p.typeStr(t, col.AutoIncrement)
		if err != nil {
			return "", fmt.Errorf("Cannot generate migration SQL. %v", err)
		}
		builder.WriteString(ts)

		if col.NotNull || col.AutoIncrement {
			builder.WriteString(" NOT NULL")
		} else {
			builder.WriteString(" NULL")
		}

		if col.DefaultValue != nil {
			builder.WriteString(" DEFAULT ")
			switch v := col.DefaultValue.(type) {
			case string:
				log.Trace("Default value of type string found, writing it out as is")
				builder.WriteString(v)
			case int:
				log.Trace("Default value of type int found. Converting to base 10 string")
				builder.WriteString(strconv.FormatInt(int64(v), 10))
			case int64:
				log.Trace("Default value of type int64 found. Converting to base 10 string")
				builder.WriteString(strconv.FormatInt(int64(v), 10))
			case uint:
				log.Trace("Default value of type uint found. Converting to base 10 string")
				builder.WriteString(strconv.FormatUint(uint64(v), 10))
			case uint64:
				log.Trace("Default value of type uint64 found. Converting to base 10 string")
				builder.WriteString(strconv.FormatUint(uint64(v), 10))
			case float32:
				log.Trace("Default value of type float32 found. Converting to base 10 string with options 'f', -1, 32")
				builder.WriteString(strconv.FormatFloat(float64(v), 'f', -1, 32))
			case float64:
				log.Trace("Default value of type float64 found. Converting to base 10 string with options 'f', -1, 64")
				builder.WriteString(strconv.FormatFloat(float64(v), 'f', -1, 64))
			case bool:
				log.Trace("Default value of type bool found. Converting to 'true'/'false' literals")
				if v {
					builder.WriteString("true")
				} else {
					builder.WriteString("false")
				}
			default:
				log.Errorf("Unable to handle default value of type %T. Failing with default value %v", v, v)
				return "", fmt.Errorf("Cannot generate migration SQL. Unable to use default value %#v", col.DefaultValue)
			}
		}
	}

	if tc.Timestamps {
		if len(tc.Columns) > 0 {
			builder.WriteString(", ")
		}
		builder.WriteString("\"ctime\" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, \"utime\" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP")
	}

	if len(tc.PrimaryKey) > 0 {
		builder.WriteString(", PRIMARY KEY (")
		for i, pk := range tc.PrimaryKey {
			if i != 0 {
				builder.WriteString(", ")
			}
			builder.WriteString("\"")
			builder.WriteString(pk)
			builder.WriteString("\"")
		}
		builder.WriteString(")")
	}

	if len(tc.ForeignKeys) > 0 {
		for _, fk := range tc.ForeignKeys {
			builder.WriteString(", CONSTRAINT ")
			builder.WriteString(fk.Name)
			builder.WriteString(" FOREIGN KEY (")
			for i, c := range fk.Keys {
				if i != 0 {
					builder.WriteString(", ")
				}
				builder.WriteString(c)
			}
			builder.WriteString(") REFERENCES ")
			builder.WriteString(fk.ReferenceTable)
			builder.WriteString(" (")
			for i, c := range fk.ReferenceKeys {
				if i != 0 {
					builder.WriteString(", ")
				}
				builder.WriteString(c)
			}
			builder.WriteString(")")
		}
	}

	builder.WriteString(");")

	if tc.Timestamps {
		builder.WriteString(`
CREATE OR REPLACE FUNCTION rove_migrate__helpers_update_modified_column()
RETURNS TRIGGER AS $$
BEGIN
	NEW.utime = now();
	RETURN NEW;
END;
$$ LANGUAGE 'plpgsql';
`)
		builder.WriteString("CREATE TRIGGER ")
		if !tc.ErrIfExists {
			builder.WriteString("IF NOT EXISTS ")
		}
		builder.WriteString("\"")
		builder.WriteString(tc.Name)
		builder.WriteString(`__update_utime_trigger"
AFTER UPDATE ON "`)
		builder.WriteString(tc.Name)
		builder.WriteString(`"
FOR EACH ROW EXECUTE PROCEDURE rove_migrate__helpers_update_modified_column();`)
	}

	return builder.String(), nil
}

// Converts an SQL field type to a Postgres type string
func (p postgresGen) typeStr(t *migs.SqlFieldType, autoIncrement bool) (string, error) {
	switch t.SqlType {
	case migs.SQL_INT:
		if autoIncrement {
			log.Debug("Interpreting SQL_INT as SERIAL due to autoIncrement")
			return "SERIAL", nil
		} else {
			return "INT", nil
		}
	case migs.SQL_BIGINT:
		if autoIncrement {
			log.Debug("Interpreting SQL_INT as BIGSERIAL due to autoIncrement")
			return "BIGSERIAL", nil
		} else {
			return "BIGINT", nil
		}
	case migs.SQL_TINYINT:
		if autoIncrement {
			log.Debug("Interpreting SQL_INT as SMALSERIAL due to autoIncrement")
			return "SMALLSERIAL", nil
		} else {
			return "SMALLINT", nil
		}
	case migs.SQL_MEDIUMINT:
		log.Warn("Ignoring size of SQL_MEDIUMINT. Not supported for postgres")
		if autoIncrement {
			log.Debug("Interpreting SQL_INT as SMALSERIAL due to autoIncrement")
			return "SERIAL", nil
		} else {
			return "INT", nil
		}
	case migs.SQL_BIT:
		log.Warn("Reinterpreting SQL_BIT as SQL_BOOl")
		return "BOOLEAN", nil
	case migs.SQL_BOOL:
		return "BOOLEAN", nil
	case migs.SQL_FLOAT:
		return "REAL", nil
	case migs.SQL_DOUBLE:
		log.Warn("Reinterpreting SQL_DOUBLE as SQL_DOUBLE_PRECISION")
		return "DOUBLE PRECISION", nil
	case migs.SQL_DOUBLE_PRECISION:
		return "DOUBLE PRECISION", nil
	case migs.SQL_MONEY:
		return "MONEY", nil
	case migs.SQL_NUMERIC:
		str := "NUMERIC"
		suffix := ""
		if t.Size != 0 || t.DecPoint != 0 {
			str += "(" + strconv.FormatInt(int64(t.Size), 10)
			suffix = ")"
		}
		if t.DecPoint != 0 {
			str += ", " + strconv.FormatInt(int64(t.DecPoint), 10)
		}
		str += suffix
		return str, nil
	case migs.SQL_DECIMAL:
		str := "DECIMAL"
		suffix := ""
		if t.Size != 0 || t.DecPoint != 0 {
			str += "(" + strconv.FormatInt(int64(t.Size), 10)
			suffix = ")"
		}
		if t.DecPoint != 0 {
			str += ", " + strconv.FormatInt(int64(t.DecPoint), 10)
		}
		str += suffix
		return str, nil
	case migs.SQL_CHAR:
		if t.Size < 1 {
			t.Size = 1
		}
		return "CHAR(" + strconv.FormatInt(int64(t.Size), 10) + ")", nil
	case migs.SQL_VARCHAR:
		if t.Size < 1 {
			t.Size = 1
		}
		return "VARCHAR(" + strconv.FormatInt(int64(t.Size), 10) + ")", nil
	case migs.SQL_TINYTEXT:
		log.Warn("Ignoring size modifier of TINYTEXT")
		return "TEXT", nil
	case migs.SQL_TEXT:
		return "TEXT", nil
	case migs.SQL_LONGTEXT:
		log.Warn("Ignoring size modifier of LONGTEXT")
		return "TEXT", nil
	case migs.SQL_TINYBLOB:
		log.Warn("Interpreting TINYBLOB as BYTEA")
		return "BYTEA", nil
	case migs.SQL_BLOB:
		log.Warn("Interpreting BLOB as BYTEA")
		return "BYTEA", nil
	case migs.SQL_LONGBLOB:
		log.Warn("Interpreting LONGBLOB as BYTEA")
		return "BYTEA", nil
	case migs.SQL_BINARY:
		return "BYTEA", nil
	case migs.SQL_VARBINARY:
		log.Warn("Interpreting VARBINARY as BYTEA")
		return "BYTEA", nil
	case migs.SQL_DATE:
		return "DATE", nil
	case migs.SQL_DATETIME:
		log.Warn("Interpreting DATETIME as TIMESTAMP")
		return "TIMESTAMP", nil
	case migs.SQL_TIMESTAMP:
		return "TIMESTAMP", nil
	case migs.SQL_TIME:
		return "TIME", nil
	case migs.SQL_YEAR:
		log.Warn("Interpreting YEAR as INT")
		return "INT", nil
	}
	return "", fmt.Errorf("Unmapped field type %#v", t.SqlType)
}

// Generates SQL rollback of alter table information
func (p postgresGen) RevAlterTable(ta common.TableAlter) (string, error) {
	if len(ta.AddColumns) == 0 && len(ta.RenameColumns) == 0 && ta.RenameTo == "" {
		return "", fmt.Errorf("Cannot generate rollback SQL. Alter table must have at least one modification")
	}
	prefixBuilder := strings.Builder{}
	prefixBuilder.WriteString("ALTER TABLE \"")
	prefixBuilder.WriteString(ta.Name)
	prefixBuilder.WriteString("\" ")
	prefix := prefixBuilder.String()
	suffix := ";\n"

	builder := strings.Builder{}

	if ta.RenameTo != "" {
		prefixBuilder := strings.Builder{}
		prefixBuilder.WriteString("ALTER TABLE \"")
		prefixBuilder.WriteString(ta.RenameTo)
		prefixBuilder.WriteString("\" ")
		prefix := prefixBuilder.String()
		builder.WriteString(prefix)
		builder.WriteString("RENAME TO \"")
		builder.WriteString(ta.Name)
		builder.WriteString("\"")
		builder.WriteString(suffix)
	}

	for _, addCol := range ta.AddColumns {
		builder.WriteString(prefix)
		builder.WriteString("DROP COLUMN \"")
		builder.WriteString(addCol.Name)
		builder.WriteString("\"")
		builder.WriteString(suffix)
	}

	for _, renameCol := range ta.RenameColumns {
		builder.WriteString(prefix)
		builder.WriteString("RENAME COLUMN \"")
		builder.WriteString(renameCol.To)
		builder.WriteString("\" TO \"")
		builder.WriteString(renameCol.From)
		builder.WriteString("\"")
		builder.WriteString(suffix)
	}

	return builder.String(), nil
}

// Generates SQL alter table information
func (p postgresGen) GenAlterTable(ta common.TableAlter) (string, error) {
	if len(ta.AddColumns) == 0 && len(ta.RenameColumns) == 0 && ta.RenameTo == "" {
		return "", fmt.Errorf("Cannot generate migration SQL. Alter table must have at least one modification")
	}

	prefixBuilder := strings.Builder{}
	prefixBuilder.WriteString("ALTER TABLE ")

	if ta.Schema != "" {
		prefixBuilder.WriteString("\"")
		prefixBuilder.WriteString(ta.Schema)
		prefixBuilder.WriteString("\".")
	}

	prefixBuilder.WriteString("\"")
	prefixBuilder.WriteString(ta.Name)
	prefixBuilder.WriteString("\" ")
	prefix := prefixBuilder.String()
	suffix := ";\n"

	builder := strings.Builder{}

	for _, renameCol := range ta.RenameColumns {
		builder.WriteString(prefix)
		builder.WriteString("RENAME COLUMN \"")
		builder.WriteString(renameCol.From)
		builder.WriteString("\" TO \"")
		builder.WriteString(renameCol.To)
		builder.WriteString("\"")
		builder.WriteString(suffix)
	}

	for _, col := range ta.AddColumns {
		t, err := migs.ToSqlFieldType(col.Type)
		if err != nil {
			return "", fmt.Errorf("Cannot generate migration SQL. %v", err)
		}
		builder.WriteString(prefix)
		builder.WriteString("ADD COLUMN ")
		builder.WriteString("\"")
		builder.WriteString(col.Name)
		builder.WriteString("\" ")
		ts, err := p.typeStr(t, col.AutoIncrement)
		if err != nil {
			return "", fmt.Errorf("Cannot generate migration SQL. %v", err)
		}
		builder.WriteString(ts)

		if col.NotNull || col.AutoIncrement {
			builder.WriteString(" NOT NULL")
		} else {
			builder.WriteString(" NULL")
		}

		if col.DefaultValue != nil {
			builder.WriteString(" DEFAULT ")
			switch v := col.DefaultValue.(type) {
			case string:
				log.Trace("Default value of type string found, writing it out as is")
				builder.WriteString(v)
			case int:
				log.Trace("Default value of type int found. Converting to base 10 string")
				builder.WriteString(strconv.FormatInt(int64(v), 10))
			case int64:
				log.Trace("Default value of type int64 found. Converting to base 10 string")
				builder.WriteString(strconv.FormatInt(int64(v), 10))
			case uint:
				log.Trace("Default value of type uint found. Converting to base 10 string")
				builder.WriteString(strconv.FormatUint(uint64(v), 10))
			case uint64:
				log.Trace("Default value of type uint64 found. Converting to base 10 string")
				builder.WriteString(strconv.FormatUint(uint64(v), 10))
			case float32:
				log.Trace("Default value of type float32 found. Converting to base 10 string with options 'f', -1, 32")
				builder.WriteString(strconv.FormatFloat(float64(v), 'f', -1, 32))
			case float64:
				log.Trace("Default value of type float64 found. Converting to base 10 string with options 'f', -1, 64")
				builder.WriteString(strconv.FormatFloat(float64(v), 'f', -1, 64))
			case bool:
				log.Trace("Default value of type bool found. Converting to 'true'/'false' literals")
				if v {
					builder.WriteString("true")
				} else {
					builder.WriteString("false")
				}
			default:
				log.Errorf("Unable to handle default value of type %T. Failing with default value %v", v, v)
				return "", fmt.Errorf("Cannot generate migration SQL. Unable to use default value %#v", col.DefaultValue)
			}
		}
		builder.WriteString(suffix)
	}

	if ta.RenameTo != "" {
		builder.WriteString(prefix)
		builder.WriteString("RENAME TO \"")
		builder.WriteString(ta.RenameTo)
		builder.WriteString("\"")
		builder.WriteString(suffix)
	}

	return builder.String(), nil
}

// Creates a URL for connecting to a postgres database
func PostgresUrlBuilder(conf *common.DbConf) (string, error) {
	if conf.Host() == "" {
		return "", fmt.Errorf("Cannot generate postgres URL. No host specified for Postres DB")
	}

	redactedBuilder := strings.Builder{}
	urlBuilder := strings.Builder{}

	redactedBuilder.WriteString("postgres://")
	urlBuilder.WriteString("postgres://")

	if conf.User() != "" {
		if len(conf.User()) > 3 {
			redactedBuilder.WriteString(conf.User()[:2] + strings.Repeat("*", 8))
		} else {
			redactedBuilder.WriteString(strings.Repeat("*", 8))
		}
		urlBuilder.WriteString(conf.User())
		if conf.Pwd() != "" {
			redactedBuilder.WriteString(":")
			urlBuilder.WriteString(":")

			redactedBuilder.WriteString(strings.Repeat("*", 8))
			urlBuilder.WriteString(conf.Pwd())
		}
		redactedBuilder.WriteString("@")
		urlBuilder.WriteString("@")
	}

	if conf.Host() == "" {
		redactedBuilder.WriteString("localhost")
		urlBuilder.WriteString("localhost")
	} else {
		redactedBuilder.WriteString(conf.Host())
		urlBuilder.WriteString(conf.Host())
	}

	redactedBuilder.WriteString(":")
	redactedBuilder.WriteString(strconv.Itoa(int(conf.Port())))

	urlBuilder.WriteString(":")
	urlBuilder.WriteString(strconv.Itoa(int(conf.Port())))

	redactedBuilder.WriteString("/")
	urlBuilder.WriteString("/")

	if conf.Database() != "" {
		redactedBuilder.WriteString(conf.Database())
		urlBuilder.WriteString(conf.Database())
	}

	if conf.Schema() != "" {
		opts := "?options=" + url.QueryEscape("-csearch_path="+conf.Schema())
		redactedBuilder.WriteString(opts)
		urlBuilder.WriteString(opts)
	}

	log.Tracef("Connection URL: %v", redactedBuilder.String())

	return urlBuilder.String(), nil
}

type postgresActions struct{}

// Gets integrity hash information
func (pa postgresActions) GetHashes(s *sqlx.DB, cnf *common.DbConf) ([]common.MigrationHashCheck, error) {
	return migs.SqlGetHashes(s, cnf)
}

// Ensures postgres database exists
func (pa postgresActions) EnsureDatabase(database string, s *sqlx.DB) error {
	createQuery := `CREATE DATABASE ` + database
	log.Infof("Running %v", createQuery)
	if _, err := s.Exec(createQuery); err != nil {
		if strings.Contains(err.Error(), `"`+database+`" already exists`) {
			return nil
		}
		return fmt.Errorf("Cannot create postgres database %#v. %v", database, err)
	}

	return nil
}

// Ensures postgres schema exists
func (pa postgresActions) EnsureSchema(schemaName string, s *sqlx.DB) error {
	createQuery := `CREATE SCHEMA IF NOT EXISTS ` + schemaName
	log.Infof("Running %v", createQuery)
	if _, err := s.Exec(createQuery); err != nil {
		return fmt.Errorf("Cannot create postgres schema %#v. %v", schemaName, err)
	}

	return nil
}

// Ensures postgres has migration tables setup
func (pa postgresActions) EnsureSetup(s *sqlx.DB, cnf *common.DbConf) error {
	createQuery := `CREATE TABLE IF NOT EXISTS rove_migrate__version_info (
	id VARCHAR(500) NOT NULL,
	version BIGINT NOT NULL,
	PRIMARY KEY(id)
);

INSERT INTO rove_migrate__version_info(id, version) VALUES('migs_schema', 1) ON CONFLICT DO NOTHING;
INSERT INTO rove_migrate__version_info(id, version) VALUES('runs_schema', 1) ON CONFLICT DO NOTHING;
INSERT INTO rove_migrate__version_info(id, version) VALUES('cmd_log_schema', 1) ON CONFLICT DO NOTHING;
INSERT INTO rove_migrate__version_info(id, version) VALUES('session_schema', 1) ON CONFLICT DO NOTHING;
INSERT INTO rove_migrate__version_info(id, version) VALUES('session_migs_schema', 1) ON CONFLICT DO NOTHING;

CREATE TABLE IF NOT EXISTS rove_migrate__migs (
	id VARCHAR(500) NOT NULL,
	state INT NOT NULL DEFAULT 0,
	error TEXT NULL,
	ctime TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	utime TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	hash CHAR(90),
	PRIMARY KEY(id)
);

CREATE OR REPLACE FUNCTION rove_migrate__update_timestamp_column()
RETURNS TRIGGER AS $$
BEGIN
	NEW.utime = now();
	RETURN NEW;
END;
$$ language 'plpgsql';

CREATE OR REPLACE TRIGGER update_rove_migrate__update_utime_migs BEFORE UPDATE
	ON rove_migrate__migs FOR EACH ROW EXECUTE PROCEDURE
	rove_migrate__update_timestamp_column();

CREATE TABLE IF NOT EXISTS rove_migrate__runs (
	id BIGSERIAL NOT NULL,
	ctime TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	utime TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	migration_id VARCHAR(500) NOT NULL,
	is_rollback BOOLEAN NOT NULL,
	error TEXT NULL,
	success BOOLEAN NOT NULL,
	PRIMARY KEY(id),
	CONSTRAINT FK_rove_migrate__run_migration FOREIGN KEY (migration_id)
		REFERENCES rove_migrate__migs(id)
		ON DELETE CASCADE
		ON UPDATE CASCADE
);

CREATE INDEX IF NOT EXISTS rove_migrate__runs_idx_migration_id ON
	rove_migrate__runs(migration_id);

CREATE OR REPLACE TRIGGER update_rove_migrate__update_utime_runs BEFORE UPDATE
	ON rove_migrate__runs FOR EACH ROW EXECUTE PROCEDURE
	rove_migrate__update_timestamp_column();

CREATE TABLE IF NOT EXISTS rove_migrate__cmd_log (
	id BIGSERIAL NOT NULL,
	run_id BIGINT NOT NULL,
	migration_id VARCHAR(500) NOT NULL,
	sql_cmd TEXT NOT NULL,
	ctime TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	utime TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY (id),
	CONSTRAINT FK_rove_migrate__runs_migration FOREIGN KEY (migration_id)
		REFERENCES rove_migrate__migs(id)
		ON DELETE CASCADE
		ON UPDATE CASCADE,
	CONSTRAINT FK_rove_migrate__cmd_run FOREIGN KEY (run_id)
		REFERENCES rove_migrate__runs(id)
		ON DELETE CASCADE
		ON UPDATE CASCADE
);

CREATE INDEX IF NOT EXISTS rove_migrate__cmd_log_idx_migration_id ON
	rove_migrate__cmd_log(migration_id);

CREATE INDEX IF NOT EXISTS rove_migrate__cmd_log_idx_run_id ON
	rove_migrate__cmd_log(run_id);

CREATE OR REPLACE TRIGGER update_rove_migrate__update_utime_cmd BEFORE UPDATE
	ON rove_migrate__cmd_log FOR EACH ROW EXECUTE PROCEDURE
	rove_migrate__update_timestamp_column();

CREATE TABLE IF NOT EXISTS rove_migrate__sessions (
	id BIGSERIAL NOT NULL,
	ctime TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	utime TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	is_rollback BOOLEAN NOT NULL,
	PRIMARY KEY (id)
);

CREATE OR REPLACE TRIGGER update_rove_migrate__update_utime_sessions BEFORE UPDATE
	ON rove_migrate__sessions FOR EACH ROW EXECUTE PROCEDURE
	rove_migrate__update_timestamp_column();

CREATE TABLE IF NOT EXISTS rove_migrate__sessions_migs (
	session_id BIGINT NOT NULL,
	migration_id VARCHAR(500) NOT NULL,
	CONSTRAINT FK_rove_migrate__sessions_session_id FOREIGN KEY (session_id)
		REFERENCES rove_migrate__sessions(id)
		ON DELETE CASCADE
		ON UPDATE CASCADE,
	CONSTRAINT FK_rove_migrate__sessions_mig_id FOREIGN KEY (migration_id)
		REFERENCES rove_migrate__migs(id)
		ON DELETE CASCADE
		ON UPDATE CASCADE
);

CREATE INDEX IF NOT EXISTS rove_migrate__session_migs_idx_session_id ON
	rove_migrate__sessions_migs(session_id);

CREATE INDEX IF NOT EXISTS rove_migrate__session_migs_idx_migration_id ON
	rove_migrate__sessions_migs(migration_id);`
	log.Info("Creating migration tables")

	if _, err := s.Exec(createQuery); err != nil {
		return fmt.Errorf("Cannot create migration tables. %v", err)
	}

	return nil
}

// Gets database information
func (pa postgresActions) GetInfo(s *sqlx.DB, cnf *common.DbConf) (*common.Info, error) {
	var query string
	var args []any

	if cnf.Schema() == "" {
		query = `SELECT table_name, table_schema
FROM information_schema.tables
WHERE table_type = 'BASE TABLE' AND table_schema NOT IN ('pg_catalog', 'information_schema');`
		args = []any{}
	} else {
		query = `SELECT DISTINCT table_name, table_schema
FROM information_schema.tables
WHERE table_type = 'BASE TABLE' AND table_schema = $1;`
		args = []any{cnf.Schema()}
	}
	colQuery := `SELECT column_name, data_type FROM information_schema.columns
WHERE table_name = $1 AND table_schema = $2`

	rows, err := s.Query(query, args...)
	if err != nil {
		return nil, fmt.Errorf("Cannot get table information. %v", err)
	}
	defer rows.Close()

	var tables []common.TableInfo
	for rows.Next() {
		var tableName, tableSchema string
		if err := rows.Scan(&tableName, &tableSchema); err != nil {
			return nil, fmt.Errorf("Cannot get table information. %v", err)
		}

		cols, err := s.Query(colQuery, tableName, tableSchema)
		if err != nil {
			return nil, fmt.Errorf("Cannot get table information. %v", err)
		}
		defer cols.Close()

		tableCols := []common.ColumnInfo{}
		for cols.Next() {
			var colName, colType string
			if err := cols.Scan(&colName, &colType); err != nil {
				return nil, fmt.Errorf("Cannot get table column information. %v", err)
			}
			tableCols = append(tableCols, common.ColumnInfo{
				Name:    colName,
				ColType: colType,
			})

		}

		if err := cols.Err(); err != nil {
			return nil, fmt.Errorf("Cannot get table column information. %v", err)
		}

		tables = append(tables, common.TableInfo{
			Name:   tableName,
			Schema: tableSchema,
			Cols:   tableCols,
		})
		cols.Close()
	}

	if err := rows.Err(); err != nil {
		return nil, fmt.Errorf("Cannot get table information. %v", err)
	}

	migInfo, err := pa.GetMigrationInfo(s, cnf)
	if err != nil {
		migInfo = nil
		if strings.HasSuffix(err.Error(), "\"rove_migrate__migs\" does not exist (SQLSTATE 42P01)") {
		} else if strings.HasSuffix(err.Error(), "\"rove_migrate__runs\" does not exist (SQLSTATE 42P01)") {
		} else if strings.HasSuffix(err.Error(), "\"rove_migrate__cmd_log\" does not exist (SQLSTATE 42P01)") {
		} else {
			return nil, fmt.Errorf("Cannot get migration information. %v", err)
		}
	}

	var indexQuery string
	if cnf.Schema() == "" {
		indexQuery = `SELECT DISTINCT indexname, tablename, schemaname
FROM pg_catalog.pg_indexes
WHERE schemaname NOT IN ('pg_catalog', 'information_schema');`
		args = []any{}
	} else {
		indexQuery = `SELECT DISTINCT indexname, tablename, schemaname
FROM pg_catalog.pg_indexes
WHERE schemaname = $1;`
		args = []any{cnf.Schema()}
	}

	indexes := []common.IndexInfo{}
	indexRows, err := s.Query(indexQuery, args...)
	if err != nil {
		return nil, fmt.Errorf("Cannot get index information. %v", err)
	}
	defer indexRows.Close()

	for indexRows.Next() {
		var indexName, tableName, schemaName string
		if err := indexRows.Scan(&indexName, &tableName, &schemaName); err != nil {
			return nil, fmt.Errorf("Cannot get index information. %v", err)
		}
		indexes = append(indexes, common.IndexInfo{Name: indexName, Table: schemaName + "." + tableName})
	}

	if err := indexRows.Err(); err != nil {
		return nil, fmt.Errorf("Cannot get index information. %v", err)
	}

	var triggerQuery string
	if cnf.Schema() == "" {
		triggerQuery = `SELECT DISTINCT trigger_name, event_object_table, event_object_schema
FROM information_schema.triggers
WHERE event_object_schema NOT IN ('pg_catalog', 'information_schema');`
		args = []any{}
	} else {
		triggerQuery = `SELECT DISTINCT trigger_name, event_object_table, event_object_schema
FROM information_schema.triggers
WHERE event_object_schema = $1;`
		args = []any{cnf.Schema()}
	}

	triggers := []common.TriggerInfo{}
	triggerRows, err := s.Query(triggerQuery, args...)
	if err != nil {
		return nil, fmt.Errorf("Cannot get trigger information. %v", err)
	}
	defer triggerRows.Close()

	for triggerRows.Next() {
		var triggerName, tableName, schemaName string
		if err := triggerRows.Scan(&triggerName, &tableName, &schemaName); err != nil {
			return nil, fmt.Errorf("Cannot get trigger information. %v", err)
		}
		triggers = append(triggers, common.TriggerInfo{Name: triggerName, Table: schemaName + "." + tableName})
	}

	if err := triggerRows.Err(); err != nil {
		return nil, fmt.Errorf("Cannot get trigger information. %v", err)
	}

	versionInfo := map[string]string{}
	versionRows, err := s.Query("SELECT id, version FROM rove_migrate__version_info")
	if err != nil {
		return nil, err
	}
	defer versionRows.Close()

	for versionRows.Next() {
		var key, ver string
		if err := versionRows.Scan(&key, &ver); err != nil {
			return nil, fmt.Errorf("Cannot get version information. %v", err)
		}
		versionInfo[key] = ver
	}

	if err := versionRows.Err(); err != nil {
		return nil, fmt.Errorf("Cannot get version information. %v", err)
	}

	sessionInfo := map[int64]*common.SessionInfo{}
	sessionRows, err := s.Query("SELECT s.id, s.ctime, s.is_rollback, m.migration_id FROM rove_migrate__sessions s INNER JOIN rove_migrate__sessions_migs m ON s.id = m.session_id ORDER BY s.id, m.migration_id ASC")
	if err != nil {
		return nil, err
	}
	defer sessionRows.Close()

	for sessionRows.Next() {
		var migrationId string
		var sessionId int64
		var ctime time.Time
		var isRollback bool
		if err := sessionRows.Scan(&sessionId, &ctime, &isRollback, &migrationId); err != nil {
			return nil, fmt.Errorf("Cannot get session information. %v", err)
		}
		if _, ok := sessionInfo[sessionId]; !ok {
			sessionInfo[sessionId] = &common.SessionInfo{Id: sessionId, IsRollback: isRollback, Ctime: ctime, Migrations: []string{}}
		}
		sessionInfo[sessionId].Migrations = append(sessionInfo[sessionId].Migrations, migrationId)
	}

	if err := sessionRows.Err(); err != nil {
		return nil, fmt.Errorf("Cannot get session information. %v", err)
	}

	sessions := []common.SessionInfo{}
	for _, s := range sessionInfo {
		sessions = append(sessions, *s)
	}
	slices.SortFunc(sessions, func(a, b common.SessionInfo) int {
		if a.Id < b.Id {
			return -1
		} else if a.Id > b.Id {
			return 1
		} else {
			return 0
		}
	})

	return &common.Info{
		Tables:      tables,
		Migrations:  migInfo,
		Indexes:     indexes,
		Triggers:    triggers,
		VersionInfo: versionInfo,
		Sessions:    sessions,
	}, nil
}

// Gets information about migrations
func (pa postgresActions) GetMigrationInfo(s *sqlx.DB, cnf *common.DbConf) (*common.MigrationOverview, error) {
	return migs.SqlMigrationInfo(s, cnf)
}

// Runs a migration
func (pa postgresActions) RunMigration(migration *common.Migration, cmds []string, s *sqlx.DB, cnf *common.DbConf) (err error) {
	return migs.SqlRunMigration(pa, migration, cmds, s, cnf)
}

// Rolls back a migration
func (pa postgresActions) RollbackMigration(migration *common.Migration, cmds []string, s *sqlx.DB, cnf *common.DbConf) error {
	return migs.SqlRollbackMigration(pa, migration, cmds, s, cnf)
}

// Gets migration information with a state
func (pa postgresActions) GetMigrated(s *sqlx.DB, cnf *common.DbConf, state int) ([]string, error) {
	return migs.SqlGetMigrated(s, cnf, state)
}

// Updates run information
func (pa postgresActions) UpdateRunInfo(runId int64, run *common.Run, tx *sqlx.Tx) error {
	return migs.SqlUpdateRunInfo(runId, run, tx)
}

// Creates run information record
func (pa postgresActions) CreateRunInfo(run *common.Run, tx *sqlx.Tx, cnf *common.DbConf) (int64, error) {
	execQuery := `INSERT INTO rove_migrate__runs
(migration_id, is_rollback, success, error)
VALUES ($1, $2, $3, $4) RETURNING id`
	res := tx.QueryRow(execQuery, run.Migration, run.IsRollback, run.Success, run.Error)
	var id int64
	if err := res.Scan(&id); err != nil {
		return 0, fmt.Errorf("Failed creating migration run record for migration %+v.", run.Migration)
	}
	return id, nil
}

// Saves command log information
func (pa postgresActions) SaveCmdInfo(cmd string, tx *sqlx.Tx, runId int64, migration *common.Migration) error {
	return migs.SqlSaveCmdInfo(cmd, migration, tx, runId)
}

// Saves migration summary information
func (pa postgresActions) SaveMigInfo(mig *common.Migration, migState int, err error, tx *sqlx.Tx, cnf *common.DbConf) error {
	return migs.SqlSaveMigInfo(mig, migState, tx, cnf, err)
}

func (pa postgresActions) Assert(cmd string, tx *sqlx.Tx, cnf *common.DbConf) error {
	return migs.SqlAssert(cmd, tx, cnf)
}

func (pa postgresActions) CreateSession(isRollback bool, s *sqlx.DB, cnf *common.DbConf) (int64, error) {
	exec := `INSERT INTO rove_migrate__sessions
(is_rollback) VALUES ($1) RETURNING id`
	res := s.QueryRow(exec, isRollback)

	var id int64
	if err := res.Scan(&id); err != nil {
		return 0, err
	}
	return id, nil
}

func (pa postgresActions) SaveMigrationToSession(sessionId int64, mig *common.Migration, s *sqlx.DB, cnf *common.DbConf) error {
	exec := `INSERT INTO rove_migrate__sessions_migs
(session_id, migration_id) VALUES ($1, $2)`
	_, err := s.Exec(exec, sessionId, mig.Name)
	if err != nil {
		return err
	}
	return nil
}

func (pa postgresActions) GetSessionInfo(sessionId int64, s *sqlx.DB, cnf *common.DbConf) (*common.SessionInfo, error) {
	return migs.SqlGetSessionInfo(sessionId, s, cnf)
}

func (pa postgresActions) GetLastSessionInfo(s *sqlx.DB, cnf *common.DbConf) (*common.SessionInfo, error) {
	return migs.SqlGetLastSessionInfo(s, cnf)
}
