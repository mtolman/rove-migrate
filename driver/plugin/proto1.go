package plugin

import (
	"bufio"
	"encoding/base64"
	"encoding/hex"
	"fmt"
	"io"
	"regexp"
	"slices"
	"strconv"
	"strings"
	"time"
)

const (
	p1_token_command = iota
	p1_token_reference
	p1_token_global
	p1_token_typed_Value
	p1_token_label
)

type protocolV1 struct{}

var ProtocolV1 = &protocolV1{}

var p1commands = map[string]*Command{}

var p1globals = map[string]*Global{}

var p1TypedValRegex = "[A-Z]+[:#%][!-~]*"
var cP1TypedValRegex *regexp.Regexp

type proto1Token struct {
	tokenType int8
	segment   string
}

func (pe ProtoError) Proto1Encode() string {
	vals := make([]ProtoVal, 0, len(pe.Vals))

	for _, vs := range pe.Vals {
		protoVal, err := ToProtoVal(vs, nil)
		if err != nil {
			protoVal = &ValString{err.Error()}
		}
		vals = append(vals, protoVal)
	}

	v := &ValArray{elements: vals}
	str, err := ProtocolV1.EncodeVal(v)
	if err != nil {
		str = "A:1 S:INTERNAL_ERROR"
	}
	return "ERR " + str
}

func DefaultProto1Parse(raw []byte) (ProtoVal, error) {
	return &ValBinary{raw}, nil
}

func makeErr(args ...string) error {
	builder := strings.Builder{}
	for _, a := range args {
		builder.WriteString(a)
	}
	return fmt.Errorf("%s", builder.String())
}

func defaultProto1Parsers() map[string]Proto1ValueParser {
	return map[string]Proto1ValueParser{
		"N": func(raw []byte) (ProtoVal, error) {
			if len(raw) != 0 {
				return nil, makeErr("Type 'N' must not have any data", "INVALID_DATA", "N")
			}
			return &ValNil{}, nil
		},
		"I": func(raw []byte) (ProtoVal, error) {
			res, err := strconv.ParseInt(string(raw), 10, 64)
			if err != nil {
				return nil, makeErr(err.Error(), "INVALID_DATA", "I")
			}
			return &ValInt{res}, nil
		},
		"R": func(raw []byte) (ProtoVal, error) {
			res, err := strconv.ParseFloat(string(raw), 64)
			if err != nil {
				return nil, makeErr(err.Error(), "INVALID_DATA", "R")
			}
			return &ValReal{res}, nil
		},
		"F": func(raw []byte) (ProtoVal, error) {
			res, err := strconv.ParseInt(string(raw), 10, 64)
			if err != nil {
				return nil, makeErr(err.Error(), "INVALID_DATA", "F")
			}
			if res == 1 {
				return &ValBool{true}, nil
			} else if res == 0 {
				return &ValBool{false}, nil
			} else {
				return nil, makeErr("Flag must have Value 1 or 0", "INVALID_DATA", "F")
			}
		},
		"S": func(raw []byte) (ProtoVal, error) {
			return &ValString{string(raw)}, nil
		},
		"B": DefaultProto1Parse,
		"U": func(raw []byte) (ProtoVal, error) {
			res, err := strconv.ParseInt(string(raw), 10, 64)
			if err != nil {
				return nil, makeErr(err.Error(), "INVALID_DATA", "U")
			}
			return &ValUnixTimestamp{res}, nil
		},
		"$": func(raw []byte) (ProtoVal, error) {
			return &ValGlobal{string(raw)}, nil
		},
		"@": func(raw []byte) (ProtoVal, error) {
			return &ValRegister{string(raw)}, nil
		},
		"A": func(raw []byte) (ProtoVal, error) {
			res, err := strconv.ParseInt(string(raw), 10, 64)
			if err != nil {
				return nil, makeErr(err.Error(), "INVALID_DATA", "A")
			}
			return &ValArray{elements: make([]ProtoVal, res)}, nil
		},
		"M": func(raw []byte) (ProtoVal, error) {
			res, err := strconv.ParseInt(string(raw), 10, 64)
			if err != nil {
				return nil, makeErr(err.Error(), "INVALID_DATA", "M")
			}
			return &ValMap{keys: make([]ProtoVal, res), values: make([]ProtoVal, res)}, nil
		},
		"D": func(raw []byte) (ProtoVal, error) {
			s := string(raw)
			res, err := time.Parse(time.RFC3339Nano, s)
			if err == nil {
				return ToProtoVal(res, nil)
			}
			res, err = time.Parse(time.RFC3339, s)
			if err == nil {
				return ToProtoVal(res, nil)
			}
			dateTimeNanoNoTx := "2006-01-02T15:04:05.999999999"
			res, err = time.Parse(dateTimeNanoNoTx, s)
			if err == nil {
				return &ValDateTime{
					ValDate: ValDate{
						year:  uint16(res.Year()),
						month: uint8(res.Month()),
						day:   uint8(res.Day()),
					},
					ValTime: ValTime{
						hour:         uint8(res.Hour()),
						minute:       uint8(res.Minute()),
						second:       uint8(res.Second()),
						secondPieces: float64(res.Nanosecond()) / 1e9,
					},
				}, nil
			}
			dateTimeNoTxNano := "2006-01-02T15:04:05"
			res, err = time.Parse(dateTimeNoTxNano, s)
			if err == nil {
				return &ValDateTime{
					ValDate: ValDate{
						year:  uint16(res.Year()),
						month: uint8(res.Month()),
						day:   uint8(res.Day()),
					},
					ValTime: ValTime{
						hour:   uint8(res.Hour()),
						minute: uint8(res.Minute()),
						second: uint8(res.Second()),
					},
				}, nil
			}
			res, err = time.Parse(time.DateOnly, s)
			if err == nil {
				return &ValDate{
					year:  uint16(res.Year()),
					month: uint8(res.Month()),
					day:   uint8(res.Day()),
				}, nil
			}

			timeOnlyNano := "15:04:05.999999999"
			res, err = time.Parse(timeOnlyNano, s)
			if err == nil {
				return &ValTime{
					hour:         uint8(res.Hour()),
					minute:       uint8(res.Minute()),
					second:       uint8(res.Second()),
					secondPieces: float64(res.Nanosecond()) / 1e9,
				}, nil
			}

			res, err = time.Parse(time.TimeOnly, s)
			if err == nil {
				return &ValTime{
					hour:   uint8(res.Hour()),
					minute: uint8(res.Minute()),
					second: uint8(res.Second()),
				}, nil
			}

			return nil, makeErr(err.Error(), "INVALID_DATA", "D")
		},
	}
}

func init() {
	for _, c := range all_commands {
		p1commands[c.proto1] = c
	}

	for _, g := range all_globals {
		p1globals[g.proto1_3] = g
	}

	cP1TypedValRegex = regexp.MustCompile(p1TypedValRegex)
}

func (n *ValNil) EncodeProto1() (string, error) { return ":", nil }

func (i *ValInt) EncodeProto1() (string, error) { return ":" + strconv.FormatInt(i.val, 10), nil }

func (v *ValReal) EncodeProto1() (string, error) {
	return ":" + strconv.FormatFloat(v.val, 'G', -1, 64), nil
}

func (v *ValBool) EncodeProto1() (string, error) {
	if v.val {
		return ":1", nil
	}
	return ":0", nil
}

func (v *ValString) EncodeProto1() (string, error) {
	if m, err := regexp.MatchString("^[!-~]*$", v.val); m && err == nil {
		return ":" + string(v.val), nil
	} else {
		return "#" + base64.StdEncoding.EncodeToString([]byte(v.val)), nil
	}
}

func (v *ValBinary) EncodeProto1() (string, error) {
	return "#" + base64.StdEncoding.EncodeToString(v.val), nil
}

func (v *ValUnixTimestamp) EncodeProto1() (string, error) {
	return ":" + strconv.FormatInt(v.val, 10), nil
}

func (v *ValGlobal) EncodeProto1() (string, error) { return v.refId, nil }

func (v *ValRegister) EncodeProto1() (string, error) { return v.refId, nil }

func (v *ValDate) EncodeProto1() (string, error) {
	builder := strings.Builder{}
	builder.WriteString(":")

	yearStr := strconv.FormatInt(int64(v.year), 10)
	if len(yearStr) < 4 {
		if _, err := builder.WriteString(strings.Repeat("0", 4-len(yearStr))); err != nil {
			return "", err
		}
	}
	if _, err := builder.WriteString(yearStr); err != nil {
		return "", err
	}
	if _, err := builder.WriteRune('-'); err != nil {
		return "", err
	}

	monthStr := strconv.FormatInt(int64(v.month), 10)
	if len(monthStr) < 2 {
		if _, err := builder.WriteRune('0'); err != nil {
			return "", err
		}
	}
	if _, err := builder.WriteString(monthStr); err != nil {
		return "", err
	}
	if _, err := builder.WriteRune('-'); err != nil {
		return "", err
	}

	dayStr := strconv.FormatInt(int64(v.day), 10)
	if len(dayStr) < 2 {
		if _, err := builder.WriteRune('0'); err != nil {
			return "", err
		}
	}
	if _, err := builder.WriteString(dayStr); err != nil {
		return "", err
	}

	return builder.String(), nil
}

func (v *ValTime) EncodeProto1() (string, error) {
	builder := strings.Builder{}
	builder.WriteString(":")

	hourStr := strconv.FormatInt(int64(v.hour), 10)
	if len(hourStr) < 2 {
		if _, err := builder.WriteRune('0'); err != nil {
			return "", err
		}
	}
	if _, err := builder.WriteString(hourStr); err != nil {
		return "", err
	}
	if _, err := builder.WriteRune(':'); err != nil {
		return "", err
	}

	minuteStr := strconv.FormatInt(int64(v.minute), 10)
	if len(minuteStr) < 2 {
		if _, err := builder.WriteRune('0'); err != nil {
			return "", err
		}
	}
	if _, err := builder.WriteString(minuteStr); err != nil {
		return "", err
	}
	if _, err := builder.WriteRune(':'); err != nil {
		return "", err
	}

	secStr := strconv.FormatInt(int64(v.second), 10)
	if len(secStr) < 2 {
		if _, err := builder.WriteRune('0'); err != nil {
			return "", err
		}
	}
	if _, err := builder.WriteString(secStr); err != nil {
		return "", err
	}

	if v.secondPieces != 0 {
		str := []rune(strconv.FormatFloat(v.secondPieces, 'f', -1, 64))
		targetLen := 10
		end := targetLen + 1
		if len(str) < end {
			end = len(str)
		}
		segment := str[1:end]
		if _, err := builder.WriteString(string(segment)); err != nil {
			return "", err
		}
		if len(segment) < targetLen {
			builder.WriteString(strings.Repeat("0", targetLen-len(segment)))
		}
	}

	return builder.String(), nil
}

func (v *ValDateTime) EncodeProto1() (string, error) {
	de, err := v.ValDate.EncodeProto1()
	if err != nil {
		return "", err
	}
	te, err := v.ValTime.EncodeProto1()
	if err != nil {
		return "", err
	}
	return de + "T" + te[1:], nil
}

func (v *valTimezoneOffset) EncodeProto1() (string, error) {
	builder := strings.Builder{}
	if v.hourOffset > 0 {
		if _, err := builder.WriteRune('+'); err != nil {
			return "", err
		}
	} else {
		if _, err := builder.WriteRune('-'); err != nil {
			return "", err
		}
	}
	hr := v.hourOffset
	if hr < 0 {
		hr *= -1
	}

	hourStr := strconv.FormatInt(int64(hr), 10)
	if len(hourStr) < 2 {
		if _, err := builder.WriteRune('0'); err != nil {
			return "", err
		}
	}
	if _, err := builder.WriteString(hourStr); err != nil {
		return "", err
	}
	if _, err := builder.WriteRune(':'); err != nil {
		return "", err
	}

	minStr := strconv.FormatInt(int64(v.minuteOffset), 10)
	if len(minStr) < 2 {
		if _, err := builder.WriteRune('0'); err != nil {
			return "", err
		}
	}
	if _, err := builder.WriteString(minStr); err != nil {
		return "", err
	}
	return builder.String(), nil
}

func (v *ValDateTimeTimezone) EncodeProto1() (string, error) {
	s, err := v.ValDateTime.EncodeProto1()
	if err != nil {
		return "", err
	}

	o, err := v.valTimezoneOffset.EncodeProto1()
	if err != nil {
		return "", err
	}
	builder := strings.Builder{}
	builder.WriteString(s)
	builder.WriteString(o)
	return builder.String(), nil
}

func (v *ValArray) EncodeProto1() (string, error) {
	builder := strings.Builder{}

	builder.WriteString(":")
	builder.WriteString(strconv.FormatInt(int64(len(v.elements)), 10))

	for _, e := range v.elements {
		if _, err := builder.WriteString(" "); err != nil {
			return "", err
		}
		str, err := ProtocolV1.EncodeVal(e)
		if err != nil {
			return "", err
		}
		if _, err := builder.WriteString(str); err != nil {
			return "", err
		}
	}

	return builder.String(), nil
}

func (v *ValMap) EncodeProto1() (string, error) {
	if len(v.keys) != len(v.values) {
		return "", makeErr("Invalid map! Length of keys and Values must match!", "INVALID_DATA", "M")
	}

	builder := strings.Builder{}

	builder.WriteString(":")
	builder.WriteString(strconv.FormatInt(int64(len(v.keys)), 10))

	entries := make([]string, 0, len(v.keys))

	for i := range v.keys {
		b := strings.Builder{}
		str, err := ProtocolV1.EncodeVal(v.keys[i])
		if err != nil {
			return "", err
		}
		if _, err := b.WriteString(str); err != nil {
			return "", err
		}

		if _, err := b.WriteString(" "); err != nil {
			return "", err
		}
		str, err = ProtocolV1.EncodeVal(v.values[i])
		if err != nil {
			return "", err
		}
		if _, err := b.WriteString(str); err != nil {
			return "", err
		}
		entries = append(entries, b.String())
	}

	slices.Sort(entries)

	for _, s := range entries {
		builder.WriteString(" ")
		builder.WriteString(s)
	}

	return builder.String(), nil
}

func (tc *TypeContext) parseProto1(typeStr string, mark string, raw string) (ProtoVal, error) {
	parser, ok := tc.typesByP1[typeStr]
	if !ok {
		if tc.parent != nil {
			return tc.parent.parseProto1(typeStr, mark, raw)
		} else {
			return nil, makeErr(fmt.Sprintf("Invalid type %#v", typeStr), "INVALID_TYPE", typeStr)
		}
	}

	var rawDecoded []byte
	var err error
	switch mark {
	case ":":
		rawDecoded = []byte(raw)
	case "%":
		rawDecoded, err = hex.DecodeString(string(raw))
		if err != nil {
			return nil, makeErr(fmt.Sprintf("Invalid hex! %#v", err), "INVALID_HEX")
		}
	case "#":
		rawDecoded, err = base64.StdEncoding.DecodeString(string(raw))
		if err != nil {
			return nil, makeErr(fmt.Sprintf("Invalid Base64! %#v", err), "INVALID_BASE64")
		}
	}
	return parser.Proto1Parser(rawDecoded)
}

func (p *protocolV1) parseTypedValue(i *int, tokens []proto1Token, tc *TypeContext) (ProtoVal, error) {
	if tokens[*i].tokenType != p1_token_typed_Value {
		return nil, makeErr(fmt.Sprintf("Expected typed Value, received %#v", tokens[*i].tokenType), "ILLEGAL_TOKEN")
	}

	index := strings.IndexAny(tokens[*i].segment, ":#%")
	pType, mark, val := tokens[*i].segment[0:index], tokens[*i].segment[index:index+1], tokens[*i].segment[index+1:]
	res, err := tc.parseProto1(pType, mark, val)
	if err != nil {
		return nil, err
	}

	switch res := res.(type) {
	case *ValArray:
		cnt := 0
		for *i += 1; *i < len(tokens) && cnt < len(res.elements); *i, cnt = *i+1, cnt+1 {
			val, err := p.parseValue(i, tokens, tc)
			if err != nil {
				return nil, err
			}
			res.elements[cnt] = val
		}
		if cnt < len(res.elements) {
			return nil, makeErr(fmt.Sprintf("Expected %#v elements, found %#v", len(res.elements), cnt), "INVALID_DATA", "A")
		}

		// Decrementing since index should end on last token consumed, not next token to consume
		*i -= 1
		return res, nil
	case *ValMap:
		cnt := 0
		for *i += 1; *i+1 < len(tokens) && cnt < len(res.keys); *i, cnt = *i+1, cnt+1 {
			val, err := p.parseValue(i, tokens, tc)
			if err != nil {
				return nil, err
			}
			res.keys[cnt] = val

			*i += 1

			val, err = p.parseValue(i, tokens, tc)
			if err != nil {
				return nil, err
			}

			res.values[cnt] = val
		}
		if cnt < len(res.keys) {
			return nil, makeErr(fmt.Sprintf("Expected %#v elements, found %#v", len(res.keys), cnt), "INVALID_DATA", "M")
		}

		// Decrementing since index should end on last token consumed, not next token to consume
		*i -= 1
		return res, nil
	}
	return res, nil
}

func (p *protocolV1) parseValue(i *int, tokens []proto1Token, tc *TypeContext) (ProtoVal, error) {
	switch tokens[*i].tokenType {
	case p1_token_command:
		return nil, makeErr("Expected Value, found command", "UNEXPECTED_COMMAND", tokens[*i].segment)
	case p1_token_global:
		name := tokens[*i].segment[1:]
		if _, ok := p1globals[name]; !ok {
			return nil, makeErr(fmt.Sprintf("Invalid global $%#v", name), "ILLEGAL_GLOBAL", tokens[*i].segment)
		}
		return &ValGlobal{name}, nil
	case p1_token_reference:
		return &ValRegister{tokens[*i].segment[1:]}, nil
	case p1_token_typed_Value:
		return p.parseTypedValue(i, tokens, tc)
	}
	return nil, makeErr(fmt.Sprintf("Unknown token type %#v", tokens[*i].tokenType), "ILLEGAL_TOKEN")
}

func (p *protocolV1) decodeVals(i *int, tokens []proto1Token, tc *TypeContext) ([]ProtoVal, error) {
	Vals := []ProtoVal{}

	for ; *i < len(tokens); *i++ {
		val, err := p.parseValue(i, tokens, tc)
		if err != nil {
			return nil, err
		}
		Vals = append(Vals, val)
	}

	return Vals, nil
}

func (p *protocolV1) EncodeVal(v ProtoVal) (string, error) {
	typeEncode := v.GetProtoType().Proto1
	ValEncode, err := v.EncodeProto1()
	if err != nil {
		return "", err
	}
	return typeEncode + ValEncode, nil
}

func (p *protocolV1) isLabel(segment []rune) bool {
	return len(segment) > 0 && segment[0] == '%'
}

func (p *protocolV1) tokenizeLine(input io.Reader) ([]proto1Token, error) {
	segmentScan := bufio.NewScanner(input)
	segmentScan.Split(bufio.ScanWords)

	segments := [][]rune{}

	for segmentScan.Scan() {
		segment := segmentScan.Text()
		segments = append(segments, []rune(segment))
	}

	if len(segments) == 0 {
		return nil, nil
	}

	res := []proto1Token{}
	start := 0
	if p.isLabel(segments[start]) {
		res = append(res, proto1Token{p1_token_label, string(segments[start])})
		start++
	}

	if !p.isCommand(string(segments[start])) {
		return nil, makeErr(fmt.Sprintf("Expected a command, recieved '%s' instead", string(segments[0])), "MISSING_COMMAND")
	}
	res = append(res, proto1Token{p1_token_command, string(segments[start])})
	start++

	for _, seg := range segments[start:] {
		s := string(seg)
		switch {
		case p.isReference(s):
			res = append(res, proto1Token{p1_token_reference, s})
		case p.isGlobal(s):
			res = append(res, proto1Token{p1_token_global, s})
		case p.isTypedValue(s):
			res = append(res, proto1Token{p1_token_typed_Value, s})
		default:
			return nil, makeErr(fmt.Sprintf("Expected a @reference, $global, or TYPEd:Value. Received: %s", s), "ILLEGAL_TOKEN")
		}
	}

	return res, nil
}

func (p *protocolV1) parseLine(input io.Reader, tc *TypeContext) (*ProtoCommand, error) {
	tokens, err := p.tokenizeLine(input)
	if err != nil {
		return nil, err
	}

	label := ""
	i := 1
	if tokens[0].tokenType == p1_token_label {
		label = tokens[0].segment[1:]
		i = 2
	}

	if tokens[i-1].tokenType != p1_token_command {
		return nil, makeErr("Expected command", "MISSING_COMMAND")
	}

	commandToken := tokens[i-1]
	cmd, ok := p1commands[commandToken.segment]
	if !ok {
		return nil, makeErr(fmt.Sprintf("Unknown command %#v", commandToken.segment), "INVALID_COMMAND")
	}

	Vals, err := p.decodeVals(&i, tokens, tc)
	if err != nil {
		return nil, err
	}

	command := &ProtoCommand{
		label: label,
		cmd:   cmd,
		args:  Vals,
	}

	numArgs := len(Vals)
	for _, c := range command.cmd.proto2 {
		if numArgs == c.numParams {
			return command, nil
		}
	}
	return nil, makeErr(fmt.Sprintf("Illegal number of args for command %#v", cmd.proto1), "INCORRECT_ARGS")
}

func (p *protocolV1) isCommand(s string) bool {
	_, ok := p1commands[s]
	return ok
}

func (p *protocolV1) isTypedValue(s string) bool {
	return cP1TypedValRegex.MatchString(s)
}

func (p *protocolV1) isReference(s string) bool {
	return s[0] == '@'
}

func (p *protocolV1) isGlobal(s string) bool {
	return s[0] == '$'
}

func (p *protocolV1) ParseResponseLine(line string, ec *SendContext) (*ProtoCommand, error) {
	input := strings.NewReader(line)
	cmd, err := p.parseLine(input, ec.Tc)
	if err != nil {
		return nil, err
	}
	return cmd, nil
}

func (p *protocolV1) SendCommand(command ProtoCommand, out io.Writer, ec *SendContext) error {
	builder := strings.Builder{}
	if command.label != "" {
		builder.WriteString("%")
		builder.WriteString(command.label)
		builder.WriteString(" ")
	}
	builder.WriteString(command.cmd.proto1)
	for _, arg := range command.args {
		builder.WriteString(" ")
		if arg == nil {
			arg = &ValNil{}
		}
		str, err := p.EncodeVal(arg)
		if err != nil {
			return err
		}
		builder.WriteString(str)
	}
	builder.WriteString("\n")
	cmd := builder.String()

	_, err := io.WriteString(out,cmd)
	if err != nil {
		return err
	}
	return nil
}

