package plugin_test

import (
	"context"
	"rove_migrate/common"
	"rove_migrate/conf"
	"rove_migrate/db"
	_ "rove_migrate/driver/plugin"
	"testing"
	"time"
)

func (pluginDB) RunDatascriptTest(t *testing.T, fn func(params)) {
	conf, err := common.NewConf(
		conf.WithDriver("test_plugin/datascript"),
		conf.WithActionsProvider("nil"),
	)

	if err != nil {
		t.Fatal(err)
	}

	conn, err := db.ConnectTo(conf)
	if err != nil {
		t.Fatal(err)
	}
	defer func() {
		if err := conn.Close(); err != nil {
			t.Fatal(err)
		}
	}()

	params := params{pluginTester, t, conn.DB}

	fn(params)
}

func TestDatascriptConn(t *testing.T) {
	pluginTester.RunDatascriptTest(t, testConn)
}

func TestDatascriptExecQuery(t *testing.T) {
	pluginTester.RunDatascriptTest(t, testDatascriptExecQuery)
}

func TestDatascriptPrepareStmtExecQuery(t *testing.T) {
	pluginTester.RunDatascriptTest(t, testDatascriptPrepareStmt)
}

func testDatascriptNoTxSupport(t params) {
	defer traceOut(traceIn("testDatascriptExecQuery"))
	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(5*time.Second))
	defer cancel()

	conn, err := t.Conn(ctx)
	defer func() {
		if err := conn.Close(); err != nil {
			t.Fatal(err)
		}
	}()

	if err != nil {
		t.Fatalf("Unable to get connection. Error: %#v", err)
	}

	_, err = conn.BeginTx(ctx, nil)

	if err == nil {
		t.Errorf("Expected error for creating transaction")
	}
}

func testDatascriptExecQuery(t params) {
	defer traceOut(traceIn("testDatascriptExecQuery"))
	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(5*time.Second))
	defer cancel()

	conn, err := t.Conn(ctx)
	defer func() {
		if err := conn.Close(); err != nil {
			t.Fatal(err)
		}
	}()

	if err != nil {
		t.Fatalf("Unable to get connection. Error: %#v", err)
	}

	res, err := conn.ExecContext(ctx, "[{:db/id -1 :entity/id 12 :entity/name \"john\"} {:db/id -2 :entity/id 23 :entity/name \"bob\"}]")
	if err != nil {
		t.Fatalf("Could not execute query. Error: %#v", err.Error())
	}

	lastInsertId, err := res.LastInsertId()
	if err != nil {
		t.Fatalf("Could not get last insert id. Error: %#v", err)
	}
	if lastInsertId != 1 {
		t.Fatalf("Expected lastInsertId 1, received %v", lastInsertId)
	}

	numRows, err := res.RowsAffected()
	if err != nil {
		t.Fatalf("Could not get count of rows affected. Error: %#v", err)
	}
	if numRows != 2 {
		t.Fatalf("Expected rows affected 2, receivedc %v", lastInsertId)
	}

	queryRes, err := conn.QueryContext(ctx, "[:find ?id ?name :where [?e :entity/id ?id] [?e :entity/name ?name]]")
	if err != nil {
		t.Fatal(err)
	}
	defer func() {
		if err := queryRes.Close(); err != nil {
			t.Fatal(err)
		}
	}()

	count := 0
	for queryRes.Next() {
		count++
		var id int64
		var name string
		if err := queryRes.Scan(&id, &name); err != nil {
			t.Fatal(err)
		}

		if !(id == 12 && name == "john") && !(id == 23 && name == "bob") {
			t.Errorf("Unexpected row %v   %v", id, name)
		}
	}

	if count != 2 {
		t.Errorf("Expected 2 rows, received %v", count)
	}

	var name string
	queryRow := conn.QueryRowContext(ctx, "[:find [?name] :where [?e :entity/id 12] [?e :entity/name ?name]]")
	if err := queryRow.Scan(&name); err != nil {
		t.Fatal(err)
	}
	if !(name == "john") {
		t.Errorf("Unexpected name %v", name)
	}
}

func testDatascriptPrepareStmt(t params) {
	defer traceOut(traceIn("testDatascriptPrepareStmt"))
	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(5000000*time.Second))
	defer cancel()

	conn, err := t.Conn(ctx)
	if err != nil {
		t.Fatalf("Unable to get connection. Error: %#v", err)
	}
	defer func() {
		if err := conn.Close(); err != nil {
			t.Fatal(err)
		}
	}()

	stmtExec, err := conn.PrepareContext(ctx, "[{:db/id -1 :entity/id 12 :entity/name \"john\"} {:db/id -2 :entity/id 23 :entity/name \"bob\"}]")

	if err != nil {
		t.Fatal(err)
	}
	defer func() {
		if err := stmtExec.Close(); err != nil {
			t.Fatal(err)
		}
	}()

	res, err := stmtExec.ExecContext(ctx)
	if err != nil {
		t.Fatalf("Could not execute query. Error: %#v", err.Error())
	}

	lastInsertId, err := res.LastInsertId()
	if err != nil {
		t.Fatalf("Could not get last insert id. Error: %#v", err)
	}
	if lastInsertId != 1 {
		t.Fatalf("Expected lastInsertId 1, received %v", lastInsertId)
	}

	numRows, err := res.RowsAffected()
	if err != nil {
		t.Fatalf("Could not get count of rows affected. Error: %#v", err)
	}
	if numRows != 2 {
		t.Fatalf("Expected rows affected 2, receivedc %v", lastInsertId)
	}

	stmtQuery, err := conn.PrepareContext(ctx, "[:find ?id ?name :where [?e :entity/id ?id] [?e :entity/name ?name]]")

	if err != nil {
		t.Fatal(err)
	}
	defer func() {
		if err := stmtQuery.Close(); err != nil {
			t.Fatal(err)
		}
	}()

	queryRes, err := stmtQuery.Query()
	if err != nil {
		t.Fatal(err)
	}
	defer func() {
		if err := queryRes.Close(); err != nil {
			t.Fatal(err)
		}
	}()

	count := 0
	for queryRes.Next() {
		count++
		var id int64
		var name string
		if err := queryRes.Scan(&id, &name); err != nil {
			t.Fatal(err)
		}

		if !(id == 12 && name == "john") && !(id == 23 && name == "bob") {
			t.Errorf("Unexpected row %v   %v", id, name)
		}
	}

	if count != 2 {
		t.Errorf("Expected 2 rows, received %v", count)
	}

	var name string
	stmtQueryRow, err := conn.PrepareContext(ctx, "[:find [?name] :where [?e :entity/id 12] [?e :entity/name ?name]]")
	queryRow := stmtQueryRow.QueryRowContext(ctx)
	if err := queryRow.Scan(&name); err != nil {
		t.Fatal(err)
	}

	if !(name == "john") {
		t.Errorf("Unexpected name %v", name)
	}
}
