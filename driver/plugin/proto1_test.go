package plugin

import (
	"bytes"
	"context"
	"fmt"
	"strings"
	"testing"
	"time"
)

func TestEncodingProto1(t *testing.T) {
	tests := []struct {
		val      ProtoVal
		expected string
	}{
		{&ValNil{}, "N:"},
		{&ValInt{1}, "I:1"},
		{&ValInt{120391203509720489}, "I:120391203509720489"},
		{&ValInt{-1}, "I:-1"},
		{&ValReal{0}, "R:0"},
		{&ValReal{13.320392093201}, "R:13.320392093201"},
		{&ValReal{9.231232e+100}, "R:9.231232E+100"},
		{&ValReal{-9.231232e-100}, "R:-9.231232E-100"},
		{&ValBool{true}, "F:1"},
		{&ValBool{false}, "F:0"},
		{&ValString{"basic"}, "S:basic"},
		{&ValString{"SELECT * FROM users WHERE id = ?"}, "S#U0VMRUNUICogRlJPTSB1c2VycyBXSEVSRSBpZCA9ID8="},
		{&ValBinary{[]byte("basic")}, "B#YmFzaWM="},
		{&ValBinary{[]byte("SELECT * FROM users WHERE id = ?")}, "B#U0VMRUNUICogRlJPTSB1c2VycyBXSEVSRSBpZCA9ID8="},
		{&ValUnixTimestamp{12309049283}, "U:12309049283"},
		{&ValGlobal{"conf"}, "$conf"},
		{&ValRegister{"res1"}, "@res1"},
		{&ValDate{202, 6, 4}, "D:0202-06-04"},
		{&ValDate{2021, 12, 8}, "D:2021-12-08"},
		{&ValTime{hour: 2, minute: 6, second: 4}, "D:02:06:04"},
		{&ValTime{hour: 2, minute: 6, second: 4, secondPieces: 0.12345678901234}, "D:02:06:04.123456789"},
		{&ValDateTime{ValDate{2020, 4, 3}, ValTime{hour: 2, minute: 6, second: 4, secondPieces: 0.23}}, "D:2020-04-03T02:06:04.230000000"},
		{&ValDateTimeTimezone{
			ValDateTime{
				ValDate{2020, 4, 3},
				ValTime{hour: 2, minute: 6, second: 4, secondPieces: 0.23},
			},
			valTimezoneOffset{-2, 3},
		}, "D:2020-04-03T02:06:04.230000000-02:03"},
		{&ValDateTimeTimezone{
			ValDateTime{
				ValDate{2020, 4, 3},
				ValTime{hour: 2, minute: 6, second: 4},
			},
			valTimezoneOffset{2, 3},
		}, "D:2020-04-03T02:06:04+02:03"},
		{&ValArray{[]ProtoVal{
			&ValInt{2},
			&ValNil{},
			&ValArray{[]ProtoVal{&ValReal{0}}},
			&ValString{"hello"},
			&ValArray{},
		}}, "A:5 I:2 N: A:1 R:0 S:hello A:0"},
		{&ValMap{
			[]ProtoVal{
				&ValString{"test"},
				&ValInt{2},
				&ValNil{},
			},
			[]ProtoVal{
				&ValArray{[]ProtoVal{&ValReal{0}}},
				&ValString{"hello"},
				&ValArray{},
			},
			}, "M:3 I:2 S:hello N: A:0 S:test A:1 R:0"},
	}

	for _, tt := range tests {
		test := tt
		t.Run(fmt.Sprintf("%T %s", test.val, test.expected), func(t *testing.T) {
			actual, err := ProtocolV1.EncodeVal(test.val)
			if err != nil {
				t.Fatalf("Unexpected error %v", err)
			}
			if actual != test.expected {
				t.Errorf("Expected %#v, recieved %#v", test.expected, actual)
			}
		})
	}
}

func TestDecodingProto1(t *testing.T) {
	tests := []struct {
		expected ProtoVal
		input    string
	}{
		{&ValNil{}, "N:"},
		{&ValInt{1}, "I:1"},
		{&ValInt{120391203509720489}, "I:120391203509720489"},
		{&ValInt{-1}, "I:-1"},
		{&ValReal{0}, "R:0"},
		{&ValReal{13.320392093201}, "R:13.320392093201"},
		{&ValReal{9.231232e+100}, "R:9.231232E+100"},
		{&ValReal{-9.231232e-100}, "R:-9.231232E-100"},
		{&ValBool{true}, "F:1"},
		{&ValBool{false}, "F:0"},
		{&ValString{"basic"}, "S:basic"},
		{&ValString{"SELECT * FROM users WHERE id = ?"}, "S#U0VMRUNUICogRlJPTSB1c2VycyBXSEVSRSBpZCA9ID8="},
		{&ValBinary{[]byte("basic")}, "B#YmFzaWM="},
		{&ValBinary{[]byte("SELECT * FROM users WHERE id = ?")}, "B#U0VMRUNUICogRlJPTSB1c2VycyBXSEVSRSBpZCA9ID8="},
		{&ValUnixTimestamp{12309049283}, "U:12309049283"},
		{&ValGlobal{"conf"}, "$conf"},
		{&ValRegister{"res1"}, "@res1"},
		{&ValDate{202, 6, 4}, "D:0202-06-04"},
		{&ValDate{2021, 12, 8}, "D:2021-12-08"},
		{&ValTime{hour: 2, minute: 6, second: 4}, "D:02:06:04"},
		{&ValTime{hour: 2, minute: 6, second: 4, secondPieces: 0.123233433}, "D:02:06:04.123233433"},
		{&ValDateTime{ValDate{2020, 4, 3}, ValTime{hour: 2, minute: 6, second: 4, secondPieces: 0.23}}, "D:2020-04-03T02:06:04.230000"},
		{&ValDateTimeTimezone{
			ValDateTime{
				ValDate{2020, 4, 3},
				ValTime{hour: 2, minute: 6, second: 4, secondPieces: 0.23},
			},
			valTimezoneOffset{-2, 3},
		}, "D:2020-04-03T02:06:04.230000-02:03"},
		{&ValDateTimeTimezone{
			ValDateTime{
				ValDate{2020, 4, 3},
				ValTime{hour: 2, minute: 6, second: 4},
			},
			valTimezoneOffset{2, 3},
		}, "D:2020-04-03T02:06:04+02:03"},
		{&ValArray{[]ProtoVal{
			&ValInt{2},
			&ValNil{},
			&ValArray{[]ProtoVal{&ValReal{0}}},
			&ValString{"hello"},
			&ValArray{},
		}}, "A:5 I:2 N: A:1 R:0 S:hello A:0"},
		{&ValArray{[]ProtoVal{
			&ValInt{2},
			&ValNil{},
			&ValReal{0},
			&ValString{"hello"},
		}}, "A:4 I:2 N: R:0 S:hello"},
		{&ValMap{
			keys: []ProtoVal{
				&ValString{"test"},
				&ValInt{2},
				&ValNil{},
				&ValMap{
					keys: []ProtoVal{
						&ValArray{elements: []ProtoVal{&ValInt{0}}},
						&ValString{"good"},
					},
					values: []ProtoVal{
						&ValInt{2},
						&ValString{"bad"},
					},
				},
			},
			values: []ProtoVal{
				&ValArray{[]ProtoVal{&ValReal{0}}},
				&ValString{"hello"},
				&ValArray{},
				&ValString{"hi"},
			},
		}, "M:4 S:test A:1 R:0 I:2 S:hello N: A:0 M:2 A:1 I:0 I:2 S:good S:bad S:hi"},
	}

	tc := NewTypeContext()

	for _, tt := range tests {
		test := tt
		t.Run(test.input, func(t *testing.T) {
			in := strings.NewReader("VAL " + test.input)
			tokens, err := ProtocolV1.tokenizeLine(in)
			if err != nil {
				t.Fatalf("Unexpected error! %#v", err)
			}

			i := 1
			val, err := ProtocolV1.decodeVals(&i, tokens, tc)

			if err != nil {
				t.Fatalf("Unexpected error! %#v", err)
			}

			if len(val) != 1 {
				t.Fatalf("Expected 1 Value, got %#v", val)
			}

			if !ProtoValDeepEquals(val[0], test.expected) {
				e, _ := ProtocolV1.EncodeVal(test.expected)
				v, _ := ProtocolV1.EncodeVal(val[0])
				t.Errorf("Expected %#v, recieved %#v", e, v)
			}
		})
	}
}

func TestTokenizeProto1(t *testing.T) {
	tests := []struct {
		input    string
		expected []proto1Token
	}{
		{"ACK", []proto1Token{{p1_token_command, "ACK"}}},
		{"%label1 ACK", []proto1Token{{p1_token_label, "%label1"}, {p1_token_command, "ACK"}}},
		{"NSO", []proto1Token{{p1_token_command, "NSO"}}},
		{"ERR S:BAD_DATA", []proto1Token{
			{p1_token_command, "ERR"},
			{p1_token_typed_Value, "S:BAD_DATA"},
		}},
		{"VAL $conf", []proto1Token{
			{p1_token_command, "VAL"},
			{p1_token_global, "$conf"},
		}},
		{"VAL @conf", []proto1Token{
			{p1_token_command, "VAL"},
			{p1_token_reference, "@conf"},
		}},
		{"USE I:1", []proto1Token{
			{p1_token_command, "USE"},
			{p1_token_typed_Value, "I:1"},
		}},
		{"STR F:0", []proto1Token{
			{p1_token_command, "STR"},
			{p1_token_typed_Value, "F:0"},
		}},
		{"%label ABT", []proto1Token{
			{p1_token_label, "%label"},
			{p1_token_command, "ABT"},
		}},
		{"PNG @conn", []proto1Token{
			{p1_token_command, "PNG"},
			{p1_token_reference, "@conn"},
		}},
		{"VLD", []proto1Token{
			{p1_token_command, "VLD"},
		}},
		{"BYE", []proto1Token{
			{p1_token_command, "BYE"},
		}},
		{"CNF S:host S:localhost", []proto1Token{
			{p1_token_command, "CNF"},
			{p1_token_typed_Value, "S:host"},
			{p1_token_typed_Value, "S:localhost"},
		}},
		{"RST", []proto1Token{
			{p1_token_command, "RST"},
		}},
		{"ENB S:EXT S:my_extension", []proto1Token{
			{p1_token_command, "ENB"},
			{p1_token_typed_Value, "S:EXT"},
			{p1_token_typed_Value, "S:my_extension"},
		}},
		{"DIS S:EXT S:my_extension", []proto1Token{
			{p1_token_command, "DIS"},
			{p1_token_typed_Value, "S:EXT"},
			{p1_token_typed_Value, "S:my_extension"},
		}},
		{"LMT S:DRV S:sqlite", []proto1Token{
			{p1_token_command, "LMT"},
			{p1_token_typed_Value, "S:DRV"},
			{p1_token_typed_Value, "S:sqlite"},
		}},
		{"LMT S:GEN S:sqlite S:createTable", []proto1Token{
			{p1_token_command, "LMT"},
			{p1_token_typed_Value, "S:GEN"},
			{p1_token_typed_Value, "S:sqlite"},
			{p1_token_typed_Value, "S:createTable"},
		}},
		{"LMT S:GEN S:sqlite S:createTable S:params", []proto1Token{
			{p1_token_command, "LMT"},
			{p1_token_typed_Value, "S:GEN"},
			{p1_token_typed_Value, "S:sqlite"},
			{p1_token_typed_Value, "S:createTable"},
			{p1_token_typed_Value, "S:params"},
		}},
		{"HLP S:EXT S:my_extension", []proto1Token{
			{p1_token_command, "HLP"},
			{p1_token_typed_Value, "S:EXT"},
			{p1_token_typed_Value, "S:my_extension"},
		}},
		{"HLP S:DRV S:sqlite S:my_extension ", []proto1Token{
			{p1_token_command, "HLP"},
			{p1_token_typed_Value, "S:DRV"},
			{p1_token_typed_Value, "S:sqlite"},
			{p1_token_typed_Value, "S:my_extension"},
		}},
		{"ALC A:2 S:test I:23", []proto1Token{
			{p1_token_command, "ALC"},
			{p1_token_typed_Value, "A:2"},
			{p1_token_typed_Value, "S:test"},
			{p1_token_typed_Value, "I:23"},
		}},
		{"ALC A:2 S:test I:23 @res", []proto1Token{
			{p1_token_command, "ALC"},
			{p1_token_typed_Value, "A:2"},
			{p1_token_typed_Value, "S:test"},
			{p1_token_typed_Value, "I:23"},
			{p1_token_reference, "@res"},
		}},
		{"FRE @res", []proto1Token{
			{p1_token_command, "FRE"},
			{p1_token_reference, "@res"},
		}},
		{"CON $conf", []proto1Token{
			{p1_token_command, "CON"},
			{p1_token_global, "$conf"},
		}},
		{"CON $conf @myconn", []proto1Token{
			{p1_token_command, "CON"},
			{p1_token_global, "$conf"},
			{p1_token_reference, "@myconn"},
		}},
		{"PRE S#U0VMRUNUICogRlJPTSB0ZXN0Cg==", []proto1Token{
			{p1_token_command, "PRE"},
			{p1_token_typed_Value, "S#U0VMRUNUICogRlJPTSB0ZXN0Cg=="},
		}},
		{"PRE S#U0VMRUNUICogRlJPTSB0ZXN0Cg== @stmt", []proto1Token{
			{p1_token_command, "PRE"},
			{p1_token_typed_Value, "S#U0VMRUNUICogRlJPTSB0ZXN0Cg=="},
			{p1_token_reference, "@stmt"},
		}},
		{"QRY S%53454C454354202A2046524F4D20746573740A @args", []proto1Token{
			{p1_token_command, "QRY"},
			{p1_token_typed_Value, "S%53454C454354202A2046524F4D20746573740A"},
			{p1_token_reference, "@args"},
		}},
		{"QRY S%53454C454354202A2046524F4D20746573740A A:0 @outRes", []proto1Token{
			{p1_token_command, "QRY"},
			{p1_token_typed_Value, "S%53454C454354202A2046524F4D20746573740A"},
			{p1_token_typed_Value, "A:0"},
			{p1_token_reference, "@outRes"},
		}},
		{"EXC S%53454C454354202A2046524F4D20746573740A @args", []proto1Token{
			{p1_token_command, "EXC"},
			{p1_token_typed_Value, "S%53454C454354202A2046524F4D20746573740A"},
			{p1_token_reference, "@args"},
		}},
		{"EXC S%53454C454354202A2046524F4D20746573740A N: @outRes", []proto1Token{
			{p1_token_command, "EXC"},
			{p1_token_typed_Value, "S%53454C454354202A2046524F4D20746573740A"},
			{p1_token_typed_Value, "N:"},
			{p1_token_reference, "@outRes"},
		}},
		{"HNX @test", []proto1Token{
			{p1_token_command, "HNX"},
			{p1_token_reference, "@test"},
		}},
		{"ROW @test", []proto1Token{
			{p1_token_command, "ROW"},
			{p1_token_reference, "@test"},
		}},
		{"HNS @test", []proto1Token{
			{p1_token_command, "HNS"},
			{p1_token_reference, "@test"},
		}},
		{"NXS @test", []proto1Token{
			{p1_token_command, "NXS"},
			{p1_token_reference, "@test"},
		}},
		{"COL @res", []proto1Token{
			{p1_token_command, "COL"},
			{p1_token_reference, "@res"},
		}},
		{"COL @res I:2", []proto1Token{
			{p1_token_command, "COL"},
			{p1_token_reference, "@res"},
			{p1_token_typed_Value, "I:2"},
		}},
		{"CNL @res", []proto1Token{
			{p1_token_command, "CNL"},
			{p1_token_reference, "@res"},
		}},
		{"CNL @res I:2", []proto1Token{
			{p1_token_command, "CNL"},
			{p1_token_reference, "@res"},
			{p1_token_typed_Value, "I:2"},
		}},
		{"CDT @res", []proto1Token{
			{p1_token_command, "CDT"},
			{p1_token_reference, "@res"},
		}},
		{"CDT @res I:2", []proto1Token{
			{p1_token_command, "CDT"},
			{p1_token_reference, "@res"},
			{p1_token_typed_Value, "I:2"},
		}},
		{"CTL @res", []proto1Token{
			{p1_token_command, "CTL"},
			{p1_token_reference, "@res"},
		}},
		{"CTL @res I:2", []proto1Token{
			{p1_token_command, "CTL"},
			{p1_token_reference, "@res"},
			{p1_token_typed_Value, "I:2"},
		}},
		{"CPL @res", []proto1Token{
			{p1_token_command, "CPL"},
			{p1_token_reference, "@res"},
		}},
		{"CPL @res I:2", []proto1Token{
			{p1_token_command, "CPL"},
			{p1_token_reference, "@res"},
			{p1_token_typed_Value, "I:2"},
		}},
		{"BTX", []proto1Token{
			{p1_token_command, "BTX"},
		}},
		{"BTX @opts", []proto1Token{
			{p1_token_command, "BTX"},
			{p1_token_reference, "@opts"},
		}},
		{"BTX @opts @tx", []proto1Token{
			{p1_token_command, "BTX"},
			{p1_token_reference, "@opts"},
			{p1_token_reference, "@tx"},
		}},
		{"CMT @tx", []proto1Token{
			{p1_token_command, "CMT"},
			{p1_token_reference, "@tx"},
		}},
		{"RLB @tx", []proto1Token{
			{p1_token_command, "RLB"},
			{p1_token_reference, "@tx"},
		}},
		{"GNI", []proto1Token{
			{p1_token_command, "GNI"},
		}},
		{"GNI S:sqlite", []proto1Token{
			{p1_token_command, "GNI"},
			{p1_token_typed_Value, "S:sqlite"},
		}},
		{"GNI S:sqlite S:createTable", []proto1Token{
			{p1_token_command, "GNI"},
			{p1_token_typed_Value, "S:sqlite"},
			{p1_token_typed_Value, "S:createTable"},
		}},
		{"GEN S:sqlite S:createTable S:UP M:0", []proto1Token{
			{p1_token_command, "GEN"},
			{p1_token_typed_Value, "S:sqlite"},
			{p1_token_typed_Value, "S:createTable"},
			{p1_token_typed_Value, "S:UP"},
			{p1_token_typed_Value, "M:0"},
		}},
	}

	for _, tt := range tests {
		test := tt
		t.Run(test.input, func(t *testing.T) {
			in := strings.NewReader(test.input)
			tokens, err := ProtocolV1.tokenizeLine(in)

			if err != nil {
				t.Fatalf("Unexpected error! %#v", err)
			}

			if len(tokens) != len(test.expected) {
				t.Fatalf("InValid input! Expected %#v but received %#v", test.expected, tokens)
			}

			for i := range tokens {
				tok := tokens[i]
				expected := test.expected[i]
				if tok.tokenType != expected.tokenType {
					t.Errorf("Wrong token type! Expected %#v got %#v", expected.tokenType, tok.tokenType)
				}
				if tok.segment != expected.segment {
					t.Errorf("Wrong segment! Expected %#v got %#v", expected.segment, tok.segment)
				}
			}
		})
	}
}

func TestParseProto1(t *testing.T) {
	tests := []struct {
		input    string
		expected *ProtoCommand
	}{
		{"ACK", &ProtoCommand{"", ack_c, []ProtoVal{}}},
		{"%label1 ACK", &ProtoCommand{"label1", ack_c, []ProtoVal{}}},
		{"%label ABT", &ProtoCommand{"label", abort_c, []ProtoVal{}}},
		{"NSO", &ProtoCommand{"", not_supported_c, []ProtoVal{}}},
		{"ERR A:1 S:BAD_DATA", &ProtoCommand{"", error_c, []ProtoVal{&ValArray{[]ProtoVal{&ValString{"BAD_DATA"}}}}}},
		{"VAL $conf", &ProtoCommand{"", value_c, []ProtoVal{&ValGlobal{"conf"}}}},
		{"VAL @conf", &ProtoCommand{"", value_c, []ProtoVal{&ValRegister{"conf"}}}},
		{"VAL S:conf", &ProtoCommand{"", value_c, []ProtoVal{&ValString{"conf"}}}},
		{"USE I:1", &ProtoCommand{"", use_c, []ProtoVal{&ValInt{1}}}},
		{"STR F:1", &ProtoCommand{"", strict_c, []ProtoVal{&ValBool{true}}}},
		{"STR F:0", &ProtoCommand{"", strict_c, []ProtoVal{&ValBool{false}}}},
		{"PNG @conn", &ProtoCommand{"", ping_c, []ProtoVal{&ValRegister{"conn"}}}},
		{"VLD", &ProtoCommand{"", valid_c, []ProtoVal{}}},
		{"BYE", &ProtoCommand{"", bye_c, []ProtoVal{}}},
		{"CNF S:host S:localhost", &ProtoCommand{"", conf_set_c, []ProtoVal{&ValString{"host"}, &ValString{"localhost"}}}},
		{"RST", &ProtoCommand{"", reset_c, []ProtoVal{}}},
		{"ENB S:EXT S:my_extension", &ProtoCommand{"", enable_c, []ProtoVal{&ValString{"EXT"}, &ValString{"my_extension"}}}},
		{"DIS S:EXT S:my_extension", &ProtoCommand{"", disable_c, []ProtoVal{&ValString{"EXT"}, &ValString{"my_extension"}}}},
		{"LMT S:DRV S:sqlite", &ProtoCommand{"", limits_c, []ProtoVal{&ValString{"DRV"}, &ValString{"sqlite"}}}},
		{"LMT S:GEN S:sqlite S:createTable", &ProtoCommand{"", limits_c, []ProtoVal{&ValString{"GEN"}, &ValString{"sqlite"}, &ValString{"createTable"}}}},
		{"HLP S:EXT S:my_extension", &ProtoCommand{"", help_c, []ProtoVal{&ValString{"EXT"}, &ValString{"my_extension"}}}},
		{"HLP S:DRV S:sqlite S:my_extension", &ProtoCommand{"", help_c, []ProtoVal{&ValString{"DRV"}, &ValString{"sqlite"}, &ValString{"my_extension"}}}},
		{"ALC A:2 S:test I:23", &ProtoCommand{"", allocate_c, []ProtoVal{&ValArray{[]ProtoVal{&ValString{"test"}, &ValInt{23}}}}}},
		{"ALC A:2 S:test I:23 @res", &ProtoCommand{"", allocate_c, []ProtoVal{&ValArray{[]ProtoVal{&ValString{"test"}, &ValInt{23}}}, &ValRegister{"res"}}}},
		{"FRE @res", &ProtoCommand{"", free_c, []ProtoVal{&ValRegister{"res"}}}},
		{"CON $conf", &ProtoCommand{"", connect_c, []ProtoVal{&ValGlobal{"conf"}}}},
		{"CON $conf @myconn", &ProtoCommand{"", connect_c, []ProtoVal{&ValGlobal{"conf"}, &ValRegister{"myconn"}}}},
		{"PRE @conn S#U0VMRUNUICogRlJPTSB0ZXN0Cg==", &ProtoCommand{"", prepare_c, []ProtoVal{&ValRegister{"conn"}, &ValString{"SELECT * FROM test\n"}}}},
		{"PRE @conn S#U0VMRUNUICogRlJPTSB0ZXN0Cg== @stmt", &ProtoCommand{"", prepare_c, []ProtoVal{&ValRegister{"conn"}, &ValString{"SELECT * FROM test\n"}, &ValRegister{"stmt"}}}},
		{"QRY @conn S%53454C454354202A2046524F4D20746573740A @args", &ProtoCommand{"", query_c, []ProtoVal{&ValRegister{"conn"}, &ValString{"SELECT * FROM test\n"}, &ValRegister{"args"}}}},
		{"QRY @conn S%53454C454354202A2046524F4D20746573740A @args @outRes", &ProtoCommand{"", query_c, []ProtoVal{&ValRegister{"conn"}, &ValString{"SELECT * FROM test\n"}, &ValRegister{"args"}, &ValRegister{"outRes"}}}},
		{"EXC @conn S%53454C454354202A2046524F4D20746573740A @args", &ProtoCommand{"", execute_c, []ProtoVal{&ValRegister{"conn"}, &ValString{"SELECT * FROM test\n"}, &ValRegister{"args"}}}},
		{"HNX @test", &ProtoCommand{"", has_next_row_c, []ProtoVal{&ValRegister{"test"}}}},
		{"ROW @test", &ProtoCommand{"", row_c, []ProtoVal{&ValRegister{"test"}}}},
		{"HNS @test", &ProtoCommand{"", has_next_set_c, []ProtoVal{&ValRegister{"test"}}}},
		{"NXS @test", &ProtoCommand{"", next_set_c, []ProtoVal{&ValRegister{"test"}}}},
		{"COL @res", &ProtoCommand{"", column_info_c, []ProtoVal{&ValRegister{"res"}}}},
		{"COL @res I:2", &ProtoCommand{"", column_info_c, []ProtoVal{&ValRegister{"res"}, &ValInt{2}}}},
		{"CNL @res", &ProtoCommand{"", column_nullable_c, []ProtoVal{&ValRegister{"res"}}}},
		{"CNL @res I:2", &ProtoCommand{"", column_nullable_c, []ProtoVal{&ValRegister{"res"}, &ValInt{2}}}},
		{"CDT @res", &ProtoCommand{"", column_type_c, []ProtoVal{&ValRegister{"res"}}}},
		{"CDT @res I:2", &ProtoCommand{"", column_type_c, []ProtoVal{&ValRegister{"res"}, &ValInt{2}}}},
		{"CTL @res", &ProtoCommand{"", column_type_len_c, []ProtoVal{&ValRegister{"res"}}}},
		{"CTL @res I:2", &ProtoCommand{"", column_type_len_c, []ProtoVal{&ValRegister{"res"}, &ValInt{2}}}},
		{"CPL @res", &ProtoCommand{"", column_precision_len_c, []ProtoVal{&ValRegister{"res"}}}},
		{"CPL @res I:2", &ProtoCommand{"", column_precision_len_c, []ProtoVal{&ValRegister{"res"}, &ValInt{2}}}},
		{"BTX", &ProtoCommand{"", begin_transaction_c, []ProtoVal{}}},
		{"BTX @opts", &ProtoCommand{"", begin_transaction_c, []ProtoVal{&ValRegister{"opts"}}}},
		{"BTX @opts @tx", &ProtoCommand{"", begin_transaction_c, []ProtoVal{&ValRegister{"opts"}, &ValRegister{"tx"}}}},
		{"CMT @tx", &ProtoCommand{"", commit_transaction_c, []ProtoVal{&ValRegister{"tx"}}}},
		{"RLB @tx", &ProtoCommand{"", rollback_transaction_c, []ProtoVal{&ValRegister{"tx"}}}},
		{"GNI", &ProtoCommand{"", generate_info_c, []ProtoVal{}}},
		{"GNI S:sqlite", &ProtoCommand{"", generate_info_c, []ProtoVal{&ValString{"sqlite"}}}},
		{"GNI S:sqlite S:createTable", &ProtoCommand{"", generate_info_c, []ProtoVal{&ValString{"sqlite"}, &ValString{"createTable"}}}},
		{"GEN S:sqlite S:createTable S:UP M:0", &ProtoCommand{"", generate_c, []ProtoVal{&ValString{"sqlite"}, &ValString{"createTable"}, &ValString{"UP"}, &ValMap{}}}},
	}

	tc := NewTypeContext()
	for _, tt := range tests {
		test := tt
		t.Run(test.input, func(t *testing.T) {
			in := strings.NewReader(test.input)
			cmd, err := ProtocolV1.parseLine(in, tc)

			if err != nil {
				t.Fatalf("Unexpected error! %#v", err)
			}

			if cmd.cmd != test.expected.cmd {
				t.Fatalf("Expected command %#v, received %#v", *cmd.cmd, *test.expected.cmd)
			}

			if cmd.label != test.expected.label {
				t.Fatalf("Expected label %#v, received %#v", cmd.label, test.expected.label)
			}

			if !ProtoValsDeepEquals(cmd.args, test.expected.args) {
				e, _ := ProtocolV1.EncodeVal(&ValArray{test.expected.args})
				v, _ := ProtocolV1.EncodeVal(&ValArray{cmd.args})
				t.Fatalf("Args not equal! Expected: %#v, Received %#v", e, v)
			}
		})
	}
}

func TestSendMock(t *testing.T) {
	tests := []struct {
		cmd         string
		res         string
		expectedRes ProtoVal
	}{
		{"CON $conf\n", "RDY\nACK\n", nil},
	}

	for _, tt := range tests {
		test := tt
		t.Run(test.cmd, func(t *testing.T) {
			cmdReader := strings.NewReader(test.cmd[0 : len(test.cmd)-1])
			reader := strings.NewReader(test.res)
			var buf bytes.Buffer

			ctxSend, cancel := context.WithTimeout(context.Background(), time.Duration(5 * time.Second))
			defer cancel()

			sc, err := NewSendContext(ctxSend, &buf, reader)
			if err != nil {
				t.Fatal(err)
			}

			command, err := ProtocolV1.parseLine(cmdReader, sc.Tc)
			if err != nil {
				t.Fatalf("Unexpected error! %#v", err)
			}

			if err := sc.SendCommand(*command); err != nil {
				t.Fatalf("Unexpected error when sending! %#v", err)
			}
			if buf.String() != test.cmd {
				t.Fatalf("Command mismatch! Expected %#v, Received %#v", test.cmd, buf.String())
			}

			reader = strings.NewReader(test.res)
			ctx, _ := context.WithTimeout(context.Background(), time.Duration(5 * time.Second))
			_, response, err := sc.ReadResponse(ctx)
			if err != nil {
				t.Fatalf("Unexpected error when reading response! %#v", err)
			}

			resNil := response == nil
			expectedNil := test.expectedRes == nil

			if resNil != expectedNil {
				t.Fatalf("Incorrect response! Expected %#v, received %#v", test.res, response)
			}

			if !ProtoValDeepEquals(response, test.expectedRes) {
				e, _ := ProtocolV1.EncodeVal(test.expectedRes)
				v, _ := ProtocolV1.EncodeVal(response)
				t.Fatalf("Incorrect response! Expected %#v, received %#v", e, v)
			}
		})
	}
}
