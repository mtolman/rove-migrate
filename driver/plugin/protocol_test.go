package plugin_test

import (
	"bytes"
	"context"
	"rove_migrate/common"
	"rove_migrate/driver/plugin"
	"strings"
	"testing"
	"time"
)

func TestToProtoVal(t *testing.T) {
	ack, err := plugin.CommandByProto1Name("ACK")

	if err != nil {
		panic(err)
	}

	dt, err := time.Parse(time.RFC3339, "2022-02-01T01:23:22+00:00")
	if err != nil {
		panic(err)
	}

	tests := []struct {
		goVal    any
		expected plugin.ProtoVal
	}{
		{1, plugin.NewIntVal(1)},
		{int64(1), plugin.NewIntVal(1)},
		{uint8(1), plugin.NewIntVal(1)},
		{"test", plugin.NewStringVal("test")},
		{ack, plugin.NewMapVal(
			plugin.NewIntVal(1), plugin.NewStringVal("ACK"),
			plugin.NewIntVal(2), plugin.NewArrayVal(
				plugin.NewArrayVal(
					plugin.NewIntVal(0),
					plugin.NewIntVal(0x00),
				),
			),
			plugin.NewIntVal(3), plugin.NewStringVal("ack"),
		)},
		{12.5, plugin.NewRealVal(12.5)},
		// Rune arrays should be handled like strings
		{[]rune("hello"), plugin.NewStringVal("hello")},

		// byte arrays should be handled like binaries
		{[]byte("hello"), plugin.NewBinaryVal([]byte("hello"))},
		{dt, plugin.NewDateTimeTimezoneVal(2022, 2, 1, 1, 23, 22, 0.0, 0, 0)},
		{true, plugin.NewBoolVal(true)},
		{[]int{1, 2, 3}, plugin.NewArrayVal(
			plugin.NewIntVal(1), plugin.NewIntVal(2), plugin.NewIntVal(3),
		)},
		{map[string]int{"hello": 1, "goodbye": 2, "stork": 3}, plugin.NewMapVal(
			plugin.NewStringVal("stork"), plugin.NewIntVal(3),
			plugin.NewStringVal("hello"), plugin.NewIntVal(1),
			plugin.NewStringVal("goodbye"), plugin.NewIntVal(2),
		)},
	}

	for _, tt := range tests {
		actual, err := plugin.ToProtoVal(tt.goVal, nil)
		if err != nil {
			t.Fatalf("Unexpected error %v", err)
		}
		if !plugin.ProtoValDeepEquals(actual, tt.expected) {
			e, _ := tt.expected.EncodeProto1()
			a, _ := actual.EncodeProto1()
			t.Errorf("Expected %#v, received %#v", e, a)
		}
	}
}

type testStruct struct {
	NormalField     int
	RenamedField    string `protoVal:"rf"`
	SkippedField    int    `protoVal:"-"`
	unexportedField string
}

type testStruct2 struct {
	NormalField     int
	PtrField        *int
	SliceField      []string
	MapField        map[string]int64
	RenamedField    string `protoVal:"rf"`
	SkippedField    int    `protoVal:"-"`
	unexportedField string
}

func (t testStruct) ToMap() map[string]any {
	return map[string]any{
		"NormalField":     t.NormalField,
		"rf":              t.RenamedField,
		"SkippedField":    t.SkippedField,
		"unexportedField": t.unexportedField,
	}
}

func TestEncodeValue(t *testing.T) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(5 * time.Second))
	defer cancel()
	reader := strings.NewReader("RDY\n")
	var buf bytes.Buffer
	sc, err := plugin.NewSendContext(ctx, &buf, reader)
	if err != nil {
		t.Fatal(err)
	}

	i := 2

	tests := []struct {
		name string
		input    any
		expected string
	}{
		{
			"encode struct pointer",
			&testStruct{23, "Hello", 53, "test"},
			"M:2 S:NormalField I:23 S:rf S:Hello",
		},
		{
			"struct 2 encode nil",
			&testStruct2{123, nil, nil, nil, "rename", 23, "test"},
			"M:5 S:MapField N: S:NormalField I:123 S:PtrField N: S:SliceField N: S:rf S:rename",
		},
		{
			"struct 2 encode non-nil",
			&testStruct2{123, &i, []string{"hello"}, map[string]int64{"hello": 23}, "rename", 23, "test"},
			"M:5 S:MapField M:1 S:hello I:23 S:NormalField I:123 S:PtrField I:2 S:SliceField A:1 S:hello S:rf S:rename",
		},
		{
			"int",
			int(34),
			"I:34",
		},
		{
			"string",
			"testing",
			"S:testing",
		},
		{
			"bool",
			true,
			"F:1",
		},
		{
			"real",
			12.23,
			"R:12.23",
		},
		{
			"map",
			map[string]string{"hello": "world"},
			"M:1 S:hello S:world",
		},
		{
			"slice",
			[]string{"hello", "world"},
			"A:2 S:hello S:world",
		},
		{
			"string",
			"testing base64",
			"S#dGVzdGluZyBiYXNlNjQ=",
		},
		{
			"uint",
			uint(34),
			"I:34",
		},
		{
			"nil",
			nil,
			"N:",
		},
		{
			"tableCreate",
			common.TableCreate{
				Schema: "test_schema",
				Name: "users",
				Columns: []common.Column{
					common.Column{Name: "id", Type: "INT", NotNull: true, AutoIncrement: true},
					common.Column{Name: "name", Type: "TEXT", NotNull: true},
				},
				ErrIfExists: false,
				Timestamps: true,
				PrimaryKey: []string{"id"},
				ForeignKeys: []common.ForeignKey{
					common.ForeignKey{Name: "fk1", Keys: []string{"name"}, ReferenceTable: "names", ReferenceKeys: []string{"n"}, OnDelete: nil, OnUpdate: nil},
				},
			},
			"M:7 S:columns A:2 M:5 S:autoincrement F:1 S:default N: S:name S:id S:notNull F:1 S:type S:INT M:5 S:autoincrement F:0 S:default N: S:name S:name S:notNull F:1 S:type S:TEXT S:errIfExists F:0 S:foreignKeys A:1 M:6 S:keys A:1 S:name S:name S:fk1 S:onDelete N: S:onUpdate N: S:references A:1 S:n S:table S:names S:name S:users S:primaryKey A:1 S:id S:schema S:test_schema S:timestamps F:1",
		},
		{
			"alterTable",
			common.TableAlter{
				Schema: "test_schema",
				Name: "users",
				RenameTo: "test_users",
				AddColumns: []common.Column{
					common.Column{Name: "id", Type: "INT", NotNull: true, AutoIncrement: true},
					common.Column{Name: "name", Type: "TEXT", NotNull: true},
				},
				RenameColumns: []common.ColRename{
					common.ColRename{From: "best", To: "worst"},
				},
			},
			"M:5 S:addColumns A:2 M:5 S:autoincrement F:1 S:default N: S:name S:id S:notNull F:1 S:type S:INT M:5 S:autoincrement F:0 S:default N: S:name S:name S:notNull F:1 S:type S:TEXT S:name S:users S:renameColumns A:1 M:2 S:from S:best S:to S:worst S:renameTo S:test_users S:schema S:test_schema",
		},
		{
			"createIndex",
			common.IndexCreate{
				Schema: "test_schema",
				Name: "users_index",
				Unique: true,
				Table: "users",
				ErrIfExists: true,
				Columns: []string{"id", "name"},
				Where: "",
			},
			"M:7 S:columns A:2 S:id S:name S:errIfExists F:1 S:name S:users_index S:schema S:test_schema S:table S:users S:unique F:1 S:where S:",
		},
	}

	for _, tt := range tests {
		test := tt
		t.Run(tt.name, func(t *testing.T) {
			val := sc.ToProtoValPanic(test.input)
			res, err := plugin.ProtocolV1.EncodeVal(val)

			if err != nil {
				t.Fatal(err)
			}

			if res != test.expected {
				t.Errorf("Expected %#v, received %#v", test.expected, res)
			}
		})
	}
}

func TestDecodeValue(t *testing.T) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(5 * time.Second))
	defer cancel()
	reader := strings.NewReader("RDY\n")
	var buf bytes.Buffer
	sc, err := plugin.NewSendContext(ctx, &buf, reader)

	if err != nil {
		t.Fatal(err)
	}

	t.Run("Test decode int", func(t *testing.T) {
		var out, expected int64
		expected = 12
		if err := sc.To(plugin.NewIntVal(expected), &out); err != nil {
			t.Fatal(err)
		}

		if out != expected {
			t.Errorf("Expected %v, received %v", expected, out)
		}
	})

	t.Run("Test decode float", func(t *testing.T) {
		var out, expected float64
		expected = 12.2
		if err := sc.To(plugin.NewRealVal(expected), &out); err != nil {
			t.Fatal(err)
		}

		if out != expected {
			t.Errorf("Expected %v, received %v", expected, out)
		}
	})

	t.Run("Test decode string", func(t *testing.T) {
		var out, expected string
		expected = "Hello World!"
		if err := sc.To(plugin.NewStringVal(expected), &out); err != nil {
			t.Fatal(err)
		}

		if out != expected {
			t.Errorf("Expected %v, received %v", expected, out)
		}
	})

	t.Run("Test decode string slice", func(t *testing.T) {
		var out, expected []string
		expected = []string{"Hello World!", "Hola!"}
		if err := sc.To(sc.ToProtoValPanic(expected), &out); err != nil {
			t.Fatal(err)
		}

		if len(out) != len(expected) {
			t.Errorf("Expected %v, received %v", expected, out)
		}
		for i := range out {
			if out[i] != expected[i] {
				t.Errorf("Expected %v, received %v", expected, out)
			}
		}
	})

	t.Run("Test decode any slice", func(t *testing.T) {
		var out, expected []any
		expected = []any{"Hello World!", int64(12)}
		if err := sc.To(sc.ToProtoValPanic(expected), &out); err != nil {
			t.Fatal(err)
		}

		if len(out) != len(expected) {
			t.Errorf("Expected %v, received %v", expected, out)
		}
		for i := range out {
			if out[i] != expected[i] {
				t.Errorf("Expected %#v, received %#v", expected, out)
			}
		}
	})

	t.Run("Test decode map[string]string", func(t *testing.T) {
		var out, expected map[string]string
		expected = map[string]string{"Hello World!": "Hola!"}
		if err := sc.To(sc.ToProtoValPanic(expected), &out); err != nil {
			t.Fatal(err)
		}

		if len(out) != len(expected) {
			t.Errorf("Expected %v, received %v", expected, out)
		}
		for i := range out {
			if out[i] != expected[i] {
				t.Errorf("Expected %v, received %v", expected, out)
			}
		}
	})

	t.Run("Test decode map[any]any", func(t *testing.T) {
		var out, expected map[any]any
		expected = map[any]any{"Hello World!": "Hola!", int64(23): int64(12)}
		if err := sc.To(sc.ToProtoValPanic(expected), &out); err != nil {
			t.Fatal(err)
		}

		if len(out) != len(expected) {
			t.Errorf("Expected %v, received %v", expected, out)
		}
		for i := range out {
			if out[i] != expected[i] {
				t.Errorf("Expected %v, received %v", expected, out)
			}
		}
	})

	t.Run("Test decode []map[string]string", func(t *testing.T) {
		var out, expected []map[string]string
		expected = []map[string]string{{"Hello World!": "Hola!"}}
		if err := sc.To(sc.ToProtoValPanic(expected), &out); err != nil {
			t.Fatal(err)
		}

		if len(out) != len(expected) {
			t.Errorf("Expected %v, received %v", expected, out)
		}
		for i := range out {
			for j := range out[i] {
				if out[i][j] != expected[i][j] {
					t.Errorf("Expected %v, received %v", expected, out)
				}
			}
		}
	})

	t.Run("Test decode []map[string][]string", func(t *testing.T) {
		var out, expected []map[string][]string
		expected = []map[string][]string{{"Hello World!": []string{"Hola!"}}}
		if err := sc.To(sc.ToProtoValPanic(expected), &out); err != nil {
			t.Fatal(err)
		}

		if len(out) != len(expected) {
			t.Errorf("Expected %v, received %v", expected, out)
		}
		for i := range out {
			for j := range out[i] {
				for k := range out[i][j] {
					if out[i][j][k] != expected[i][j][k] {
						t.Errorf("Expected %v, received %v", expected, out)
					}
				}
			}
		}
	})

	t.Run("Test decode struct", func(t *testing.T) {
		var out, expected testStruct
		expected = testStruct{23, "Hello", 53, "test"}
		if err := sc.To(sc.ToProtoValPanic(expected), &out); err != nil {
			t.Fatal(err)
		}

		if out.NormalField != expected.NormalField {
			t.Errorf("Expected %v, received %v", expected, out)
		}

		if out.RenamedField != expected.RenamedField {
			t.Errorf("Expected %v, received %v", expected, out)
		}
	})

	t.Run("Test decode struct ptr", func(t *testing.T) {
		var out, expected *testStruct
		expected = &testStruct{23, "Hello", 53, "test"}
		if err := sc.To(sc.ToProtoValPanic(expected.ToMap()), &out); err != nil {
			t.Fatal(err)
		}

		if out.NormalField != expected.NormalField {
			t.Errorf("Expected %v, received %v", expected, out)
		}

		if out.RenamedField != expected.RenamedField {
			t.Errorf("Expected %v, received %v", expected, out)
		}
	})

	t.Run("Test decode into any", func(t *testing.T) {
		var out any
		expected := true
		if err := sc.To(sc.ToProtoValPanic(expected), &out); err != nil {
			t.Fatal(err)
		}

		if expected != out {
			t.Errorf("Expected %v, received %v", expected, out)
		}
	})

	t.Run("Test decode bool", func(t *testing.T) {
		var out, expected bool
		expected = true
		if err := sc.To(sc.ToProtoValPanic(expected), &out); err != nil {
			t.Fatal(err)
		}

		if expected != out {
			t.Errorf("Expected %v, received %v", expected, out)
		}
	})

	t.Run("Test Struct 2 decode nil", func(t *testing.T) {
		var out, expected testStruct2
		expected = testStruct2{
			NormalField:     23,
			PtrField:        nil,
			SliceField:      nil,
			MapField:        nil,
			RenamedField:    "test",
			SkippedField:    34,
			unexportedField: "test",
		}

		v := sc.ToProtoValPanic(expected)
		if err := sc.To(v, &out); err != nil {
			t.Fatal(err)
		}

		if out.NormalField != expected.NormalField {
			t.Errorf("Expected %v, received %v", expected, out)
		}

		if out.PtrField != expected.PtrField && *out.PtrField != *expected.PtrField {
			t.Errorf("Expected %v, received %v", expected, out)
		}

		if len(out.SliceField) != len(expected.SliceField) {
			t.Errorf("Expected %v, received %v", expected, out)
		}

		for i := range out.SliceField {
			if out.SliceField[i] != expected.SliceField[i] {
				t.Errorf("Expected %v, received %v", expected, out)
			}
		}

		if out.MapField != nil {
			if expected.MapField == nil {
				t.Errorf("Expected %v, received %v", expected, out)
			} else {
				if len(out.MapField) != len(expected.MapField) {
					t.Errorf("Expected %v, received %v", expected, out)
				}

				for i := range out.MapField {
					if out.MapField[i] != expected.MapField[i] {
						t.Errorf("Expected %v, received %v", expected, out)
					}
				}
			}
		} else if expected.MapField != nil {
			t.Errorf("Expected %v, received %v", expected, out)
		}

		if out.RenamedField != expected.RenamedField {
			t.Errorf("Expected %v, received %v", expected, out)
		}
	})
}

func TestToGoVal(t *testing.T) {
	dt, err := time.Parse(time.RFC3339, "2022-02-01T01:23:22+00:00")
	if err != nil {
		panic(err)
	}

	tests := []struct {
		protoVal plugin.ProtoVal
		expected any
	}{
		{plugin.NewIntVal(1), int64(1)},
		{plugin.NewStringVal("test"), "test"},
		{plugin.NewBinaryVal([]byte("test")), []byte("test")},
		{plugin.NewRealVal(12.5), 12.5},
		{plugin.NewDateTimeTimezoneVal(2022, 2, 1, 1, 23, 22, 0.0, 0, 0), dt},
		{plugin.NewBoolVal(true), true},
		{plugin.NewArrayVal(
			plugin.NewIntVal(1), plugin.NewIntVal(2), plugin.NewIntVal(3),
		), []any{int64(1), int64(2), int64(3)}},
		{plugin.NewMapVal(
			plugin.NewStringVal("stork"), plugin.NewIntVal(3),
			plugin.NewStringVal("hello"), plugin.NewIntVal(1),
			plugin.NewStringVal("goodbye"), plugin.NewIntVal(2),
		), map[any]any{"hello": int64(1), "goodbye": int64(2), "stork": int64(3)}},
	}

	for _, tt := range tests {
		actual, err := plugin.FromProtoVal(tt.protoVal)
		if err != nil {
			t.Fatalf("Unexpected error %v", err)
		}

		var match bool

		switch a := actual.(type) {
		case []byte:
			switch b := tt.expected.(type) {
			case []byte:
				match = string(a) == string(b)
			default:
				match = false
			}
		case []any:
			switch b := tt.expected.(type) {
			case []any:
				if len(a) != len(b) {
					match = false
				} else {
					match = true
					for i := range a {
						av, bv := a[i], b[i]
						switch avv := av.(type) {
						default:
							match = false
						case int64:
							switch bvv := bv.(type) {
							default:
								match = false
							case int64:
								match = avv == bvv
							}
						}
					}
				}
			default:
				match = false
			}
		case map[any]any:
			switch b := tt.expected.(type) {
			case map[any]any:
				if len(a) != len(b) {
					match = false
				} else {
					match = true
					for k, av := range a {
						bv, ok := b[k]
						if !ok {
							match = false
							break
						}

						switch avv := av.(type) {
						default:
							match = false
						case int64:
							switch bvv := bv.(type) {
							default:
								match = false
							case int64:
								match = avv == bvv
							}
						}
					}
				}
			default:
				match = false
			}
		default:
			match = actual == tt.expected
		}
		if !match {
			t.Errorf("Expected %#v, received %#v", tt.expected, actual)
		}
	}
}
