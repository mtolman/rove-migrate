(ns test-plugin.core
  (:gen-class)
  (:require [clojure.string :as str]
            [clojure.instant :as instant]
            [datascript.core :as d]
            [clojure.edn :as edn]
            [next.jdbc :as jdbc]
            [next.jdbc.transaction :as tx]
            [next.jdbc.prepare :as p]
            [clojure.test :as t])
  (:import java.util.Base64))

(defn- label-of [segments]
  (if (empty? segments)
    ""
    (if (str/starts-with? (first segments) "%")
      (subs (first segments) 1)
      "")))

(declare exec-command)

(defonce ^:private proto (atom 1))
(defonce ^:private conf (atom {}))
(defonce ^:private refs (atom {}))
(defonce ^:private strict (atom false))
(def ^:private protos [1])
(def ^:private drivers ["datascript" "h2"])
(def ^:private async false)
(def ^:private plugin-name "test_plugin")
(def ^:private version "0.0.0")
(def ^:private docstring "")
(def ^:private generators ["h2"])

(defn- init-conf []
  (reset! conf {"file" (System/getenv "DB_FILE")
                "host" (System/getenv "DB_HOST")
                "user" (System/getenv "DB_USER")
                "pwd"  (System/getenv "DB_PWD")
                "driver" (or (System/getenv "DB_DRIVER") "datascript")
                "generator" (or (System/getenv "DB_GENERATOR") "h2")
                "port" (parse-long (or (System/getenv "DB_PORT") "0"))
                "database" (System/getenv "DB_DATABASE")
                "schema" (System/getenv "DB_SCHEMA")}))

(defn- cols-from-datascript-query [query]
  (let [find (drop-while #(not= % :find) query)
        params (second find)
        params (if (vector? params) params (take-while symbol? (rest find)))
        cols (map (comp #(subs % 1) str) params)]
    cols))

(t/deftest cols-from-datascript-query-test
  (t/is (= ["id" "name" "bob"] (cols-from-datascript-query '[:find [?id ?name ?bob] :in [] :where []])))
  (t/is (= ["id" "name" "bob"] (cols-from-datascript-query '[:find ?id ?name ?bob :in [] :where []])))
  (t/is (= ["id" "name" "bob"] (cols-from-datascript-query '[:find ?id ?name ?bob :where []]))))

(defprotocol Generator
  (engine-info [this] "Information about what generations the generator supports")
  (generation-info [this generation] "Information about a specific generation")
  (generate [this generation variance params] "Run generation algorithm"))

(defrecord H2Generator []
  Generator
  (engine-info [this] {"createTable" {"variance" ["UP" "DOWN"] "summary" "SQL to create a table" "docstring" "Takes a map of 'name', 'columns' where 'columns' is a map of names and types"}})
  (generation-info [this generation]
    (let [info (engine-info this)]
      (get info generation)))
  (generate [this generation variance params]
    (case generation
      "createTable" (case variance
                      "UP" (str "CREATE TABLE IF NOT EXISTS "
                                (get params "name")
                                " ("
                                (str/join ", "
                                          (map (fn [[name type]]
                                                 (str name " " type))
                                               (get params "columns")))
                                ");")
                      "DOWN" (str "DROP TABLE IF EXISTS "
                                  (get params "name")
                                  ";")
                      :else (throw (Exception. (str "Invalid variance " variance))))
      :else (throw (Exception. (str "Invalid generation " generation))))))

(t/deftest h2-generator-test
  (t/is (= (-> (H2Generator.)
               (generate "createTable" "UP" {"name" "users" "columns" {"id" "INT"
                                                                       "name" "TEXT"}}))
           "CREATE TABLE IF NOT EXISTS users (id INT, name TEXT);")))

(comment (-> (H2Generator.)
             (generate "createTable" "UP" {"name" "users" "columns" {"id" "INT"
                                                                     "name" "TEXT"}})))

(defmulti generator-for identity)
(defmethod generator-for "h2" [_] (H2Generator.))
(comment (-> "h2"
             (generator-for)
             (generate "createTable" "UP" {"name" "users" "columns" {"id" "INT"
                                                                     "name" "TEXT"}})))

(defprotocol QueryResult
  "Protocol for working with database queries"
  (row [this] "Returns current row and new QueryResult for getting the next row")
  (has-row? [this] "Returns whether there is another row")
  (has-next-set? [this] "Returns whether there is another result set")
  (next-set [this] "Returns QueryResult for next result set")
  (cols [this] "Returns vector of names of columns")
  (null-cols [this] "Returns vector of which columns are nullable")
  (types-cols [this] "Returns vector of column types")
  (len-cols [this] "Data lengths of columns")
  (precision-cols [this] "Precision lengths of columns"))

(defrecord DatascriptQueryResult [result cols]
  QueryResult
  (row [this] (let [result (:result this)]
                (if (set? result)
                  [(first result) (DatascriptQueryResult. (into #{} (rest result)) (:cols this))]
                  [result (DatascriptQueryResult. nil cols)])))
  (has-row? [this] (let [result (:result this)]
                     (not (empty? result))))
  (has-next-set? [_] false)
  (next-set [_] (DatascriptQueryResult. nil []))
  (cols [this] (:cols this))
  (null-cols [this] (map (fn [_] true) (:cols this)))
  (types-cols [this] (map (fn [_] "any") (:cols this)))
  (len-cols [this] (map (fn [_] nil) (:cols this)))
  (precision-cols [this] (map (fn [_] nil) (:cols this))))

(defprotocol DbDriver
  "A driver for connecting and communicating with a database"
  (prepare [this stmt] "Prepares a statement for later execution")
  (query [this query variables] "Query database with provided query string and decoded variables")
  (execute [this stmt variables] "Execute statement against database. Returns map of {:last-insert-id (s/or (pos-int?) -1) :rows-affected (s/or (pos-int?) -1)}")
  (count-inputs [this stmt] "Counts number of inputs, used for validation. Return -1 if not supported"))

(def ^:private NON_TRANSFERABLE "<NON_TRANSFERRABLE>")

(def ^:private h2-info
  {"PNG" {:limits nil :help "Pings in-memory datascript DB"}
   "ABT" {:limits {"ignored" true} :help "Ignored"}
   "CON" {:limits nil :help "Creates new in-memory datascript DB"}
   "PRE" {:limits nil :help "Prepares SQL query"}
   "QRY" {:limits nil :help "Run SQL Query"}
   "EXC" {:limits {"ignoredParams" [3]} :help "Executes SQL query"}
   "CNI" {:limits {"na" true} :helps "Returns -1"}
   "HNX" {:limits nil :help "Checks if has next row"}
   "ROW" {:limits nil :help "Grabs current row and advances row pointer"}
   "HNS" {:limits {"na" true} :help "Result sets not supported"}
   "NXS" {:limits {"na" true} :help "Result sets not supported"}
   "COL" {:limits nil :help "Gets column information"}
   "CNL" {:limits nil :help "Gets column nullable information"}
   "CDT" {:limits nil :help "Gets column database type"}
   "CTL" {:limits nil :help "Gets column type length"}
   "CPL" {:limits nil :help "Gets column precision length"}
   "BTX" {:limits {"nso" true} :help "Starts new transaction"}
   "CMT" {:limits {"nso" true} :help "Commits transaction"}
   "RLB" {:limits {"nso" true} :help "Rolls back transaction"}})

(def ^:private datascript-info
  {"PNG" {:limits nil :help "Pings in-memory datascript DB"}
   "ABT" {:limits {"ignored" true} :help "Ignored"}
   "CON" {:limits nil :help "Creates new in-memory datascript DB"}
   "PRE" {:limits nil :help "Parses and prepares EDN datalog query"}
   "QRY" {:limits nil :help "Runs EDN datalog query"}
   "EXC" {:limits {"ignoredParams" [3]} :help "Runs datascript transact. Ignores variables arg"}
   "CNI" {:limits {"na" true} :helps "Returns -1"}
   "HNX" {:limits nil :help "Checks if has next row"}
   "ROW" {:limits nil :help "Grabs current row and advances row pointer"}
   "HNS" {:limits {"na" true} :help "Result sets not supported"}
   "NXS" {:limits {"na" true} :help "Result sets not supported"}
   "COL" {:limits nil :help "Gets column information"}
   "CNL" {:limits nil :help "Gets column nullable information"}
   "CDT" {:limits {"na" true} :help "No data type information available"}
   "CTL" {:limits {"na" true} :help "No data type information available"}
   "CPL" {:limits {"na" true} :help "No data type information available"}
   "BTX" {:limits {"nso" true} :help "Transactions not supported"}
   "CMT" {:limits {"nso" true} :help "Transactions not supported"}
   "RLB" {:limits {"nso" true} :help "Transactions not supported"}})

(defrecord DatascriptDriver [conn]
  DbDriver
  (prepare [this query] (edn/read-string query))
  (query [this query variables]
    (let [q (if (string? query) (edn/read-string query) query)
          conn (:conn this)
          query-res (apply d/q q @conn variables)]
      (DatascriptQueryResult. query-res (cols-from-datascript-query q))))
  (execute [this stmt variables]
    (let [conn (:conn this)
          s (if (string? stmt) (edn/read-string stmt) stmt)
          tx (d/transact! conn s)
          num-rows (->> tx :tx-data (map first) (into #{}) (count))
          last-insert-id (-> tx :tempids (get -1 -1))]
      {:last-insert-id last-insert-id :rows-affected num-rows})))

(defrecord H2QueryResult [result cols]
  QueryResult
  (row [this] (let [result (:result this)]
                [(first result) (H2QueryResult. (rest result) (:cols this))]))
  (has-row? [this] (let [result (:result this)]
                     (not (empty? result))))
  (has-next-set? [_] false)
  (next-set [_] (H2QueryResult. nil []))
  (cols [this] (:cols this))
  (null-cols [this] (map (fn [_] true) (:cols this)))
  (types-cols [this] (map (fn [_] "any") (:cols this)))
  (len-cols [this] (map (fn [_] nil) (:cols this)))
  (precision-cols [this] (map (fn [_] nil) (:cols this))))

(defrecord H2Driver [conn]
  DbDriver
  (prepare [this stmt] (jdbc/prepare (:conn this) [stmt]))
  (query [this stmt variables]
    (if (string? stmt)
      (with-open [q (prepare this stmt)]
        (query this q variables))
      (let [q (p/set-parameters stmt (or variables []))
            query-res (jdbc/execute! q)
            rows (->> query-res
                      (map (fn [row]
                             (->> row
                                  (map (fn [[k v]]
                                         [(-> k
                                              str
                                              (str/replace-first ":" "")
                                              str/lower-case
                                              (str/split #"/")
                                              second) v]))
                                  (into {})))))
            cols (->> rows
                      first
                      (map first))]
        (H2QueryResult. rows cols))))
  (execute [this stmt variables]
    (if (string? stmt)
      (with-open [q (prepare this stmt)]
        (execute this q variables))
      (let [q (p/set-parameters stmt (or variables []))
            exec-res (jdbc/execute-one! q)]
        {:last-insert-id 0 :rows-affected (get exec-res :next.jdbc/update-count 0)})))

  (count-inputs [this stmt] -1))

(defmulti get-connection #(get %1 "driver"))
(defmethod get-connection "datascript" [_] (DatascriptDriver. (d/create-conn)))
(defmethod get-connection "h2" [conf] (H2Driver. (jdbc/get-connection {:dbtype "h2:mem" :dbname (get conf "database" "test")})))

(comment (def h2-conn (get-connection {"driver" "h2"})))
(comment (execute h2-conn "DROP TABLE IF EXISTS users" []))
(comment (execute h2-conn "CREATE TABLE IF NOT EXISTS users (id INT PRIMARY KEY, name TEXT)" []))
(comment (execute h2-conn "INSERT INTO users (id, name) VALUES (12, 'john'), (23, 'bob')" []))
(comment (prepare h2-conn "SELECT * FROM users"))
(comment (query h2-conn "SELECT * FROM users" []))
(comment (let [res (jdbc/execute! (:conn h2-conn) ["SELECT id, name FROM users"])
               cols (map
                     (fn [[k _]]
                       (str/lower-case
                        (second
                         (str/split (str k) #"/"))))
                     (first res))]
           (->
            (H2QueryResult. res cols)
            row
            second
            row
            second
            has-row?)))

(defmulti driver-info identity)
(defmethod driver-info "datascript" [_]
  datascript-info)
(defmethod driver-info "h2" [_]
  h2-info)

(defn- driver-help
  ([driver] (->> driver
                 (driver-info)
                 (filter (comp :help second))
                 (map #(vector (first %1) (:help (second %1))))
                 (into {})))
  ([driver cmd] (-> driver
                    (driver-info)
                    (get cmd)
                    (:help))))

(defn- driver-limits
  ([driver] (->> driver
                 (driver-info)
                 (filter (comp :limits second))
                 (map #(vector (first %1) (:limits (second %1))))
                 (into {})))
  ([driver cmd] (-> driver
                    (driver-info)
                    (get cmd)
                    (:limits))))
(comment (def connection (get-connection {"driver" "datascript"})))
(comment (edn/read-string "[:find ?id ?name ?bob :where []]"))
(comment (query connection "[:find ?id ?name ?bob :where []]" []))

(t/deftest driver-limits-tests
  (t/is (< 0 (count (driver-limits "datascript"))))
  (t/is (= 0 (count (filter (fn [[_ x]] (string? x)) (driver-limits "datascript")))))
  (t/is (= {"nso" true} (driver-limits "datascript" "BTX")))
  (t/is (nil? (driver-limits "datascript" "COL"))))

(t/deftest driver-limits-help-tests
  (t/is (< 0 (count (driver-help "datascript"))))
  (t/is (= 0 (count (filter (fn [[_ x]] (not (string? x))) (driver-help "datascript")))))
  (t/is (not (empty? (driver-help "datascript" "BTX"))))
  (t/is (not (empty (driver-help "datascript" "COL")))))

(def ^:private known-commands [;; Responses 
                               {:v1 "ACK" :v2 [[0 0x00]] :v3 "ack"}
                               {:v1 "NSO" :v2 [[0 0x01]] :v3 "not_supported_operation"}
                               {:v1 "ERR" :v2 [[1, 0x02]] :v3 "error"}

                               ;; Response and request
                               {:v1 "VAL" :v2 [[1, 0x03]] :v3 "value"}

                               ;; Protocol
                               {:v1 "USE" :v2 [[1, 0x04]] :v3 "use"}
                               {:v1 "STR" :v2 [[1, 0x05]] :v3 "strict"}

                               ;; Lifecycle Management
                               {:v1 "PNG" :v2 [[1, 0x07]] :v3 "ping"}
                               {:v1 "VLD" :v2 [[0, 0x08]] :v3 "valid"}
                               {:v1 "BYE" :v2 [[0, 0x09]] :v3 "bye"}

                               ;; Process execution
                               {:v1 "ABT" :v2 [[0, 0x0C]] :v3 "abort"}

                               ;; global conf
                               {:v1 "CNF" :v2 [[2, 0x10]] :v3 "conf"}
                               {:v1 "RST" :v2 [[0, 0x11]] :v3 "reset"}

                               ;; Extensions, types, etc
                               {:v1 "ENB" :v2 [[2, 0x12]] :v3 "enable"}
                               {:v1 "DIS" :v2 [[2, 0x13]] :v3 "disable"}
                               {:v1 "LMT" :v2 [[2, 0x14], [3, 0x15]] :v3 "limits"}
                               {:v1 "HLP" :v2 [[2, 0x17], [3, 0x18]] :v3 "help"}
                               {:v1 "EXT" :v2 [[3, 0x19]] :v3 "extension"}

                               ;; Resource management
                               {:v1 "ALC" :v2 [[1, 0x1B], [2, 0x1C]] :v3 "alloc"}
                               {:v1 "FRE" :v2 [[1, 0x1D]] :v3 "free"}

                               ;; Connections
                               {:v1 "CON" :v2 [[1, 0x1E], [2, 0x1F]] :v3 "connect"}

                               ;; Queries
                               {:v1 "PRE" :v2 [[2, 0x20], [3, 0x21]] :v3 "prepare"}
                               {:v1 "QRY" :v2 [[3, 0x22], [4, 0x23]] :v3 "query"}
                               {:v1 "EXC" :v2 [[3, 0x24]] :v3 "execute"}
                               {:v1 "CNI" :v2 [[2, 0x25]] :v3 "count_num_inputs"}

                               ;; Result Sets
                               {:v1 "HNX" :v2 [[1, 0x28]] :v3 "has_next_row"}
                               {:v1 "ROW" :v2 [[1, 0x29]] :v3 "row"}
                               {:v1 "HNS" :v2 [[1, 0x2A]] :v3 "has_next_set"}
                               {:v1 "NXS" :v2 [[1, 0x2B]] :v3 "next_set"}

                               ;; Columns
                               {:v1 "COL" :v2 [[1, 0x30], [2, 0x31]] :v3 "column"}
                               {:v1 "CNL" :v2 [[1, 0x32], [2, 0x33]] :v3 "column_nullable"}
                               {:v1 "CDT" :v2 [[1, 0x34], [2, 0x35]] :v3 "column_type"}
                               {:v1 "CTL" :v2 [[1, 0x36], [2, 0x37]] :v3 "column_type_len"}
                               {:v1 "CPL" :v2 [[1, 0x38], [2, 0x39]] :v3 "column_precision_len"}

                               ;; Transactions
                               {:v1 "BTX" :v2 [[0, 0x40], [1, 0x41], [2, 0x42]] :v3 "begin_tx"}
                               {:v1 "CMT" :v2 [[1, 0x46]] :v3 "commit"}
                               {:v1 "RLB" :v2 [[1, 0x47]] :v3 "rollback"}

                               ;; Generation
                               {:v1 "GNI" :v2 [[0, 0x60], [1, 0x61], [2, 0x62]] :v3 "generator_info"}
                               {:v1 "GEN" :v2 [[4, 0x63]] :v3 "generate"}])
(def ^:private v1-commands (into {} (map #(vector (:v1 %1) %1) known-commands))) ; #'test-plugin.core/v1-commands

(defmulti validate-cmd$ :proto)
(defmethod validate-cmd$ 1 [input]
  (let [cmd (:cmd input)]
    (if (not (contains? v1-commands cmd))
      (throw (Exception. (str "Invalid command" cmd)))
      cmd)))

(defn validate-cmd [cmd] (validate-cmd$ {:proto @proto :cmd cmd}))

(defn- command-of [segments]
  (let [cmd (if (= "" (label-of segments))
              (first segments)
              (second segments))]
    (validate-cmd cmd)))

(defn- str-to-bytes [s]
  (bytes (byte-array (map (comp byte int) s))))

(declare decode-arg)
(defn- decode-typed-arg-v1 [type arg other-args]
  (if (bytes? arg)
    (case type
      "B" [arg other-args]
      (decode-typed-arg-v1 type (String. arg) other-args))
    (case type
      "N" nil
      "S" [arg other-args]
      "I" [(parse-long arg) other-args]
      "R" [(parse-double arg) other-args]
      "B" [(str-to-bytes arg) other-args]
      "F" [(= (first arg) \1) other-args]
      "U" [(java.sql.Timestamp. (.longValue (* 1000 (parse-long arg)))) other-args]
      "A" (let [len (parse-long arg)]
            (loop [cnt len remaining other-args found []]
              (cond
                (<= cnt 0) [found remaining]
                (empty? remaining) (throw (Exception. "INVALID_DATA"))
                :else (let [next-arg (first remaining)
                            others (rest remaining)
                            [parsed next-others] (decode-arg next-arg others)]
                        (recur (dec cnt) next-others (conj found parsed))))))
      "M" (let [len (parse-long arg)]
            (loop [cnt len remaining other-args found {}]
              (cond
                (<= cnt 0) [found remaining]
                (<= (count remaining) 1) (throw (Exception. "INVALID_DATA"))
                :else (let [next-key (first remaining)
                            key-others (rest remaining)
                            [parsed-key next-others-key] (decode-arg next-key key-others)]
                        (if (empty? remaining) (throw (Exception. "INVALID_DATA"))
                            (let [next-val (first next-others-key)
                                  val-others (rest next-others-key)
                                  [parsed-val next-others-val] (decode-arg next-val val-others)]
                              (recur (dec cnt) next-others-val (assoc found parsed-key parsed-val))))))))
      "D" [(instant/read-instant-calendar arg) other-args]

      ;; TODO: Array (A), Map (M), Date/Time (D)
      (throw (Exception. (str "Type unsupported: " type))))))

(defn- base64-decode [s]
  (-> (Base64/getDecoder) (.decode s) bytes))

(t/deftest base64-decode-test
  (t/is (= "hello world" (String. (base64-decode "aGVsbG8gd29ybGQ=")))))

(defn- base64-encode-str [s]
  (let [b (if (string? s) (str-to-bytes s) s)]
    (-> (Base64/getEncoder) (.encodeToString b))))

(t/deftest base64-encode-str-test
  (t/is (= "aGVsbG8gd29ybGQ=" (base64-encode-str "hello world"))))

(defn- make-register [name]
  (with-meta {:type :register :key name} {:register true}))
(defn- is-register? [val]
  (and
   (-> val meta :register true?)
   (-> val map?)
   (-> val :key nil? not)))

(t/deftest register-test
  (t/is (is-register? (make-register (random-uuid)))))

(defmulti decode-arg$ :proto)
(defmethod decode-arg$ 1 [{:keys [arg other-args]}]
  (cond
    (str/starts-with? arg "@") [(make-register (subs arg 1)) other-args]
    (str/starts-with? arg "$") [(with-meta {:type :global :key (subs arg 1)} {:global true}) other-args]
    (not (nil? (str/index-of arg ":"))) (let [[type a] (str/split arg #":")]
                                          (decode-typed-arg-v1 type a other-args))
    ;; TODO: Hex decoding
    (not (nil? (str/index-of arg "#"))) (let [[type rawArg] (str/split arg #"#")]
                                          (decode-typed-arg-v1 type (base64-decode rawArg) other-args))
    :else (throw (Exception. (str "Cannot decode value " arg)))))

(t/deftest decode-arg-proto1-test
  (t/is (= [23.0 ["I:2" "F:2"]] (decode-arg$ {:proto 1 :arg "R:23" :other-args ["I:2" "F:2"]})))
  (t/is (= [true ["I:2" "F:2"]] (decode-arg$ {:proto 1 :arg "F:1" :other-args ["I:2" "F:2"]})))
  (t/is (= [false ["I:2" "F:2"]] (decode-arg$ {:proto 1 :arg "F:0" :other-args ["I:2" "F:2"]})))
  (t/is (instance? java.sql.Timestamp (first (decode-arg$ {:proto 1 :arg "U:123908" :other-args ["I:2" "F:2"]}))))
  (t/is (instance? java.util.GregorianCalendar (first (decode-arg$ {:proto 1 :arg "D:2022-06-04T21:23:43.123456789+01:22" :other-args ["I:2" "F:2"]}))))
  (t/is (= [[2 true] ["B:23"]] (decode-arg$ {:proto 1 :arg "A:2" :other-args ["I:2" "F:1" "B:23"]})))
  (t/is (= [{2 true "23" "hello"} ["B:cool"]] (decode-arg$ {:proto 1 :arg "M:2" :other-args ["I:2" "F:1" "S:23" "S:hello" "B:cool"]})))
  (t/is (= [23 ["I:2" "F:2"]] (decode-arg$ {:proto 1 :arg "I:23" :other-args ["I:2" "F:2"]})))
  (t/is (= ["hello world#" ["I:2" "F:2"]] (decode-arg$ {:proto 1 :arg "S#aGVsbG8gd29ybGQj" :other-args ["I:2" "F:2"]})))
  (t/is (bytes? (first (decode-arg$ {:proto 1 :arg "B:23" :other-args ["I:2" "F:2"]}))))
  (t/is (= ["hello" ["I:2" "F:2"]] (decode-arg$ {:proto 1 :arg "S:hello" :other-args ["I:2" "F:2"]}))))

(defn- decode-arg [arg other-args]
  (decode-arg$ {:proto @proto :arg arg :other-args other-args}))

(defn- args-of [segments]
  (let [args (if (not= "" (label-of segments))
               (drop 2 segments)
               (rest segments))]
    (loop [args args parsed []]
      (if (empty? args)
        parsed
        (let [as (rest args)
              arg (first args)
              [parsed-val new-args] (decode-arg arg as)]
          (recur  new-args (conj parsed parsed-val)))))))

(defmulti encode-arg$ :proto)
(defmethod encode-arg$ 1 [{:keys [arg]}]
  (cond
    (is-register? arg) (str "@" (:key arg))
    (record? arg) (str "S:" NON_TRANSFERABLE)
    (string? arg) (if (and (re-matches #"^[!-~]*$" arg) (not= arg NON_TRANSFERABLE))
                    (str "S:" arg)
                    (str "S#" (base64-encode-str arg)))
    (keyword? arg) (str "S:" (subs (str arg) 1))
    (int? arg) (str "I:" arg)
    (float? arg) (str "R:" arg)
    (boolean? arg) (str "F:" (if arg "1" "0"))
    (bytes? arg) (str "B#" (base64-encode-str arg))
    (nil? arg) "N:"
    (symbol? arg) (str "S:" NON_TRANSFERABLE)
    (vector? arg) (let [vals (str/join " " (map (fn [x] (encode-arg$ {:proto 1 :arg x})) arg))
                        sep (if (empty? vals) "" " ")]
                    (str "A:" (count arg) sep vals))
    (set? arg) (let [vals (str/join " " (map (fn [x] (encode-arg$ {:proto 1 :arg x})) arg))
                     sep (if (empty? vals) "" " ")]
                 (str "A:" (count arg) sep vals))
    (seq? arg) (let [vals (str/join " " (map (fn [x] (encode-arg$ {:proto 1 :arg x})) arg))
                     sep (if (empty? vals) "" " ")]
                 (str "A:" (count arg) sep vals))
    (map? arg) (let [vals (str/join " " (apply concat (map
                                                       (fn [[k v]]
                                                         [(encode-arg$ {:proto 1 :arg k})
                                                          (encode-arg$ {:proto 1 :arg v})])
                                                       arg)))
                     sep (if (empty? vals) "" " ")]
                 (str "M:" (count arg) sep vals))
    (instance? java.sql.Timestamp arg) (str "U:" (long (/ (.getTime arg) 1000)))
    (instance? java.util.GregorianCalendar arg) (encode-arg$ {:proto 1 :arg (.toZonedDateTime arg)})
    (instance? java.time.ZonedDateTime arg) (encode-arg$ {:proto 1 :arg (.toOffsetDateTime arg)})
    (instance? java.time.OffsetDateTime arg) (let [fmt java.time.format.DateTimeFormatter/ISO_OFFSET_DATE_TIME]
                                               (str "D:" (str/replace (-> fmt (.format arg)) "Z" "+00:00")))
    (instance? java.time.LocalDateTime arg) (let [offset java.time.ZoneOffset/UTC
                                                  offsetDt (-> arg (.atOffset offset))]
                                              (encode-arg$ {:proto 1 :arg offsetDt}))
    (instance? java.time.LocalDate arg) (let [fmt java.time.format.DateTimeFormatter/ISO_DATE]
                                          (str "D:" (-> fmt (.format arg))))
    (or (instance? java.util.Date arg)
        (isa? (class arg) java.util.Date)) (let [dt (-> arg
                                                        (.toInstant)
                                                        (.atZone (java.time.ZoneId/systemDefault))
                                                        (.toLocalDateTime))]
                                             (encode-arg$ {:proto 1 :arg dt}))
    (instance? java.time.LocalTime arg) (let [fmt java.time.format.DateTimeFormatter/ISO_TIME]
                                          (str "D:" (-> fmt (.format arg))))
    (instance? java.time.OffsetTime arg) (let [fmt java.time.format.DateTimeFormatter/ISO_TIME]
                                           (str "D:" (-> fmt (.format arg))))
    :else  (str "S:" NON_TRANSFERABLE)))

(defn- encode-arg [arg]
  (encode-arg$ {:proto @proto :arg arg}))

(t/deftest encode-arg-test
  (t/is (= "A:2 S:find M:1 S:a I:3" (encode-arg ["find" {:a 3}])))
  (t/is (= "N:" (encode-arg nil)))
  (t/is (= "M:1 S:a I:2" (encode-arg {"a" 2})))
  (t/is (= "A:1 M:1 I:3 S:34" (encode-arg #{{3 :34}})))
  (t/is (= "M:0" (encode-arg {})))
  (t/is (= "A:2 S:a I:2" (encode-arg ["a" 2])))
  (t/is (= "@test" (encode-arg (make-register "test"))))
  (t/is (= "A:2 S:a I:2" (encode-arg '("a" 2))))
  (t/is (= "B#aGVsbG8=" (encode-arg (str-to-bytes "hello"))))
  (t/is (= "S:hello" (encode-arg "hello")))
  (t/is (= "S#aGVsbG8gd29ybGQ=" (encode-arg "hello world")))
  (t/is (= "I:23" (encode-arg 23)))
  (t/is (= "R:23.23" (encode-arg 23.23)))
  (t/is (= "F:1" (encode-arg true)))
  (t/is (= "F:0" (encode-arg false)))
  (t/is (= "U:1232123" (encode-arg (java.sql.Timestamp. (long 1232123000)))))
  (t/is (= "D:2015-02-04T21:07:10+00:00" (encode-arg (java.util.Date. 1423109230000))))
  (t/is (= "D:2022-03-04T12:23:43+00:00" (encode-arg (java.time.LocalDateTime/of 2022 3 4 12 23 43))))
  (t/is (= "D:2012-04-29T04:02:04+12:32" (encode-arg (java.time.OffsetDateTime/of 2012 04 29 4 2 4 0 (java.time.ZoneOffset/ofHoursMinutes 12 32)))))
  (t/is (= "D:2000-03-12T09:04:02-07:00" (encode-arg (java.time.ZonedDateTime/of 2000 3 12 9 4 2 0 (java.time.ZoneId/of "America/Denver")))))
  (t/is (= "D:2023-10-02" (encode-arg (java.time.LocalDate/of 2023 10 2))))
  (t/is (= "D:2022-02-03T01:20:34+01:20" (encode-arg (instant/read-instant-calendar "2022-02-03T01:20:34+01:20"))))
  (t/is (= "D:2022-02-02T17:00:34+00:00" (encode-arg (instant/read-instant-date "2022-02-03T01:20:34+01:20"))))
  (t/is (= "U:1643846434" (encode-arg (instant/read-instant-timestamp "2022-02-03T01:20:34+01:20")))))

(defmulti proto-encode$ :proto)
(defmethod proto-encode$ 1 [{:keys [label cmd args]}]
  (let [label-str (if (not= "" label) (str "%" label " ") "")
        cmd-str (:v1 cmd) args-str (map encode-arg args)] (str label-str cmd-str " " (str/join " " args-str))))

(defn- proto-encode [label [cmd args]]
  (proto-encode$ {:proto @proto :label label :cmd cmd :args args}))

(t/deftest proto-encode-test
  (t/is (= "VAL I:23" (proto-encode "" [(get v1-commands "VAL") [23]])))
  (t/is (= "%lbl VAL I:23" (proto-encode "lbl" [(get v1-commands "VAL") [23]])))
  (t/is (= "%label  " (proto-encode "label" []))))

(defn- read-lines [in]
  (println "RDY")
  (doseq [ln (line-seq (java.io.BufferedReader. in))]
    (let [l (str/trim ln)]
      (when (not-empty l)
        (let [segments (str/split l #" ")
              label (label-of segments)]
          (try
            (let [command (command-of segments)
                  args (args-of segments)
                  res (exec-command command args)]
              (println (proto-encode label res)))
            (catch Exception e
              (println (proto-encode label [(get v1-commands "ERR") [(-> e .getMessage)]]))
              (when @strict (throw e)))))))))

(defn- free [v]
  (when (isa? (class v) java.io.Closeable)
    (.close v)))

(defn- resolve-value [arg]
  (cond
    (is-register? arg) (let [r @refs]
                         (if (not (contains? r (:key arg)))
                           (throw (Exception. (str "Invalid register " arg)))
                           (get r (:key arg))))
    (:global (meta arg)) (case (:key arg)
                           "conf" @conf
                           "proto" @proto
                           "refs" (map first @refs)
                           "protos" protos
                           "drivers" drivers
                           "name" plugin-name
                           "version" version
                           "docstring" docstring
                           "generators" generators
                           "strict" @strict
                           "async" async
                           (throw (Exception. (str "Invalid global" arg))))
    :else arg))

(defn- get-target
  ([target] (get-target target ""))
  ([target prefix] (if (is-register? target)
                     (:key target)
                     (str prefix (str/replace (random-uuid) "-" "")))))

(defn- set-register [key val]
  (swap! refs (fn [x]
                (when (contains? x key)
                  (free (get x key)))
                (assoc x key val))))

(defn- remove-register [target]
  (swap! refs
         (fn [r]
           (if (contains? r target)
             (do
               (free (get r target))
               (dissoc r target))
             r))))

(defn- exec-command [cmd args]
  (case cmd
    "ACK" (throw (Exception. "ACK is response only"))
    "NSO" (throw (Exception. "NSO is response only"))
    "ERR" (throw (Exception. "ERR is response only"))
    "VAL" (if (not= 1 (count args))
            (throw (Exception. "Expected 1 arg for VAL"))
            [(get v1-commands "VAL") [(resolve-value (first args))]])
    "USE" (if (not= 1 (count args))
            (throw (Exception. (str "Expected 1 argument to USE")))
            (if (nil? (some #{(first args)} protos))
              (throw (Exception. (str "Unknown protocol " (first args))))
              (do
                (reset! proto (first args))
                [(get v1-commands "ACK")])))
    "STR" (let [f (first args)]
            (if (or (not= 1 (count args)) (not (boolean? f)))
              (throw (Exception. "Expected 1 boolean argument to SET"))
              (do
                (reset! strict (first args))
                [(get v1-commands "ACK")])))
    "PNG" (if (not= 1 (count args))
            (throw (Exception. "Expected 1 arg for PNG"))
            (let [conn (resolve-value (first args))]
              (when-not (satisfies? DbDriver conn)
                (throw (Exception. "Must use on driver")))
              [(get v1-commands "ACK")]))
    "VLD" [(get v1-commands "VAL") [true]]
    "BYE" (do
            (doall (map (comp second free)) @refs)
            (System/exit 0))
    "ABT" [(get v1-commands "ACK")]
    "CNF" (if (not= 2 (count args))
            (throw (Exception. "Expected exactly 2 arguments to CNF"))
            (if (not (string? (first args)))
              (throw (Exception. "Expected string for CNF key"))
              (do
                (swap! conf (fn [x] (assoc x (first args) (second args))))
                [(get v1-commands "ACK")])))
    "RST" (if (seq args)
            (throw (Exception. "Expected 0 args to RST"))
            (do (swap! refs (fn [r]
                              (doall (map (comp second free) r))
                              {}))
                [(get v1-commands "ACK")]))
    "ENB" [(get v1-commands "NSO")]
    "DIS" [(get v1-commands "NSO")]
    "LMT" (let [num-args (count args)
                driver? (= (first args) "DRV")]
            (cond
              (and driver? (= num-args 2)) [(get v1-commands "VAL") [(driver-limits (second args))]]
              (and driver? (= num-args 3)) [(get v1-commands "VAL") [(driver-limits (second args) (nth args 2))]]
              :else [(get v1-commands "VAL") [nil]]))
    "HLP" (let [num-args (count args)
                driver? (= (first args) "DRV")]
            (cond
              (and driver? (= num-args 2)) [(get v1-commands "VAL") [(driver-help (second args))]]
              (and driver? (= num-args 3)) [(get v1-commands "VAL") [(driver-help (second args) (nth args 2))]]
              :else [(get v1-commands "VAL") [nil]]))

    "ALC" (if (or (empty? args) (< 2 (count args)))
            (throw (Exception. "Expected 1 or 2 args to ALC"))
            (let [val (first args)
                  target-arg (second args)
                  target (get-target target-arg)]
              (set-register target val)
              [(get v1-commands "VAL") [(make-register target)]]))
    "FRE" (if (or (not= 1 (count args)) (not (is-register? (first args))))
            (throw (Exception. "Expected 1 register to FRE"))
            (let [target (first args)]
              (remove-register target)
              [(get v1-commands "ACK")]))

    "CON" (if (or (empty? args) (< 2 (count args)))
            (throw (Exception. "Expected 1 or 2 args for CON"))
            (let [cnf @conf
                  conn (get-connection cnf)
                  target-arg (second args)
                  target (get-target target-arg "conn")]
              (swap! refs (fn [x] (assoc x target conn)))
              [(get v1-commands "VAL") [(make-register target)]]))
    "PRE" (if (or (< (count args) 2) (< 3 (count args)))
            (throw (Exception. "Expected 2 or 3 args for PRE"))
            (let [conn (resolve-value (first args))
                  to-prep (second args)
                  target-arg (first (drop 2 args))
                  target (get-target target-arg "stmt")
                  data (prepare conn to-prep)]
              (swap! refs (fn [x] (assoc x target data)))
              [(get v1-commands "VAL") [(make-register target)]]))
    "QRY" (if (or (< (count args) 3) (< 4 (count args)))
            (throw (Exception. "Expected 3 or 4 args for QRY"))
            (let [conn (resolve-value (first args))
                  stmt (resolve-value (second args))
                  arg (resolve-value (first (drop 2 args)))
                  res (query conn stmt arg)
                  target (get-target (first (drop 3 args)) "res")]
              (swap! refs (fn [x] (assoc x target res)))
              [(get v1-commands "VAL") [(make-register target)]]))
    "EXC" (do
            (if (not= 3 (count args))
              (throw (Exception. "Expected 3 args for EXC"))
              (let [conn (resolve-value (first args))
                    stmt (resolve-value (second args))
                    arg (resolve-value (nth args 2))
                    res (execute conn stmt arg)
                    id (:last-insert-id res)
                    num-affected (:rows-affected res)]
                [(get v1-commands "VAL") [{"lastInsertId" id "numAffected" num-affected}]])))
    "ROW" (if (or (not (is-register? (first args))) (not= 1 (count args)))
            (throw (Exception. "Expected 1 register for ROW"))
            (let [res (resolve-value (first args))]
              (when-not (satisfies? QueryResult res)
                (throw (Exception. "Can only use ROW on results")))
              (let [[row nxt] (row res)]
                (swap! refs (fn [x] (assoc x (:key (first args)) nxt)))
                [(get v1-commands "VAL") [row]])))
    "NXS" (if (or (not (is-register? (first args))) (not= 1 (count args)))
            (throw (Exception. "Expected 1 register for NXS"))
            (let [res (resolve-value (first args))]
              (when-not (satisfies? QueryResult res)
                (throw (Exception. "Can only use NXS on results")))
              (let [nxt (next-set res)]
                (swap! refs (fn [x] (assoc x (:key (first args)) nxt)))
                [(get v1-commands "ACK")])))
    "HNX" (if (or (not (is-register? (first args))) (not= 1 (count args)))
            (throw (Exception. "Expected 1 register for HNX"))
            (let [res (resolve-value (first args))]
              (when-not (satisfies? QueryResult res)
                (throw (Exception. "Can only use HNX on results")))
              (let [res (has-row? res)]
                [(get v1-commands "VAL") [res]])))
    "HNS" (if (or (not (is-register? (first args))) (not= 1 (count args)))
            (throw (Exception. "Expected 1 register for HNS"))
            (let [res (resolve-value (first args))]
              (when-not (satisfies? QueryResult res)
                (throw (Exception. "Can only use HNS on results")))
              (let [res (has-next-set? res)]
                [(get v1-commands "VAL") [res]])))
    "COL" (if (or (not (is-register? (first args))) (not= 1 (count args)))
            (throw (Exception. "Expected 1 register for COL"))
            (let [res (resolve-value (first args))]
              (when-not (satisfies? QueryResult res)
                (throw (Exception. "Can only use COL on results")))
              (let [res (cols res)]
                [(get v1-commands "VAL") [res]])))
    "CNL" (if (or (not (is-register? (first args))) (not= 1 (count args)))
            (throw (Exception. "Expected 1 register for COL"))
            (let [res (resolve-value (first args))]
              (when-not (satisfies? QueryResult res)
                (throw (Exception. "Can only use COL on results")))
              (let [res (null-cols res)]
                [(get v1-commands "VAL") [res]])))
    "CDT" (if (or (not (is-register? (first args))) (not= 1 (count args)))
            (throw (Exception. "Expected 1 register for CDT"))
            (let [res (resolve-value (first args))]
              (when-not (satisfies? QueryResult res)
                (throw (Exception. "Can only use CDT on results")))
              (let [res (types-cols res)]
                [(get v1-commands "VAL") [res]])))
    "CTL" (if (or (not (is-register? (first args))) (not= 1 (count args)))
            (throw (Exception. "Expected 1 register for CTL"))
            (let [res (resolve-value (first args))]
              (when-not (satisfies? QueryResult res)
                (throw (Exception. "Can only use CTL on results")))
              (let [res (len-cols res)]
                [(get v1-commands "VAL") [res]])))
    "CPL" (if (or (not (is-register? (first args))) (not= 1 (count args)))
            (throw (Exception. "Expected 1 register for CPL"))
            (let [res (resolve-value (first args))]
              (when-not (satisfies? QueryResult res)
                (throw (Exception. "Can only use CPL on results")))
              (let [res (precision-cols res)]
                [(get v1-commands "VAL") [res]])))
    "BTX" [(get v1-commands "NSO")]
    "CMT" [(get v1-commands "NSO")]
    "RLB" [(get v1-commands "NSO")]
    "GNI" (if (< 2 (count args))
            (throw (Exception. "Can only have 0, 1, or 2 args on GNI"))
            (cond
              (empty? args) [(get v1-commands "VAL") generators]
              (= (count args) 1) (let [gen (generator-for (first args))]
                                   (if gen
                                     [(get v1-commands "VAL") (engine-info gen)]
                                     [(get v1-commands "NSO")]))
              (= (count args) 2) (let [gen (generator-for (first args))]
                                   (if gen
                                     [(get v1-commands "VAL") (generation-info gen (second args))]
                                     [(get v1-commands "NSO")]))))

    "GEN" (if (not= 4 (count args))
            (throw (Exception. "Must have 4 args for GEN"))
            (let [gen (generator-for (first args))]
              (if gen
                (let [res (generate gen
                                    (second args)
                                    (first (drop 2 args))
                                    (first (drop 3 args)))]
                  [(get v1-commands "VAL") [res]])

                [(get v1-commands "NSO")])))))

(t/deftest read-lines-test
  (init-conf)
  (let [out (with-out-str (read-lines (java.io.StringReader. "
LMT S:DRV S:datascript
LMT S:DRV S:datascript S:BTX
HLP S:DRV S:datascript
HLP S:DRV S:datascript S:BTX")))]
    (t/is (not (str/index-of out "ERR"))))
  (let [out (with-out-str (read-lines (java.io.StringReader. "
STR F:1
USE I:1
VLD
%cnn CON $conf @conn
PRE @conn S#WzpmaW5kID9pZCA/bmFtZSA6d2hlcmUgWz9lIDplbnRpdHkvaWQgP2lkXSBbP2UgOmVudGl0eS9uYW1lID9uYW1lXV0= @stmt
PRE @conn S#WzpmaW5kIFs/aWQgP25hbWVdIDp3aGVyZSBbP2UgOmVudGl0eS9pZCA/aWRdIFs/ZSA6ZW50aXR5L25hbWUgP25hbWVdXQ== @stmt2
PRE @conn S#W3s6ZGIvaWQgLTEgOmVudGl0eS9pZCAxMiA6ZW50aXR5L25hbWUgImpvaG4ifSB7OmRiL2lkIC0yIDplbnRpdHkvaWQgMjMgOmVudGl0eS9uYW1lICJib2IifV0= @exec
EXC @conn @exec N:
QRY @conn @stmt A:0 @res
QRY @conn @stmt2 A:0 @res2
%col1 COL @res
%col2 COL @res2
%hnx1 HNX @res
%hnx2 HNX @res2
%row1 ROW @res
%row2 ROW @res2
%hnx3 HNX @res
%hnx4 HNX @res2
%row3 ROW @res
%row4 ROW @res2
%valbad VAL @res
ALC S:34 @alc
%valgood VAL @alc
")))]
    (t/is (not (nil? (str/index-of out "%cnn VAL @conn"))))
    (t/is (not (nil? (str/index-of out "%col1 VAL A:2 S:id S:name"))))
    (t/is (not (nil? (str/index-of out "%col2 VAL A:2 S:id S:name"))))
    (t/is (not (nil? (str/index-of out "%hnx1 VAL F:1"))))
    (t/is (not (nil? (str/index-of out "%hnx2 VAL F:1"))))
    (t/is (not (nil? (str/index-of out "%row1 VAL A:2 I:23 S:bob"))))
    (t/is (not (nil? (str/index-of out "%row2 VAL A:2 I:23 S:bob"))))
    (t/is (not (nil? (str/index-of out "%hnx3 VAL F:1"))))
    (t/is (not (nil? (str/index-of out "%hnx4 VAL F:0"))))
    (t/is (not (nil? (str/index-of out "%row3 VAL A:2 I:12 S:john"))))
    (t/is (not (nil? (str/index-of out "%row4 VAL N:"))))
    (t/is (not (nil? (str/index-of out (str "%valbad VAL S:" NON_TRANSFERABLE)))))
    (t/is (not (nil? (str/index-of out "%valgood VAL S:34")))))
  (let [out (with-out-str (read-lines (java.io.StringReader. "
VAL $conf
VAL $proto
VAL $protos
VAL $name
VAL $version
VAL $strict
VAL $async
VAL $docstring")))]
    (t/is (= 8 (count (str/split out #"\nVAL"))))))

(comment (read-lines (java.io.StringReader. "CNF S:driver S:h2
CON $conf @connH2
PRE @connH2 S#RFJPUCBUQUJMRSBJRiBFWElTVFMgdXNlcnM7IENSRUFURSBUQUJMRSBJRiBOT1QgRVhJU1RTIHVzZXJzIChpZCBJTlQgUFJJTUFSWSBLRVksIG5hbWUgVEVYVCk7IElOU0VSVCBJTlRPIHVzZXJzIChpZCwgbmFtZSkgVkFMVUVTICgxMiwgJ2pvaG4nKSwgKDIzLCAnYm9iJyk= @execH2
EXC @connH2 @execH2 N:
PRE @connH2 S#U0VMRUNUICogRlJPTSB1c2Vycw== @stmtH2
QRY @connH2 @stmtH2 A:0 @resH2
ROW @resH2
%gni1 GNI
%gni2 GNI S:h2
%gni3 GNI S:h2 S:createTable
%gen GEN S:h2 S:createTable S:UP M:2 S:name S:users S:columns M:2 S:id S:INT S:name S:TEXT
VAL $refs")))

(t/deftest read-lines-h2-test
  (init-conf)
  (let [out (with-out-str (read-lines (java.io.StringReader. "
LMT S:DRV S:h2
LMT S:DRV S:h2 S:BTX
HLP S:DRV S:h2
HLP S:DRV S:h2 S:BTX")))]
    (t/is (not (str/index-of out "ERR"))))
  (let [out (with-out-str (read-lines (java.io.StringReader. "
STR F:1
USE I:1
CNF S:driver S:h2
VLD
%cnn CON $conf @connH2
PRE @connH2 S#RFJPUCBUQUJMRSBJRiBFWElTVFMgdXNlcnM7IENSRUFURSBUQUJMRSBJRiBOT1QgRVhJU1RTIHVzZXJzIChpZCBJTlQgUFJJTUFSWSBLRVksIG5hbWUgVEVYVCk7IElOU0VSVCBJTlRPIHVzZXJzIChpZCwgbmFtZSkgVkFMVUVTICgxMiwgJ2pvaG4nKSwgKDIzLCAnYm9iJyk= @exec
EXC @connH2 @exec N:
PRE @connH2 S#U0VMRUNUICogRlJPTSB1c2Vycw== @stmtH2
PRE @connH2 S#U0VMRUNUICogRlJPTSB1c2VycyBMSU1JVCAx @stmt2H2
QRY @connH2 @stmtH2 A:0 @resH2
QRY @connH2 @stmt2H2 A:0 @res2H2
%col1 COL @resH2
%col2 COL @res2H2
%hnx1 HNX @resH2
%hnx2 HNX @res2H2
%row1 ROW @resH2
%row2 ROW @res2H2
%hnx3 HNX @resH2
%hnx4 HNX @res2H2
%row3 ROW @resH2
%row4 ROW @res2H2
%gni1 GNI
%gni2 GNI S:h2
%gni3 GNI S:h2 S:createTable
%gen GEN S:h2 S:createTable S:UP M:2 S:name S:users S:columns M:2 S:id S:INT S:name S:TEXT
")))]
    (t/is (not (nil? (str/index-of out "RDY"))))
    (t/is (not (nil? (str/index-of out "%cnn VAL @conn"))))
    (t/is (not (nil? (str/index-of out "%col1 VAL A:2 S:id S:name"))))
    (t/is (not (nil? (str/index-of out "%col2 VAL A:2 S:id S:name"))))
    (t/is (not (nil? (str/index-of out "%hnx1 VAL F:1"))))
    (t/is (not (nil? (str/index-of out "%hnx2 VAL F:1"))))
    (t/is (not (nil? (str/index-of out "%row1 VAL M:2 S:id I:12 S:name S:john"))))
    (t/is (not (nil? (str/index-of out "%row2 VAL M:2 S:id I:12 S:name S:john"))))
    (t/is (not (nil? (str/index-of out "%hnx3 VAL F:1"))))
    (t/is (not (nil? (str/index-of out "%hnx4 VAL F:0"))))
    (t/is (not (nil? (str/index-of out "%row3 VAL M:2 S:id I:23 S:name S:bob"))))
    (t/is (not (nil? (str/index-of out "%row4 VAL N:"))))
    (t/is (not (nil? (str/index-of out "%gni1 VAL S:h2"))))
    (t/is (not (nil? (str/index-of out "%gni2 VAL A:2 S:createTable M:3 S:variance A:2 S:UP S:DOWN S:summary S#U1FMIHRvIGNyZWF0ZSBhIHRhYmxl S:docstring S#VGFrZXMgYSBtYXAgb2YgJ25hbWUnLCAnY29sdW1ucycgd2hlcmUgJ2NvbHVtbnMnIGlzIGEgbWFwIG9mIG5hbWVzIGFuZCB0eXBlcw=="))))
    (t/is (not (nil? (str/index-of out "%gni3 VAL A:2 S:variance A:2 S:UP S:DOWN A:2 S:summary S#U1FMIHRvIGNyZWF0ZSBhIHRhYmxl A:2 S:docstring S#VGFrZXMgYSBtYXAgb2YgJ25hbWUnLCAnY29sdW1ucycgd2hlcmUgJ2NvbHVtbnMnIGlzIGEgbWFwIG9mIG5hbWVzIGFuZCB0eXBlcw=="))))
    (t/is (not (nil? (str/index-of out "%gen VAL S#Q1JFQVRFIFRBQkxFIElGIE5PVCBFWElTVFMgdXNlcnMgKGlkIElOVCwgbmFtZSBURVhUKTs=")))))
  (let [out (with-out-str (read-lines (java.io.StringReader. "
VAL $conf
VAL $proto
VAL $protos
VAL $name
VAL $version
VAL $strict
VAL $async
VAL $generators
VAL $docstring")))]
    (t/is (= 10 (count (str/split out #"\nVAL"))))))

(comment (t/run-all-tests))

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (init-conf)
  (read-lines *in*))

