(defproject test_plugin "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "EPL-2.0 OR GPL-2.0-or-later WITH Classpath-exception-2.0"
            :url "https://www.eclipse.org/legal/epl-2.0/"}
  :dependencies [[org.clojure/clojure "1.11.1"]
                 [com.h2database/h2 "2.3.232"]
                 [com.github.seancorfield/next.jdbc "1.3.939"]
                 [datascript "1.7.3"]]
  :main ^:skip-aot test-plugin.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})
