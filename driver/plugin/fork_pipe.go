// This is a generic sql "driver" which actually does a fork/pipe to execute a program
package plugin

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/fs"
	"os"
	"os/exec"
	"path"
	"path/filepath"
	"rove_migrate/common"
	"rove_migrate/conf"
	"strings"

	log "github.com/sirupsen/logrus"

	"github.com/BurntSushi/toml"
	"github.com/kirsle/configdir"
)

func init() {
	loadPlugins()
}

var registeredPlugins []*conf.PluginConf

type forkPipeGen struct {
	pluginConf *conf.PluginConf
}

type forkPipeActions struct {
	pluginConf *conf.PluginConf
}

func newForkPipeUrlBuilder(plugin *conf.PluginConf) common.DbUrlBuilder {
	return func(conf *common.DbConf) (string, error) {
		m := map[string]any{
			"driver":    conf.Driver(),
			"generator": conf.Generator(),
			"user":      conf.User(),
			"password":  conf.Pwd(),
			"port":      conf.Port(),
			"host":      conf.Host(),
			"database":  conf.Database(),
			"url":       conf.DriverUrl(),
			"schema":    conf.Schema(),
			"file":	     conf.File(),
		}

		b, err := json.Marshal(m)
		if err != nil {
			return "", err
		}
		return string(b), nil
	}
}

func newForkPipeGen(conf *conf.PluginConf) common.SqlGen {
	return nil
}

func loadPlugins() error {
	confDir := configdir.LocalConfig("rove_migrate")

	pluginDirs := []string{
		".plugins",
		"plugins",
		filepath.Join(confDir, ".plugins"),
		filepath.Join(confDir, "plugins"),
	}

	envPath := os.Getenv("RM_PLUGINS")
	if envPath != "" {
		pluginDirs = append(pluginDirs, filepath.Join(envPath, ".plugins"), filepath.Join("plugins"))
	}

	for _, searchDirs := range pluginDirs {
		if err := loadPluginConfsFrom(searchDirs); err != nil {
			return err
		}
	}
	return nil
}

// Parse migrations from a JSON string
func parsePluginConf(pluginConf string, configType string) (*conf.PluginConf, error) {
	var conf *conf.PluginConf

	if configType == "json" {
		err := json.Unmarshal([]byte(pluginConf), &conf)
		if err != nil {
			log.Errorf("Could not parse JSON for migration %v. Error: %v", pluginConf, err)
			return nil, fmt.Errorf("Migration JSON parsing failed with %v", err)
		}
	} else if configType == "toml" {
		err := toml.Unmarshal([]byte(pluginConf), &conf)
		if err != nil {
			log.Errorf("Could not parse TOML for migration %v. Error: %v", pluginConf, err)
			return nil, fmt.Errorf("Migration TOML parsing failed with %v", err)
		}
	} else {
		return nil, fmt.Errorf("Invalid migration type %v, cannot load manifest %v", configType, pluginConf)
	}

	if conf.Id == "" {
		return nil, fmt.Errorf("Plugin must specify a namespace! Cannot load manifest at %v", pluginConf)
	}

	illegalChars := "/.\\$?\"'!@#%^&*()+=[]{},;:"
	if strings.ContainsAny(conf.Id, illegalChars) {
		return nil, fmt.Errorf("Cannot load plugin %#v! IDs cannot contain characters %s", conf.Id, illegalChars)
	}

	for _, driver := range conf.DbDrivers {
		if strings.ContainsAny(driver, illegalChars) {
			return nil, fmt.Errorf("Cannot load plugin %#v! Invalid driver %#v. Drivers cannot contain characters %s", conf.Id, driver, illegalChars)
		}
	}

	for _, generator := range conf.DbGenerators {
		if strings.ContainsAny(generator, illegalChars) {
			return nil, fmt.Errorf("Cannot load plugin %#v! Invalid generator %#v. Generators cannot contain characters %s", conf.Id, generator, illegalChars)
		}
	}

	return conf, nil
}

// Loads migrations from a file directory
func loadPluginConfsFrom(dir string) error {
	pluginConfFiles := []string{}
	err := filepath.Walk(dir, func(path string, info os.FileInfo, err error) error {
		if errors.Is(err, fs.ErrNotExist) {
			return nil
		}

		if err != nil {
			log.Errorf("Could not load plugins at %v. Error: %v", path, err)
			return nil
		}

		if info.IsDir() {
			return nil
		}

		fileName := strings.ToLower(filepath.Base(path))
		if fileName != "manifest.json" && fileName != "manifest.toml" {
			return nil
		}

		pluginConfFiles = append(pluginConfFiles, path)
		return nil
	})

	if err != nil {
		return nil
	}

	PLUGINLOOP:
	for _, file := range pluginConfFiles {
		contents, err := os.ReadFile(file)
		if err != nil {
			log.Errorf("Could not load plugin manifest %v. Error: %v", file, err)
			return err
		}

		configType := ""
		if strings.ToLower(filepath.Ext(file)) == ".json" {
			configType = "json"
		} else if strings.ToLower(filepath.Ext(file)) == ".toml" {
			configType = "toml"
		}

		plugin, err := parsePluginConf(string(contents), configType)
		if err != nil {
			log.Errorf("Could not load plugin manifest %v. Error: %v", file, err)
			return err
		}

		if plugin.Dir == "" {
			plugin.Dir, err = filepath.Abs(filepath.Dir(file))
			if err != nil {
				return err
			}
		} else if !filepath.IsAbs(plugin.Dir) {
			dir, err := filepath.Abs(filepath.Join(filepath.Dir(file), plugin.Dir))
			if err == nil {
				plugin.Dir = dir
			}
		}

		env, err := plugin.EnvFor()
		if err != nil {
			log.Warnf("Could not load plugin %s, no matching environment for current system. Skipping. Reason: %v", plugin.Id, err)
			continue PLUGINLOOP
		}

		for _, s := range env.Setup {
			dir := plugin.Dir
			if s.Dir != "" {
				dir = s.Dir
				if !path.IsAbs(dir) {
					dir = filepath.Join(plugin.Dir, dir)
				}
			}
			if len(s.Command) == 0 {
				log.Warnf("Empty setup command for plugin %s", plugin.Id)
				continue PLUGINLOOP
			}

			cmdExec, err := exec.LookPath(s.Command[0])
			if err != nil {
				log.Errorf("Could not setup plugin %s, unable to find command %v", plugin.Id, s.Command[0])
				return err
			}

			setupCmd := exec.Command(cmdExec, s.Command[1:]...)
			setupCmd.Dir = dir
			stdout, err := setupCmd.StdoutPipe()
			if err != nil {
				log.Errorf("Unable to setup stdout pipe for plugin setup %s. %v", plugin.Id, err)
				return err
			}
			defer stdout.Close()

			stderr, err := setupCmd.StderrPipe()
			if err != nil {
				log.Errorf("Unable to setup stderr pipe for plugin setup %s. %v", plugin.Id, err)
				return err
			}
			defer stderr.Close()

			if err := setupCmd.Start(); err != nil {
				stdoutBuild := strings.Builder{}
				io.Copy(&stdoutBuild, stdout)
				stderrBuild := strings.Builder{}
				io.Copy(&stderrBuild, stderr)
				log.Errorf("Unable to start setup command %#v in %#v for plugin %s. %v. Stdout: %v. Stderr: %v", setupCmd.String(), setupCmd.Dir, plugin.Id, err, stdoutBuild.String(), stderrBuild.String())
				return err
			}

			if err := setupCmd.Wait(); err != nil {
				stdoutBuild := strings.Builder{}
				io.Copy(&stdoutBuild, stdout)
				stderrBuild := strings.Builder{}
				io.Copy(&stderrBuild, stderr)
				log.Errorf("Unable to run setup command %#v in %#v for plugin %s. %v. Stdout: %v. Stderr: %v", setupCmd.String(), setupCmd.Dir, plugin.Id, err, stdoutBuild.String(), stderrBuild.String())
				return err
			}
		}

		for _, dbDriver := range plugin.DbDrivers {
			driver := fmt.Sprintf("%s/%s", plugin.Id, dbDriver)
			if err := common.RegisterPluginDriver(driver, newForkPipeUrlBuilder(plugin), plugin); err != nil {
				log.Errorf("Cannot register driver plugin %#v", plugin.Id)
				return err
			}
		}

		for _, dbGen := range plugin.DbGenerators {
			gen := fmt.Sprintf("%s/%s", plugin.Id, dbGen)
			if err := common.RegisterPluginGenerator(gen, newForkPipeGen(plugin), plugin); err != nil {
				log.Errorf("Cannot register generator plugin %#v", plugin.Id)
				return err
			}
		}

		registeredPlugins = append(registeredPlugins, plugin)
	}
	return nil
}

func RegisteredPlugins() []*conf.PluginConf {
	return registeredPlugins
}
