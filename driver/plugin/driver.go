package plugin

import (
	"context"
	"database/sql"
	"database/sql/driver"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"os"
	"os/exec"
	"rove_migrate/common"
	"rove_migrate/conf"
	"slices"
	"strings"

	"github.com/SentimensRG/ctx/mergectx"
	log "github.com/sirupsen/logrus"
)

var driverName = "plugin"

func init() {
	if driverName != "" {
		sql.Register(driverName, &Driver{})
	}
}

type DriverConf struct {
	File     string `json:"file"`
	Host     string `json:"host"`
	User     string `json:"user"`
	Pwd      string `json:"password"`
	Port     uint   `json:"port"`
	Database string `json:"database"`
	Schema   string `json:"schema"`
	Url      string `json:"url"`
	Driver   string `json:"driver"`
}

type Driver struct{}

type Connector struct {
	driver     Driver
	driverConf *DriverConf
	conf       *conf.PluginConf
}

type Conn struct {
	ctx         context.Context
	p           *proc
	stdinClose  io.Closer
	stdoutClose io.Closer
	register    *ValRegister
}

type Stmt struct {
	register *ValRegister
	conn     *Conn
}

type Result struct {
	LastId      int64 `protoVal:"lastInsertId"`
	NumAffected int64 `protoVal:"numAffected"`
}

type Rows struct {
	register *ValRegister
	conn     *Conn
	cols     []string
}

type Tx struct {
	register *ValRegister
	conn     *Conn
}

type proc struct {
	sendContext *SendContext
	ctx         context.Context
	cancel      context.CancelFunc
}

// Connect returns a connection to the database.
// Connect may return a cached connection (one previously
// closed), but doing so is unnecessary; the sql package
// maintains a pool of idle connections for efficient re-use.
//
// The provided context.Context is for dialing purposes only
// (see net.DialContext) and should not be stored or used for
// other purposes. A default timeout should still be used
// when dialing as a connection pool may call Connect
// asynchronously to any query.
//
// The returned connection is only used by one goroutine at a
// time.
func (c Connector) Connect(ctx context.Context) (driver.Conn, error) {
	processContext, cancel := context.WithCancel(context.Background())

	env, err := c.conf.EnvFor()
	if err != nil {
		return nil, err
	}
	pluginId := c.conf.Id

	cmd := exec.CommandContext(processContext, env.Exe[0], env.Exe[1:]...)
	cmd.Dir = c.conf.Dir

	stdout, err := cmd.StdoutPipe()
	if err != nil {
		log.Errorf("Unable to setup stdout pipe for plugin start %s. %v", pluginId, err)
		return nil, err
	}

	stdin, err := cmd.StdinPipe()
	if err != nil {
		defer stdout.Close()
		log.Errorf("Unable to setup stdin pipe for plugin start %s. %v", pluginId, err)
		return nil, err
	}

	cmd.Stderr = os.Stderr

	if err := cmd.Start(); err != nil {
		defer stdout.Close()
		defer stdin.Close()
		log.Errorf("Unable to start plugin %s. %v", pluginId, err)
		return nil, err
	}

	sc, err := NewSendContext(ctx, stdin, stdout)
	if err != nil {
		return nil, err
	}

	drvr := strings.SplitN(c.driverConf.Driver, "/", 2)[1]

	cmds := []ProtoCommand{
		ProtoCommand{
			cmd:  conf_set_c,
			args: []ProtoVal{sc.ToProtoValPanic("file"), sc.ToProtoValPanic(c.driverConf.File)},
		},
		ProtoCommand{
			cmd:  conf_set_c,
			args: []ProtoVal{sc.ToProtoValPanic("host"), sc.ToProtoValPanic(c.driverConf.Host)},
		},
		ProtoCommand{
			cmd:  conf_set_c,
			args: []ProtoVal{sc.ToProtoValPanic("user"), sc.ToProtoValPanic(c.driverConf.User)},
		},
		ProtoCommand{
			cmd:  conf_set_c,
			args: []ProtoVal{sc.ToProtoValPanic("pwd"), sc.ToProtoValPanic(c.driverConf.Pwd)},
		},
		ProtoCommand{
			cmd:  conf_set_c,
			args: []ProtoVal{sc.ToProtoValPanic("driver"), sc.ToProtoValPanic(drvr)},
		},
		ProtoCommand{
			cmd:  conf_set_c,
			args: []ProtoVal{sc.ToProtoValPanic("port"), sc.ToProtoValPanic(int64(c.driverConf.Port))},
		},
		ProtoCommand{
			cmd:  conf_set_c,
			args: []ProtoVal{sc.ToProtoValPanic("database"), sc.ToProtoValPanic(c.driverConf.Database)},
		},
		ProtoCommand{
			cmd:  conf_set_c,
			args: []ProtoVal{sc.ToProtoValPanic("schema"), sc.ToProtoValPanic(c.driverConf.Schema)},
		},
		ProtoCommand{
			cmd:  connect_c,
			args: []ProtoVal{&ValGlobal{"conf"}},
		},
	}

	procChannel := make(chan error)
	commandChannel := make(chan ProtoCommand, len(cmds))

	var connRegister *ValRegister

	go func() {
		defer func() { close(procChannel) }()

		for {
			select {
			case cmd, ok := <-commandChannel:
				if !ok {
					return
				}
				_, reg, err := sc.SendReceive(ctx, cmd)
				if err != nil {
					log.Errorf("Unable to set configuration for plugin %s! %#v", pluginId, err)
					stdin.Close()
					stdout.Close()
					cancel()
					procChannel <- err
					return
				}
				switch r := reg.(type) {
				case *ValRegister:
					connRegister = r
				}
			case <-ctx.Done():
				log.Errorf("Timeout while setting configuration for plugin %s!", pluginId)
				procChannel <- fmt.Errorf("Timeout while setting up plugin %s!", pluginId)
				return
			}
		}
	}()

	for _, c := range cmds {
		commandChannel <- c
	}
	close(commandChannel)

	select {
	case err := <-procChannel:
		if err != nil {
			stdin.Close()
			stdout.Close()
			cancel()
			return nil, err
		}

		return &Conn{
			ctx,
			&proc{
				sc,
				processContext,
				cancel,
			},
			stdin,
			stdout,
			connRegister,
		}, nil
	case <-ctx.Done():
		stdin.Close()
		stdout.Close()
		cancel()
		return nil, fmt.Errorf("Timeout while setting up plugin %s!", pluginId)
	}
}

func (c Connector) Driver() driver.Driver {
	return c.driver
}

func (d Driver) OpenConnector(jsonConf string) (driver.Connector, error) {
	var conf DriverConf
	err := json.Unmarshal([]byte(jsonConf), &conf)
	if err != nil {
		return nil, err
	}

	pluginConf, err := common.PluginConfFor(conf.Driver)
	if err != nil {
		return nil, err
	}

	c := Connector{driver: d, conf: pluginConf, driverConf: &conf}

	return c, nil
}

// If a Connector implements [io.Closer], the [database/sql.DB.Close]
// method will call the Close method and return error (if any).
func (c Connector) Close() error {
	return nil
}

// Open returns a new connection to the database.
// The name is a string in a driver-specific format.
//
// Open may return a cached connection (one previously
// closed), but doing so is unnecessary; the sql package
// maintains a pool of idle connections for efficient re-use.
//
// The returned connection is only used by one goroutine at a
// time.
func (d Driver) Open(jsonConf string) (driver.Conn, error) {
	connector, err := d.OpenConnector(jsonConf)
	if err != nil {
		return nil, err
	}
	return connector.Connect(context.Background())
}

// Prepare returns a prepared statement, bound to this connection.
func (c Conn) Prepare(query string) (driver.Stmt, error) {
	return c.PrepareContext(context.Background(), query)
}

// PrepareContext returns a prepared statement, bound to this connection.
// context is for the preparation of the statement,
// it must not store the context within the statement itself.
func (c Conn) PrepareContext(ctx context.Context, query string) (driver.Stmt, error) {
	mergeCtx := mergectx.Link(ctx, c.ctx)
	
	_, res, err := c.p.sendContext.SendReceive(
		mergeCtx,
		ProtoCommand{
			"",
			prepare_c,
			[]ProtoVal{
				c.register,
				c.p.sendContext.ToProtoValPanic(query),
			},
		},
	)

	if err != nil {
		return nil, err
	}

	switch r := res.(type) {
	case *ValRegister:
		return &Stmt{r, &c}, nil
	}

	return nil, fmt.Errorf("Invalid response from driver")
}

// Close invalidates and potentially stops any current
// prepared statements and transactions, marking this
// connection as no longer in use.
//
// Because the sql package maintains a free pool of
// connections and only calls Close when there's a surplus of
// idle connections, it shouldn't be necessary for drivers to
// do their own connection caching.
//
// Drivers must ensure all network calls made by Close
// do not block indefinitely (e.g. apply a timeout).
func (c Conn) Close() error {
	ctx := context.Background()
	c.p.sendContext.SendReceive(ctx, ProtoCommand{"", bye_c, []ProtoVal{}})
	c.p.cancel()
	if err := c.stdinClose.Close(); err != nil {
		return err
	}
	if err := c.stdoutClose.Close(); err != nil {
		return err
	}
	return nil
}

// Pinger is an optional interface that may be implemented by a [Conn].
//
// If a [Conn] does not implement Pinger, the [database/sql.DB.Ping] and
// [database/sql.DB.PingContext] will check if there is at least one [Conn] available.
//
// If Conn.Ping returns [ErrBadConn], [database/sql.DB.Ping] and [database/sql.DB.PingContext] will remove
// the [Conn] from pool.
func (c Conn) Ping(ctx context.Context) error {
	mergeCtx := mergectx.Link(ctx, c.ctx)
	resCmd, _, err := c.p.sendContext.SendReceive(
		mergeCtx,
		ProtoCommand{
			"",
			ping_c,
			[]ProtoVal{c.register},
		},
	)

	if (resCmd != nil && resCmd.cmd == not_supported_c) || errors.Is(err, ProtoNotSupportedError{}) {
		return fmt.Errorf("Cannot ping with driver")
	}

	if err != nil {
		return err
	}

	if resCmd != nil && resCmd.cmd != ack_c {
		return fmt.Errorf("Incorrect response from driver")
	}

	return nil
}

// Begin starts and returns a new transaction.
//
// Deprecated: Drivers should implement ConnBeginTx instead (or additionally).
func (c Conn) Begin() (driver.Tx, error) {
	return c.BeginTx(context.Background(), driver.TxOptions{})
}

func (c Conn) exec(ctx context.Context, query ProtoVal, arg ProtoVal) (driver.Result, error) {
	resCmd, res, err := c.p.sendContext.SendReceive(
		ctx,
		ProtoCommand{
			"",
			execute_c,
			[]ProtoVal{
				c.register,
				query,
				arg,
			},
		},
	)

	if (resCmd != nil && resCmd.cmd == not_supported_c) || errors.Is(err, ProtoNotSupportedError{}) {
		return nil, fmt.Errorf("Cannot exec with driver")
	}

	if err != nil {
		return nil, err
	}

	if resCmd != nil && resCmd.cmd != value_c && resCmd.cmd != ack_c {
		return nil, fmt.Errorf("Incorrect response from driver")
	}

	if res == nil {
		return Result{0, 0}, nil
	}

	r := Result{}
	if err := c.p.sendContext.To(res, &r); err != nil {
		return nil, err
	}

	return r, nil
}

func (c Conn) driverValueToProto(args []driver.Value) (ProtoVal, error) {
	arg := &ValArray{make([]ProtoVal, 0, len(args))}

	for _, a := range args {
		val, err := c.p.sendContext.ToProtoVal(any(a))
		if err != nil {
			return nil, err
		}
		arg.elements = append(arg.elements, val)
	}

	return arg, nil
}

func (c Conn) driverNamedValueToProto(args []driver.NamedValue) (ProtoVal, error) {
	var arg ProtoVal
	if len(args) == 0 || args[0].Name == "" {
		arr := make([]ProtoVal, 0, len(args))
		slices.SortFunc(args, func(a, b driver.NamedValue) int {
			return a.Ordinal - b.Ordinal
		})

		for _, a := range args {
			val, err := c.p.sendContext.ToProtoVal(a.Value)
			if err != nil {
				return nil, err
			}
			arr = append(arr, val)
		}
		arg = &ValArray{arr}
	} else {
		keys := make([]ProtoVal, 0, len(args))
		vals := make([]ProtoVal, 0, len(args))
		for _, a := range args {
			val, err := c.p.sendContext.ToProtoVal(a.Value)
			if err != nil {
				return nil, err
			}
			key, err := c.p.sendContext.ToProtoVal(a.Name)
			if err != nil {
				return nil, err
			}
			keys = append(keys, key)
			vals = append(vals, val)
		}

		arg = &ValMap{keys: keys, values: vals}
	}

	return arg, nil
}

// Execer is an optional interface that may be implemented by a [Conn].
//
// If a [Conn] implements neither [ExecerContext] nor [Execer],
// the [database/sql.DB.Exec] will first prepare a query, execute the statement,
// and then close the statement.
//
// Exec may return [ErrSkip].
//
// Deprecated: Drivers should implement [ExecerContext] instead.
func (c Conn) Exec(query string, args []driver.Value) (driver.Result, error) {
	arg, err := c.driverValueToProto(args)

	if err != nil {
		return nil, err
	}

	return c.exec(c.ctx, c.p.sendContext.ToProtoValPanic(query), arg)
}

// ExecerContext is an optional interface that may be implemented by a [Conn].
//
// If a [Conn] does not implement [ExecerContext], the [database/sql.DB.Exec]
// will fall back to [Execer]; if the Conn does not implement Execer either,
// [database/sql.DB.Exec] will first prepare a query, execute the statement, and then
// close the statement.
//
// ExecContext may return [ErrSkip].
//
// ExecContext must honor the context timeout and return when the context is canceled.
func (c Conn) ExecContext(ctx context.Context, query string, args []driver.NamedValue) (driver.Result, error) {
	arg, err := c.driverNamedValueToProto(args)

	if err != nil {
		return nil, err
	}

	mergedCtx := mergectx.Link(ctx, c.ctx)
	return c.exec(mergedCtx, c.p.sendContext.ToProtoValPanic(query), arg)
}

// Queryer is an optional interface that may be implemented by a [Conn].
//
// If a [Conn] implements neither [QueryerContext] nor [Queryer],
// the [database/sql.DB.Query] will first prepare a query, execute the statement,
// and then close the statement.
//
// Query may return [ErrSkip].
//
// Deprecated: Drivers should implement [QueryerContext] instead.
func (c Conn) Query(query string, args []driver.Value) (driver.Rows, error) {
	cmdArgs, err := c.driverValueToProto(args)

	if err != nil {
		return nil, err
	}


	return c.query(c.ctx, c.p.sendContext.ToProtoValPanic(query), cmdArgs)
}

func (c Conn) colsFor(reg ProtoVal) (*ValRegister, []string, error) {
	switch r := reg.(type) {
	case *ValRegister:
		var cols []string
		resCmd, colsRes, err := c.p.sendContext.SendReceive(c.ctx, ProtoCommand{
			"",
			column_info_c,
			[]ProtoVal{
				r,
			},
		})

		if (resCmd != nil && resCmd.cmd == not_supported_c) || errors.Is(err, ProtoNotSupportedError{}) {
			return r, nil, fmt.Errorf("Cannot get columns with driver")
		}

		if err != nil {
			return r, nil, err
		} else if err := c.p.sendContext.To(colsRes, &cols); err != nil {
			return r, nil, err
		}

		return r, cols, nil
	}
	return nil, nil, fmt.Errorf("Invalid register of type %T", reg)
}

func (c Conn) query(ctx context.Context, query ProtoVal, arg ProtoVal) (driver.Rows, error) {
	resCmd, response, err := c.p.sendContext.SendReceive(ctx, ProtoCommand{
		"",
		query_c,
		[]ProtoVal{
			c.register,
			query,
			arg,
		},
	})

	if (resCmd != nil && resCmd.cmd == not_supported_c) || errors.Is(err, ProtoNotSupportedError{}) {
		return nil, fmt.Errorf("Cannot get query from driver")
	}

	if err != nil {
		return nil, err
	}

	reg, cols, err := c.colsFor(response)

	if err != nil {
		if reg != nil {
			return Rows{reg, &c, nil}, nil
		}
		return nil, err
	}

	return Rows{reg, &c, cols}, nil
}

// QueryerContext is an optional interface that may be implemented by a [Conn].
//
// If a [Conn] does not implement QueryerContext, the [database/sql.DB.Query]
// will fall back to [Queryer]; if the [Conn] does not implement [Queryer] either,
// [database/sql.DB.Query] will first prepare a query, execute the statement, and then
// close the statement.
//
// QueryContext may return [ErrSkip].
//
// QueryContext must honor the context timeout and return when the context is canceled.
func (c Conn) QueryContext(ctx context.Context, query string, args []driver.NamedValue) (driver.Rows, error) {
	arg, err := c.driverNamedValueToProto(args)

	if err != nil {
		return nil, err
	}

	mergeCtx := mergectx.Link(ctx, c.ctx)
	return c.query(mergeCtx, c.p.sendContext.ToProtoValPanic(query), arg)
}

// SessionResetter may be implemented by [Conn] to allow drivers to reset the
// session state associated with the connection and to signal a bad connection.
//
// ResetSession is called prior to executing a query on the connection
// if the connection has been used before. If the driver returns ErrBadConn
// the connection is discarded.
func (c Conn) ResetSession(ctx context.Context) error {
	mergeCtx := mergectx.Link(ctx, c.ctx)
	_, _, err := c.p.sendContext.SendReceive(mergeCtx, ProtoCommand{
		"",
		reset_c,
		[]ProtoVal{},
	})

	if err != nil {
		return err
	}

	return nil
}

// BeginTx starts and returns a new transaction.
// If the context is canceled by the user the sql package will
// call Tx.Rollback before discarding and closing the connection.
//
// This must check opts.Isolation to determine if there is a set
// isolation level. If the driver does not support a non-default
// level and one is set or if there is a non-default isolation level
// that is not supported, an error must be returned.
//
// This must also check opts.ReadOnly to determine if the read-only
// value is true to either set the read-only transaction property if supported
// or return an error if it is not supported.
func (c Conn) BeginTx(ctx context.Context, opts driver.TxOptions) (driver.Tx, error) {
	mergeCtx := mergectx.Link(ctx, c.ctx)
	_, res, err := c.p.sendContext.SendReceive(mergeCtx, ProtoCommand{
		"",
		begin_transaction_c,
		[]ProtoVal{},
	})

	if err != nil {
		return nil, err
	}

	switch r := res.(type) {
	case *ValRegister:
		return Tx{r, &c}, nil
	}

	return nil, fmt.Errorf("Unexpected driver response")
}

// Validator may be implemented by [Conn] to allow drivers to
// signal if a connection is valid or if it should be discarded.
//
// If implemented, drivers may return the underlying error from queries,
// even if the connection should be discarded by the connection pool.
// IsValid is called prior to placing the connection into the
// connection pool. The connection will be discarded if false is returned.
func (c Conn) IsValid() bool {
	_, res, err := c.p.sendContext.SendReceive(c.ctx, ProtoCommand{
		"",
		valid_c,
		[]ProtoVal{},
	})

	if err != nil {
		return false
	}

	switch r := res.(type) {
	case *ValBool:
		return r.val
	}

	return false
}

// Close closes the statement.
//
// As of Go 1.1, a Stmt will not be closed if it's in use
// by any queries.
//
// Drivers must ensure all network calls made by Close
// do not block indefinitely (e.g. apply a timeout).
func (s Stmt) Close() error {
	_, _, err := s.conn.p.sendContext.SendReceive(s.conn.p.ctx, ProtoCommand{"", free_c, []ProtoVal{s.register}})
	return err
}

// NumInput returns the number of placeholder parameters.
//
// If NumInput returns >= 0, the sql package will sanity check
// argument counts from callers and return errors to the caller
// before the statement's Exec or Query methods are called.
//
// NumInput may also return -1, if the driver doesn't know
// its number of placeholders. In that case, the sql package
// will not sanity check Exec or Query argument counts.
func (s Stmt) NumInput() int {
	_, res, err := s.conn.p.sendContext.SendReceive(s.conn.p.ctx, ProtoCommand{"", count_num_input_c, []ProtoVal{s.register}})
	if err != nil {
		return -1
	}

	switch r := res.(type) {
	case *ValInt:
		return int(r.val)
	}
	return -1
}

// LastInsertId returns the database's auto-generated ID
// after, for example, an INSERT into a table with primary
// key.
func (r Result) LastInsertId() (int64, error) {
	return r.LastId, nil
}

// RowsAffected returns the number of rows affected by the
// query.
func (r Result) RowsAffected() (int64, error) {
	return r.NumAffected, nil
}

// Exec executes a query that doesn't return rows, such
// as an INSERT or UPDATE.
//
// Deprecated: Drivers should implement StmtExecContext instead (or additionally).
func (s Stmt) Exec(args []driver.Value) (driver.Result, error) {
	arg, err := s.conn.driverValueToProto(args)

	if err != nil {
		return nil, err
	}

	return s.conn.exec(s.conn.ctx, s.register, arg)
}

// Query executes a query that may return rows, such as a
// SELECT.
//
// Deprecated: Drivers should implement StmtQueryContext instead (or additionally).
func (s Stmt) Query(args []driver.Value) (driver.Rows, error) {
	arg, err := s.conn.driverValueToProto(args)

	if err != nil {
		return nil, err
	}

	return s.conn.query(s.conn.ctx, s.register, arg)
}

// ExecContext executes a query that doesn't return rows, such
// as an INSERT or UPDATE.
//
// ExecContext must honor the context timeout and return when it is canceled.
func (s Stmt) ExecContext(ctx context.Context, args []driver.NamedValue) (driver.Result, error) {
	arg, err := s.conn.driverNamedValueToProto(args)

	if err != nil {
		return nil, err
	}

	mergeCtx := mergectx.Link(ctx, s.conn.ctx)
	return s.conn.exec(mergeCtx, s.register, arg)
}

// QueryContext executes a query that may return rows, such as a
// SELECT.
//
// QueryContext must honor the context timeout and return when it is canceled.
func (s Stmt) QueryContext(ctx context.Context, args []driver.NamedValue) (driver.Rows, error) {
	arg, err := s.conn.driverNamedValueToProto(args)

	if err != nil {
		return nil, err
	}

	mergeCtx := mergectx.Link(ctx, s.conn.ctx)
	return s.conn.query(mergeCtx, s.register, arg)
}

// Columns returns the names of the columns. The number of
// columns of the result is inferred from the length of the
// slice. If a particular column name isn't known, an empty
// string should be returned for that entry.
func (r Rows) Columns() []string {
	return r.cols
}

// Close closes the rows iterator.
func (r Rows) Close() error {
	_, _, err := r.conn.p.sendContext.SendReceive(r.conn.ctx, ProtoCommand{
		"",
		free_c,
		[]ProtoVal{r.register},
	})

	if err != nil {
		return err
	}
	return nil
}

// Next is called to populate the next row of data into
// the provided slice. The provided slice will be the same
// size as the Columns() are wide.
//
// Next should return io.EOF when there are no more rows.
//
// The dest should not be written to outside of Next. Care
// should be taken when closing Rows not to modify
// a buffer held in dest.
func (r Rows) Next(dest []driver.Value) error {
	_, hasNext, err := r.conn.p.sendContext.SendReceive(r.conn.ctx, ProtoCommand{
		"",
		has_next_row_c,
		[]ProtoVal{r.register},
	})

	if err != nil {
		return err
	}
		
	switch r := hasNext.(type) {
	case *ValBool:
		if !r.val {
			return io.EOF
		}
	default:
		return io.EOF
	}

	_, rows, err := r.conn.p.sendContext.SendReceive(r.conn.ctx, ProtoCommand{
		"",
		row_c,
		[]ProtoVal{r.register},
	})

	if err != nil {
		return err
	}

	switch row := rows.(type) {
	case *ValArray:
		if len(row.elements) < len(dest) {
			return io.EOF
		}

		for i := range dest {
			if err := r.conn.p.sendContext.To(row.elements[i], &dest[i]); err != nil {
				return err
			}
		}
	case *ValMap:
		if len(row.keys) < len(dest) {
			return io.EOF
		}
		var m map[string]any
		if err := r.conn.p.sendContext.To(row, &m); err != nil {
			return err
		}

		for i := range dest {
			key := r.cols[i]

			v, ok := m[key]
			if !ok {
				return io.EOF
			}

			dest[i] = v
		}
	default:
		return io.EOF
	}

	return nil
}

// HasNextResultSet is called at the end of the current result set and
// reports whether there is another result set after the current one.
func (r Rows) HasNextResultSet() bool {
	_, res, err := r.conn.p.sendContext.SendReceive(r.conn.ctx, ProtoCommand{
		"",
		has_next_set_c,
		[]ProtoVal{r.register},
	})

	if err != nil {
		return false
	}

	switch rs := res.(type) {
	case *ValBool:
		return rs.val
	}
	return false
}

// NextResultSet advances the driver to the next result set even
// if there are remaining rows in the current result set.
//
// NextResultSet should return io.EOF when there are no more result sets.
func (r Rows) NextResultSet() error {
	_, _, err := r.conn.p.sendContext.SendReceive(r.conn.ctx, ProtoCommand{
		"",
		next_set_c,
		[]ProtoVal{r.register},
	})

	if err != nil {
		return err
	}
	
	return nil
}

// RowsColumnTypeDatabaseTypeName may be implemented by [Rows]. It should return the
// database system type name without the length. Type names should be uppercase.
// Examples of returned types: "VARCHAR", "NVARCHAR", "VARCHAR2", "CHAR", "TEXT",
// "DECIMAL", "SMALLINT", "INT", "BIGINT", "BOOL", "[]BIGINT", "JSONB", "XML",
// "TIMESTAMP".
func (r Rows) ColumnTypeDatabaseTypeName(index int) string {
	_, res, err := r.conn.p.sendContext.SendReceive(r.conn.ctx, ProtoCommand{
		"",
		column_type_c,
		[]ProtoVal{r.register, r.conn.p.sendContext.ToProtoValPanic(index)},
	})

	if err != nil {
		return "UNKNOWN"
	}

	switch rs := res.(type) {
	case *ValString:
		return rs.val
	case *ValBinary:
		return string(rs.val)
	}
	return "UNKNOWN"
}

// RowsColumnTypeLength may be implemented by [Rows]. It should return the length
// of the column type if the column is a variable length type. If the column is
// not a variable length type ok should return false.
// If length is not limited other than system limits, it should return [math.MaxInt64].
// The following are examples of returned values for various types:
//
//	TEXT          (math.MaxInt64, true)
//	varchar(10)   (10, true)
//	nvarchar(10)  (10, true)
//	decimal       (0, false)
//	int           (0, false)
//	bytea(30)     (30, true)
func (r Rows) ColumnTypeLength(index int) (length int64, ok bool) {
	_, res, err := r.conn.p.sendContext.SendReceive(r.conn.ctx, ProtoCommand{
		"",
		column_type_len_c,
		[]ProtoVal{r.register, r.conn.p.sendContext.ToProtoValPanic(index)},
	})

	if err != nil {
		return 0, false
	}

	switch rs := res.(type) {
	case *ValInt:
		return rs.val, true
	}
	return 0, false
}

// RowsColumnTypeNullable may be implemented by [Rows]. The nullable value should
// be true if it is known the column may be null, or false if the column is known
// to be not nullable.
// If the column nullability is unknown, ok should be false.
func (r Rows) ColumnTypeNullable(index int) (nullable, ok bool) {
	_, res, err := r.conn.p.sendContext.SendReceive(r.conn.ctx, ProtoCommand{
		"",
		column_nullable_c,
		[]ProtoVal{r.register, r.conn.p.sendContext.ToProtoValPanic(index)},
	})

	if err != nil {
		return false, false
	}

	switch rs := res.(type) {
	case *ValBool:
		return rs.val, true
	}
	return false, false
}

// RowsColumnTypePrecisionScale may be implemented by [Rows]. It should return
// the precision and scale for decimal types. If not applicable, ok should be false.
// The following are examples of returned values for various types:
//
//	decimal(38, 4)    (38, 4, true)
//	int               (0, 0, false)
//	decimal           (math.MaxInt64, math.MaxInt64, true)
func (r Rows) ColumnTypePrecisionScale(index int) (precision, scale int64, ok bool) {
	_, res, err := r.conn.p.sendContext.SendReceive(r.conn.ctx, ProtoCommand{
		"",
		column_precision_len_c,
		[]ProtoVal{r.register, r.conn.p.sendContext.ToProtoValPanic(index)},
	})

	if err != nil {
		return 0, 0, false
	}

	switch rs := res.(type) {
	case *ValArray:
		if len(rs.elements) != 2 {
			return 0, 0, false
		}
		if err := r.conn.p.sendContext.To(rs.elements[0], &precision); err != nil {
			return 0, 0, false
		}
		if err := r.conn.p.sendContext.To(rs.elements[0], &scale); err != nil {
			return 0, 0, false
		}
		return precision, scale, true
	}

	return 0, 0, false
}

func (tx Tx) Commit() error {
	_, _, err := tx.conn.p.sendContext.SendReceive(tx.conn.ctx, ProtoCommand{
		"",
		commit_transaction_c,
		[]ProtoVal{tx.register},
	})

	if err != nil {
		return err
	}

	return nil
}

func (tx Tx) Rollback() error {
	_, _, err := tx.conn.p.sendContext.SendReceive(tx.conn.ctx, ProtoCommand{
		"",
		rollback_transaction_c,
		[]ProtoVal{tx.register},
	})

	if err != nil {
		return err
	}

	return nil
}

