package plugin

import (
	"bufio"
	"cmp"
	"context"
	"errors"
	"fmt"
	"io"
	"reflect"
	"slices"
	"strings"
	"sync"
	"time"
)

type p2 struct {
	numParams int
	code      uint8
}

type Command struct {
	proto1 string
	proto2 []p2
	proto3 string
}

type Proto1ValueParser func(raw []byte) (ProtoVal, error)

type ProtoType struct {
	Proto1       string
	Proto2       uint8
	Proto1Parser Proto1ValueParser
}

type Global struct {
	proto1_3 string
	proto2   uint8
}

type ProtoError struct {
	Vals []any
}

type ContextClosedError struct{}

func (e ContextClosedError) Error() string {
	return "Context closed"
}

type RemoteRegister struct {
	Name string
}

type ProtoNotSupportedError struct{}

func (p ProtoNotSupportedError) Error() string {
	return "Operation not supported"
}

func (pe ProtoError) Error() string {
	return fmt.Sprintf("Response Error: %v", pe.Vals)
}

var (
	// Responses only
	ack_c           = &Command{"ACK", []p2{{0, 0x00}}, "ack"}
	not_supported_c = &Command{"NSO", []p2{{0, 0x01}}, "not_supported_operation"}
	error_c         = &Command{"ERR", []p2{{1, 0x02}}, "error"}

	// Request and response
	value_c = &Command{"VAL", []p2{{1, 0x03}}, "value"}

	// Protocol
	use_c    = &Command{"USE", []p2{{1, 0x04}}, "use"}
	strict_c = &Command{"STR", []p2{{1, 0x05}}, "strict"}

	// Lifecycle Management
	ping_c  = &Command{"PNG", []p2{{1, 0x07}}, "ping"}
	valid_c = &Command{"VLD", []p2{{0, 0x08}}, "valid"}
	bye_c   = &Command{"BYE", []p2{{0, 0x09}}, "bye"}

	// Process execution
	abort_c = &Command{"ABT", []p2{{0, 0x0C}}, "abort"}

	// global conf
	conf_set_c = &Command{"CNF", []p2{{2, 0x10}}, "conf"}
	reset_c    = &Command{"RST", []p2{{0, 0x11}}, "reset"}

	// Extensions, types, etc
	enable_c    = &Command{"ENB", []p2{{2, 0x12}}, "enable"}
	disable_c   = &Command{"DIS", []p2{{2, 0x13}}, "disable"}
	limits_c    = &Command{"LMT", []p2{{2, 0x14}, {3, 0x15}}, "limits"}
	help_c      = &Command{"HLP", []p2{{2, 0x17}, {3, 0x18}}, "help"}
	extension_c = &Command{"EXT", []p2{{3, 0x19}}, "extension"}

	// Resource management
	allocate_c = &Command{"ALC", []p2{{1, 0x1B}, {2, 0x1C}}, "alloc"}
	free_c     = &Command{"FRE", []p2{{1, 0x1D}}, "free"}

	// Connections
	connect_c = &Command{"CON", []p2{{1, 0x1E}, {2, 0x1F}}, "connect"}

	// Queries
	prepare_c         = &Command{"PRE", []p2{{2, 0x20}, {3, 0x21}}, "prepare"}
	query_c           = &Command{"QRY", []p2{{3, 0x22}, {4, 0x23}}, "query"}
	execute_c         = &Command{"EXC", []p2{{3, 0x24}}, "execute"}
	count_num_input_c = &Command{"CNI", []p2{{2, 0x25}}, "count_num_inputs"}

	// Result Sets
	has_next_row_c = &Command{"HNX", []p2{{1, 0x28}}, "has_next_row"}
	row_c          = &Command{"ROW", []p2{{1, 0x29}}, "row"}
	has_next_set_c = &Command{"HNS", []p2{{1, 0x2A}}, "has_next_set"}
	next_set_c     = &Command{"NXS", []p2{{1, 0x2B}}, "next_set"}

	// Columns
	column_info_c          = &Command{"COL", []p2{{1, 0x30}, {2, 0x31}}, "column"}
	column_nullable_c      = &Command{"CNL", []p2{{1, 0x32}, {2, 0x33}}, "column_nullable"}
	column_type_c          = &Command{"CDT", []p2{{1, 0x34}, {2, 0x35}}, "column_type"}
	column_type_len_c      = &Command{"CTL", []p2{{1, 0x36}, {2, 0x37}}, "column_type_len"}
	column_precision_len_c = &Command{"CPL", []p2{{1, 0x38}, {2, 0x39}}, "column_precision_len"}

	// Transactions
	begin_transaction_c    = &Command{"BTX", []p2{{0, 0x40}, {1, 0x41}, {2, 0x42}}, "begin_tx"}
	commit_transaction_c   = &Command{"CMT", []p2{{1, 0x46}}, "commit"}
	rollback_transaction_c = &Command{"RLB", []p2{{1, 0x47}}, "rollback"}

	// Generation
	generate_info_c = &Command{"GNI", []p2{{0, 0x60}, {1, 0x61}, {2, 0x62}}, "generator_info"}
	generate_c      = &Command{"GEN", []p2{{4, 0x63}}, "generate"}

	all_commands = []*Command{
		ack_c, not_supported_c, error_c, value_c, use_c, strict_c, ping_c, valid_c,
		bye_c, conf_set_c, reset_c, enable_c, disable_c, limits_c, help_c,
		allocate_c, free_c, connect_c, prepare_c, query_c, execute_c, count_num_input_c,
		has_next_row_c, row_c, has_next_set_c, next_set_c, column_info_c, column_nullable_c,
		column_type_c, column_type_len_c, column_precision_len_c, begin_transaction_c,
		commit_transaction_c, rollback_transaction_c, generate_info_c, generate_c, abort_c,
	}
)

func CommandByProto1Name(name string) (*Command, error) {
	name = strings.ToUpper(name)
	for _, c := range all_commands {
		if c.proto1 == name {
			return c, nil
		}
	}
	return nil, fmt.Errorf("Invalid command")
}

var nil_t, int_t, real_t, flag_t, string_t, binary_t, unix_timestamp_t,
	global_t, register_t, date_t, time_t, date_time_t, date_time_timezone_t,
	array_t, map_t *ProtoType
var all_types []*ProtoType

func init() {
	proto1Parse := defaultProto1Parsers()
	nil_t = &ProtoType{"N", 0x00, proto1Parse["N"]}
	int_t = &ProtoType{"I", 0x01, proto1Parse["I"]}
	real_t = &ProtoType{"R", 0x02, proto1Parse["R"]}
	flag_t = &ProtoType{"F", 0x03, proto1Parse["F"]}
	string_t = &ProtoType{"S", 0x04, proto1Parse["S"]}
	binary_t = &ProtoType{"B", 0x05, proto1Parse["B"]}
	unix_timestamp_t = &ProtoType{"U", 0x06, proto1Parse["U"]}

	// references
	global_t = &ProtoType{"$", 0x11, proto1Parse["$"]}
	register_t = &ProtoType{"@", 0x12, proto1Parse["@"]}

	// date/time
	date_t = &ProtoType{"D", 0x22, proto1Parse["D"]}
	time_t = &ProtoType{"D", 0x21, proto1Parse["D"]}
	date_time_t = &ProtoType{"D", 0x23, proto1Parse["D"]}
	date_time_timezone_t = &ProtoType{"D", 0x27, proto1Parse["D"]}

	// compound
	array_t = &ProtoType{"A", 0x30, proto1Parse["A"]}
	map_t = &ProtoType{"M", 0x31, proto1Parse["M"]}

	all_types = []*ProtoType{
		nil_t, int_t, real_t, flag_t, string_t, binary_t, unix_timestamp_t,
		global_t, register_t, date_t, time_t, date_time_t, date_time_timezone_t,
		array_t, map_t,
	}
}

var (
	// protocol
	gbl_proto  = &Global{"proto", 0x01}
	gbl_async  = &Global{"async", 0x02}
	gbl_protos = &Global{"protos", 0x03}
	gbl_strict = &Global{"strict", 0x04}

	// metadata
	gbl_name    = &Global{"name", 0x10}
	gbl_version = &Global{"version", 0x11}
	gbl_docstr  = &Global{"docstring", 0x12}

	// state
	gbl_refs = &Global{"refs", 0x20}
	gbl_conf = &Global{"conf", 0x21}

	// capabilities
	gbl_drivers      = &Global{"drivers", 0x30}
	gbl_generators   = &Global{"generators", 0x31}
	gbl_extensions   = &Global{"extensions", 0x32}
	gbl_custom_types = &Global{"customtypes", 0x33}
	gbl_commands     = &Global{"commands", 0x34}

	all_globals = []*Global{
		gbl_proto, gbl_protos, gbl_strict, gbl_name, gbl_version, gbl_docstr,
		gbl_refs, gbl_conf, gbl_drivers, gbl_generators, gbl_extensions,
		gbl_custom_types, gbl_commands,
	}
)

type TypeContext struct {
	typesByP1 map[string]*ProtoType
	parent    *TypeContext
}

func (tc *TypeContext) IsKnownType(t *ProtoType) bool {
	if _, ok := tc.typesByP1[t.Proto1]; !ok {
		return false
	}

	// TODO: Add checks for other types

	if tc.parent != nil {
		return tc.parent.IsKnownType(t)
	}

	return true
}

type ProtocolSender interface {
	ParseResponseLine(line string, ec *SendContext) (*ProtoCommand, error)
	SendCommand(command ProtoCommand, out io.Writer, ec *SendContext) error
}

type SendContext struct {
	Tc        *TypeContext
	protocols map[int]ProtocolSender
	proto     ProtocolSender
	lineCh    chan string
	commands  io.Writer
}

func (sc *SendContext) RegisterProtocol(id int, protocol ProtocolSender) {
	sc.protocols[id] = protocol
}

func (sc *SendContext) ToProtoValPanic(goVal interface{}) ProtoVal {
	val, err := ToProtoVal(goVal, sc.Tc)
	if err != nil {
		panic(err)
	}
	return val
}

func (sc *SendContext) ToProtoVal(goVal interface{}) (ProtoVal, error) {
	return ToProtoVal(goVal, sc.Tc)
}

type field struct {
	name  string
	tag   bool
	index []int
	typ   reflect.Type
}

type structFields struct {
	list        []field
	byExactName map[string]*field
}

var fieldCache sync.Map

func cachedTypeFields(rt reflect.Type) structFields {
	if f, ok := fieldCache.Load(rt); ok {
		return f.(structFields)
	}
	f, _ := fieldCache.LoadOrStore(rt, typeFields(rt))
	return f.(structFields)
}

func parseTag(tag string) string {
	t, _, _ := strings.Cut(tag, ",")
	return t
}

func typeFields(t reflect.Type) structFields {
	current := []field{}
	next := []field{{typ: t}}

	var count, nextCount map[reflect.Type]int
	visited := map[reflect.Type]struct{}{}

	var fields []field

	for len(next) > 0 {
		current, next = next, current[:0]

		for _, curField := range current {
			if _, ok := visited[curField.typ]; ok {
				continue
			}
			visited[curField.typ] = struct{}{}

			for i := 0; i < curField.typ.NumField(); i++ {
				structField := curField.typ.Field(i)
				if structField.Anonymous {
					t := structField.Type
					if t.Kind() == reflect.Pointer {
						t = t.Elem()
					}
					if !structField.IsExported() && t.Kind() != reflect.Struct {
						continue
					}
				} else if !structField.IsExported() {
					continue
				}

				tag := structField.Tag.Get("protoVal")
				if tag == "-" {
					continue
				}
				name := parseTag(tag)

				index := make([]int, len(curField.index)+1)
				copy(index, curField.index)
				index[len(curField.index)] = i

				fieldType := structField.Type
				if fieldType.Name() == "" && fieldType.Kind() == reflect.Pointer {
					fieldType = fieldType.Elem()
				}

				if name != "" || !structField.Anonymous || fieldType.Kind() != reflect.Struct {
					tagged := name != ""
					if name == "" {
						name = structField.Name
					}
					f := field{
						name:  name,
						tag:   tagged,
						index: index,
						typ:   fieldType,
					}
					fields = append(fields, f)
					if count[f.typ] > 1 {
						fields = append(fields, fields[len(fields)-1])
					}
					continue
				}

				nextCount[fieldType]++
				if nextCount[fieldType] == 1 {
					next = append(next, field{name: fieldType.Name(), index: index, typ: fieldType})
				}
			}
		}
	}

	slices.SortFunc(fields, func(a, b field) int {
		if c := strings.Compare(a.name, b.name); c != 0 {
			return c
		}
		if c := cmp.Compare(len(a.index), len(b.index)); c != 0 {
			return c
		}
		if a.tag != b.tag {
			if a.tag {
				return -1
			}
			return +1
		}
		return slices.Compare(a.index, b.index)
	})

	out := fields[:0]
	for advance, i := 0, 0; i < len(fields); i += advance {
		// One iteration per name.
		// Find the sequence of fields with the name of this first field.
		fi := fields[i]
		name := fi.name
		for advance = 1; i+advance < len(fields); advance++ {
			fj := fields[i+advance]
			if fj.name != name {
				break
			}
		}
		if advance == 1 { // Only one field with this name
			out = append(out, fi)
			continue
		}
		dominant, ok := dominantField(fields[i : i+advance])
		if ok {
			out = append(out, dominant)
		}
	}

	fields = out
	slices.SortFunc(fields, func(i, j field) int {
		return slices.Compare(i.index, j.index)
	})

	exactNameIndex := make(map[string]*field, len(fields))
	for i, field := range fields {
		exactNameIndex[field.name] = &fields[i]
	}

	return structFields{fields, exactNameIndex}
}

func dominantField(fields []field) (field, bool) {
	// The fields are sorted in increasing index-length order, then by presence of tag.
	// That means that the first field is the dominant one. We need only check
	// for error cases: two fields at top level, either both tagged or neither tagged.
	if len(fields) > 1 && len(fields[0].index) == len(fields[1].index) && fields[0].tag == fields[1].tag {
		return field{}, false
	}
	return fields[0], true
}

func (sc *SendContext) to(protoVal ProtoVal, rv reflect.Value) error {
	for rv.Kind() == reflect.Pointer {
		if rv.IsNil() {
			switch protoVal.(type) {
			case *ValNil:
				return nil
			default:
				rv.Set(reflect.New(rv.Type().Elem()))
			}
		}
		rv = rv.Elem()
	}

	isNilable := rv.Kind() == reflect.Chan || rv.Kind() == reflect.Map || rv.Kind() == reflect.Pointer || rv.Kind() == reflect.Interface || rv.Kind() == reflect.Func || rv.Kind() == reflect.Slice
	switch pv := protoVal.(type) {
	case *ValNil:
		if !isNilable {
			return fmt.Errorf("Cannot assign nil to %#v", rv.Type().String())
		}
		if !rv.IsNil() {
			rv.Set(reflect.ValueOf(nil))
		}
		return nil
	case *ValArray:
		if rv.Kind() != reflect.Slice && rv.Kind() != reflect.Array {
			return fmt.Errorf("Can only unpack ValArray into slices and arrays")
		}
		if rv.Kind() == reflect.Slice {
			if rv.Cap() < len(pv.elements) {
				rv.Grow(len(pv.elements) - rv.Cap() + 1)
			}
			rv.SetLen(len(pv.elements))
		}
		for i, e := range pv.elements {
			if i >= rv.Len() {
				break
			}

			if err := sc.to(e, rv.Index(i)); err != nil {
				return err
			}
		}
		return nil
	case *ValMap:
		if len(pv.keys) != len(pv.values) {
			return fmt.Errorf("Invalid ValMap, mismatched keys and vals")
		}

		if rv.Kind() == reflect.Map {
			if rv.IsNil() {
				rv.Set(reflect.MakeMap(rv.Type()))
			}
			keyType := rv.Type().Key()
			keyKind := keyType.Kind()
			compKind := reflect.Invalid
			validKinds := []reflect.Kind{
				reflect.String,
				reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64,
				reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64,
			}
			if i := slices.Index(validKinds, keyKind); i >= 0 {
				compKind = validKinds[i]
			}

			for i := range pv.keys {
				key, err := pv.keys[i].ToGoVal()
				if err != nil {
					return err
				}

				krefl := reflect.ValueOf(key)

				insertVal := func(keyRefl reflect.Value) error {
					mapElem := reflect.New(rv.Type().Elem())
					if err := sc.to(pv.values[i], mapElem.Elem()); err != nil {
						return err
					}
					rv.SetMapIndex(keyRefl, mapElem.Elem())
					return nil
				}

				if krefl.Kind() == reflect.String && compKind == reflect.String {
					if err := insertVal(krefl); err != nil {
						return err
					}
				} else if compKind != reflect.String && compKind != reflect.Invalid && krefl.CanInt() {
					key = krefl.Int()
					krefl = reflect.ValueOf(key)
					if err := insertVal(krefl); err != nil {
						return err
					}
				} else if compKind != reflect.Invalid {
					return fmt.Errorf("Cannot use type %T as map key", key)
				} else {
					switch krefl.Kind() {
					case reflect.String, reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64,
						reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
						if err := insertVal(krefl); err != nil {
							return err
						}
					default:
						return fmt.Errorf("Cannot use type %T as map key", key)
					}
				}
			}
			return nil
		} else if rv.Kind() == reflect.Struct {
			fields := cachedTypeFields(rv.Type())
			for i := range pv.keys {
				var key string
				err := sc.To(pv.keys[i], &key)

				if err != nil {
					return err
				}

				field := fields.byExactName[key]
				if field == nil {
					// skip unknown fields
					continue
				}

				var mapElem reflect.Value
				for _, i := range field.index {
					if rv.Kind() == reflect.Pointer {
						if rv.IsNil() {
							if !rv.CanSet() {
								return fmt.Errorf("Unable to set field %s in %s", key, rv.Type().String())
							}
							rv.Set(reflect.New(rv.Type().Elem()))
						}
						rv = rv.Elem()
					}
					mapElem = rv.Field(i)
				}

				if err := sc.to(pv.values[i], mapElem); err != nil {
					return err
				}
			}
			return nil
		} else {
			return fmt.Errorf("Maps must go into a Go map or Go struct")
		}
	}

	goVal, err := protoVal.ToGoVal()
	if err != nil {
		return err
	}

	gv := reflect.ValueOf(goVal)
	switch {
	case gv.CanInt() && rv.CanInt():
		rv.SetInt(gv.Int())
	case gv.CanFloat() && rv.CanFloat():
		rv.SetFloat(gv.Float())
	case gv.Kind() == reflect.String && rv.Kind() == reflect.String:
		rv.SetString(gv.String())
	case gv.Kind() == reflect.Bool && rv.Kind() == reflect.Bool:
		rv.SetBool(gv.Bool())
	case gv.Kind() == reflect.Slice && rv.Kind() == reflect.Slice:
		switch goVal.(type) {
		default:
			return fmt.Errorf("Expected []any, received %T", goVal)
		}
	case gv.Kind() == reflect.Pointer && gv.IsNil():
		rv.Set(reflect.Zero(rv.Type()))
	case rv.Kind() == reflect.Interface:
		rv.Set(gv)
	default:
		return fmt.Errorf("Unable to assign %T to elem of %#v", goVal, rv.Type().String())
	}
	return nil
}

func (sc *SendContext) To(protoVal ProtoVal, out any) error {
	rv := reflect.ValueOf(out)
	if rv.Kind() != reflect.Pointer {
		return fmt.Errorf("Must provide pointer for output")
	}
	if !rv.Elem().CanSet() {
		return fmt.Errorf("Must provide writeable pointer for output")
	}
	return sc.to(protoVal, rv.Elem())
}

func (sc *SendContext) SendCommand(command ProtoCommand) error {
	return sc.proto.SendCommand(command, sc.commands, sc)
}

func (sc *SendContext) ReadResponse(ctx context.Context) (*ProtoCommand, ProtoVal, error) {
	select {
	case line, ok := <-sc.lineCh:
		if !ok {
			return nil, nil, fmt.Errorf("At EOF")
		}
		cmd, err := sc.proto.ParseResponseLine(line, sc)

		if err != nil {
			return nil, nil, err
		}

		switch cmd.cmd {
		case ack_c:
			return cmd, nil, nil
		case not_supported_c:
			return cmd, nil, &ProtoNotSupportedError{}
		case error_c:
			vals := make([]any, 0, len(cmd.args))
			for _, a := range cmd.args {
				v, err := FromProtoVal(a)

				if err != nil {
					vals = append(vals, err)
				} else {
					vals = append(vals, v)
				}
			}
			return cmd, nil, &ProtoError{vals}
		case value_c:
			return cmd, cmd.args[0], nil
		default:
			return cmd, nil, fmt.Errorf("Received non-respose command!")
		}
	case <-ctx.Done():
		return nil, nil, ContextClosedError{}
	}
}

type sendReceiveResponse struct {
	cmd *ProtoCommand
	val ProtoVal
	err error
}

func (sc *SendContext) SendReceive(ctx context.Context, command ProtoCommand) (*ProtoCommand, ProtoVal, error) {
	ch := make(chan sendReceiveResponse)

	err := sc.SendCommand(command)
	if err != nil {
		return nil, nil, err
	}

	go func() {
		defer func() {
			close(ch)
		}()
		cmd, val, err := sc.ReadResponse(ctx)
		if err != nil {
			if errors.Is(err, ContextClosedError{}) {
				return
			}
			ch <- sendReceiveResponse{cmd, nil, err}
		}
		ch <- sendReceiveResponse{cmd, val, nil}
	}()

	select {
	case res := <-ch:
		if res.err != nil {
			return nil, nil, res.err
		}
		return res.cmd, res.val, nil
	case <-ctx.Done():
		return nil, nil, ContextClosedError{}
	}
}

func NewSendContext(ctx context.Context, commandIO io.Writer, responseIO io.Reader) (*SendContext, error) {
	ch := make(chan string)
	go func() {
		s := bufio.NewScanner(responseIO)
		s.Split(bufio.ScanLines)
		foundStart := false
		for s.Scan() {
			if foundStart {
				ch <- s.Text()
			} else if strings.TrimSpace(s.Text()) == "RDY" {
				foundStart = true
				ch <- s.Text()
			}
		}
	}()
	ec := SendContext{
		NewTypeContext(),
		map[int]ProtocolSender{
			1: ProtocolV1,
		},
		ProtocolV1,
		ch,
		commandIO,
	}
	select {
	case <-ctx.Done():
		return nil, fmt.Errorf("Context closed before plugin was ready")
	case <-ch:
		return &ec, nil
	}
}

type ProtoValEncodable interface {
	ToProtoVal(tc *TypeContext) (ProtoVal, error)
}

func FromProtoVal(val ProtoVal) (any, error) {
	if val == nil {
		return val, nil
	}
	return val.ToGoVal()
}

func ToProtoVal(goVal any, tc *TypeContext) (ProtoVal, error) {
	if goVal == nil {
		return &ValNil{}, nil
	}

	if tc == nil {
		tc = NewTypeContext()
	}

	switch v := goVal.(type) {
	case *Command:
		p2 := &ValArray{}
		for _, p := range v.proto2 {
			p2.elements = append(
				p2.elements,
				&ValArray{
					[]ProtoVal{
						&ValInt{int64(p.numParams)},
						&ValInt{int64(p.code)},
					},
				},
			)
		}
		return &ValMap{
			keys: []ProtoVal{
				&ValInt{1},
				&ValInt{2},
				&ValInt{3},
			},
			values: []ProtoVal{
				&ValString{v.proto1},
				p2,
				&ValString{v.proto3},
			},
		}, nil
	case int:
		return &ValInt{int64(v)}, nil
	case int8:
		return &ValInt{int64(v)}, nil
	case int16:
		return &ValInt{int64(v)}, nil
	case int32:
		return &ValInt{int64(v)}, nil
	case int64:
		return &ValInt{v}, nil
	case uint:
		return &ValInt{int64(v)}, nil
	case uint8:
		return &ValInt{int64(v)}, nil
	case uint16:
		return &ValInt{int64(v)}, nil
	case uint32:
		return &ValInt{int64(v)}, nil
	case uint64:
		return &ValInt{int64(v)}, nil
	case float32:
		return &ValReal{float64(v)}, nil
	case float64:
		return &ValReal{float64(v)}, nil
	case string:
		return &ValString{v}, nil
	case []rune:
		return &ValString{string(v)}, nil
	case []byte:
		return &ValBinary{v}, nil
	case time.Time:
		_, tzSeconds := v.Zone()
		tzNegative := tzSeconds < 0
		if tzNegative {
			tzSeconds *= -1
		}
		tzMinutesOverflow := tzSeconds / 60
		tzMinutes := tzMinutesOverflow % 60
		tzHours := (tzMinutesOverflow / 60) % 24
		if tzNegative {
			tzHours *= -1
		}
		return &ValDateTimeTimezone{
			ValDateTime: ValDateTime{
				ValDate: ValDate{
					year:  uint16(v.Year()),
					month: uint8(v.Month()),
					day:   uint8(v.Day()),
				},
				ValTime: ValTime{
					hour:         uint8(v.Hour()),
					minute:       uint8(v.Minute()),
					second:       uint8(v.Second()),
					secondPieces: float64(v.Nanosecond()) / 1e9,
				},
			},
			valTimezoneOffset: valTimezoneOffset{
				hourOffset:   int8(tzHours),
				minuteOffset: uint8(tzMinutes),
			},
		}, nil
	case bool:
		return &ValBool{v}, nil
	case ProtoValEncodable:
		return v.ToProtoVal(tc)
	case *Global:
		return &ValGlobal{v.proto1_3}, nil
	case *RemoteRegister:
		return &ValRegister{v.Name}, nil
	}

	v := reflect.ValueOf(goVal)
	return from(v, tc)
}

func from(rv reflect.Value, tc *TypeContext) (ProtoVal, error) {
	isNilable := rv.Kind() == reflect.Chan || rv.Kind() == reflect.Map || rv.Kind() == reflect.Pointer || rv.Kind() == reflect.Interface || rv.Kind() == reflect.Func || rv.Kind() == reflect.Slice
	switch {
	case isNilable && rv.IsNil():
		return &ValNil{}, nil
	case rv.Kind() == reflect.Pointer:
		return from(rv.Elem(), tc)
	case rv.CanInt():
		return &ValInt{rv.Int()}, nil
	case rv.CanFloat():
		return &ValReal{rv.Float()}, nil
	case rv.CanUint():
		return &ValInt{int64(rv.Uint())}, nil
	case rv.Kind() == reflect.String:
		return &ValString{rv.String()}, nil
	case rv.Kind() == reflect.Bool:
		return &ValBool{rv.Bool()}, nil
	case rv.Type() == reflect.TypeOf(([]byte)(nil)):
		return &ValBinary{rv.Bytes()}, nil
	case rv.Type() == reflect.TypeOf(time.Time{}):
		return ToProtoVal(rv.Interface(), tc)
	case rv.Kind() == reflect.Slice || rv.Kind() == reflect.Array:
		res := &ValArray{elements: make([]ProtoVal, 0, rv.Len())}
		for i := range rv.Len() {
			val := rv.Index(i)
			pv, err := from(val, tc)
			if err != nil {
				return nil, err
			}
			res.elements = append(res.elements, pv)
		}
		return res, nil
	case rv.Kind() == reflect.Struct:
		fields := cachedTypeFields(rv.Type())
		res := &ValMap{
			keys:   make([]ProtoVal, 0, len(fields.byExactName)),
			values: make([]ProtoVal, 0, len(fields.byExactName)),
		}
		for name, f := range fields.byExactName {
			rf := rv.FieldByIndex(f.index)
			keyProto := &ValString{name}
			valProto, err := from(rf, tc)

			if err != nil {
				return nil, err
			}
			res.keys = append(res.keys, keyProto)
			res.values = append(res.values, valProto)
		}
		return res, nil
	case rv.Kind() == reflect.Map:
		res := &ValMap{
			keys:   make([]ProtoVal, 0, rv.Len()),
			values: make([]ProtoVal, 0, rv.Len()),
		}

		for k, e := range rv.Seq2() {
			key, err := from(k, tc)
			if err != nil {
				return nil, err
			}

			val, err := from(e, tc)
			if err != nil {
				return nil, err
			}

			res.keys = append(res.keys, key)
			res.values = append(res.values, val)
		}

		return res, nil
	case rv.CanInterface():
		return ToProtoVal(rv.Interface(), tc)
	}
	return nil, fmt.Errorf("Canot convert type to ProtoVal %#v", rv.Type().String())
}

func LayerTypeContext(base *TypeContext, newTypes []*ProtoType) *TypeContext {
	tc := &TypeContext{}
	tc.parent = base
	tc.typesByP1 = make(map[string]*ProtoType, len(newTypes))

	for _, t := range newTypes {
		tc.typesByP1[t.Proto1] = t
	}
	return tc
}

func NewTypeContext() *TypeContext {
	return LayerTypeContext(nil, all_types)
}

type ValNil struct{}

func NewNilVal() *ValNil {
	return &ValNil{}
}

func (n *ValNil) GetProtoType() *ProtoType { return nil_t }
func (n *ValNil) ToGoVal() (any, error)    { return nil, nil }

type ValInt struct{ val int64 }

func NewIntVal(val int64) *ValInt {
	return &ValInt{val}
}
func (v *ValInt) ToNativeGoVal() (int64, error) { return v.val, nil }
func (v *ValInt) ToGoVal() (any, error)         { return v.ToNativeGoVal() }

func (i *ValInt) GetProtoType() *ProtoType { return int_t }

type ValReal struct{ val float64 }

func NewRealVal(val float64) *ValReal {
	return &ValReal{val}
}

func (v *ValReal) ToNativeGoVal() (float64, error) { return v.val, nil }
func (v *ValReal) ToGoVal() (any, error)           { return v.ToNativeGoVal() }
func (v *ValReal) GetProtoType() *ProtoType        { return real_t }

type ValBool struct{ val bool }

func NewBoolVal(val bool) *ValBool {
	return &ValBool{val}
}

func (v *ValBool) GetProtoType() *ProtoType     { return flag_t }
func (v *ValBool) ToNativeGoVal() (bool, error) { return v.val, nil }
func (v *ValBool) ToGoVal() (any, error)        { return v.ToNativeGoVal() }

type ValString struct{ val string }

func NewStringVal(val string) *ValString {
	return &ValString{val}
}

func (v *ValString) GetProtoType() *ProtoType       { return string_t }
func (v *ValString) ToNativeGoVal() (string, error) { return v.val, nil }
func (v *ValString) ToGoVal() (any, error)          { return v.ToNativeGoVal() }

type ValBinary struct{ val []byte }

func NewBinaryVal(val []byte) *ValBinary {
	return &ValBinary{val}
}

func (v *ValBinary) GetProtoType() *ProtoType       { return binary_t }
func (v *ValBinary) ToNativeGoVal() ([]byte, error) { return v.val, nil }
func (v *ValBinary) ToGoVal() (any, error)          { return v.ToNativeGoVal() }

type ValUnixTimestamp struct{ val int64 }

func NewUnixTimestampVal(val int64) *ValUnixTimestamp {
	return &ValUnixTimestamp{val}
}
func (v *ValUnixTimestamp) ToNativeGoVal() (time.Time, error) { return time.Unix(v.val, 0), nil }
func (v *ValUnixTimestamp) ToGoVal() (any, error)             { return v.ToNativeGoVal() }
func (v *ValUnixTimestamp) GetProtoType() *ProtoType          { return unix_timestamp_t }

type ValGlobal struct{ refId string }

func NewGlobalVal(refId string) *ValGlobal {
	return &ValGlobal{refId}
}

func (v *ValGlobal) GetProtoType() *ProtoType { return global_t }
func (v *ValGlobal) ToGoVal() (any, error) {
	for _, g := range all_globals {
		if g.proto1_3 == v.refId {
			return g, nil
		}
	}
	return nil, fmt.Errorf("Invalid global value")
}

type ValRegister struct{ refId string }

func NewRegisterVal(refId string) *ValRegister {
	return &ValRegister{refId}
}
func (v *ValRegister) ToGoVal() (any, error) {
	return &RemoteRegister{v.refId}, nil
}

func (v *ValRegister) GetProtoType() *ProtoType { return register_t }

type ValDate struct {
	year  uint16
	month uint8
	day   uint8
}

func (v *ValDate) GetProtoType() *ProtoType { return date_t }
func (v *ValDate) ToNativeGoVal() (time.Time, error) {
	return time.Date(int(v.year), time.Month(v.month), int(v.day), 0, 0, 0, 0, time.Local), nil
}
func (v *ValDate) ToGoVal() (any, error) { return v.ToNativeGoVal() }

type ValTime struct {
	secondPieces float64
	hour         uint8
	minute       uint8
	second       uint8
}

func (v *ValTime) GetProtoType() *ProtoType { return time_t }
func (v *ValTime) ToNativeGoVal() (any, error) {
	return time.Date(0, 0, 0, int(v.hour), int(v.minute), int(v.second), int(v.secondPieces*1e9), time.Local), nil
}
func (v *ValTime) ToGoVal() (any, error) { return v.ToNativeGoVal() }

type ValDateTime struct {
	ValDate
	ValTime
}

func (v *ValDateTime) GetProtoType() *ProtoType { return date_time_t }
func (v *ValDateTime) ToNativeGoVal() (any, error) {
	return time.Date(int(v.year), time.Month(v.month), int(v.day), int(v.hour), int(v.minute), int(v.second), int(v.secondPieces*1e9), time.Local), nil
}
func (v *ValDateTime) ToGoVal() (any, error) { return v.ToNativeGoVal() }

type valTimezoneOffset struct {
	hourOffset   int8
	minuteOffset uint8
}

type ValDateTimeTimezone struct {
	ValDateTime
	valTimezoneOffset
}

func (v *ValDateTimeTimezone) ToNativeGoVal() (any, error) {
	tz := time.FixedZone("", (int(v.valTimezoneOffset.hourOffset)*60+int(v.valTimezoneOffset.minuteOffset))*60)
	return time.Date(int(v.year), time.Month(v.month), int(v.day), int(v.hour), int(v.minute), int(v.second), int(v.secondPieces*1e9), tz), nil
}
func (v *ValDateTimeTimezone) ToGoVal() (any, error) { return v.ToNativeGoVal() }

func NewTimeVal(hour, minute, second, nanoSeconds int) *ValTime {
	return &ValTime{float64(nanoSeconds) / 1e9, uint8(hour), uint8(minute), uint8(second)}
}

func NewDateVal(year, month, day int) *ValDate {
	return &ValDate{uint16(year), uint8(month), uint8(day)}
}

func NewDateTimeVal(year, month, day, hour, minute, second, nanoSeconds int) *ValDateTime {
	return &ValDateTime{
		*NewDateVal(year, month, day),
		*NewTimeVal(hour, minute, second, nanoSeconds),
	}
}

func NewDateTimeTimezoneVal(year, month, day, hour, minute, second, nanoSeconds, hourOffset, minuteOffset int) *ValDateTimeTimezone {
	return &ValDateTimeTimezone{
		*NewDateTimeVal(year, month, day, hour, minute, second, nanoSeconds),
		valTimezoneOffset{int8(hourOffset), uint8(minuteOffset)},
	}
}

func (v *ValDateTimeTimezone) GetProtoType() *ProtoType { return date_time_timezone_t }

type ValArray struct{ elements []ProtoVal }

func NewArrayVal(vals ...ProtoVal) *ValArray { return &ValArray{vals} }
func (v *ValArray) ToGoVal() (any, error) {
	return v.ToNativeGoVal()
}
func (v *ValArray) ToNativeGoVal() ([]any, error) {
	res := make([]any, 0, len(v.elements))
	for _, e := range v.elements {
		val, err := e.ToGoVal()
		if err != nil {
			return nil, err
		}
		res = append(res, val)
	}
	return res, nil
}
func (v *ValArray) GetProtoType() *ProtoType { return array_t }

type ValMap struct {
	keys   []ProtoVal
	values []ProtoVal
}

func NewMapVal(keyValues ...ProtoVal) *ValMap {
	if len(keyValues)%2 == 1 {
		panic("must provide key/value pairs to NewMapVal")
	}

	keys := make([]ProtoVal, 0, len(keyValues)/2)
	vals := make([]ProtoVal, 0, len(keyValues)/2)

	for i, e := range keyValues {
		if i%2 == 0 {
			keys = append(keys, e)
		} else {
			vals = append(vals, e)
		}
	}

	return &ValMap{keys, vals}
}
func (v *ValMap) ToGoVal() (any, error) {
	return v.ToNativeGoVal()
}
func (v *ValMap) ToNativeGoVal() (map[any]any, error) {
	res := make(map[any]any, len(v.keys))
	for i := range v.keys {
		k := v.keys[i]
		key, err := k.ToGoVal()
		if err != nil {
			return nil, err
		}

		keyRefl := reflect.ValueOf(key)
		if !keyRefl.Comparable() {
			return nil, fmt.Errorf("Cannot use value of type %T as key", key)
		}

		val, err := v.values[i].ToGoVal()
		if err != nil {
			return nil, err
		}
		res[key] = val
	}
	return res, nil
}

func (v *ValMap) GetProtoType() *ProtoType { return map_t }

type ProtoVal interface {
	GetProtoType() *ProtoType
	EncodeProto1() (string, error)
	ToGoVal() (any, error)
}

type NativeProtoVal[T any] interface {
	ToNativeGoVal() (T, error)
}

type ProtoCommand struct {
	label string
	cmd   *Command
	args  []ProtoVal
}

func isTypeChar(ch rune) bool {
	return ch >= 'A' && ch <= 'Z'
}

func ProtoValDeepEquals(a, b ProtoVal) bool {
	if a == b {
		return true
	}

	if a.GetProtoType().Proto2 != b.GetProtoType().Proto2 {
		return false
	}

	// For maps, handle arbitrary ordering of elements
	switch av := a.(type) {
	case *ValMap:
		switch bv := b.(type) {
		case *ValMap:
			if len(av.keys) != len(bv.keys) {
				return false
			}
			aKV := make([][]ProtoVal, 0, len(av.keys))
			bKV := make([][]ProtoVal, 0, len(av.keys))
			for i := range av.keys {
				aKV = append(aKV, []ProtoVal{av.keys[i], av.values[i]})
				bKV = append(bKV, []ProtoVal{bv.keys[i], bv.values[i]})
			}
			for _, aKVPair := range aKV {
				found := false
				for bi, bKVPair := range bKV {
					if ProtoValDeepEquals(aKVPair[0], bKVPair[0]) && ProtoValDeepEquals(aKVPair[1], bKVPair[1]) {
						// If we found a match, "remove" it so we don't have to keep looking
						// Also lets us keep track of if all of our keys were found in b
						found = true
						tmp := bKV[0]
						cur := bKV[bi]
						bKV[0] = cur
						bKV[bi] = tmp
						bKV = bKV[1:]
						// break from our inner loop
						break
					}
				}
				// If we couldn't find our a key-value pair, then we don't match
				if !found {
					return false
				}
			}
			if len(bKV) != 0 {
				return false
			}
			return true
		default:
			return false
		}
	}

	as, aerr := ProtocolV1.EncodeVal(a)
	bs, berr := ProtocolV1.EncodeVal(b)

	if aerr != nil {
		return false
	}

	if berr != nil {
		return false
	}

	return as == bs
}

func ProtoValsDeepEquals(a, b []ProtoVal) bool {
	if len(a) != len(b) {
		return false
	}

	for i := range a {
		if !ProtoValDeepEquals(a[i], b[i]) {
			return false
		}
	}

	return true
}

// < RDY
// > DRV
// < VAL A:2 S:sqlite S:mysql
// > DRV S:sqlite
// < VAL M:n S:TX F:1 S:INT_MAX I:65355 S:INT_MIN I:0 ....
// > GNI
// < VAL A:2 S:sqlite S:mysql
// > GNI S:sqlite
// < VAL A:3 M:3 S:ID S:createTable S:VAR A:2 S:UP S:DOWN S:SUMMARY S:createsTable M:3 S:ID S:alterTable S:VAR A:2 S:UP S:DOWN S:SUMMARY S:createsTable M:3 S:ID S:createIndes S:VAR A:2 S:UP S:DOWN S:SUMMARY S:createsTable
// > GNI S:sqlite S:createsTable
// < VAL M:4 S:ID alterTable S:VAR ... S:SUMMARY ... S:DOCSTR ...

// < VAL I:0
// > SEL $protos
// < VAL A:2 I:1 I:2
// > SET $proto I:1
// < ACK
// > SET $cnf:file S:file.db
// < ACK
// > SET $cnf:host
// > SET S:localhost
// < ACK
// > SET $cnf:url
// < ACK
// > SET S#aGVsbG8gd29ybGQ=
// < ACK
// > SET $cnf:pwd
// < ACK
// > SET S#aGkNCnRoZXJl
// < ACK
// > SET $cnf:engine S:sqlite
// < ACK
// > SET $cnf:port I:8080
// < ACK
// > SET $cnf M[3] ...
// < ACK
// > CON $cnf
// < @con1
// > PRE @con1 S#U0VMRUNUICogRlJPTSB1c2Vycw0KV0hFUkUgbmFtZSA9ICJ0ZXN0aW5nIiBPUiBpZCA9ICQx
// < VAL @stmt1
// > ALC A:1 I:98765
// < ACK @args1
// > QRY @stmt1 @args1
// < VAL @res1
// > END @args1
// < ACK
// > COL @res1
// < VAL A:5 S:id S:name S:email S:active S:ctime
// > ROW @res:1
// < VAL A:5 I:532 S:Bob Smith S:example.com F:1 D:2024-02-04T00:00:00+00:00
// > HNX				# has next
// < VAL F:1
// > ROW @res:1
// < VAL A:5 I:533 S:Smith N: F:1 D:2024-03-04T00:00:00+00:00
// > HNX
// < VAL F:0
// > CDT @res1				# Database type
// < VAL A:5 S:INT S:TEXT S:VARCHAR S:BOOL S:DATETIME
// > CNL @res:1				# Nullable
// < VAL A:5 F:0 F:0 F:1 F:0 F:0
// > CTL @res1				# ProtoType length
// < VAL A:5 I:11 N: I:500 N: N:
// > CPL A:5 N: N: N: A:2 I:38 I:4 # ProtoType Precision Lengths
// > END @res1				# Close
// < ACK
// > ROW @res:1
// < ERR S:DOES_NOT_EXIST
// > END @stmt:1
// < ACK
// > BTX @con1 # Begin Transaction
// < @tx1
// > CMT @tx1 # Commit
// < ACK
// > RLB @tx1 # Rollback
// < ERR S:ALREADY_COMMITTED
// > EXC @con1 S#SU5TRVJUIGludG8gdXNlciAobmFtZSwgZW1haWwpIFZBTFVFUyAoJDEsICQyKQ== A:2 S:Jane N:   # Execute
// < VAL A:2 I:654 I:1    # LastInsertId, RowsAffected
// > CNI S#SU5TRVJUIGludG8gdXNlciAobmFtZSwgZW1haWwpIFZBTFVFUyAoJDEsICQyKQ==   # Count number input
// < VAL I:2
// > ALC M:2 S:name S:Bob S:email N:
// < VAL @args2
// > EXC @con1 S#SU5TRVJUIGludG8gdXNlciAobmFtZSwgZW1haWwpIFZBTFVFUyAoOm5hbWUsIDplbWFpbCk= @args2
// < VAL A:2 I:655 I:1
// > ALC B:cyBkZmVsIGtqbDJqaw==
// > END @con1
// < ACK
// > QUERY @con1 S:QUERY
// < ERR S:CONN_CLOSED
// > BYE

//// : - ASCII representation (cannot have whitespace or non-printables)
//// # - Base64 encoded
//// % - HEX encoded

//// I - Int (No limit on size)
//// S - String
//// N - Nil (always "N0:"
//// F - Flag (bool)
//// B - Binary (base64 encoded in ASCII)
//// D - Date/Time (ISO 8601)
//// U - Unix Timestamp, UTC (in seconds)
//// R - Real (double)
//// A[n] - Array of n
//// M[n] - Map of n

// ACK - Acknowledge (do action but no result)
// NSO - Didn't do action because not supported
// VAL @<key> - Result is stored in a reference (must free with END)
// END @<key> - Free reference
// PRE (S<n>:<v> | B<n>:<v>) - Prepare statement (always returns VAL)
// VAL <v> - Raw value
// ERR <V> - Error and message
// QRY (S<n>:<v> | @<key>) (<v> | @<key>) - Run query (always returns VAL)
// EXC (S<n>:<v> | @<key>) (<v> | @<key>) - Execute (always returns VAL with lastinsertid/null & rows affected or ERR)
// SET $<key> <v> - Sets global constant
// CON <v> - Establish database connection with configuration
// CON @<key> <v> - Establish database connection and assign it to variable (errs if variable is taken)
// RST - Close database connection and close any references (but not global constants)
// USE <v> - Use protocol version (used to "upgrade" protocol)
// COL @<key> - Get column names for result set
// HNX - Has next row?
// HNS - Has next result set?
// NXT - Switch to next result set
// CNL @<key> - Get which columns are nullable
// CDT @<key> - Get column database type names
// CDT @<key> <index> - Get column database type name
// CTL @<key> - Get column type lengths
// CTL @<key> <index> - Get column type length
// CPL @<key> - Get column precision lengths
// CPL @<key> <index> - Get column precision length
// BTX - Begin transaction
// ROW @<key> - Get next row from result set
// CMT - Commit transaction
// RLB - Rollback transaction
// CNI - Count number of input values
// PNG - Check liveliness of connection (returns ACK if good or ERR if not)
// CAN <v> - Checks if driver has capability
// DRV - Retrieve driver limitations
// BTO <v> - Begin transaction with options
// VLD - Checks validity of connection/
// BYE - Free resources, rollback/abort transactions, and shutdown
// GEN <v> <v> <v> - Generate string/binary representing database actions for a datastructure (e.g. generate create table command)
// ENB <v> - Enables extension (e.g. extended types)
// DIS <v> - Disables extension
// ALC <v> - Allocate variable with value
// ALC @<key> <v> - Allocate variable with value and name if name is not used. Errors if name is used

///

/// $strict - Flag for whether or not the program is in strict mode (if in strict mode, an ERR will also exit the program with a non-zero exit code)
/// $proto - current protocol
/// $protos - supported protocol
/// $conf - current database configuration settings
/// $refs - list of active references (Readonly)
/// $feat - list of extra/optional features supported (Readonly)
/// $name - plugin name (Readonly)
/// $version - plugin version (Readonly)
/// $extensions - Gets map of extensions

// < VAL A[1] M[2] Sn:ID Sn:sqlite Sn:IS A[2] Sn:DRIVER Sn:GENERATOR
// < ERR Sn:NO_ENGINE_SELECTED
// > SET $conf:engine Sn:sqlite
// < VAL A[2]  S11:createTable S10:alterTable
// > SEL $gens:createTable
// < VAL M[3] S2:UP F:1 S24:DOWN F:0 S2:PARAMS M[4] Sn:SCHEMA Sn:STR Sn:NAME TS Sn:COLUMNS TA:1 TM:1:1 TS TS
// > GEN S11:createTable S2:UP M[4] Sn:SCHEMA Sn:test_schema Sn:NAME Sn:test_table Sn:Columns M[2] Sn:ID Sn:TEXT Sn:NAME Sn:TEXT

// benefits of Version 1 protocol:
// - Easy to write a parser for
// - Simple but flexible

// limits of Version 1 protocol:
// - Not very human friendly
// - Not efficient

// Proto 1
// COMMAND (SPACE (REF|GLOBAL|TYPE <MARK> VALUE))*
// MARK = : # %
// REF = @[a-zA-Z_0-9]
// GLOBAL = \$[a-zA-Z_0-9]
// TYPE = [A-Z]+
// VALUE = [\x21-\x7E]

// Proto 2
// COMMAND NUM_ARGS TYPE_BYTES* VALUE_BYTES*

// Proto3
// COMMAND LPAREN SPACE* (VALUE (SPACE+ VALUE)*)? SPACE* RPAREN
// VALUE = ARRAY | MAP | GLOBAL | REFERENCE | NUM | STR
// ARRAY = [SPACE* (VALUE (SPACE+ VALUE)*) SPACE*]
// MAP = {SPACE* (VALUE SPACE+ VALUE (SPACE+VALUE SPACE+ VALUE)*) SPACE*}
// IDENTIFIER =
//

// Version 2 protocol:
// Note: bytes are shown in hex, in actual protocol would be bytes

/// Responses Only

// 00 ACK - Acknowledge (do action but no result)
// 01 NSO - Didn't do action because not supported
// 02 ERR <V> - Error and message
// 03 VAL <v> | @<key> - value

/// Protocol

// 04 USE <v> - Use protocol version (used to "upgrade" protocol)
// ACK | ERR ... | NSO
// 05 STR <v> - Set strict mode
// ACK | ERR ... | NSO

/// Lifecycle Management

// 07 PNG @<key> - Checks liveliness of connection
// ACK | ERR | NSO
// 08 VLD - Checks validity of the plugin (whether it should be discarded from a conn pool or not, returns F:1 or F:0)
// VAL F:(1|0) | NSO
// 09 BYE - Free resources, rollback/abort transactions, and shutdown
// ACK

/// global Conf

// 10 CNF <key> <v> - Sets global config
// ACK | ERR ...
// 11 RST - Close database connection and free any references (but not global constants)
// ACK | ERR ...

/// Extensions, types, etc

// 12 ENB <EXT|TYPE> <v> - Enables extension/type
// ACK | ERR ... | NSO
// 13 DIS <EXT|TYPE> <v> - Disables extension/type
// ACK | ERR ... | NSO
// 14 LMT S:<DRV|GEN> <engine> - Limitations of driver/generator
// VAL ... | ERR ...
// 15 LMT S:<DRV|GEN> <engine> <type|command> - Limitations of types/commands for driver/generator
// VAL ... | ERR ...
// 17 HLP S:<EXT|TYP> v - Help documentation for extension/type
// VAL ... | ERR ...
// 18 HLP S:<DRV|GEN> <engine> v - Help documentation for generator/driver
// VAL ... | ERR ...

/// Resource management

// 1B ALC <v> - Allocate variable with value. Will assign to free name
// VAL ... | ERR ...
// 1C ALC <v> @<key> - Allocate variable with value and name if name is not used. Errors if name is used
// VAL ... | ERR ...
// 1D FRE @<key> - Free reference
// VAL ... | ERR ...

/// Connections

// 1E CON <v> - Establish database connection with configuration. Will assign to free name
// VAL ... | ERR ... | NSO
// 1F CON <v> @<key> - Establish database connection and assign it to variable (errs if variable is taken)
// VAL ... | ERR ... | NSO

/// Queries

// 20 PRE (S<n>:<v> | B<n>:<v>) - Prepare statement (always returns VAL)
// VAL ... | ERR ... | NSO
// 21 PRE (S<n>:<v> | B<n>:<v>) @<out> - Prepare statement and put into ouput
// AKW | ERR ... | NSO
// 22 QRY (S<n>:<v> | @<key>) (<v> | @<key>) - Run query (always returns VAL). If no params, send A:0
// VAL ... | ERR ... | NSO
// 23 QRY (S<n>:<v> | @<key>) (<v> | @<key>) @<out> - Run query (always returns VAL). If no params, send A:0
// VAL ... | ERR ... | NSO
// 24 EXC (S<n>:<v> | @<key>) (<v> | @<key>) - Execute (always returns VAL with lastinsertid/null & rows affected or ERR). If no params, send A:0
// 25 CNI <v> - Count number of input values
// VAL ... | ERR ... | NSO

/// Result Sets

// 28 HNX @<key> - Has next row?
// VAL ... | ERR ... | NSO
// 29 ROW @<key> - Get next row from result set
// VAL ... | ERR ... | NSO
// 2j HNS @<key> - Has next result set?
// VAL ... | ERR ... | NSO
// 2B NXS @<key> - Switch to next result set
// VAL ... | ERR ... | NSO

/// Columns

// 30 COL @<key> - Get column names for result set
// VAL ... | ERR ... | NSO
// 31 COL @<key> <index> - Get column name for result set
// VAL ... | ERR ... | NSO
// 32 CNL @<key> - Get which columns are nullable
// VAL ... | ERR ... | NSO
// 33 CNL @<key> <index> - Get which columns are nullable
// VAL ... | ERR ... | NSO
// 34 CDT @<key> - Get column database type names
// VAL ... | ERR ... | NSO
// 35 CDT @<key> <index> - Get column database type name
// VAL ... | ERR ... | NSO
// 36 CTL @<key> - Get column type lengths
// VAL ... | ERR ... | NSO
// 37 CTL @<key> <index> - Get column type length
// VAL ... | ERR ... | NSO
// 38 CPL @<key> - Get column precision lengths
// VAL ... | ERR ... | NSO
// 39 CPL @<key> <index> - Get column precision length
// VAL ... | ERR ... | NSO

// Transactions

// 40 BTX - Begin transaction
// VAL ... | ERR ... | NSO
// 41 BTX <v> - Begin transaction with options
// VAL ... | ERR ... | NSO
// 42 BTX <v> @<out> - Begin transaction with options, store to reference
// VAL ... | ERR ... | NSO
// 46 CMT <v> - Commit transaction
// VAL ... | ERR ... | NSO
// 47 RLB <v> - Rollback transaction
// VAL ... | ERR ... | NSO

/// Generation

// A0 GNI - List of engines with supported generators
// VAL ... | ERR ... | NSO
// A1 GNI <engine> - List of generators with their variance and summary for an engine
// VAL ... | ERR ... | NSO
// A2 GNI <engine> <generator> - Generator information with variance, summary, and doc string
// VAL ... | ERR ... | NSO
// A3 GEN <engine/conf> <generator> <variance> <params> - Generate string/binary
// VAL ... | ERR ... | NSO

//// I - Int (No limit on size)
//// S - String
//// N - Nil (always "N0:"
//// F - Flag (bool)
//// B - Binary (base64 encoded in ASCII)
//// D - Date/Time (ISO 8601)
//// U - Unix Timestamp, UTC (in seconds)
//// R - Real (double)
//// A:n - Array of n
//// M:n - Map of n

// TYPES
// 00 NIL
// 01 INT
// 02 REAL
// 03 FLAG
// 04 STR
// 05 BINARY
// 06 UNIX TIMESTAMP
// 07 UNIX (NANO)

// 11 GLOBAL
// 12 REGISTER

// 22 DATE
// 21 TIME
// 23 DATE+TIME
// 27 DATE+TIME+TIMEZONE OFFSET

// 30 ARRAY
// 31 MAP

// 40-FF [CUSTOM RANGE]

// Protocol info
/// 01 $proto - current protocol
/// 02 $protos - supported protocol
/// 03 $strict - Flag for whether or not the program is in strict mode (if in strict mode, an ERR will also exit the program with a non-zero exit code)

// Metadata
/// 04 $name - plugin name (Readonly)
/// 05 $version - plugin version (Readonly)
/// 06 $docstring - plugin documentation

// State
/// 07 $refs - list of active references (Readonly)
/// 08 $conf - current database configuration settings

// Capabilities and extensions
/// 10 $drivers - list of drivers and settings
/// 11 $generators - list of generators and settings
/// 12 $extensions - list of extensions
/// 13 $customtypes - list of custom types
/// 14 $commands - list of supported standard commands

// Benefits of version 2 protocol
// - Efficient binary based
// - Easy for a computer to work with
// - Simple to translate to/from Version 1 protocol

// limits of version 2 protocol
// - Extremely hard for a human to read
// - Strongly tied to version 1 protocol (though this is intentional)

// Version 3 protocol:
// < RDY
// > SET $proto I:3
// < ack
// > select($engines)
// < ["sqlite" "mysql"]
// > select($proto)
// > 2
// > select($protos)
// > [0 1 2]
// > set($cnf.file "file.db")
// < ack
// > set($cnf {host "localhost" url "hello world" pwd "hi\nthere" engine "sqlite" port 8080}
// < ack
// > connect($cnf)
// < @conn1
// > alloc({engine "mysql" host "localhost" url "hello world" port 8080})
// < @arg1
// > alloc(@mysql {engine "mysql" host "localhost" url "hello world" port 8080})
// > conn(@mysqlConn connect(mysql))
// < @mysqlConn
// > query(@conn1 "SELECT * FROM users LIMIT 2")
// < @res1
// > cols(@res1)
// < ["id" "name" "age"]
// > hasNext(@res1)
// < true
// > row(@res1)
// < [12 "bob" nil]
// > row(@res1)
// < [1 "fred" 32]
// > hasNext(@res1)
// < false
// > row(@res1)
// < error("NO_MORE_ROWS")
// > cols(@res1)
// < ["id" "name" "age"]
// > types(@res1)
// < ["INT" "TEXT" "INT"]
// > types(@res1)[1]
// < "TEXT"
// > nullable(@res1)
// < [false false true]
// > nullable(@res1)[2]
// < true
// > nullable(@res1)[4]
// < error("INDEX_OUT_OF_BOUNDS")
// > hasNext(@res1)
// < false
// > end(@res1)
// < ack

// Benefits of version 3 protocol
// - Very human friendly
// - Easy to work with and read
// - Simple to translate to/from Version 1 protocol

// limits of version 3 protocol
// - Inefficient
// - harder to parse
// - Strongly tied to version 1 protocol (though this is intentional)
