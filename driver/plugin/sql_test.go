package plugin_test

import (
	"context"
	"time"
	"fmt"
	"github.com/jmoiron/sqlx"
	"rove_migrate/common"
	"rove_migrate/conf"
	"rove_migrate/db"
	"testing"
)

type params struct {
	dbType pluginDB
	*testing.T
	*sqlx.DB
}

var (
	pluginTester = pluginDB{}
)

type pluginDB struct {
	conf *conf.PluginConf
}

func (pluginDB) RunH2Test(t *testing.T, fn func(params)) {
	fmt.Printf("Registered: %#v\n", common.DbDrivers())
	conf, err := common.NewConf(
		conf.WithDriver("test_plugin/h2"),
		conf.WithActionsProvider("nil"),
	)

	if err != nil {
		t.Fatal(err)
	}

	conn, err := db.ConnectTo(conf)
	if err != nil {
		t.Fatal(err)
	}
	defer func() {
		if err := conn.Close(); err != nil {
			t.Fatal(err)
		}
	}()

	params := params{pluginTester, t, conn.DB}

	fn(params)
}

func TestH2Conn(t *testing.T) { pluginTester.RunH2Test(t, testConn) }

func TestH2ExecQuery(t *testing.T) { pluginTester.RunH2Test(t, testSqlExecQuery) }

func TestH2PrepareStmt(t *testing.T) { pluginTester.RunH2Test(t, testSqlPrepareStmt) }

func traceIn(str string) string {
	fmt.Printf("Starting %s\n", str)
	return str
}

func traceOut(str string) {
	fmt.Printf("Ending %s\n", str)
}

func testConn(t params) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(8 * time.Second))
	defer cancel()

	conn, err := t.Conn(ctx)
	if err != nil {
		t.Fatalf("Unable to get connection. Error: %#v", err)
	}

	defer func() {
		if err := conn.Close(); err != nil {
			t.Fatal(err)
		}
	}()

	if err := conn.PingContext(ctx); err != nil {
		t.Fatalf("Could not verify db connection. Error: %#v", err)
	}
}

func testSqlTx(t params) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(8 * time.Second))
	defer cancel()

	conn, err := t.Conn(ctx)
	if err != nil {
		t.Fatalf("Unable to get connection. Error: %#v", err)
	}

	defer func() {
		if err := conn.Close(); err != nil {
			t.Fatal(err)
		}
	}()

	tx, err := conn.BeginTx(ctx, nil)
	if err != nil {
		t.Fatalf("Unable to start transaction")
	}

	initQuery := `DROP TABLE IF EXISTS users;
CREATE TABLE IF NOT EXISTS users (id INT PRIMARY KEY, name TEXT);
INSERT INTO users (id, name) VALUES (14, 'ryan'), (89, 'chad');`

	if _, err := tx.ExecContext(ctx, initQuery); err != nil {
		t.Fatalf("Could not execute data init for %#v. Error: %#v", initQuery, err.Error())
	}

	if err := tx.Rollback(); err != nil {
		t.Fatal(err)
	}

	tx, err = conn.BeginTx(ctx, nil)

	initQuery = `DROP TABLE IF EXISTS users;
CREATE TABLE IF NOT EXISTS users (id INT PRIMARY KEY, name TEXT);
INSERT INTO users (id, name) VALUES (12, 'john'), (23, 'bob');`

	if _, err := tx.ExecContext(ctx, initQuery); err != nil {
		t.Fatalf("Could not execute data init for %#v. Error: %#v", initQuery, err.Error())
	}
	if err := tx.Commit(); err != nil {
		t.Fatal(err)
	}

	queryRes, err := conn.QueryContext(ctx, "SELECT id, name FROM users;")
	if err != nil {
		t.Fatal(err)
	}
	defer func() {
		if err := queryRes.Close(); err != nil {
			t.Fatal(err)
		}
	}()

	count := 0
	for queryRes.Next() {
		count++
		var id int64
		var name string
		if err := queryRes.Scan(&id, &name); err != nil {
			t.Fatal(err)
		}

		if !(id == 12 && name == "john") && !(id == 23 && name == "bob") {
			t.Errorf("Unexpected row %v   %v", id, name)
		}
	}

	if count != 2 {
		t.Errorf("Expected 2 rows, received %v", count)
	}
	
	var name string
	queryRow := conn.QueryRowContext(ctx, "SELECT name FROM users WHERE id = 12;")
	if err := queryRow.Scan(&name); err != nil {
		t.Fatal(err)
	}
	if !(name == "john") {
		t.Errorf("Unexpected name %v", name)
	}
}

func testSqlExecQuery(t params) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(8 * time.Second))
	defer cancel()

	conn, err := t.Conn(ctx)
	if err != nil {
		t.Fatalf("Unable to get connection. Error: %#v", err)
	}

	defer func() {
		if err := conn.Close(); err != nil {
			t.Fatal(err)
		}
	}()

	initQuery := `DROP TABLE IF EXISTS users;
CREATE TABLE IF NOT EXISTS users (id INT PRIMARY KEY, name TEXT);
INSERT INTO users (id, name) VALUES (12, 'john'), (23, 'bob');`

	if _, err := conn.ExecContext(ctx, initQuery); err != nil {
		t.Fatalf("Could not execute data init for %#v. Error: %#v", initQuery, err.Error())
	}

	queryRes, err := conn.QueryContext(ctx, "SELECT id, name FROM users;")
	if err != nil {
		t.Fatal(err)
	}
	defer func() {
		if err := queryRes.Close(); err != nil {
			t.Fatal(err)
		}
	}()

	count := 0
	for queryRes.Next() {
		count++
		var id int64
		var name string
		if err := queryRes.Scan(&id, &name); err != nil {
			t.Fatal(err)
		}

		if !(id == 12 && name == "john") && !(id == 23 && name == "bob") {
			t.Errorf("Unexpected row %v   %v", id, name)
		}
	}

	if count != 2 {
		t.Errorf("Expected 2 rows, received %v", count)
	}
	
	var name string
	queryRow := conn.QueryRowContext(ctx, "SELECT name FROM users WHERE id = 12;")
	if err := queryRow.Scan(&name); err != nil {
		t.Fatal(err)
	}
	if !(name == "john") {
		t.Errorf("Unexpected name %v", name)
	}
}


func testSqlPrepareStmt(t params) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(8 * time.Second))
	defer cancel()

	conn, err := t.Conn(ctx)
	if err != nil {
		t.Fatalf("Unable to get connection. Error: %#v", err)
	}

	defer func() {
		if err := conn.Close(); err != nil {
			t.Fatal(err)
		}
	}()

	initQuery := `DROP TABLE IF EXISTS users;
CREATE TABLE IF NOT EXISTS users (id INT PRIMARY KEY, name TEXT);
INSERT INTO users (id, name) VALUES (12, 'john'), (23, 'bob');`

	stmtExec, err := conn.PrepareContext(ctx, initQuery)
	if err != nil {
		t.Fatal(err)
	}
	defer func() {
		if err := stmtExec.Close(); err != nil {
			t.Fatal(err)
		}
	}()

	if _, err := stmtExec.ExecContext(ctx); err != nil {
		t.Fatalf("Could not execute data init for %#v. Error: %#v", initQuery, err.Error())
	}

	stmtQuery, err := conn.PrepareContext(ctx, "SELECT id, name FROM users;")
	
	if err != nil {
		t.Fatal(err)
	}
	defer func() {
		if err := stmtQuery.Close(); err != nil {
			t.Fatal(err)
		}
	}()

	queryRes, err := stmtQuery.QueryContext(ctx)
	if err != nil {
		t.Fatal(err)
	}
	defer func() {
		if err := queryRes.Close(); err != nil {
			t.Fatal(err)
		}
	}()

	count := 0
	for queryRes.Next() {
		count++
		var id int64
		var name string
		if err := queryRes.Scan(&id, &name); err != nil {
			t.Fatal(err)
		}

		if !(id == 12 && name == "john") && !(id == 23 && name == "bob") {
			t.Errorf("Unexpected row %v   %v", id, name)
		}
	}

	if count != 2 {
		t.Errorf("Expected 2 rows, received %v", count)
	}
	
	stmtRowQuery, err := conn.PrepareContext(ctx, "SELECT name FROM users WHERE id = 12;")
	
	if err != nil {
		t.Fatal(err)
	}
	defer func() {
		if err := stmtQuery.Close(); err != nil {
			t.Fatal(err)
		}
	}()

	var name string
	queryRow := stmtRowQuery.QueryRowContext(ctx)
	if err := queryRow.Scan(&name); err != nil {
		t.Fatal(err)
	}
	if !(name == "john") {
		t.Errorf("Unexpected name %v", name)
	}
}

