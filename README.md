Rove Migrate
============

Rove Migrate is a dependency-tree based SQL migrator. Intead of ordering SQL migrations based on timestamps or file name ordering, migrations are orderd based on dependencies. Each migration is defined in a JSON file and declares which migrations it depends on. Rove Migrate will then create a dependency tree and then run migrations based on that dependency tree.

Additionally, Rove Migrate keeps track of detailed migration logs, including what migrations were ran, when they were ran, and what commands were ran. Doing so allows developers and database administrators to keep track of what changes were made to their database and when those changes were made. This allows for easier recovery and auditing when something goes wrong, such as failed migrations, failed rollbacks, or divergent databases.

Rove Migrate is written in GoLang. This allows it to be compiled to a single executable, and it allows for cross-compilation.

## Usage

Database parameters can be specified through CLI params, ENV variables, and/or TOML files. To specify a TOML file, use the `--conf <value>` CLI flag or the `DB_CONF` environment variable.

### Configuration

The following global configuration options are supported:

* Database Type/Engine - The Database Engine being connected to
    * Note: Use rove_migrate dbtypes to get a list of valid engines
    * Flags: `--dbtype`
    * Env Vars: `DB_TYPE`
    * TOML: `dbtype`
* Database - The Database to connect to
    * Note: Only used if the DB Engine differentiates Database and Schema, otherwise use Schema
    * Flags: `--database`, `--db`
    * Env Vars: `DB_DATABASE`
    * TOML: `database`, `db`
* Schema - The database schema to connect to
    * Note: If the engine doesn't differentiate databases and schemas, then this value is used.
    * Flags: `--schema`
    * Env Vars: `DB_SCHEMA`
    * TOML: `schema`
* File - The file to use for local/embedded databases
    * Note: Cannot be used with host/port
    * Flags: `--file`
    * Env Vars: `DB_FILE`
    * TOML: `file`
* Host - The host to connect to through a network protocol (e.g. TCP)
    * Note: Cannot be used with file
    * Flags: `--host`
    * Env Vars: `DB_HOST`
    * TOML: `host`
* Port - The port to connect to on the host
    * Note: Cannot be used with file. Defaults to default port for database engine/type.
    * Flags: `--port`
    * Env Vars: `DB_PORT`
    * TOML: `port`
* User - The user to use for authentication
    * Flags: `--user`, `--usr`
    * Env Vars: `DB_USER`
    * TOML: `user`, `usr`
* Password - The password to use for authentication
    * Flags: `--password`, `--pwd`
    * Env Vars: `DB_PWD`
    * TOML: `password`, `pwd`
* URL - The driver URL string for connecting to the database
    * Note: The migrator does not try to parse this URL to get schema/database information. Users should also pass in schema/database information with dedicated flags for most accurate results.
    * Flags: `--url`
    * Env Vars: `DB_URL`
    * TOML: `url`
* Log File - The file to log to. If not specified will use stderr
    * Flags: `--logFile`
    * Env Vars: `RM_LOG_FILE`
    * TOML: `logFile`
* Log JSON - Will format logs as JSON
    * Flags: `--logJson`
    * Env Vars: `RM_LOG_JSON`
    * TOML: `logJson`
* Log Level - Determines log level (defaults to "warn")
    * Flags: `--logLevel`
    * Env Vars: `RM_LOG_LEVEL`
    * TOML: `logLevel`
* Log No Colors - Turns off color logging (tries to auto detect support by default)
    * Flags: `--logNoColors`
    * Env Vars: `RM_LOG_NO_COLS`
    * TOML: `logNoColors`
* Password Prompt - Prompts password through stdin (ignores passwords passed through any other method)
    * Flags: `--promptPassword`, `--promptPwd`, `--pp`
    * Env Vars: `RM_PWD_PROMPT`

### Commands

The following commands are available.

* `check-conn`
    * Checks the connection to the database with the provided configuration. Useful for debugging.
* `dbtypes`
    * Lists all valid dbtypes
* `dep-tree`
    * Prints migration dependency tree
* `info`
    * Prints information about the database, including existing tables, indexes, ran migrations, etc.
* `init`
    * Initializes the migration tables in a database. Can pass `--create-database` and `--create-schema` to also create the database and schema (respectively)
    * Note: If you are only interested in creating the migration tables, that is done automatically as part of the migrate command
* `limits`
    * Prints known limitations with the selected DB Engine/Type
* `migrate`
    * Performs a forward migration on a database. Will create migration tables if needed
    * Can pass a target to migrate to with `--target`
    * Can view which migrations will run with `--dry-run`
    * Can do minimal migration to target with `--min`
        * Not including `--min` will migrate based on sorted topological order of entire dependency tree which could include disjointed subtrees
* `rollback`
    * Performs a roll back on a database.
    * Can pass a target to rollback to with `--target`
    * Can view which migrations will rollback with `--dry-run`
    * Can do minimal rollback to revert target with `--min`
        * Not including `--min` will revert based on sorted topological order of entire dependency tree which could include disjointed subtrees
* `sessions`
    * Lists migration session information
* `validate`
    * Validates migration scripts and state of the database
    * Can be ran in strict mode to make warnings show as errors
* `help`
    * Shows help messages.
    * Can provide a command to get help related to that command

## Road Map

* [x] Forward migrations (all, with target)
* [x] Rollback migrations (all, with target)
* [x] SQLite support
* [x] PostgreSQL support
* [x] Run SQL files
* [x] Run SQL snippets
* [x] Basic database-independent Create Table (and rollback)
* [x] Basic database-independent Alter Table
* [x] Basic database-independent Create Index
* [x] Migration validation
* [x] Improved error messages
* [x] Logging to file options
* [x] Database state verification (i.e. ensure dependency tree lines up with database)
* [x] Migration hashing and verification (i.e. detect script changes)
* [x] Interactive password prompting (with hidden password)
* [x] Dependency Tree Visualization (ASCII, static)
* [x] Minimal migrations (i.e. migrate minimal tree to support target)
* [x] Minimal rollback (i.e. rollback minimal tree to remove target)
* [x] Automated database testing framework
* [x] Track and undo specific migration sessions
* [x] Database URL parameter as alternative to host/port/user/password/file
* [ ] Plugin/extension system for adding additional driver support
* [ ] Limitation detection protocol
* [ ] Additional Database Support (e.g. MySQL, MS SQL, etc)
* [ ] Fork/Pipe protocol for interactive calling from external programs

## Running Tests

The recommended way to run tests is to create a TOML config file for postgres at `.conf/postgres.toml`. This file will have the connection information needed to connect to your postgres instance. Once this file is created, you can run `bash conftest.sh` which will load the configuration settings from the TOML file. The `.conf/` directory is ignored by git.

Example TOML file:

```toml
dbtype = "postgres"
host = "localhost"
database = "postgres"
port = 5432
pwd = "password"
user = "postgres"
schema = "public"
```

Note: the bash script does not handle comments at this time

Alternatively, you can define the following environment variables and then run `go test ./...`
* TEST_PGX_DATABASE
* TEST_PGX_HOST
* TEST_PGX_USER
* TEST_PGX_PWD
* TEST_PGX_PORT

The tests will automatically create and destroy a new schema with a random name. Make sure your test user has access for creating schemas.

## Licence

Rove Migrate is licenced under the MPL2.0
