#!/usr/bin/env bash

# Runs go tests using conf files from the (not checked in) .conf/ directory
# Note: Does NOT get the schema from the file! Uses the auto generated test schema instead
# This means that your user needs to have schema creation permissions

PGX=${PGX:-'.conf/postgres.toml'}
CLEANUP_SCOPE=${CLEANUP_SCOPE:-'schema'}

# Gets a field with a single name
get_from_file() {
	echo "$(cat "$1" | grep "^$2*" | awk -F '=' '{print $2}' | sed -e 's/[ \t]*//' | sed -e 's/"//' | sed -e 's/"$//' | tail -n 1)"
}

# Gets a field with 2 names
get_from_file_2() {
	echo "$(cat "$1" | grep -e "^$2*" -e "^$3*" | awk -F '=' '{print $2}' | sed -e 's/[ \t]*//' | sed -e 's/"//' | sed -e 's/"$//' | tail -n 1)"
}

# Gets a field with 2 names but excludes 1 name
get_from_file_2_n1() {
	echo "$(cat "$1" | grep -e "^$2*" -e "^$3*" | grep -invert -e "^$4*" | awk -F '=' '{print $2}' | sed -e 's/[ \t]*//' | sed -e 's/"//' | sed -e 's/"$//' | tail -n 1)"
}

# Setup postgres variables
export TEST_PGX_DATABASE="$(get_from_file_2_n1 "$PGX" 'database' 'db' 'dbtype')"
export TEST_PGX_HOST="$(get_from_file "$PGX" 'host')"
export TEST_PGX_USER="$(get_from_file_2 "$PGX" 'user' 'usr')"
export TEST_PGX_PWD="$(get_from_file_2 "$PGX" 'password' 'pwd')"
export TEST_PGX_PORT="$(get_from_file "$PGX" 'port')"
export TEST_PGX_CLEANUP_SCOPE="$CLEANUP_SCOPE"

echo "Running go tests..."
eval "go test -tags=test_all ./..."
# eval "go test -v -tags=test_all ./driver/plugin"
