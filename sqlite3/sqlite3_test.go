package sqlite3

import (
	"rove_migrate/common"
	"rove_migrate/conf"
	"rove_migrate/migs"
	"testing"
)

func TestParseSql(t *testing.T) {
	json := `{
	"name": "test",
	"depsOn": ["init"],
	"actions": [
		{
			"raw": {
				"up": "CREATE TABLE users (id INT)",
				"down": "DROP TABLE users"
			}
		}
	]
}`
	mig, err := migs.ParseMigration(json, "json")
	if err != nil {
		t.Fatalf("Received unexpected error %v", err)
	}

	cnf, err := common.NewConf(conf.WithDriver("sqlite3"), conf.WithGenerator("sqlite3"), conf.WithFile("test.db"))
	if err != nil {
		t.Fatalf("Received unexpected error %v", err)
	}

	ups, err := mig.Up(cnf)
	if err != nil {
		t.Fatalf("Received unexpected error %v", err)
	}

	if len(ups) != 1 {
		t.Errorf("Expected len 1, received %d", len(ups))
	}

	expected := "CREATE TABLE users (id INT)"
	if ups[0] != expected {
		t.Errorf("Expected %#v, received %#v", expected, ups[0])
	}
}

func TestSqlUp(t *testing.T) {
	mig := &common.Migration{
		Name: "test",
		Deps: []common.Dep{},
		Actions: []common.Action{
			common.Action{
				Sql: &common.SqlMig{
					Up:   "CREATE TABLE IF NOT EXISTS test (id INT)",
					Down: "DROP TABLE IF EXISTS test CASCADE",
				},
			},
		},
	}
	cnf, err := common.NewConf(conf.WithDriver("sqlite3"), conf.WithGenerator("sqlite3"), conf.WithFile("test.db"))
	if err != nil {
		t.Fatalf("Received unexpected error %v", err)
	}

	ups, err := mig.Up(cnf)

	if err != nil {
		t.Fatalf("Received unexpected error %v", err)
	}

	if len(ups) != 1 {
		t.Errorf("Expected len 1, received %d", len(ups))
	}

	expected := mig.Actions[0].Sql.Up
	if ups[0] != expected {
		t.Errorf("Expected %#v, received %#v", ups[0], expected)
	}
}

func TestCreateTable(t *testing.T) {
	gen := sqliteGen{}
	tests := []struct {
		name string
		tc   common.TableCreate
		ex   string
	}{
		{
			"Basic Create",
			common.TableCreate{
				Name: "user",
				Columns: []common.Column{
					{Name: "id", AutoIncrement: true, Type: "bigint", NotNull: true},
					{Name: "name", Type: "text", NotNull: false},
				},
				ErrIfExists: false,
				Timestamps:  false,
				PrimaryKey:  []string{"id"},
				ForeignKeys: []common.ForeignKey{},
			},
			`CREATE TABLE IF NOT EXISTS user (id INTEGER NOT NULL PRIMARY KEY, name TEXT NULL);`,
		},
		{
			"Timestamp Create",
			common.TableCreate{
				Name: "user",
				Columns: []common.Column{
					{Name: "id", AutoIncrement: true, Type: "bigint", NotNull: true},
					{Name: "name", Type: "text", NotNull: false},
				},
				ErrIfExists: true,
				Timestamps:  true,
				PrimaryKey:  []string{"id"},
				ForeignKeys: []common.ForeignKey{},
			},
			`CREATE TABLE user (id INTEGER NOT NULL PRIMARY KEY, name TEXT NULL, ctime TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, utime TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP);
CREATE TRIGGER IF NOT EXISTS [user__update_utime_trigger]
AFTER UPDATE ON user
FOR EACH ROW
BEGIN
	UPDATE user SET utime = CURRENT_TIMESTAMP WHERE id = old.id;
END;`,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			sql, err := gen.Generate("createTable", "UP", test.tc)
			if err != nil {
				t.Fatalf("Unexpected error %#v", err)
			}
			if sql != test.ex {
				t.Errorf("Unexpected SQL. Expected %#v, received %#v", test.ex, sql)
			}
		})
	}
}

func TestRevertCreateTable(t *testing.T) {
	gen := sqliteGen{}
	tests := []struct {
		name string
		tc   common.TableCreate
		ex   string
	}{
		{
			"Basic Create",
			common.TableCreate{
				Name: "user",
				Columns: []common.Column{
					{Name: "id", AutoIncrement: true, Type: "bigint", NotNull: true},
					{Name: "name", Type: "text", NotNull: false},
				},
				ErrIfExists: false,
				Timestamps:  false,
				PrimaryKey:  []string{"id"},
				ForeignKeys: []common.ForeignKey{},
			},
			`DROP TABLE IF EXISTS user;`,
		},
		{
			"Timestamp Create",
			common.TableCreate{
				Name: "user",
				Columns: []common.Column{
					{Name: "id", AutoIncrement: true, Type: "bigint", NotNull: true},
					{Name: "name", Type: "text", NotNull: false},
				},
				ErrIfExists: true,
				Timestamps:  true,
				PrimaryKey:  []string{"id"},
				ForeignKeys: []common.ForeignKey{},
			},
			`DROP TRIGGER [user__update_utime_trigger];
DROP TABLE user;`,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			sql, err := gen.Generate("createTable", "DOWN", test.tc)
			if err != nil {
				t.Fatalf("Unexpected error %#v", err)
			}
			if sql != test.ex {
				t.Errorf("Unexpected SQL. Expected %#v, received %#v", test.ex, sql)
			}
		})
	}
}

func TestAlterTable(t *testing.T) {
	gen := sqliteGen{}
	tests := []struct {
		name string
		tc   common.TableAlter
		ex   string
	}{
		{
			"Basic Alter",
			common.TableAlter{
				Name: "user",
				AddColumns: []common.Column{
					{Name: "id", AutoIncrement: true, Type: "bigint", NotNull: true},
					{Name: "name", Type: "text", NotNull: false},
				},
				RenameColumns: []common.ColRename{
					{From: "email", To: "emailAddress"},
				},
				RenameTo: "users",
			},
			`ALTER TABLE user RENAME COLUMN email TO emailAddress;
ALTER TABLE user ADD COLUMN id INTEGER NOT NULL PRIMARY KEY;
ALTER TABLE user ADD COLUMN name TEXT NULL;
ALTER TABLE user RENAME TO users;
`,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			sql, err := gen.Generate("alterTable", "UP", test.tc)
			if err != nil {
				t.Fatalf("Unexpected error %#v", err)
			}
			if sql != test.ex {
				t.Errorf("Unexpected SQL. Expected %#v, received %#v", test.ex, sql)
			}
		})
	}
}

func TestRevertAlterTable(t *testing.T) {
	gen := sqliteGen{}
	tests := []struct {
		name string
		tc   common.TableAlter
		ex   string
	}{
		{
			"Basic Alter",
			common.TableAlter{
				Name: "user",
				AddColumns: []common.Column{
					{Name: "id", AutoIncrement: true, Type: "bigint", NotNull: true},
					{Name: "name", Type: "text", NotNull: false},
				},
				RenameColumns: []common.ColRename{
					{From: "email", To: "emailAddress"},
				},
				RenameTo: "users",
			},
			`ALTER TABLE users RENAME TO user;
ALTER TABLE user DROP COLUMN id;
ALTER TABLE user DROP COLUMN name;
ALTER TABLE user RENAME COLUMN emailAddress TO email;
`,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			sql, err := gen.RevAlterTable(test.tc)
			if err != nil {
				t.Fatalf("Unexpected error %#v", err)
			}
			if sql != test.ex {
				t.Errorf("Unexpected SQL. Expected %#v, received %#v", test.ex, sql)
			}
		})
	}
}

func TestCreateIndex(t *testing.T) {
	gen := sqliteGen{}
	tests := []struct {
		name string
		tc   common.IndexCreate
		ex   string
	}{
		{
			"Unique Create",
			common.IndexCreate{
				Name:  "user_index",
				Table: "user",
				Columns: []string{
					"id",
					"name",
				},
				ErrIfExists: false,
				Unique:      true,
				Where:       "",
			},
			`CREATE UNIQUE INDEX IF NOT EXISTS user_index ON user(id, name)`,
		},
		{
			"Constrained Create",
			common.IndexCreate{
				Name:  "user_index",
				Table: "user",
				Columns: []string{
					"id",
					"name",
				},
				ErrIfExists: true,
				Unique:      false,
				Where:       "(id > 20)",
			},
			`CREATE INDEX user_index ON user(id, name) WHERE (id > 20)`,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			sql, err := gen.Generate("createIndex", "UP", test.tc)
			if err != nil {
				t.Fatalf("Unexpected error %#v", err)
			}
			if sql != test.ex {
				t.Errorf("Unexpected SQL. Expected %#v, received %#v", test.ex, sql)
			}
		})
	}
}

func TestRevertCreateIndex(t *testing.T) {
	gen := sqliteGen{}
	tests := []struct {
		name string
		tc   common.IndexCreate
		ex   string
	}{
		{
			"Unique Create",
			common.IndexCreate{
				Name:  "user_index",
				Table: "user",
				Columns: []string{
					"id",
					"name",
				},
				ErrIfExists: false,
				Unique:      true,
				Where:       "",
			},
			`DROP INDEX IF EXISTS user_index`,
		},
		{
			"Constrained Create",
			common.IndexCreate{
				Name:  "user_index",
				Table: "user",
				Columns: []string{
					"id",
					"name",
				},
				ErrIfExists: true,
				Unique:      false,
				Where:       "(id > 20)",
			},
			`DROP INDEX user_index`,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			sql, err := gen.Generate("createIndex", "DOWN", test.tc)
			if err != nil {
				t.Fatalf("Unexpected error %#v", err)
			}
			if sql != test.ex {
				t.Errorf("Unexpected SQL. Expected %#v, received %#v", test.ex, sql)
			}
		})
	}
}
