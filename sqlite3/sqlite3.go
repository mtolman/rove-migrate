package sqlite3

import (
	"fmt"
	"rove_migrate/common"
	"rove_migrate/migs"
	"slices"
	"strconv"
	"strings"
	"time"

	"github.com/jmoiron/sqlx"
	log "github.com/sirupsen/logrus"
	_ "modernc.org/sqlite"
)

type sqliteGen struct{}

type sqlite3Actions struct{}

// Generates Sqlite3 driver URL
func SqliteUrlBuilder(conf *common.DbConf) (string, error) {
	if conf.File() == "" {
		return "", fmt.Errorf("Cannot build URL. No file specified for SQLite3 DB")
	}

	urlBuilder := strings.Builder{}
	urlBuilder.WriteString("file:")
	urlBuilder.WriteString(conf.File())

	log.Tracef("Connection url: %v", urlBuilder.String())

	return urlBuilder.String(), nil
}

// Registers postgres to the database registry
func init() {
	log.Debug("Registering sqlite3...")
	common.RegisterFileDriver(
		"sqlite3",
		"sqlite",
		sqlite3Actions{},
		SqliteUrlBuilder,
		common.WithDriverLimits(` - No Schema support (ignored)
 - No Database support (replaced with "file")
 - Local File system Only`))
	common.RegisterNativeGenerator(
		"sqlite3",
		sqliteGen{},
		common.WithGeneratorLimits(` - No Foreign Key Support (ignored)
 - No Schema support (ignored)
 - No Database support (replaced with "file")
 - Generated timestamp columns can only happen on tables with primary keys
 - Creates additional triggers for tables with timestamp columns
 - Most types are simplified to a small subset of types
			`),
	)
}

func (s sqliteGen) Generate(generation, variance string, args any) (string, error) {
	return common.GenerateNative(s, generation, variance, args)
}

// SQL generation for creating an index
func (s sqliteGen) GenCreateIndex(ic common.IndexCreate) (string, error) {
	builder := strings.Builder{}
	if len(ic.Columns) == 0 {
		return "", fmt.Errorf("Cannot create index. Must specify columns to be indexed")
	}

	builder.WriteString("CREATE ")
	if ic.Unique {
		builder.WriteString("UNIQUE ")
	}
	builder.WriteString("INDEX ")

	if !ic.ErrIfExists {
		builder.WriteString("IF NOT EXISTS ")
	}

	builder.WriteString(ic.Name)
	builder.WriteString(" ON ")
	builder.WriteString(ic.Table)
	builder.WriteString("(")

	for i, index := range ic.Columns {
		if i != 0 {
			builder.WriteString(", ")
		}
		builder.WriteString(index)
	}
	builder.WriteString(")")

	if ic.Where != "" {
		builder.WriteString(" WHERE ")
		builder.WriteString(ic.Where)
	}

	return builder.String(), nil
}

// SQL generation for rolling back a create index
func (s sqliteGen) RevCreateIndex(ic common.IndexCreate) (string, error) {
	builder := strings.Builder{}
	if len(ic.Columns) == 0 {
		return "", fmt.Errorf("Cannot perform rollback. Invalid index migration. Must specify columns in index")
	}

	builder.WriteString("DROP INDEX ")
	if !ic.ErrIfExists {
		builder.WriteString("IF EXISTS ")
	}
	builder.WriteString(ic.Name)

	return builder.String(), nil
}

// SQL generation for altering a table
func (s sqliteGen) GenAlterTable(ta common.TableAlter) (string, error) {
	if len(ta.AddColumns) == 0 && len(ta.RenameColumns) == 0 && ta.RenameTo == "" {
		return "", fmt.Errorf("Cannot generate migration SQL. Alter table must have at least one modification")
	}

	prefixBuilder := strings.Builder{}
	prefixBuilder.WriteString("ALTER TABLE ")
	prefixBuilder.WriteString(ta.Name)
	prefixBuilder.WriteString(" ")
	prefix := prefixBuilder.String()
	suffix := ";\n"

	builder := strings.Builder{}

	for _, renameCol := range ta.RenameColumns {
		builder.WriteString(prefix)
		builder.WriteString("RENAME COLUMN ")
		builder.WriteString(renameCol.From)
		builder.WriteString(" TO ")
		builder.WriteString(renameCol.To)
		builder.WriteString(suffix)
	}

	for _, addCol := range ta.AddColumns {
		builder.WriteString(prefix)
		builder.WriteString("ADD COLUMN ")

		t, err := migs.ToSqlFieldType(addCol.Type)
		if err != nil {
			return "", fmt.Errorf("Cannot generate migration SQL. %v", err)
		}
		builder.WriteString(addCol.Name)
		builder.WriteString(" ")
		ts, err := s.typeStr(t)
		if err != nil {
			return "", fmt.Errorf("Cannot generate migration SQL. %v", err)
		}
		builder.WriteString(ts)

		if addCol.NotNull {
			builder.WriteString(" NOT NULL")
		} else {
			builder.WriteString(" NULL")
		}

		if addCol.AutoIncrement {
			builder.WriteString(" PRIMARY KEY")
		}
		if addCol.DefaultValue != nil {
			builder.WriteString(" DEFAULT ")
			switch v := addCol.DefaultValue.(type) {
			case string:
				log.Trace("Default value of type string found, writing it out as is")
				builder.WriteString(v)
			case int:
				log.Trace("Default value of type int found. Converting to base 10 string")
				builder.WriteString(strconv.FormatInt(int64(v), 10))
			case int64:
				log.Trace("Default value of type int64 found. Converting to base 10 string")
				builder.WriteString(strconv.FormatInt(int64(v), 10))
			case uint:
				log.Trace("Default value of type uint found. Converting to base 10 string")
				builder.WriteString(strconv.FormatUint(uint64(v), 10))
			case uint64:
				log.Trace("Default value of type uint64 found. Converting to base 10 string")
				builder.WriteString(strconv.FormatUint(uint64(v), 10))
			case float32:
				log.Trace("Default value of type float32 found. Converting to base 10 string with options 'f', -1, 32")
				builder.WriteString(strconv.FormatFloat(float64(v), 'f', -1, 32))
			case float64:
				log.Trace("Default value of type float64 found. Converting to base 10 string with options 'f', -1, 64")
				builder.WriteString(strconv.FormatFloat(float64(v), 'f', -1, 64))
			case bool:
				log.Trace("Default value of type bool found. Converting to 'true'/'false' literals")
				if v {
					builder.WriteString("true")
				} else {
					builder.WriteString("false")
				}
			default:
				log.Errorf("Unable to handle default value of type %T. Failing with default value %v", v, v)
				return "", fmt.Errorf("Cannot generate migration SQL. Unable to use default value %#v", v)
			}
		}
		builder.WriteString(suffix)
	}

	if ta.RenameTo != "" {
		builder.WriteString(prefix)
		builder.WriteString("RENAME TO ")
		builder.WriteString(ta.RenameTo)
		builder.WriteString(suffix)
	}

	return builder.String(), nil
}

// Rollbacks an alter table
func (s sqliteGen) RevAlterTable(ta common.TableAlter) (string, error) {
	if len(ta.AddColumns) == 0 && len(ta.RenameColumns) == 0 && ta.RenameTo == "" {
		return "", fmt.Errorf("Cannot generate rollback SQL. Alter table must have at least one modification")
	}
	prefixBuilder := strings.Builder{}
	prefixBuilder.WriteString("ALTER TABLE ")
	prefixBuilder.WriteString(ta.Name)
	prefixBuilder.WriteString(" ")
	prefix := prefixBuilder.String()
	suffix := ";\n"

	builder := strings.Builder{}

	if ta.RenameTo != "" {
		prefixBuilder := strings.Builder{}
		prefixBuilder.WriteString("ALTER TABLE ")
		prefixBuilder.WriteString(ta.RenameTo)
		prefixBuilder.WriteString(" ")
		prefix := prefixBuilder.String()
		builder.WriteString(prefix)
		builder.WriteString("RENAME TO ")
		builder.WriteString(ta.Name)
		builder.WriteString(suffix)
	}

	for _, addCol := range ta.AddColumns {
		builder.WriteString(prefix)
		builder.WriteString("DROP COLUMN ")
		builder.WriteString(addCol.Name)
		builder.WriteString(suffix)
	}

	for _, renameCol := range ta.RenameColumns {
		builder.WriteString(prefix)
		builder.WriteString("RENAME COLUMN ")
		builder.WriteString(renameCol.To)
		builder.WriteString(" TO ")
		builder.WriteString(renameCol.From)
		builder.WriteString(suffix)
	}

	return builder.String(), nil
}

// Rollbacks a table creation
func (s sqliteGen) RevCreateTable(tc common.TableCreate) (string, error) {
	builder := strings.Builder{}

	if tc.Timestamps {
		builder.WriteString("DROP TRIGGER ")
		if !tc.ErrIfExists {
			builder.WriteString("IF EXISTS ")
		}
		builder.WriteString("[")
		builder.WriteString(tc.Name)
		builder.WriteString("__update_utime_trigger];\n")
	}

	builder.WriteString("DROP TABLE ")
	if !tc.ErrIfExists {
		builder.WriteString("IF EXISTS ")
	}
	builder.WriteString(tc.Name)
	builder.WriteString(";")
	return builder.String(), nil
}

// Generates SQL for creating a table
func (s sqliteGen) GenCreateTable(tc common.TableCreate) (string, error) {
	builder := strings.Builder{}

	builder.WriteString("CREATE TABLE ")
	if !tc.ErrIfExists {
		builder.WriteString("IF NOT EXISTS ")
	}
	builder.WriteString(tc.Name)
	builder.WriteString(" (")

	foundPk := false
	for i, col := range tc.Columns {
		t, err := migs.ToSqlFieldType(col.Type)
		if err != nil {
			return "", fmt.Errorf("Cannot generate migration SQL. %v", err)
		}
		if i != 0 {
			builder.WriteString(", ")
		}
		builder.WriteString(col.Name)
		builder.WriteString(" ")
		ts, err := s.typeStr(t)
		if err != nil {
			return "", fmt.Errorf("Cannot generate migration SQL. %v", err)
		}
		builder.WriteString(ts)

		if col.NotNull {
			builder.WriteString(" NOT NULL")
		} else {
			builder.WriteString(" NULL")
		}

		if col.AutoIncrement || slices.Index(tc.PrimaryKey, col.Name) >= 0 {
			foundPk = true
			builder.WriteString(" PRIMARY KEY")
		}
		if col.DefaultValue != nil {
			builder.WriteString(" DEFAULT ")
			switch v := col.DefaultValue.(type) {
			case string:
				log.Trace("Default value of type string found, writing it out as is")
				builder.WriteString(v)
			case int:
				log.Trace("Default value of type int found. Converting to base 10 string")
				builder.WriteString(strconv.FormatInt(int64(v), 10))
			case int64:
				log.Trace("Default value of type int64 found. Converting to base 10 string")
				builder.WriteString(strconv.FormatInt(int64(v), 10))
			case uint:
				log.Trace("Default value of type uint found. Converting to base 10 string")
				builder.WriteString(strconv.FormatUint(uint64(v), 10))
			case uint64:
				log.Trace("Default value of type uint64 found. Converting to base 10 string")
				builder.WriteString(strconv.FormatUint(uint64(v), 10))
			case float32:
				log.Trace("Default value of type float32 found. Converting to base 10 string with options 'f', -1, 32")
				builder.WriteString(strconv.FormatFloat(float64(v), 'f', -1, 32))
			case float64:
				log.Trace("Default value of type float64 found. Converting to base 10 string with options 'f', -1, 64")
				builder.WriteString(strconv.FormatFloat(float64(v), 'f', -1, 64))
			case bool:
				log.Trace("Default value of type bool found. Converting to 'true'/'false' literals")
				if v {
					builder.WriteString("true")
				} else {
					builder.WriteString("false")
				}
			default:
				log.Errorf("Unable to handle default value of type %T. Failing with default value %v", v, v)
				return "", fmt.Errorf("Cannot generate migration SQL. Unable to use default value %#v", col.DefaultValue)
			}
		}
	}

	if tc.Timestamps {
		if !foundPk {
			return "", fmt.Errorf("Cannot generate migration SQL. Cannot add timestamps to a table with no primary key with sqlite3.")
		}
		if len(tc.Columns) > 0 {
			builder.WriteString(", ")
		}
		builder.WriteString("ctime TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, utime TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP")
	}

	builder.WriteString(");")

	if tc.Timestamps {
		builder.WriteString("\nCREATE TRIGGER IF NOT EXISTS [")
		builder.WriteString(tc.Name)
		builder.WriteString(`__update_utime_trigger]
AFTER UPDATE ON `)
		builder.WriteString(tc.Name)
		builder.WriteString(`
FOR EACH ROW
BEGIN
	UPDATE `)
		builder.WriteString(tc.Name)
		builder.WriteString(` SET utime = CURRENT_TIMESTAMP WHERE `)
		for i, pk := range tc.PrimaryKey {
			if i != 0 {
				builder.WriteString(" AND ")
			}
			builder.WriteString(pk)
			builder.WriteString(" = old.")
			builder.WriteString(pk)
		}
		builder.WriteString(`;
END;`)
	}

	return builder.String(), nil
}

// Converts SQL field type information into a Sqlite3 type
func (s sqliteGen) typeStr(t *migs.SqlFieldType) (string, error) {
	switch t.SqlType {
	case migs.SQL_INT:
		return "INTEGER", nil
	case migs.SQL_BIGINT:
		log.Warn("Converting BIGINT to INTEGER")
		return "INTEGER", nil
	case migs.SQL_TINYINT:
		log.Warn("Converting TINYINT to INTEGER")
		return "INTEGER", nil
	case migs.SQL_MEDIUMINT:
		log.Warn("Converting MEDIUMINT to INTEGER")
		return "INTEGER", nil
	case migs.SQL_BIT:
		log.Warn("Converting BIT to INTEGER")
		return "INTEGER", nil
	case migs.SQL_BOOL:
		log.Warn("Converting BOOL to NUMERIC")
		return "NUMERIC", nil
	case migs.SQL_FLOAT:
		log.Warn("Converting FLOAT to REAL")
		return "REAL", nil
	case migs.SQL_DOUBLE:
		return "REAL", nil
	case migs.SQL_DOUBLE_PRECISION:
		log.Warn("Converting DOUBLE PRECISION to REAL")
		return "REAL", nil
	case migs.SQL_DECIMAL:
		log.Warn("Converting DECIMAL to NUMERIC")
		return "NUMERIC", nil
	case migs.SQL_MONEY:
		log.Warn("Converting DECIMAL to NUMERIC")
		return "NUMERIC", nil
	case migs.SQL_NUMERIC:
		return "NUMERIC", nil
	case migs.SQL_CHAR:
		log.Warn("Converting CHAR to TEXT")
		return "TEXT", nil
	case migs.SQL_VARCHAR:
		log.Warn("Converting VARCHAR to TEXT")
		return "TEXT", nil
	case migs.SQL_TINYTEXT:
		log.Warn("Converting TINYTEXT to TEXT")
		return "TEXT", nil
	case migs.SQL_TEXT:
		return "TEXT", nil
	case migs.SQL_LONGTEXT:
		log.Warn("Converting LONGTEXT to TEXT")
		return "TEXT", nil
	case migs.SQL_TINYBLOB:
		log.Warn("Converting TINYBLOB to BLOB")
		return "BLOB", nil
	case migs.SQL_BLOB:
		return "BLOB", nil
	case migs.SQL_LONGBLOB:
		log.Warn("Converting LONGBLOB to BLOB")
		return "BLOB", nil
	case migs.SQL_BINARY:
		log.Warn("Converting BINARY to BLOB")
		return "BLOB", nil
	case migs.SQL_VARBINARY:
		log.Warn("Converting VARBINARY to BLOB")
		return "BLOB", nil
	case migs.SQL_DATE:
		return "DATE", nil
	case migs.SQL_DATETIME:
		return "DATETIME", nil
	case migs.SQL_TIMESTAMP:
		return "TIMESTAMP", nil
	case migs.SQL_TIME:
		return "TIME", nil
	case migs.SQL_YEAR:
		log.Warn("Converting YEAR to NUMERIC")
		return "NUMERIC", nil
	}
	return "", fmt.Errorf("Unmapped field type %#v", t.SqlType)
}

// Ensures migration tables are setup
func (sa sqlite3Actions) EnsureSetup(s *sqlx.DB, cnf *common.DbConf) error {
	createQuery :=
		`CREATE TABLE IF NOT EXISTS rove_migrate__version_info (
	id VARCHAR(500) NOT NULL,
	version INT NOT NULL,
	PRIMARY KEY(id)
);

INSERT OR IGNORE INTO rove_migrate__version_info(id, version) VALUES('migs_schema', 1);
INSERT OR IGNORE INTO rove_migrate__version_info(id, version) VALUES('runs_schema', 1);
INSERT OR IGNORE INTO rove_migrate__version_info(id, version) VALUES('cmd_log_schema', 1);
INSERT OR IGNORE INTO rove_migrate__version_info(id, version) VALUES('session_schema', 1);
INSERT OR IGNORE INTO rove_migrate__version_info(id, version) VALUES('session_migs_schema', 1);

CREATE TABLE IF NOT EXISTS rove_migrate__migs (
	id VARCHAR(500) NOT NULL,
	state INT NOT NULL DEFAULT 0,
	error TEXT NULL,
	ctime TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	utime TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	hash CHAR(90),
	PRIMARY KEY(id)
);

CREATE TRIGGER IF NOT EXISTS [rove_migrate__update_utime_migs]
AFTER UPDATE ON rove_migrate__migs
FOR EACH ROW
BEGIN
	UPDATE rove_migrate__migs SET utime = CURRENT_TIMESTAMP WHERE id = old.id;
END;

CREATE TABLE IF NOT EXISTS rove_migrate__runs (
	id INTEGER PRIMARY KEY,
	ctime TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	utime TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	migration_id VARCHAR(500) NOT NULL,
	is_rollback INTEGER NOT NULL,
	error TEXT NULL,
	success INTEGER NOT NULL
);

CREATE INDEX IF NOT EXISTS rove_migrate__runs_indx_migration_id ON
	rove_migrate__runs(migration_id);

CREATE TRIGGER IF NOT EXISTS [rove_migrate__update_utime_runs]
AFTER UPDATE ON rove_migrate__runs
FOR EACH ROW
BEGIN
	UPDATE rove_migrate__runs SET utime = CURRENT_TIMESTAMP WHERE id = old.id;
END;

CREATE TABLE IF NOT EXISTS rove_migrate__cmd_log (
	id INTEGER PRIMARY KEY,
	run_id INTEGER NOT NULL,
	migration_id VARCHAR(500) NOT NULL,
	sql_cmd TEXT NOT NULL,
	ctime TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	utime TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE INDEX IF NOT EXISTS rove_migrate__cmd_log_indx_migration_id ON
	rove_migrate__cmd_log(migration_id);

CREATE INDEX IF NOT EXISTS rove_migrate__cmd_log_indx_runid ON
	rove_migrate__cmd_log(run_id);

CREATE TRIGGER IF NOT EXISTS [rove_migrate__update_utime_cmd]
AFTER UPDATE ON rove_migrate__cmd_log
FOR EACH ROW
BEGIN
	UPDATE rove_migrate__cmd_log SET utime = CURRENT_TIMESTAMP WHERE id = old.id;
END;

CREATE TABLE IF NOT EXISTS rove_migrate__sessions (
	id INTEGER NOT NULL PRIMARY KEY,
	ctime TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	utime TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	is_rollback INTEGER NOT NULL
);

CREATE TRIGGER IF NOT EXISTS [rove_migrate__update_utime_sessions]
AFTER UPDATE ON rove_migrate__sessions
FOR EACH ROW
BEGIN
	UPDATE rove_migrate__sessions SET utime = CURRENT_TIMESTAMP WHERE id = old.id;
END;

CREATE TABLE IF NOT EXISTS rove_migrate__sessions_migs (
	session_id INTEGER NOT NULL,
	migration_id VARCHAR(500) NOT NULL
);

CREATE INDEX IF NOT EXISTS rove_migrate__sessions_migs_indx_session ON
	rove_migrate__sessions_migs(session_id);

CREATE INDEX IF NOT EXISTS rove_migrate__sessions_migs_indx_migration ON
	rove_migrate__sessions_migs(migration_id);
	`
	log.Info("Creating migration tables")

	if _, err := s.Exec(createQuery); err != nil {
		return fmt.Errorf("Unable to create migration tables. %v", err)
	}

	return nil
}

// Gets database state information
func (sa sqlite3Actions) GetInfo(s *sqlx.DB, cnf *common.DbConf) (*common.Info, error) {
	var query string
	var args []any
	var db string

	if cnf.File() != "" {
		db = "\"" + cnf.File() + "\""
	} else {
		db = "\"" + cnf.DriverUrl() + "\""
	}

	query = `SELECT DISTINCT tbl_name FROM sqlite_master WHERE type = "table" AND tbl_name NOT NULL AND tbl_name <> '' ORDER BY tbl_name`

	rows, err := s.Query(query, args...)
	if err != nil {
		return nil, fmt.Errorf("Unable to get migration info. %v", err)
	}
	defer rows.Close()

	colQuery := `SELECT name, type, "notnull", pk FROM pragma_table_info($1) ORDER BY name`

	var tables []common.TableInfo
	for rows.Next() {
		var tableName string
		if err := rows.Scan(&tableName); err != nil {
			return nil, fmt.Errorf("Unable to get table info. %v", err)
		}

		cols, err := s.Query(colQuery, tableName)
		if err != nil {
			return nil, fmt.Errorf("Unable to get table info. %v", err)
		}
		defer cols.Close()

		tableCols := []common.ColumnInfo{}
		for cols.Next() {
			var colName, colType string
			var notNull, pk bool
			if err := cols.Scan(&colName, &colType, &notNull, &pk); err != nil {
				return nil, fmt.Errorf("Unable to get table column info. %v", err)
			}
			if colType == "" {
				colType = "N/A"
			}
			tableCols = append(tableCols, common.ColumnInfo{Name: colName, ColType: colType})

		}

		if err := cols.Err(); err != nil {
			return nil, fmt.Errorf("Unable to get table column info. %v", err)
		}

		tables = append(tables, common.TableInfo{Name: tableName, Schema: db, Cols: tableCols})
		cols.Close()
	}

	if err := rows.Err(); err != nil {
		return nil, fmt.Errorf("Unable to get table info. %v", err)
	}

	migInfo, err := sa.GetMigrationInfo(s, cnf)
	if err != nil {
		return nil, fmt.Errorf("Unable to get migration info. %v", err)
	}

	indexQuery := `SELECT name, tbl_name FROM sqlite_master WHERE type = "index" AND tbl_name NOT NULL AND tbl_name <> '' ORDER BY name`
	var indexes []common.IndexInfo

	indexRows, err := s.Query(indexQuery, args...)
	if err != nil {
		return nil, fmt.Errorf("Unable to get index info. %v", err)
	}
	defer indexRows.Close()

	for indexRows.Next() {
		indexInfo := common.IndexInfo{}
		if err := indexRows.Scan(&indexInfo.Name, &indexInfo.Table); err != nil {
			return nil, fmt.Errorf("Unable to get index info. %v", err)
		}
		indexes = append(indexes, indexInfo)
	}

	if err := indexRows.Err(); err != nil {
		return nil, fmt.Errorf("Unable to get index info. %v", err)
	}

	triggerQuery := `SELECT name, tbl_name FROM sqlite_master WHERE type = "trigger" AND tbl_name NOT NULL AND tbl_name <> '' ORDER BY name`
	triggerRows, err := s.Query(triggerQuery, args...)
	if err != nil {
		return nil, fmt.Errorf("Unable to get trigger info. %v", err)
	}
	defer indexRows.Close()

	var triggers []common.TriggerInfo
	for triggerRows.Next() {
		triggerInfo := common.TriggerInfo{}
		if err := triggerRows.Scan(&triggerInfo.Name, &triggerInfo.Table); err != nil {
			return nil, fmt.Errorf("Unable to get trigger info. %v", err)
		}
		triggers = append(triggers, triggerInfo)
	}

	if err := triggerRows.Err(); err != nil {
		return nil, fmt.Errorf("Unable to get trigger info. %v", err)
	}

	versionInfo := map[string]string{}
	versionRows, err := s.Query("SELECT id, version FROM rove_migrate__version_info")
	if err != nil {
		return nil, err
	}
	defer versionRows.Close()

	for versionRows.Next() {
		var key, ver string
		if err := versionRows.Scan(&key, &ver); err != nil {
			return nil, fmt.Errorf("Cannot get version information. %v", err)
		}
		versionInfo[key] = ver
	}

	if err := versionRows.Err(); err != nil {
		return nil, fmt.Errorf("Cannot get version information. %v", err)
	}

	sessionInfo := map[int64]*common.SessionInfo{}
	sessionRows, err := s.Query("SELECT s.id, s.ctime, s.is_rollback, m.migration_id FROM rove_migrate__sessions s INNER JOIN rove_migrate__sessions_migs m ON s.id = m.session_id ORDER BY s.id, m.migration_id ASC")
	if err != nil {
		return nil, err
	}
	defer sessionRows.Close()

	for sessionRows.Next() {
		var migrationId string
		var sessionId int64
		var ctime time.Time
		var isRollback bool
		if err := sessionRows.Scan(&sessionId, &ctime, &isRollback, &migrationId); err != nil {
			return nil, fmt.Errorf("Cannot get session information. %v", err)
		}
		if _, ok := sessionInfo[sessionId]; !ok {
			sessionInfo[sessionId] = &common.SessionInfo{Id: sessionId, IsRollback: isRollback, Ctime: ctime, Migrations: []string{}}
		}
		sessionInfo[sessionId].Migrations = append(sessionInfo[sessionId].Migrations, migrationId)
	}

	if err := sessionRows.Err(); err != nil {
		return nil, fmt.Errorf("Cannot get session information. %v", err)
	}

	sessions := []common.SessionInfo{}
	for _, s := range sessionInfo {
		sessions = append(sessions, *s)
	}
	slices.SortFunc(sessions, func(a, b common.SessionInfo) int {
		if a.Id < b.Id {
			return -1
		} else if a.Id > b.Id {
			return 1
		} else {
			return 0
		}
	})

	return &common.Info{
		Tables:      tables,
		Migrations:  migInfo,
		Indexes:     indexes,
		Triggers:    triggers,
		VersionInfo: versionInfo,
		Sessions:    sessions,
	}, nil
}

// Gets migration information
func (sa sqlite3Actions) GetMigrationInfo(s *sqlx.DB, cnf *common.DbConf) (*common.MigrationOverview, error) {
	return migs.SqlMigrationInfo(s, cnf)
}

// Runs a migration
func (sa sqlite3Actions) RunMigration(migration *common.Migration, cmds []string, s *sqlx.DB, cnf *common.DbConf) (err error) {
	return migs.SqlRunMigration(sa, migration, cmds, s, cnf)
}

// Rollbacks a migration
func (sa sqlite3Actions) RollbackMigration(migration *common.Migration, cmds []string, s *sqlx.DB, cnf *common.DbConf) error {
	return migs.SqlRollbackMigration(sa, migration, cmds, s, cnf)
}

// Get migrations with given state
func (sa sqlite3Actions) GetMigrated(s *sqlx.DB, cnf *common.DbConf, state int) ([]string, error) {
	return migs.SqlGetMigrated(s, cnf, state)
}

// Gets integrity hashing
func (sa sqlite3Actions) GetHashes(s *sqlx.DB, cnf *common.DbConf) ([]common.MigrationHashCheck, error) {
	return migs.SqlGetHashes(s, cnf)
}

// Ensures a database is setup
func (sa sqlite3Actions) EnsureDatabase(database string, s *sqlx.DB) error {
	log.Info("Ensuring the database is a no-op for sqlite")
	return nil
}

// Ensures a schema is setup
func (sa sqlite3Actions) EnsureSchema(schemaName string, s *sqlx.DB) error {
	log.Info("Ensuring the schema is a no-op for sqlite")
	return nil
}

// Updates migration run info
func (sq sqlite3Actions) UpdateRunInfo(runId int64, run *common.Run, tx *sqlx.Tx) error {
	return migs.SqlUpdateRunInfo(runId, run, tx)
}

// Creates migration run info
func (sa sqlite3Actions) CreateRunInfo(run *common.Run, tx *sqlx.Tx, cnf *common.DbConf) (int64, error) {
	execQuery := `INSERT INTO rove_migrate__runs
(migration_id, is_rollback, success, error)
VALUES ($1, $2, $3, $4)`
	res, err := tx.Exec(execQuery, run.Migration, run.IsRollback, run.Success, run.Error)
	if err != nil {
		return 0, fmt.Errorf("Failed creating migration run record for migration %+v.", run.Migration)
	}
	return res.LastInsertId()
}

// Saves command log information
func (sa sqlite3Actions) SaveCmdInfo(cmd string, tx *sqlx.Tx, runId int64, migration *common.Migration) error {
	return migs.SqlSaveCmdInfo(cmd, migration, tx, runId)
}

// Saves migration information
func (sa sqlite3Actions) SaveMigInfo(mig *common.Migration, migState int, err error, tx *sqlx.Tx, cnf *common.DbConf) error {
	return migs.SqlSaveMigInfo(mig, migState, tx, cnf, err)
}

func (sa sqlite3Actions) Assert(cmd string, tx *sqlx.Tx, cnf *common.DbConf) error {
	return migs.SqlAssert(cmd, tx, cnf)
}

func (sa sqlite3Actions) CreateSession(isRollback bool, s *sqlx.DB, cnf *common.DbConf) (int64, error) {
	exec := `INSERT INTO rove_migrate__sessions
(is_rollback) VALUES ($1)`
	var arg int
	if isRollback {
		arg = 1
	} else {
		arg = 0
	}
	res, err := s.Exec(exec, arg)
	if err != nil {
		return 0, err
	}
	return res.LastInsertId()
}

func (sa sqlite3Actions) SaveMigrationToSession(sessionId int64, mig *common.Migration, s *sqlx.DB, cnf *common.DbConf) error {
	exec := `INSERT INTO rove_migrate__sessions_migs
(session_id, migration_id) VALUES ($1, $2)`
	_, err := s.Exec(exec, sessionId, mig.Name)
	if err != nil {
		return err
	}
	return nil
}

func (sa sqlite3Actions) GetSessionInfo(sessionId int64, s *sqlx.DB, cnf *common.DbConf) (*common.SessionInfo, error) {
	return migs.SqlGetSessionInfo(sessionId, s, cnf)
}

func (sa sqlite3Actions) GetLastSessionInfo(s *sqlx.DB, cnf *common.DbConf) (*common.SessionInfo, error) {
	return migs.SqlGetLastSessionInfo(s, cnf)
}
