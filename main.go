package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"rove_migrate/common"
	"rove_migrate/conf"
	"rove_migrate/db"
	"rove_migrate/driver/plugin"
	"rove_migrate/migs"
	_ "rove_migrate/postgres"
	_ "rove_migrate/sqlite3"
	"sort"
	"strconv"
	"strings"
	"syscall"

	"github.com/dominikbraun/graph"
	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"
	"github.com/urfave/cli/v2/altsrc"
	"github.com/xlab/treeprint"
	"golang.org/x/term"
)

const version = "v0.3.0"

// Grabs the migration dependnecy tree for visualization
func migTree(migrations []*common.Migration) (graph.Graph[string, string], error) {
	// Doing a rollback graph to show leafs at top
	return db.MakeRollbackGraph(migrations)
}

// Sets the password pointer from stdin if we should prompt the user
// Will override any configuration settings for password
func setPassword(pwd *string, pwdPrompt bool) error {
	if pwdPrompt {
		if term.IsTerminal(int(os.Stdin.Fd())) {
			fmt.Printf("Password: ")
			bytePassword, err := term.ReadPassword(int(syscall.Stdin))
			if err != nil {
				return err
			}
			*pwd = string(bytePassword)
			fmt.Printf("\n")
		} else {
			fmt.Printf("WARNING! SECURE PASSWORD ENTRY UNAVAILBLE! PROCEED AT YOUR OWN RISK!\n")
			fmt.Printf("Password (insecure): ")
			reader := bufio.NewReader(os.Stdin)
			line, err := reader.ReadString('\n')
			if err != nil {
				return err
			}
			line = line[:len(line)-1]
			// Trim \r for windows
			if strings.HasSuffix(line, "\r") {
				line = line[:len(line)-1]
			}
			*pwd = line
			fmt.Printf("\n")
		}
	}
	return nil
}

// Entry point for the application
func main() {
	var localFile, host, actions, driver, generator, database, user, pwd, schema, logFile, logLevel, driverUrl string
	var port uint
	var dry, logNoColors, logJson, strictMode, pwdPrompt, minimal bool
	migsDir := ""
	target := ""

	// Bulid a list of drivers for our usage strings
	dbDriversBuilder := strings.Builder{}
	for i, driver := range common.DbDrivers() {
		if i != 0 {
			dbDriversBuilder.WriteString(", ")
		}
		dbDriversBuilder.WriteString(driver)
	}
	dbDrivers := dbDriversBuilder.String()

	// Bulid a list of drivers for our usage strings
	dbGeneratorBuilder := strings.Builder{}
	for i, driver := range common.DbGenerators() {
		if i != 0 {
			dbGeneratorBuilder.WriteString(", ")
		}
		dbGeneratorBuilder.WriteString(driver)
	}
	dbGenerators := dbGeneratorBuilder.String()

	dbActionsBuilder := strings.Builder{}
	for i, action := range common.MigActions() {
		if i != 0 {
			dbActionsBuilder.WriteString(", ")
		}
		dbActionsBuilder.WriteString(action)
	}
	dbActions := dbActionsBuilder.String()

	// Initializes our logger
	initLogger := func() {
		if logLevel == "" || logLevel == "warn" {
			// Handles the "value not provided" case
			log.SetLevel(log.ErrorLevel)
		} else if logLevel == "quiet" {
			log.SetOutput(io.Discard)
		} else if logFile != "" {
			file, err := os.OpenFile(logFile, os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0666)
			if err != nil {
				log.Errorf("Failed to open log file, using default stderr. Failure reason: %v", err)
			} else {
				log.SetOutput(file)
			}
		}
		if logJson {
			log.SetFormatter(&log.JSONFormatter{})
		} else if logNoColors {
			log.SetFormatter(&log.TextFormatter{DisableColors: true, FullTimestamp: true})
		} else {
			log.SetFormatter(&log.TextFormatter{FullTimestamp: true})
		}
	}

	// Initializes any configuration we need for running database commands
	initConf := func() (*common.DbConf, error) {
		initLogger()

		if driverUrl == "" {
			if err := setPassword(&pwd, pwdPrompt); err != nil {
				return nil, err
			}
		} else if pwdPrompt {
			return nil, fmt.Errorf("Cannot use password prompt when a url is specified")
		}

		conf, err := common.NewConf(
			conf.WithActionsProvider(actions),
			conf.WithGenerator(generator),
			conf.WithDriver(driver),
			conf.WithUrl(driverUrl),
			conf.WithFile(localFile),
			conf.WithHost(host),
			conf.WithUser(user),
			conf.WithDb(database),
			conf.WithSchema(schema),
			conf.WithPwd(pwd),
			conf.WithPort(port),
		)
		return conf, err

	}

	// Sets up global flags (these are meant for general configuration)
	flags := []cli.Flag{
		altsrc.NewStringFlag(
			&cli.StringFlag{
				Name:        "logLevel",
				Usage:       "Determines logging level. One of [trace,debug,info,warn,error,fatal,panic,quiet]",
				Destination: &logLevel,
				Value:       "warn",
				EnvVars:     []string{"RM_LOG_LEVEL"},
				// Validates and sets the log level
				Action: func(ctx *cli.Context, v string) error {
					v = strings.ToLower(v)
					switch v {
					case "trace":
						log.SetLevel(log.TraceLevel)
					case "debug":
						log.SetLevel(log.DebugLevel)
					case "info":
						log.SetLevel(log.InfoLevel)
					case "warn":
						log.SetLevel(log.WarnLevel)
					case "error":
						log.SetLevel(log.ErrorLevel)
					case "fatal":
						log.SetLevel(log.FatalLevel)
					case "panic":
						log.SetLevel(log.PanicLevel)
					case "quiet":
					default:
						return fmt.Errorf("Unknown log level %v", v)
					}
					return nil
				},
			},
		),
		altsrc.NewStringFlag(
			&cli.StringFlag{
				Name:        "logFile",
				Usage:       "File to write logs to instead of stderr. If file openning fails, will write to stderr as fallback",
				Destination: &logFile,
				EnvVars:     []string{"RM_LOG_FILE"},
			},
		),
		altsrc.NewBoolFlag(
			&cli.BoolFlag{
				Name:        "logJson",
				Usage:       "Log with JSON format",
				Destination: &logJson,
				EnvVars:     []string{"RM_LOG_JSON"},
			},
		),
		altsrc.NewBoolFlag(
			&cli.BoolFlag{
				Name:        "logNoColors",
				Usage:       "If provided, will turn off colors in log output",
				Destination: &logNoColors,
				EnvVars:     []string{"RM_LOG_NO_COLS"},
			},
		),
		altsrc.NewStringFlag(
			&cli.StringFlag{
				Name:        "url",
				Usage:       "Driver connection URL. Will override url generation. May still need to specify database/schema with dedicated flags for accurate results or some functionality.",
				Destination: &driverUrl,
				EnvVars:     []string{"DB_URL"},
			},
		),
		altsrc.NewStringFlag(
			&cli.StringFlag{
				Name:        "file",
				Usage:       "File for embedded database engines. Cannot be used with host/port.",
				Destination: &localFile,
				EnvVars:     []string{"DB_FILE"},
			},
		),
		altsrc.NewStringFlag(
			&cli.StringFlag{
				Name:        "host",
				Usage:       "Database host to connect to. Cannot be used with file.",
				Destination: &host,
				EnvVars:     []string{"DB_HOST"},
			},
		),
		altsrc.NewStringFlag(
			&cli.StringFlag{
				Name:        "actions",
				Usage:       "Migration actions provider for generating actions SQL. [" + dbActions + "]",
				Destination: &actions,
				EnvVars:     []string{"DB_ACTIONS"},
			},
		),
		altsrc.NewStringFlag(
			&cli.StringFlag{
				Name:        "generator",
				Usage:       "Database generator to use for generating schema. One of [" + dbGenerators + "]",
				Destination: &generator,
				EnvVars:     []string{"DB_GENERATOR"},
			},
		),
		altsrc.NewStringFlag(
			&cli.StringFlag{
				Name:        "driver",
				Usage:       "Database driver to connect with. One of [" + dbDrivers + "]",
				Destination: &driver,
				EnvVars:     []string{"DB_DRIVER"},
			},
		),
		altsrc.NewStringFlag(
			&cli.StringFlag{
				Name:        "user",
				Aliases:     []string{"usr"},
				Usage:       "Database user to use",
				Destination: &user,
				EnvVars:     []string{"DB_USER"},
			},
		),
		altsrc.NewStringFlag(
			&cli.StringFlag{
				Name:        "database",
				Aliases:     []string{"db"},
				Usage:       "Database to connect to. If the database doesn't differentiate between schema and database, then only use \"schema\"",
				Destination: &database,
				EnvVars:     []string{"DB_DATABASE"},
			},
		),
		altsrc.NewStringFlag(
			&cli.StringFlag{
				Name:        "schema",
				Usage:       "Schema to connect to. If the database doesn't differentiate between schema and database, then only use \"schema\"",
				Destination: &schema,
				EnvVars:     []string{"DB_SCHEMA"},
			},
		),
		&cli.BoolFlag{
			Name:        "promptPassword",
			Aliases:     []string{"promptPwd", "pp"},
			Usage:       "Prompt for a password on stdin instead of using the CLI, config file, or ENV vars",
			Destination: &pwdPrompt,
			EnvVars:     []string{"RM_PWD_PROMPT"},
		},
		altsrc.NewStringFlag(
			&cli.StringFlag{
				Name:        "password",
				Aliases:     []string{"pwd"},
				Usage:       "Database password to use",
				Destination: &pwd,
				EnvVars:     []string{"DB_PWD"},
			},
		),
		altsrc.NewUintFlag(
			&cli.UintFlag{
				Name:        "port",
				Usage:       "Port to connect to; will default to standard database port. Cannot be used with file.",
				Destination: &port,
				EnvVars:     []string{"DB_PORT"},
				Action: func(ctx *cli.Context, v uint) error {
					if v >= 65536 {
						return fmt.Errorf("Flag port value %v out of range[0-65535]", v)
					}
					return nil
				},
			},
		),
		&cli.StringFlag{
			Name:    "conf",
			Usage:   "TOML file with DB connection options",
			EnvVars: []string{"DB_CONF"},
		},
	}

	// Sets up our application CLI commands
	commands := []*cli.Command{
		{
			Name:  "generators",
			Usage: "Prints available database generators for connecting",
			// Get list of available DB engines
			Action: func(cCtx *cli.Context) error {
				initLogger()
				generators := common.DbGenerators()
				for _, e := range generators {
					fmt.Println("- " + e)
				}
				return nil
			},
		},
		{
			Name:  "drivers",
			Usage: "Prints available database drivers for connecting",
			// Get list of available DB engines
			Action: func(cCtx *cli.Context) error {
				initLogger()
				drivers := common.DbDrivers()
				for _, e := range drivers {
					fmt.Println("- " + e)
				}
				return nil
			},
		},
		{
			Name:  "generator-limits",
			Usage: "Prints limitations of database generator",
			// Gets limitations of a specific database engine
			Action: func(cCtx *cli.Context) error {
				initLogger()
				limits, err := common.GeneratorLimitations(generator)
				if err != nil {
					return err
				}
				fmt.Println(generator + ":")
				if limits == "" {
					fmt.Println("None")
				} else {
					fmt.Println(limits)
				}
				return nil
			},
		},
		{
			Name:  "driver-limits",
			Usage: "Prints limitations of database driver",
			// Gets limitations of a specific database engine
			Action: func(cCtx *cli.Context) error {
				initLogger()
				limits, err := common.DriverLimitations(driver)
				if err != nil {
					return err
				}
				fmt.Println(driver + ":")
				if limits == "" {
					fmt.Println("None")
				} else {
					fmt.Println(limits)
				}
				return nil
			},
		},
		{
			Name:  "plugins",
			Usage: "Prints registered plugins",
			Action: func(cCtx *cli.Context) error {
				initLogger()
				plugins := plugin.RegisteredPlugins()

				fmt.Println("Plugins:")
				if len(plugins) == 0 {
					fmt.Println("None")
				} else {
					for _, plugin := range plugins {
						fmt.Printf(" - %v\n", plugin.Id)
					}
				}
				return nil
			},
		},
		{
			Name:  "dep-tree",
			Usage: "Prints migration dependency tree",
			Flags: []cli.Flag{
				altsrc.NewStringFlag(
					&cli.StringFlag{
						Name:        "migsDir",
						Usage:       "Folder holding migration files",
						Destination: &migsDir,
						Required:    true,
						Aliases:     []string{"migs", "m"},
						EnvVars:     []string{"MIGS_DIR"},
					},
				),
			},
			// Prints a dependency tree with ASCII
			Action: func(cCtx *cli.Context) error {
				initLogger()
				migrations, err := migs.LoadFrom(migsDir)
				if err != nil {
					return err
				}

				depGraph, err := migTree(migrations)
				if err != nil {
					return err
				}

				tree := treeprint.NewWithRoot(" ")

				pm, err := depGraph.PredecessorMap()
				if err != nil {
					return nil
				}

				order, err := graph.StableTopologicalSort(depGraph, func(a, b string) bool {
					return a < b
				})
				if err != nil {
					return nil
				}

				nodes := map[string][]treeprint.Tree{}

				for _, o := range order {
					deps := pm[o]
					if l := len(deps); l == 0 {
						nodes[o] = []treeprint.Tree{tree.AddBranch(o)}
					} else {
						nodes[o] = make([]treeprint.Tree, 0, l)
						for k, _ := range deps {
							for _, pnode := range nodes[k] {
								n := pnode.AddBranch(o)
								nodes[o] = append(nodes[o], n)
							}
						}
					}
				}

				fmt.Println("Dependency Tree:")
				fmt.Println(tree.String())

				return nil
			},
		},
		{
			Name:  "sessions",
			Usage: "Lists migration sessions",
			Flags: []cli.Flag{
				&cli.StringFlag{
					Name:        "session",
					Usage:       "Session id to get information for (either a number or 'last')",
					Destination: &target,
					Aliases:     []string{"s"},
				},
			},
			Action: func(cCtx *cli.Context) error {
				conf, err := initConf()
				if err != nil {
					return err
				}

				c, err := db.ConnectTo(conf)
				if err != nil {
					return err
				}
				defer c.Close()

				var info *common.Info
				var session *common.SessionInfo

				if target == "" {
					info, err = c.GetInfo()

					if err != nil {
						return err
					}
				} else if target == "last" {
					session, err = c.GetLastSessionInfo()

					if err != nil {
						return err
					}

					info = &common.Info{Sessions: []common.SessionInfo{*session}}
				} else if id, err := strconv.ParseInt(target, 10, 64); err == nil {
					session, err = c.GetSessionInfo(id)

					if err != nil {
						return err
					}

					info = &common.Info{Sessions: []common.SessionInfo{*session}}
				} else {
					return fmt.Errorf("Invalid session provided! Expected a numeric session id or 'last'")
				}

				fmt.Printf(info.SessionsStr())
				return err
			},
		},
		{
			Flags: []cli.Flag{
				altsrc.NewStringFlag(
					&cli.StringFlag{
						Name:        "migsDir",
						Required:    true,
						Usage:       "Folder holding migration files",
						Destination: &migsDir,
						Aliases:     []string{"migs", "m"},
						EnvVars:     []string{"MIGS_DIR"},
					},
				),
				&cli.StringFlag{
					Name:        "session",
					Usage:       "Session ID to revert (either a number or 'last')",
					Destination: &target,
					Aliases:     []string{"s"},
				},
				&cli.BoolFlag{
					Name:        "dry-run",
					Aliases:     []string{"dry"},
					Usage:       "Do a dry run without running the migrations",
					Destination: &dry,
				},
			},
			Name:  "revert-session",
			Usage: "Reverts a migration session. For forward migrations, will perform rollbacks. For rollbacks, will perform forward migrations",
			Action: func(cCtx *cli.Context) error {
				conf, err := initConf()
				if err != nil {
					return err
				}

				if target == "" {
					return fmt.Errorf("Reverting a session requires specifying a session")
				}
				target = strings.ToLower(target)

				migrations, err := migs.LoadFrom(migsDir)
				if err != nil {
					return err
				}

				c, err := db.ConnectTo(conf)
				if err != nil {
					return err
				}
				defer c.Close()

				var session *common.SessionInfo
				if target == "last" {
					session, err = c.GetLastSessionInfo()
				} else if id, err := strconv.ParseInt(target, 10, 64); err == nil {
					session, err = c.GetSessionInfo(id)
				} else {
					return fmt.Errorf("Invalid session provided! Expected a numeric session id or 'last'")
				}

				if err != nil {
					return err
				}

				info, err := c.RevertSession(session, dry, migrations)
				if err != nil {
					return err
				}

				fmt.Printf(info)

				return nil
			},
		},
		{
			Name:  "check-conn",
			Usage: "Checks connection to remote database server",
			// Checks that a database connection can be established
			Action: func(cCtx *cli.Context) error {
				conf, err := initConf()
				if err != nil {
					return err
				}

				c, err := db.ConnectTo(conf)
				if err != nil {
					return err
				}
				defer c.Close()

				if err := c.Ping(); err != nil {
					return err
				}

				fmt.Printf("CONNECTION SUCCESS!\n")

				return err
			},
		},
		{
			Flags: []cli.Flag{
				&cli.BoolFlag{
					Name:  "create-database",
					Usage: "Creates the database if it doesn't already exist. Requires create database permission, and access to the default database.",
				},
				&cli.BoolFlag{
					Name:  "create-schema",
					Usage: "Creates the schema if it doesn't already exist. Requires create schema permission.",
				},
			},
			Name:  "init",
			Usage: "Initializes database for migrations",
			// Initializes a database for migrations
			Action: func(cCtx *cli.Context) error {
				initLogger()
				if err := setPassword(&pwd, pwdPrompt); err != nil {
					return err
				}

				if cCtx.Bool("create-database") {
					if database == "" {
						return fmt.Errorf("Database not specified with --database flag. Please specify database, even if using --url flag")
					}
					conf, err := common.NewConf(
						conf.WithActionsProvider(actions),
						conf.WithGenerator(generator),
						conf.WithDriver(driver),
						conf.WithFile(localFile),
						conf.WithHost(host),
						conf.WithUser(user),
						conf.WithPwd(pwd),
						conf.WithPort(port),
					)
					if err != nil {
						return err
					}
					c, err := db.ConnectTo(conf)
					if err != nil {
						return err
					}
					defer c.Close()

					if err := c.EnsureDatabase(database); err != nil {
						return err
					}
					c.Close()
					log.Info("ENSURED DATABASE EXISTS " + database)
				}

				if cCtx.Bool("create-schema") {
					if schema == "" {
						return fmt.Errorf("Schema not specified with --schema flag. Please specify schema, even if using --url flag")
					}
					conf, err := common.NewConf(
						conf.WithActionsProvider(actions),
						conf.WithGenerator(generator),
						conf.WithDriver(driver),
						conf.WithFile(localFile),
						conf.WithHost(host),
						conf.WithUser(user),
						conf.WithDb(database),
						conf.WithPwd(pwd),
						conf.WithPort(port),
					)
					if err != nil {
						return err
					}
					c, err := db.ConnectTo(conf)
					if err != nil {
						return err
					}
					defer c.Close()

					if err := c.EnsureSchema(schema); err != nil {
						return nil
					}
					c.Close()
					log.Info("ENSURED SCHEMA EXISTS " + database)
				}

				conf, err := common.NewConf(
					conf.WithActionsProvider(actions),
					conf.WithGenerator(generator),
					conf.WithDriver(driver),
					conf.WithUrl(driverUrl),
					conf.WithFile(localFile),
					conf.WithHost(host),
					conf.WithUser(user),
					conf.WithDb(database),
					conf.WithSchema(schema),
					conf.WithPwd(pwd),
					conf.WithPort(port),
				)
				if err != nil {
					return err
				}

				c, err := db.ConnectTo(conf)
				if err != nil {
					return err
				}
				defer c.Close()

				if err := c.EnsureSetup(); err != nil {
					return err
				}
				fmt.Printf("INIT SUCCESS!\n")

				return err
			},
		},
		{
			Name:  "info",
			Usage: "Gets information about the database",
			// Gets information about the current database
			Action: func(cCtx *cli.Context) error {
				conf, err := initConf()
				if err != nil {
					return err
				}

				c, err := db.ConnectTo(conf)
				if err != nil {
					return err
				}
				defer c.Close()

				info, err := c.GetInfo()
				if err != nil {
					return err
				}
				fmt.Printf(info.String())

				return err
			},
		},
		{
			Name:  "validate",
			Usage: "Validates migration scripts and state of database",
			Flags: []cli.Flag{
				altsrc.NewStringFlag(
					&cli.StringFlag{
						Name:        "migsDir",
						Usage:       "Folder holding migration files",
						Destination: &migsDir,
						Required:    true,
						Aliases:     []string{"migs", "m"},
						EnvVars:     []string{"MIGS_DIR"},
					},
				),
				&cli.BoolFlag{
					Name:        "strict",
					Usage:       "Script warnings are treated as errors",
					Destination: &strictMode,
				},
				&cli.BoolFlag{
					Name:        "migrationsOnly",
					Aliases:     []string{"migsOnly"},
					Usage:       "Only validate migration scripts, don't validate database integrity",
					Destination: &dry,
				},
			},
			// Validates current database and migration state
			Action: func(cCtx *cli.Context) error {
				conf, err := initConf()
				if err != nil {
					return err
				}

				c, err := db.ConnectTo(conf)
				if err != nil {
					return err
				}
				defer c.Close()

				migrations, err := migs.LoadFrom(migsDir)
				if err != nil {
					return err
				}

				err = c.Validate(migrations, dry, strictMode)
				if err != nil {
					log.Error(err)
					fmt.Println(err)
					return fmt.Errorf("VALIDATION FAILED!")
				}

				fmt.Printf("VALIDATION PASSED!\n")

				return err
			},
		},
		{
			Name:  "migrate",
			Usage: "Performs one or more database migrations",
			Flags: []cli.Flag{
				altsrc.NewStringFlag(
					&cli.StringFlag{
						Name:        "migsDir",
						Required:    true,
						Usage:       "Folder holding migration files",
						Destination: &migsDir,
						Aliases:     []string{"migs", "m"},
						EnvVars:     []string{"MIGS_DIR"},
					},
				),
				&cli.StringFlag{
					Name:        "target",
					Usage:       "Migration target",
					Destination: &target,
					Aliases:     []string{"t"},
				},
				&cli.BoolFlag{
					Name:        "minimal",
					Aliases:     []string{"min"},
					Usage:       "Migrate the minimal set to get the target",
					Destination: &minimal,
				},
				&cli.BoolFlag{
					Name:        "dry-run",
					Aliases:     []string{"dry"},
					Usage:       "Do a dry run without running the migrations",
					Destination: &dry,
				},
			},
			// Performs a migration on a database
			Action: func(cCtx *cli.Context) error {
				conf, err := initConf()
				if err != nil {
					return err
				}

				if minimal && target == "" {
					return fmt.Errorf("Minimal migrations requires a target")
				}

				c, err := db.ConnectTo(conf)
				if err != nil {
					return err
				}
				defer c.Close()

				migrations, err := migs.LoadFrom(migsDir)
				if err != nil {
					return err
				}

				var info string

				if dry {
					info, err = c.PlanMigrations(migrations, target, minimal)
				} else {
					info, err = c.RunMigrations(migrations, target, minimal)
				}

				fmt.Printf(info)
				if err != nil {
					return err
				}

				return err
			},
		},
		{
			Name:  "rollback",
			Usage: "Rolls back (reverts) one or more database migrations",
			Flags: []cli.Flag{
				altsrc.NewStringFlag(
					&cli.StringFlag{
						Name:        "migsDir",
						Required:    true,
						Usage:       "Folder holding migration files",
						Destination: &migsDir,
						Aliases:     []string{"migs", "m"},
						EnvVars:     []string{"MIGS_DIR"},
					},
				),
				&cli.BoolFlag{
					Name:        "minimal",
					Aliases:     []string{"min"},
					Usage:       "Migrate the minimal set to get the target",
					Destination: &minimal,
				},
				&cli.StringFlag{
					Name:        "target",
					Usage:       "Rollback target",
					Destination: &target,
					Aliases:     []string{"t"},
				},
				&cli.BoolFlag{
					Name:        "dry-run",
					Aliases:     []string{"dry"},
					Usage:       "Do a dry run without running the migrations",
					Destination: &dry,
				},
			},
			// Rolls back a database
			Action: func(cCtx *cli.Context) error {
				conf, err := initConf()
				if err != nil {
					return err
				}

				if minimal && target == "" {
					return fmt.Errorf("Minimal migrations requires a target")
				}

				c, err := db.ConnectTo(conf)
				if err != nil {
					return err
				}
				defer c.Close()

				migrations, err := migs.LoadFrom(migsDir)
				if err != nil {
					return err
				}

				var info string

				if dry {
					info, err = c.PlanRollback(migrations, target, minimal)
				} else {
					info, err = c.RollbackMigrations(migrations, target, minimal)
				}

				fmt.Printf(info)
				if err != nil {
					return err
				}

				return err
			},
		},
	}

	app := &cli.App{
		Name:     "rove_migrate",
		Usage:    version + " - A dependency-tree based database migrator",
		Flags:    flags,
		Commands: commands,
		Before:   altsrc.InitInputSourceWithContext(flags, altsrc.NewTomlSourceFromFlagFunc("conf")),
	}

	sort.Sort(cli.FlagsByName(app.Flags))
	sort.Sort(cli.CommandsByName(app.Commands))

	if err := app.Run(os.Args); err != nil {
		fmt.Print(err)
		os.Exit(1)
	}
}
