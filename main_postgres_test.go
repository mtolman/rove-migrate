package main

import (
	"io"
	"math/rand"
	"os"
	"os/exec"
	"path/filepath"
	"rove_migrate/common"
	"rove_migrate/conf"
	"rove_migrate/db"
	_ "rove_migrate/sqlite3"
	"strconv"
	"strings"
	"testing"
)

var letters = []rune("abcdefghijklmnopqrstuvwxyz")

func getenv(key, fallback string) string {
	value := os.Getenv(key)
	if len(value) == 0 {
		return fallback
	}
	return value
}

func randStr() string {
	b := make([]rune, 20)
	for i := range 20 {
		b[i] = letters[rand.Intn(len(letters))]
	}
	return string(b)
}

func postgresConf() (*common.DbConf, func()) {
	database := getenv("TEST_PGX_DB", "test_postgres")
	schema := getenv("TEST_PGX_SCHEMA", "test_postgres_"+randStr())
	host := getenv("TEST_PGX_HOST", "localhost")
	user := getenv("TEST_PGX_USER", "postgres")
	pwd := getenv("TEST_PGX_PWD", "")
	toCleanup := getenv("TEST_PGX_CLEANUP_SCOPE", "schema")

	iport, err := strconv.Atoi(getenv("TEST_PGX_PORT", "5432"))
	if err != nil {
		panic(err)
	}
	port := uint(iport)
	cleanup := func() {
		switch strings.ToLower(toCleanup) {
		case "database":
			conf, err := common.NewConf(
				conf.WithDriver("postgres"),
				conf.WithFile(""),
				conf.WithHost(host),
				conf.WithUser(user),
				conf.WithPwd(pwd),
				conf.WithPort(port),
			)
			if err != nil {
				panic(err)
			}
			conn, err := db.ConnectTo(conf)
			if err != nil {
				panic(err)
			}
			if _, err := conn.DB.Exec("DROP DATABASE " + database + " WITH (FORCE)"); err != nil {
				panic(err)
			}
		case "schema":
			conf, err := common.NewConf(
				conf.WithDriver("postgres"),
				conf.WithFile(""),
				conf.WithHost(host),
				conf.WithUser(user),
				conf.WithDb(database),
				conf.WithPwd(pwd),
				conf.WithPort(port),
			)
			if err != nil {
				panic(err)
			}
			conn, err := db.ConnectTo(conf)
			if err != nil {
				panic(err)
			}
			if _, err := conn.DB.Exec("DROP SCHEMA " + schema + " CASCADE"); err != nil {
				panic(err)
			}
		}
	}

	conf, err := common.NewConf(
		conf.WithDriver("postgres"),
		conf.WithFile(""),
		conf.WithHost(host),
		conf.WithUser(user),
		conf.WithDb(database),
		conf.WithSchema(schema),
		conf.WithPwd(pwd),
		conf.WithPort(port),
	)

	if err != nil {
		panic(err)
	}

	return conf, cleanup
}

func postgresCmdOpts(cnf *common.DbConf) []string {
	opts := []string{
		"--driver",
		cnf.Driver(),
		"--host",
		cnf.Host(),
		"--port",
		strconv.FormatUint(uint64(cnf.Port()), 10),
		"--user",
		cnf.User(),
		"--schema",
		cnf.Schema(),
		"--db",
		cnf.Database(),
	}
	if cnf.Pwd() != "" {
		opts = append(
			opts,
			"--pwd",
			cnf.Pwd(),
		)
	} else {
		opts = append(opts, "--promptPassword")
	}
	return opts
}

func postgresInitCmd(cmdPath string, cnf *common.DbConf) *exec.Cmd {
	opts := append(postgresCmdOpts(cnf), "init", "--create-database", "--create-schema")
	return exec.Command(
		cmdPath,
		opts...,
	)
}

func postgresCheckConnCmd(cmdPath string, cnf *common.DbConf) *exec.Cmd {
	opts := append(postgresCmdOpts(cnf), "check-conn")
	return exec.Command(
		cmdPath,
		opts...,
	)
}

func postgresInfoCmd(cmdPath string, cnf *common.DbConf) *exec.Cmd {
	opts := append(postgresCmdOpts(cnf), "info")
	return exec.Command(
		cmdPath,
		opts...,
	)
}

func TestPostgresPwdPrompt(t *testing.T) {
	fullConf, clean := postgresConf()
	defer clean()

	pwd := fullConf.Pwd()
	cnf, err := common.NewConf(
		conf.WithDriver("postgres"),
		conf.WithFile(""),
		conf.WithHost(fullConf.Host()),
		conf.WithUser(fullConf.User()),
		conf.WithDb(fullConf.Database()),
		conf.WithSchema(fullConf.Schema()),
		// No password, we'll pass that with stdin
		conf.WithPort(fullConf.Port()),
	)

	dir, err := os.Getwd()
	if err != nil {
		t.Fatal(err)
	}

	cmdPath := filepath.Join(dir, binName)

	t.Run("Init", func(t *testing.T) {
		cmd := postgresInitCmd(cmdPath, cnf)
		cmdStdIn, err := cmd.StdinPipe()
		if err != nil {
			t.Fatal(err)
		}
		go func() {
			defer cmdStdIn.Close()
			io.WriteString(cmdStdIn, pwd+"\n")
		}()

		out, err := cmd.CombinedOutput()
		if err != nil {
			t.Fatalf("Unexpected error: %+v\nOutput: %s", err, out)
		}
	})

	t.Run("Test Conn", func(t *testing.T) {
		cmd := postgresCheckConnCmd(cmdPath, cnf)
		cmdStdIn, err := cmd.StdinPipe()
		if err != nil {
			t.Fatal(err)
		}

		go func() {
			defer cmdStdIn.Close()
			io.WriteString(cmdStdIn, pwd+"\n")
		}()

		out, err := cmd.CombinedOutput()
		if err != nil {
			t.Fatalf("Unexpected error: %+v\nOutput: %s", err, out)
		}

		outStr := string(out)
		expectedSequences := []string{
			"Password", ":",
			"CONNECTION SUCCESS!",
		}

		assertHasOrderedSequence(t, expectedSequences, outStr)
	})
}

func TestPostgresBasic(t *testing.T) {
	cnf, clean := postgresConf()
	defer clean()

	dir, err := os.Getwd()
	if err != nil {
		t.Fatal(err)
	}

	cmdPath := filepath.Join(dir, binName)

	t.Run("Init", func(t *testing.T) {
		cmd := postgresInitCmd(cmdPath, cnf)
		out, err := cmd.CombinedOutput()
		if err != nil {
			t.Fatalf("Unexpected error: %+v\nOutput: %s", err, out)
		}
	})

	t.Run("Test Conn", func(t *testing.T) {
		cmd := postgresCheckConnCmd(cmdPath, cnf)
		out, err := cmd.CombinedOutput()
		if err != nil {
			t.Fatalf("Unexpected error: %+v\nOutput: %s", err, out)
		}

		outStr := string(out)
		expectedStr := "CONNECTION SUCCESS!\n"

		if outStr != expectedStr {
			t.Errorf("Expected output %#v, received %#v", expectedStr, outStr)
		}
	})

	t.Run("Test Blank Info", func(t *testing.T) {
		cmd := postgresInfoCmd(cmdPath, cnf)
		out, err := cmd.CombinedOutput()
		if err != nil {
			t.Fatalf("Unexpected error: %+v\nOutput: %s", err, out)
		}

		outStr := string(out)
		expectedSequences := []string{
			"Tables:",
			cnf.Schema(), `│rove_migrate__cmd_log`, `│6 `,
			cnf.Schema(), `│rove_migrate__migs`, `│6 `,
			cnf.Schema(), `│rove_migrate__runs`, `│7 `,

			cnf.Schema(), `.rove_migrate__cmd_log`,
			"ctime",
			"id",
			"migration_id",
			"run_id",
			"sql_cmd",
			"utime",

			cnf.Schema(), `.rove_migrate__migs`,
			"ctime",
			"error",
			"id",
			"state",
			"utime",

			cnf.Schema(), `.rove_migrate__runs`,
			"ctime",
			"error",
			"id",
			"is_rollback",
			"migration_id",
			"success",
			"utime",

			"Indexes:",
			"rove_migrate__migs",

			"Triggers:",
			"rove_migrate__update_utime_cmd",
			"rove_migrate__update_utime_migs",
			"rove_migrate__update_utime_runs",

			"Migrations:",
			"No information found",

			"Migration Runs:",
			"No information found",

			"Migration Commands:",
			"No information found",
		}

		assertHasOrderedSequence(t, expectedSequences, outStr)
	})

	t.Run("Test Migration", func(t *testing.T) {
		migDir, err := os.MkdirTemp(dir, "test_migrations")
		if err != nil {
			t.Fatal(err)
		}
		defer os.RemoveAll(migDir)

		if err := writePgsqlSqlMigFiles(migDir); err != nil {
			t.Fatal(err)
		}

		opts := append(postgresCmdOpts(cnf), "migrate", "--migs", migDir)
		cmd := exec.Command(cmdPath, opts...)
		out, err := cmd.CombinedOutput()
		if err != nil {
			t.Fatalf("Unexpected error: %+v\nOutput: %s", err, out)
		}

		expectedSequences := []string{
			"Migrations Ran:",
			"test_sql_mig",
		}

		outStr := string(out)

		assertHasOrderedSequence(t, expectedSequences, outStr)
	})

	t.Run("Test Post Basic Migration Info", func(t *testing.T) {
		cmd := postgresInfoCmd(cmdPath, cnf)
		out, err := cmd.CombinedOutput()
		if err != nil {
			t.Fatalf("Unexpected error: %+v\nOutput: %s", err, out)
		}

		outStr := string(out)
		expectedSequences := []string{
			"Tables:",
			cnf.Schema(), `│rove_migrate__cmd_log`, `│6 `,
			cnf.Schema(), `│rove_migrate__migs`, `│6 `,
			cnf.Schema(), `│rove_migrate__runs`, `│7 `,
			cnf.Schema(), `│user`, `│2 `,

			cnf.Schema(), `.rove_migrate__cmd_log`,
			"ctime",
			"id",
			"migration_id",
			"run_id",
			"sql_cmd",
			"utime",

			cnf.Schema(), `.rove_migrate__migs`,
			"ctime",
			"error",
			"id",
			"state",
			"utime",

			cnf.Schema(), `.rove_migrate__runs`,
			"ctime",
			"error",
			"id",
			"is_rollback",
			"migration_id",
			"success",
			"utime",

			cnf.Schema(), `.user`,
			"id",
			"name",

			"Indexes:",
			"rove_migrate__migs",

			"Triggers:",
			"rove_migrate__update_utime_cmd",
			"rove_migrate__update_utime_migs",
			"rove_migrate__update_utime_runs",

			"Migration Sessions:",
			"1", "NO", "test_sql_mig",

			"Migrations:",
			"test_sql_mig│SUCCESS│     │1 ",

			"Migration Runs:",
			"1 ", "test_sql_mig", "NO", "YES",

			"Migration Commands:",
			"CREATE TABLE IF NOT EXISTS user",
		}

		assertHasOrderedSequence(t, expectedSequences, outStr)
	})

	t.Run("Test Rollback", func(t *testing.T) {
		migDir, err := os.MkdirTemp(dir, "test_migrations")
		if err != nil {
			t.Fatal(err)
		}
		defer os.RemoveAll(migDir)

		if err := writePgsqlSqlMigFiles(migDir); err != nil {
			t.Fatal(err)
		}

		opts := append(postgresCmdOpts(cnf), "rollback", "--migs", migDir)
		cmd := exec.Command(cmdPath, opts...)
		out, err := cmd.CombinedOutput()
		if err != nil {
			t.Fatalf("Unexpected error: %+v\nOutput: %s", err, out)
		}

		expectedSequences := []string{
			"Migrations Rolled Back:",
			"test_sql_mig",
		}

		outStr := string(out)

		assertHasOrderedSequence(t, expectedSequences, outStr)
	})

	t.Run("Test Post Basic Rollback Info", func(t *testing.T) {
		cmd := postgresInfoCmd(cmdPath, cnf)
		out, err := cmd.CombinedOutput()
		if err != nil {
			t.Fatalf("Unexpected error: %+v\nOutput: %s", err, out)
		}

		outStr := string(out)
		expectedSequences := []string{
			"Tables:",
			cnf.Schema(), `│rove_migrate__cmd_log`, `│6 `,
			cnf.Schema(), `│rove_migrate__migs`, `│6 `,
			cnf.Schema(), `│rove_migrate__runs`, `│7 `,

			cnf.Schema(), `.rove_migrate__cmd_log`,
			"ctime",
			"id",
			"migration_id",
			"run_id",
			"sql_cmd",
			"utime",

			cnf.Schema(), `.rove_migrate__migs`,
			"ctime",
			"error",
			"id",
			"state",
			"utime",

			cnf.Schema(), `.rove_migrate__runs`,
			"ctime",
			"error",
			"id",
			"is_rollback",
			"migration_id",
			"success",
			"utime",

			"Indexes:",
			"rove_migrate__migs",

			"Triggers:",
			"rove_migrate__update_utime_cmd",
			"rove_migrate__update_utime_migs",
			"rove_migrate__update_utime_runs",

			"Migrations:",
			"test_sql_mig│ROLLBACK│     │2 ",

			"Migration Runs:",
			"1 ", "test_sql_mig", "NO", "YES",
			"2 ", "test_sql_mig", "YES", "YES",

			"Migration Commands:",
			"CREATE TABLE IF NOT EXISTS user",
			"DROP TABLE IF EXISTS user",
		}

		assertHasOrderedSequence(t, expectedSequences, outStr)
	})
}

func TestPostgresJson(t *testing.T) {
	cnf, clean := postgresConf()
	defer clean()

	dir, err := os.Getwd()
	if err != nil {
		t.Fatal(err)
	}

	cmdPath := filepath.Join(dir, binName)

	t.Run("Init", func(t *testing.T) {
		cmd := postgresInitCmd(cmdPath, cnf)
		out, err := cmd.CombinedOutput()
		if err != nil {
			t.Fatalf("Unexpected error: %+v\nOutput: %s", err, out)
		}
	})

	t.Run("Test Migration", func(t *testing.T) {
		migDir, err := os.MkdirTemp(dir, "test_migrations")
		if err != nil {
			t.Fatal(err)
		}
		defer os.RemoveAll(migDir)

		if err := writeJsonMigFiles(migDir); err != nil {
			t.Fatal(err)
		}

		opts := append(postgresCmdOpts(cnf), "migrate", "--migs", migDir)
		cmd := exec.Command(cmdPath, opts...)
		out, err := cmd.CombinedOutput()
		if err != nil {
			t.Fatalf("Unexpected error: %+v\nOutput: %s", err, out)
		}

		expectedSequences := []string{
			"Migrations Ran:",
			"test_json_mig_1",
		}

		outStr := string(out)

		assertHasOrderedSequence(t, expectedSequences, outStr)
	})

	t.Run("Test Post Basic Migration Info", func(t *testing.T) {
		cmd := postgresInfoCmd(cmdPath, cnf)
		out, err := cmd.CombinedOutput()
		if err != nil {
			t.Fatalf("Unexpected error: %+v\nOutput: %s", err, out)
		}

		outStr := string(out)
		expectedSequences := []string{
			"Tables:",
			cnf.Schema(), `│rove_migrate__cmd_log`, `│6 `,
			cnf.Schema(), `│rove_migrate__migs`, `│6 `,
			cnf.Schema(), `│rove_migrate__runs`, `│7 `,
			cnf.Schema(), `│users `, `│4 `,

			cnf.Schema(), `.rove_migrate__cmd_log`,
			"ctime",
			"id",
			"migration_id",
			"run_id",
			"sql_cmd",
			"utime",

			cnf.Schema(), `.rove_migrate__migs`,
			"ctime",
			"error",
			"id",
			"state",
			"utime",

			cnf.Schema(), `.rove_migrate__runs`,
			"ctime",
			"error",
			"id",
			"is_rollback",
			"migration_id",
			"success",
			"utime",

			cnf.Schema(), `.users`,
			"active",
			"email",
			"full_name",
			"id",

			"Indexes:",
			"rove_migrate__migs",
			"user_index", "users",

			"Triggers:",
			"rove_migrate__update_utime_cmd",
			"rove_migrate__update_utime_migs",
			"rove_migrate__update_utime_runs",

			"Migrations:",
			"test_json_mig_1│SUCCESS│     │1 ",

			"Migration Runs:",
			"1 ", "test_json_mig_1", "NO", "YES",

			"Migration Commands:",
			"CREATE TABLE IF NOT EXISTS \"user\"",
			"ALTER TABLE \"user\" RENAME COLUMN \"name\" ",
			"CREATE UNIQUE INDEX IF NOT EXISTS \"user_in",
		}

		assertHasOrderedSequence(t, expectedSequences, outStr)
	})

	t.Run("Test Rollback", func(t *testing.T) {
		migDir, err := os.MkdirTemp(dir, "test_migrations")
		if err != nil {
			t.Fatal(err)
		}
		defer os.RemoveAll(migDir)

		if err := writeJsonMigFiles(migDir); err != nil {
			t.Fatal(err)
		}

		opts := append(postgresCmdOpts(cnf), "rollback", "--migs", migDir)
		cmd := exec.Command(cmdPath, opts...)
		out, err := cmd.CombinedOutput()
		if err != nil {
			t.Fatalf("Unexpected error: %+v\nOutput: %s", err, out)
		}

		expectedSequences := []string{
			"Migrations Rolled Back:",
			"test_json_mig_1",
		}

		outStr := string(out)

		assertHasOrderedSequence(t, expectedSequences, outStr)
	})

	t.Run("Test Post Basic Rollback Info", func(t *testing.T) {
		cmd := postgresInfoCmd(cmdPath, cnf)
		out, err := cmd.CombinedOutput()
		if err != nil {
			t.Fatalf("Unexpected error: %+v\nOutput: %s", err, out)
		}

		outStr := string(out)
		expectedSequences := []string{
			"Tables:",
			cnf.Schema(), `│rove_migrate__cmd_log`, `│6 `,
			cnf.Schema(), `│rove_migrate__migs`, `│6 `,
			cnf.Schema(), `│rove_migrate__runs`, `│7 `,
			cnf.Schema(), `│rove_migrate__version_info`, `│2 `,

			cnf.Schema(), `.rove_migrate__cmd_log`,
			"ctime",
			"id",
			"migration_id",
			"run_id",
			"sql_cmd",
			"utime",

			cnf.Schema(), `.rove_migrate__migs`,
			"ctime",
			"error",
			"id",
			"state",
			"utime",

			cnf.Schema(), `.rove_migrate__runs`,
			"ctime",
			"error",
			"id",
			"is_rollback",
			"migration_id",
			"success",
			"utime",

			"Indexes:",
			"rove_migrate__migs",

			"Triggers:",
			"rove_migrate__update_utime_cmd",
			"rove_migrate__update_utime_migs",
			"rove_migrate__update_utime_runs",

			"Migrations:",
			"test_json_mig_1│ROLLBACK│     │2 ",

			"Migration Runs:",
			"1 ", "test_json_mig_1", "NO", "YES",
			"2 ", "test_json_mig_1", "YES", "YES",

			"Migration Commands:",
			"CREATE TABLE IF NOT EXISTS \"user\"",
			"ALTER TABLE \"user\" RENAME COLUMN \"name\" ",
			"CREATE UNIQUE INDEX IF NOT EXISTS \"user_in",
			"DROP INDEX IF EXISTS \"user_index\"",
			"ALTER TABLE \"users\" RENAME TO \"user\"",
			"DROP TABLE IF EXISTS \"user\"",
		}

		assertHasOrderedSequence(t, expectedSequences, outStr)
	})
}

func TestPostgresAssert(t *testing.T) {
	cnf, clean := postgresConf()
	defer clean()

	dir, err := os.Getwd()
	if err != nil {
		t.Fatal(err)
	}

	cmdPath := filepath.Join(dir, binName)

	t.Run("Init", func(t *testing.T) {
		cmd := postgresInitCmd(cmdPath, cnf)
		out, err := cmd.CombinedOutput()
		if err != nil {
			t.Fatalf("Unexpected error: %+v\nOutput: %s", err, out)
		}
	})

	t.Run("Test Good Assert Migration", func(t *testing.T) {
		migDir, err := os.MkdirTemp(dir, "test_migrations")
		if err != nil {
			t.Fatal(err)
		}
		defer os.RemoveAll(migDir)

		if err := writePgsqlSqlMigGoodAssertFiles(migDir); err != nil {
			t.Fatal(err)
		}

		opts := append(postgresCmdOpts(cnf), "migrate", "--migs", migDir)
		cmd := exec.Command(cmdPath, opts...)
		out, err := cmd.CombinedOutput()
		if err != nil {
			t.Fatalf("Unexpected error: %+v\nOutput: %s", err, out)
		}

		expectedSequences := []string{
			"Migrations Ran:",
			"test_sql_mig",
		}

		outStr := string(out)

		assertHasOrderedSequence(t, expectedSequences, outStr)
	})

	t.Run("Test Rollback", func(t *testing.T) {
		migDir, err := os.MkdirTemp(dir, "test_migrations")
		if err != nil {
			t.Fatal(err)
		}
		defer os.RemoveAll(migDir)

		if err := writePgsqlSqlMigFiles(migDir); err != nil {
			t.Fatal(err)
		}

		opts := append(postgresCmdOpts(cnf), "rollback", "--migs", migDir)
		cmd := exec.Command(cmdPath, opts...)
		out, err := cmd.CombinedOutput()
		if err != nil {
			t.Fatalf("Unexpected error: %+v\nOutput: %s", err, out)
		}

		expectedSequences := []string{
			"Migrations Rolled Back:",
			"test_sql_mig",
		}

		outStr := string(out)

		assertHasOrderedSequence(t, expectedSequences, outStr)
	})

	t.Run("Test Bad Assert Migration", func(t *testing.T) {
		migDir, err := os.MkdirTemp(dir, "test_migrations")
		if err != nil {
			t.Fatal(err)
		}
		defer os.RemoveAll(migDir)

		if err := writePgsqlSqlMigBadAssertFiles(migDir); err != nil {
			t.Fatal(err)
		}

		opts := append(postgresCmdOpts(cnf), "migrate", "--migs", migDir)
		cmd := exec.Command(cmdPath, opts...)
		out, err := cmd.CombinedOutput()
		if err == nil {
			t.Fatalf("Expected error: %+v\nOutput: %s", err, out)
		}

		expectedSequences := []string{
			"Assertions failed for migration test_sql_mig",
		}

		outStr := string(out)

		assertHasOrderedSequence(t, expectedSequences, outStr)
	})

}

func writePgsqlSqlMigBadAssertFiles(dir string) error {
	mig := `{
	"name": "test_sql_mig",
	"actions": [
		{
			"raw": {
				"up": "CREATE TABLE IF NOT EXISTS users (id BIGSERIAL PRIMARY KEY, name TEXT)",
				"down": "DROP TABLE IF EXISTS users"
			}
		}
	],
	"asserts": [
		{
			"raw": "do $$\nbegin\nASSERT 1 = 0;\nend$$;"
		}
	]
}
`
	migPath := filepath.Join(dir, "asserts.json")
	err := os.WriteFile(migPath, []byte(mig), 0644)
	if err != nil {
		return err
	}

	return nil
}

func writePgsqlSqlMigGoodAssertFiles(dir string) error {
	mig := `{
	"name": "test_sql_mig",
	"actions": [
		{
			"raw": {
				"up": "CREATE TABLE IF NOT EXISTS users (id BIGSERIAL PRIMARY KEY, name TEXT)",
				"down": "DROP TABLE IF EXISTS users"
			}
		}
	],
	"asserts": [
		{
			"raw": "do $$\nbegin\nASSERT 1 = 1;\nend$$;"
		}
	]
}
`
	migPath := filepath.Join(dir, "asserts.json")
	err := os.WriteFile(migPath, []byte(mig), 0644)
	if err != nil {
		return err
	}

	return nil
}

func writePgsqlSqlMigFiles(dir string) error {
	basicMig := `{
	"name": "test_sql_mig",
	"actions": [
		{
			"raw": {
				"up": "CREATE TABLE IF NOT EXISTS users (id BIGSERIAL PRIMARY KEY, name TEXT)",
				"down": "DROP TABLE IF EXISTS users"
			}
		}
	]
}
`
	basicMigPath := filepath.Join(dir, "basic.json")
	err := os.WriteFile(basicMigPath, []byte(basicMig), 0644)
	if err != nil {
		return err
	}

	return nil
}
