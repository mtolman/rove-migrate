package main

import (
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
	"runtime"
	"strings"
	"testing"
)

var (
	binName = "test_rove_migrate"
)

func TestMain(m *testing.M) {
	if runtime.GOOS == "windows" {
		binName += ".exe"
	}
	fmt.Println("Building tool...")

	build := exec.Command("go", "build", "-o", binName)

	if err := build.Run(); err != nil {
		fmt.Fprintf(os.Stderr, "Cannot build tool %s: %s", binName, err)
		os.Exit(1)
	}

	fmt.Println("Running tests...")
	result := m.Run()

	fmt.Println("Cleaning up...")
	os.Remove(binName)

	os.Exit(result)
}

func writeDepMigFiles2(dir string) error {
	migs := []struct {
		file string
		sql  string
	}{
		{
			"z",
			`{
	"name": "test_dep_mig_1",
	"actions": [
		{
			"raw": {
				"up": "CREATE TABLE IF NOT EXISTS user (id INT PRIMARY KEY, name TEXT)",
				"down": "DROP TABLE IF EXISTS user"
			}
		}
	]
}
`,
		},
		{
			"a",
			`{
	"name": "test_dep_mig_2",
	"depsOn": ["test_dep_mig_1"],
	"actions": [
		{
			"raw": {
				"up": "ALTER TABLE user ADD email TEXT",
				"down": "ALTER TABLE user DROP COLUMN email"
			}
		}
	]
}
`,
		},
		{
			"c",
			`{
	"name": "test_dep_mig_3",
	"depsOn": ["test_dep_mig_2"],
	"actions": [
		{
			"raw": {
				"up": "",
				"down": ""
			}
		}
	]
}
`,
		},
		{
			"d",
			`{
	"name": "test_dep_mig_4",
	"depsOn": ["test_dep_mig_3", "test_dep_mig_2"],
	"actions": [
		{
			"raw": {
				"up": "",
				"down": ""
			}
		}
	]
}
`,
		},
		{
			"f",
			`{
	"name": "test_dep_mig_5",
	"depsOn": [],
	"actions": [
		{
			"raw": {
				"up": "",
				"down": ""
			}
		}
	]
}
`,
		},
		{
			"2g",
			`{
	"name": "test_dep_mig_6",
	"depsOn": [],
	"actions": [
		{
			"raw": {
				"up": "",
				"down": ""
			}
		}
	]
}
`,
		},
		{
			"5g",
			`{
	"name": "test_dep_mig_7",
	"depsOn": ["test_dep_mig_6", "test_dep_mig_3", "test_dep_mig_1"],
	"actions": [
		{
			"raw": {
				"up": "",
				"down": ""
			}
		}
	]
}
`,
		},
	}

	for _, mig := range migs {
		migPath := filepath.Join(dir, mig.file+".json")
		err := os.WriteFile(migPath, []byte(mig.sql), 0644)
		if err != nil {
			return err
		}
	}

	return nil
}

func writeDepMigFiles(dir string) error {
	migs := []struct {
		file string
		sql  string
	}{
		{
			"z",
			`{
	"name": "test_dep_mig_1",
	"actions": [
		{
			"raw": {
				"up": "CREATE TABLE IF NOT EXISTS user (id INT PRIMARY KEY, name TEXT)",
				"down": "DROP TABLE IF EXISTS user"
			}
		}
	]
}
`,
		},
		{
			"a",
			`{
	"name": "test_dep_mig_2",
	"depsOn": ["test_dep_mig_1"],
	"actions": [
		{
			"raw": {
				"up": "ALTER TABLE user ADD email TEXT",
				"down": "ALTER TABLE user DROP COLUMN email"
			}
		}
	]
}
`,
		},
		{
			"c",
			`{
	"name": "test_dep_mig_3",
	"depsOn": ["test_dep_mig_2"],
	"actions": [
		{
			"raw": {
				"up": "",
				"down": ""
			}
		}
	]
}
`,
		},
		{
			"d",
			`{
	"name": "test_dep_mig_4",
	"depsOn": ["test_dep_mig_3"],
	"actions": [
		{
			"raw": {
				"up": "",
				"down": ""
			}
		}
	]
}
`,
		},
	}

	for _, mig := range migs {
		migPath := filepath.Join(dir, mig.file+".json")
		err := os.WriteFile(migPath, []byte(mig.sql), 0644)
		if err != nil {
			return err
		}
	}

	return nil
}

func assertHasOrderedSequence(t *testing.T, expectedSequences []string, outStr string) {
	t.Helper()
	for _, seq := range expectedSequences {
		if !strings.Contains(outStr, seq) {
			t.Errorf("Could not find %#v in output. Output:\n\n %s", seq, outStr)
		} else {
			_, outStr, _ = strings.Cut(outStr, seq)
		}
	}
}

func writeSqliteFileMigFiles(dir string) error {
	upSql := "CREATE TABLE IF NOT EXISTS user (id INT PRIMARY KEY, name TEXT)"
	downSql := "DROP TABLE IF EXISTS user"
	basicMig := `{
	"name": "test_sql_mig",
	"actions": [
		{
			"file": {
				"up": "nested/up.sql",
				"down": "nested/down.sql"
			}
		}
	]
}
`
	basicMigPath := filepath.Join(dir, "basic.json")
	if err := os.WriteFile(basicMigPath, []byte(basicMig), 0644); err != nil {
		return err
	}

	nestedPath := filepath.Join(dir, "nested")
	if err := os.Mkdir(nestedPath, 0777); err != nil {
		return err
	}

	upMigPath := filepath.Join(nestedPath, "up.sql")
	if err := os.WriteFile(upMigPath, []byte(upSql), 0644); err != nil {
		return err
	}

	downMigPath := filepath.Join(nestedPath, "down.sql")
	if err := os.WriteFile(downMigPath, []byte(downSql), 0644); err != nil {
		return err
	}
	return nil
}

func writeRedirectedSqliteFileMigFiles(dir string) error {
	upSql := "CREATE TABLE IF NOT EXISTS user (id INT PRIMARY KEY, name TEXT)"
	downSql := "DROP TABLE IF EXISTS user"
	basicMig := `{
	"name": "test_sql_mig",
	"fileDir": "migs",
	"actions": [
		{
			"file": {
				"up": "nested/up.sql",
				"down": "nested/down.sql"
			}
		}
	]
}
`
	basicMigPath := filepath.Join(dir, "basic.json")
	if err := os.WriteFile(basicMigPath, []byte(basicMig), 0644); err != nil {
		return err
	}
	nestedPath := filepath.Join(dir, "migs")
	if err := os.Mkdir(nestedPath, 0777); err != nil {
		return err
	}

	nestedPath = filepath.Join(nestedPath, "nested")
	if err := os.Mkdir(nestedPath, 0777); err != nil {
		return err
	}

	upMigPath := filepath.Join(nestedPath, "up.sql")
	if err := os.WriteFile(upMigPath, []byte(upSql), 0644); err != nil {
		return err
	}

	downMigPath := filepath.Join(nestedPath, "down.sql")
	if err := os.WriteFile(downMigPath, []byte(downSql), 0644); err != nil {
		return err
	}
	return nil
}

func writeSqlMigFiles(dir string) error {
	basicMig := `{
	"name": "test_sql_mig",
	"actions": [
		{
			"raw": {
				"up": "CREATE TABLE IF NOT EXISTS user (id INT PRIMARY KEY, name TEXT)",
				"down": "DROP TABLE IF EXISTS user"
			}
		}
	]
}
`
	basicMigPath := filepath.Join(dir, "basic.json")
	err := os.WriteFile(basicMigPath, []byte(basicMig), 0644)
	if err != nil {
		return err
	}

	return nil
}

func writeJsonMigFiles(dir string) error {
	migs := []struct {
		file string
		sql  string
	}{
		{
			"z",
			`{
	"name": "test_json_mig_1",
	"actions": [
		{
			"createTable": {
				"name": "user",
				"columns": [
					{
						"name": "id",
						"autoincrement": true,
						"type": "integer"
					},
					{
						"name": "active",
						"type": "boolean",
						"default": false
					},
					{
						"name": "name",
						"type": "text",
						"notNull": true
					}
				],
				"primaryKey": ["id"]
			}
		},
		{
			"alterTable": {
				"name": "user",
				"renameTo": "users",
				"addColumns": [
					{
						"name": "email",
						"type": "text"
					}
				],
				"renameColumns": [
					{
						"from": "name",
						"to": "full_name"
					}
				]
			}
		},
		{
			"createIndex": {
				"name": "user_index",
				"table": "users",
				"columns": [ "full_name", "email" ],
				"unique": true
			}
		}
	]
}
`,
		},
	}

	for _, mig := range migs {
		migPath := filepath.Join(dir, mig.file+".json")
		err := os.WriteFile(migPath, []byte(mig.sql), 0644)
		if err != nil {
			return err
		}
	}

	return nil
}
