package db

import (
	"fmt"
	"slices"
	"strings"

	"github.com/dominikbraun/graph"

	"rove_migrate/common"

	"github.com/jmoiron/sqlx"
	log "github.com/sirupsen/logrus"
)

// Represents a database connection and it's associated configuration
type DbConn struct {
	*sqlx.DB
	conf    *common.DbConf
	actions common.DbActions
}

// Connects to a database with a given configuration
func ConnectTo(conf *common.DbConf) (*DbConn, error) {
	if conf == nil {
		return nil, fmt.Errorf("Cannot connect to DB. Received a nil DB conf")
	}

	driver, err := conf.SqlDriver()
	if err != nil {
		return nil, fmt.Errorf("Cannot connect to DB. %v", err)
	}
	log.Infof("Using driver %v to connect to database", driver)

	url, err := conf.Url()
	if err != nil {
		return nil, fmt.Errorf("Cannot connect to DB. %v", err)
	}

	conn, err := sqlx.Open(driver, url)
	if err != nil {
		return nil, fmt.Errorf("Cannot connect to DB. %v", err)
	}

	actions, err := conf.MigrationActions()
	if err != nil {
		return nil, fmt.Errorf("Cannot connect to DB. %v", err)
	}

	return &DbConn{conn, conf, actions}, nil
}

// Closes a database connection
func (conn *DbConn) Close() error {
	return conn.DB.Close()
}

// Gets database information from a database
func (conn *DbConn) GetInfo() (*common.Info, error) {
	log.Info("Getting information")
	return conn.actions.GetInfo(conn.DB, conn.conf)
}

// Ensures database "database" is created
func (conn *DbConn) EnsureDatabase(dbName string) error {
	log.Info("Ensuring database is created")
	return conn.actions.EnsureDatabase(dbName, conn.DB)
}

// Ensures database schema is created
func (conn *DbConn) EnsureSchema(schemaName string) error {
	log.Info("Ensuring schema is created")
	return conn.actions.EnsureSchema(schemaName, conn.DB)
}

// Ensures database is setup with migration tables
func (conn *DbConn) EnsureSetup() error {
	log.Info("Ensuring migration tables are setup")
	return conn.actions.EnsureSetup(conn.DB, conn.conf)
}

// Gets successful migration ids
func (conn *DbConn) getMigrated() ([]string, error) {
	log.Info("Getting list of successful migrations")
	return conn.actions.GetMigrated(conn.DB, conn.conf, common.MIG_STATE_SUCCESS)
}

// Gets errored migration ids
func (conn *DbConn) getErrored() ([]string, error) {
	log.Info("Getting list of errored migrations")
	return conn.actions.GetMigrated(conn.DB, conn.conf, common.MIG_STATE_ERROR)
}

// Gets errored rollback migration ids
func (conn *DbConn) getErroredRollbacks() ([]string, error) {
	log.Info("Getting list of errored rollbacks")
	return conn.actions.GetMigrated(conn.DB, conn.conf, common.MIG_STATE_ROLLBACK_ERROR)
}

// Gets id of migrations in an unknown state
func (conn *DbConn) getUnknown() ([]string, error) {
	log.Info("Getting list of unkown migrations")
	return conn.actions.GetMigrated(conn.DB, conn.conf, common.MIG_STATE_UNKNOWN)
}

// Gets integrity hash information for migrations
func (conn *DbConn) getHashes() ([]common.MigrationHashCheck, error) {
	log.Info("Getting database hashes")
	return conn.actions.GetHashes(conn.DB, conn.conf)
}

// Verifies the integrity of the current databse state against the current migration file state
func (conn *DbConn) verifyDatabaseState(migrations []*common.Migration, userMigrationFlow bool) (depGraph graph.Graph[string, string], migrated map[string]struct{}, err error) {
	log.Info("Verifying database state")
	delayedErrors := []error{}
	defer func() {
		if len(delayedErrors) > 0 {
			if err == nil {
				err = fmt.Errorf("Failed database integrity checks")
			} else {
				err = fmt.Errorf("%v\n%v", err, "Failed database integrity checks")
			}
			for _, e := range delayedErrors {
				err = fmt.Errorf("%v\n%v", err, e)
			}
		}
	}()

	errorList, err := conn.getErrored()
	if err != nil {
		return nil, nil, fmt.Errorf("Cannot verify database state. Unable to get errored migration data. %v", err)
	}
	errRollback, err := conn.getErroredRollbacks()
	if err != nil {
		return nil, nil, fmt.Errorf("Cannot verify database state. Unable to get errored rollback data. %v", err)
	}

	if len(errorList) > 0 {
		if userMigrationFlow {
			err := fmt.Errorf("Failed migrations detected! Manual intervention required. Please fix database! Failed migrations: %+v. Once DB is fixed, set failed migration states to '0' to rerun migration or '1' to skip migration.", errorList)
			log.Error(err)
			return nil, nil, err
		} else {
			err := fmt.Errorf("Failed migrations detected! Failed: %+v", errorList)
			delayedErrors = append(delayedErrors, err)
			log.Error(err)
		}
	}

	if len(errRollback) > 0 {
		if userMigrationFlow {
			err := fmt.Errorf("Failed rollbacks detected! Manual intervention required. Please fix database! Failed rollbacks: %+v. Once DB is fixed, set failed migration states to '0' to rerun forward migration or '1' to skip forward migration.", errorList)
			log.Error(err)
			return nil, nil, err
		} else {
			err := fmt.Errorf("Failed rollbacks detected! Failed: %+v", errRollback)
			delayedErrors = append(delayedErrors, err)
			log.Error(err)
		}
	}

	if userMigrationFlow {
		unknownList, err := conn.getUnknown()
		if err != nil {
			if userMigrationFlow {
				log.Warnf("Unable to get list of unknown migration states. Assuming list is empty. Cause: %v", err)
				unknownList = []string{}
			} else {
				return nil, nil, fmt.Errorf("Cannot verify database state. Unable to get migrations in unknown state. %v", err)
			}
		}
		if len(unknownList) > 0 {
			log.Warnf("Several migrations in unknown state. Assuming they aren't migrated. Unknown: %v", err)
		}
	}

	// Get what's been successfully migrated
	log.Info("Building dependency tree")
	migratedList, err := conn.getMigrated()
	if err != nil {
		return nil, nil, fmt.Errorf("Cannot verify database state. Unable to get migration data. %v", err)
	}

	migrated = map[string]struct{}{}
	for _, m := range migratedList {
		migrated[m] = struct{}{}
	}

	// Build correct graph of database state. We'll use this to compare against what's been ran
	depGraph, err = MakeMigrationGraph(migrations)
	if err != nil {
		return nil, nil, err
	}

	log.Info("Checking database migrations match expected dependency tree")
	correct := map[string]struct{}{}

	// Edge map gives a list of what each migration depends on
	// Used to ensure that the migraged migrations all have satisfied dependencies
	edgeMap, err := depGraph.PredecessorMap()
	if err != nil {
		return nil, nil, fmt.Errorf("Migration analysis failed. %v", err)
	}

	// Err Map gives a list of what each migration is depended on
	// Used for improving error messages with a list of what a missing migration is depended on
	errMap, err := depGraph.AdjacencyMap()
	if err != nil {
		return nil, nil, fmt.Errorf("Migration analysis failed. %v", err)
	}

	if err := verifyDeps(migratedList, migrated, correct, edgeMap, errMap); err != nil {
		return nil, nil, fmt.Errorf("Database in invalid state. Not all dependencies are satisfied. %+v", err)
	}

	log.Info("Checking migration hashes")
	expectedHashes := make(map[string]string, len(migrations))
	migsToHash := make(map[string]string, len(migrations))
	for _, m := range migrations {
		hash, err := m.IntegrityHash()
		if err != nil {
			return nil, nil, fmt.Errorf("Unable to generate expected integrity hash for migration %v", m.Name)
		}
		expectedHashes[hash] = m.Name
		migsToHash[m.Name] = hash
	}

	hashes, err := conn.getHashes()
	if err != nil {
		return nil, nil, fmt.Errorf("Unable to fetch validation hashes. %v", err)
	}

	for _, hashCheck := range hashes {
		if strings.HasPrefix(hashCheck.Hash, "0:") {
			// Always a warning, even in strict validation mode
			log.Warnf("Migration %v has a database hash set to \"ignore\". Ignored hashes are indicated with a \"0:\" at the start!", hashCheck.Name)
			continue
		}
		expectedMigration, found := expectedHashes[hashCheck.Hash]
		if !found {
			if h, ok := migsToHash[hashCheck.Name]; ok {
				e := fmt.Errorf("Mismatched hash for migration %v! Database hash: %+v. Migration hash: %+v", hashCheck.Name, hashCheck.Hash, h)
				log.Error(e)
				delayedErrors = append(delayedErrors, e)
				if userMigrationFlow {
					err = fmt.Errorf("Mismatched integrity hashes found between database and migration scripts! This usually means a script was changed after a migration was ran. To ignore this error, change the database hash to start with '0:' instead of '1:'")
				}
			} else {
				e := fmt.Errorf("Migration %v exists in the database, but the associated migration script cannot be found!", hashCheck.Name)
				log.Error(e)
				delayedErrors = append(delayedErrors, e)
				if userMigrationFlow {
					err = fmt.Errorf("Mismatched integrity hashes found between database and migration scripts! This usually means a script was changed after a migration was ran. To ignore this error, change the database hash to start with '0:' instead of '1:'")
				}
			}
		} else if expectedMigration != hashCheck.Name {
			// Shouldn't fail in this way. To do so would mean breaking SHA512 since we hash the name as well
			e := fmt.Errorf("Integrity hash checks failed for %v", hashCheck.Name)
			log.Error(e)
			delayedErrors = append(delayedErrors, e)
			if userMigrationFlow {
				err = fmt.Errorf("Mismatched integrity hashes found between database and migration scripts! This usually means a script was changed after a migration was ran. To ignore this error, change the database hash to start with '0:' instead of '1:'")
			}
		}
	}

	return depGraph, migrated, err
}

// Verifies that dependencies of migrated migrations are also migrated successfully
func verifyDeps(
	toCheck []string,
	migrated map[string]struct{},
	checked map[string]struct{},
	edgeMap map[string]map[string]graph.Edge[string],
	errMap map[string]map[string]graph.Edge[string],
) error {
	if len(toCheck) == 0 {
		return nil
	}
	cur := toCheck[0]
	next := toCheck[1:]

	// if we aren't cached, do a transitive tree check
	if _, ok := checked[cur]; !ok {
		if _, wasMigrated := migrated[cur]; !wasMigrated {
			log.Errorf("Dependency tree mismatch! Expected migration %+v to have been migrated!", cur)

			dependedOnMap := errMap[cur]
			dependedOn := make([]string, 0, len(dependedOnMap))
			for m, _ := range dependedOnMap {
				if _, ok := migrated[m]; ok {
					dependedOn = append(dependedOn, m)
				}
			}
			return fmt.Errorf("Expected migration %+v to have been migrated! Depended on by: %+v", cur, dependedOn)
		}

		edges := edgeMap[cur]

		toCheck := []string{}
		for connection, _ := range edges {
			toCheck = append(toCheck, connection)
		}

		if err := verifyDeps(toCheck, migrated, checked, edgeMap, errMap); err != nil {
			return err
		}

		checked[cur] = struct{}{}
	}

	return verifyDeps(next, migrated, checked, edgeMap, errMap)
}

// Gets the ordering for a tree subset starting at the current target
func getOrdering(edgeMap map[string]map[string]graph.Edge[string], ordering map[string]int, curOrder *int, curTarget string) {
	if _, ok := ordering[curTarget]; ok || curTarget == "" {
		return
	}

	ordering[curTarget] = *curOrder

	for k, _ := range edgeMap[curTarget] {
		*curOrder = 1 + *curOrder
		getOrdering(edgeMap, ordering, curOrder, k)
	}
}

// Plans how to run migrations
func (conn *DbConn) plan(migrations []*common.Migration, target string, userMigrationFlow bool, minimal bool) ([]*common.Migration, error) {
	g, migrated, err := conn.verifyDatabaseState(migrations, userMigrationFlow)
	if err != nil {
		return nil, err
	}

	log.Info("Planning migration execution")
	resMigs := make([]*common.Migration, 0, len(migrations))

	matchMigFound := false

	if minimal {
		m, err := g.PredecessorMap()
		if err != nil {
			return nil, err
		}
		ordering := map[string]int{}
		curOrder := 0
		getOrdering(m, ordering, &curOrder, target)

		// Limit output to non-migrated migrations in minimal tree
		for _, mig := range migrations {
			if _, ok := migrated[mig.Name]; !ok {
				if _, ok := ordering[mig.Name]; ok {
					resMigs = append(resMigs, mig)
				}
			}
			if mig.Name == target {
				matchMigFound = true
			}
		}
	} else {
		// Limit output to non-migrated migrations
		for _, mig := range migrations {
			if _, ok := migrated[mig.Name]; !ok {
				resMigs = append(resMigs, mig)
			}
			if mig.Name == target {
				matchMigFound = true
			}
		}
	}

	if target != "" && !matchMigFound {
		return nil, fmt.Errorf("Cannot create plan. Invalid target! Migration %#v does not exist!", target)
	}

	if len(resMigs) == 0 {
		return resMigs, nil
	}

	order, err := graph.StableTopologicalSort(g, func(a, b string) bool {
		return a < b
	})

	if err != nil {
		return nil, fmt.Errorf("Cannot create plan. %v", err)
	}

	orderIndex := make(map[string]int)
	for i, o := range order {
		orderIndex[o] = i
	}

	slices.SortFunc(resMigs, func(a, b *common.Migration) int {
		aO := orderIndex[a.Name]
		bO := orderIndex[b.Name]
		return aO - bO
	})

	end := slices.IndexFunc(resMigs, func(m *common.Migration) bool {
		return m.Name == target
	})

	if end >= 0 {
		return resMigs[:(end + 1)], nil
	} else {
		return resMigs, nil
	}
}

// Validates database and migration integrity
func (conn *DbConn) Validate(migrations []*common.Migration, scriptsOnly bool, strict bool) error {
	log.Info("Validating migrations")

	totalErrors := []error{}
	errors := []error{}
	for _, mig := range migrations {
		cmds, err := mig.Up(conn.conf)
		if err != nil {
			errors = append(errors, fmt.Errorf("Invalid migration %v. Reason: %+v", mig.Name, err))
			return err
		}

		if len(cmds) == 0 {
			if strict {
				errors = append(errors, fmt.Errorf("Migration %v is a no-op!", mig.Name))
				continue
			}
			log.Warnf("Migration %+v is a no-op!", mig.Name)
		}
	}

	if len(errors) > 0 {
		err := fmt.Errorf("Invalid migrations!")
		for _, e := range errors {
			err = fmt.Errorf("%v\n- %v", err, e)
		}
		totalErrors = append(totalErrors, err)
		errors = []error{}
	}

	log.Info("Validating database integrity")
	dbInfo, err := conn.GetInfo()
	if err != nil {
		totalErrors = append(totalErrors, err)
	} else {
		for m, mInfo := range dbInfo.Migrations.Migrations {
			if strict {
				errors = append(errors, fmt.Errorf("Migration %v is missing run information", m))
				continue
			}
			if mInfo.RunCount == 0 {
				log.Warnf("Migration %v is missing run information", m)
			}
		}
		if len(errors) > 0 {
			err := fmt.Errorf("Database in invalid state.")
			for _, e := range errors {
				err = fmt.Errorf("%v\n- %v", err, e)
			}
			totalErrors = append(totalErrors, err)
			errors = []error{}
		}
	}

	if _, err := conn.plan(migrations, "", false, false); err != nil {
		totalErrors = append(totalErrors, err)
	}

	unknownList, err := conn.getUnknown()
	if err != nil {
		return fmt.Errorf("Cannot verify database state. Unable to get migrations in unknown state. %v", err)
	}
	if len(unknownList) > 0 {
		if strict {
			err := fmt.Errorf("Several migrations in unknown state. This usually indicates a database was manually fixed. Migrations: %v", unknownList)
			totalErrors = append(totalErrors, err)
		} else {
			log.Warnf("Several migrations in unknown state. This usually happens when a database is manually fixed. Migrations: %v", unknownList)
		}
	}

	log.Info("Finished validation checks")

	if len(totalErrors) > 0 {
		err := fmt.Errorf("FAILURES DETECTED!")
		for _, e := range totalErrors {
			log.Error(e)
			err = fmt.Errorf("%v\n%v", err, e)
		}
		return err
	}
	return nil
}

// Plans how migrations would be ran against the database
func (conn *DbConn) PlanMigrations(migrations []*common.Migration, target string, minimal bool) (string, error) {
	if err := conn.EnsureSetup(); err != nil {
		return "", fmt.Errorf("Cannot plan migrations. %v", err)
	}
	plans, err := conn.plan(migrations, target, true, minimal)
	if err != nil {
		return "", err
	}
	builder := strings.Builder{}

	builder.WriteString("Migration Plan:\n")
	for _, pl := range plans {
		builder.WriteString(pl.Name)
		builder.WriteString("\n")
	}

	return builder.String(), nil
}

// Runs migrations against the database
func (conn *DbConn) RunMigrations(migrations []*common.Migration, target string, minimal bool) (string, error) {
	if err := conn.EnsureSetup(); err != nil {
		return "", fmt.Errorf("Cannot run migrations. %v", err)
	}

	builder := strings.Builder{}
	builder.WriteString("Migrations Ran:\n")

	order, err := conn.plan(migrations, target, true, minimal)
	if err != nil {
		return "", fmt.Errorf("Cannot run migrations. %v", err)
	}

	log.Info("Creating migration session")
	sessionId, err := conn.actions.CreateSession(false, conn.DB, conn.conf)
	if err != nil {
		return "", fmt.Errorf("Unable to create migration session. %v", err)
	}
	log.Infof("Started migration session %v", sessionId)

	log.Info("Running migrations")
	for _, mig := range order {
		cmds, err := mig.Up(conn.conf)
		if err != nil {
			log.Errorf("Failed to get commands for migration %v. Error %v", mig.Name, err)
			return builder.String(), err
		}

		if len(cmds) == 0 {
			log.Warnf("Migration %+v is a no-op!", mig.Name)
		}

		if err := conn.actions.RunMigration(mig, cmds, conn.DB, conn.conf); err != nil {
			log.Error(err)
			return builder.String(), fmt.Errorf("Migration failure detected. %v", err)
		} else if err := conn.actions.SaveMigrationToSession(sessionId, mig, conn.DB, conn.conf); err != nil {
			// We record this first since we did do database changes
			// We then run assertions since assertions could mark it as a failure, but the changes persist
			return builder.String(), fmt.Errorf("Unable to record that migration %v ran as part of session %v", mig.Name, sessionId)
		} else if err := conn.RunAssertions(mig); err != nil {
			log.Error(err)
			return builder.String(), fmt.Errorf("%v. Once the issues is resolved, mark the migration as either success (1) to skip or unknown (0) to retry.", err)
		} else {
			builder.WriteString(mig.Name)
			builder.WriteString("\n")
		}
	}

	return builder.String(), nil
}

// Runs assertions for a migration
// Runs assertions in a transaction and then rolls back the transaction
func (conn *DbConn) RunAssertions(migration *common.Migration) (totalErr error) {
	var aTx *sqlx.Tx

	defer func() {
		if aTx == nil {
			return
		}

		if err := aTx.Rollback(); err != nil {
			log.Panicf("Could not rollback database assertion transaction! Reason: %+v", err)
		}

		if totalErr != nil {
			tx, e := conn.DB.Beginx()
			if e != nil {
				log.Panicf("Could not update migration %+v info to reflect assertion failure! Reason: %e", migration.Name, e)
			}
			if e := conn.actions.SaveMigInfo(migration, common.MIG_STATE_ERROR, totalErr, tx, conn.conf); e != nil {
				log.Panicf("Could not update migration %+v info to reflect assertion failure! Reason: %e", migration.Name, e)
			}
			if e := tx.Commit(); e != nil {
				log.Panicf("Could not update migration %+v info to reflect assertion failure! Reason: %e", migration.Name, e)
			}
		}
	}()

	asserts, err := migration.Assertions()
	if err != nil {
		log.Error(err)
		return err
	}

	if len(asserts) == 0 {
		return nil
	}

	aTx, err = conn.DB.Beginx()
	if err != nil {
		log.Error(err)
		return err
	}

	for _, a := range asserts {
		if err := conn.actions.Assert(a, aTx, conn.conf); err != nil {
			err = fmt.Errorf("Assertions failed for migration %+v! Error: %+v", migration.Name, err)
			log.Error(err)
			return err
		}
	}

	return nil
}

// Plans how to rollback migrations
func (conn *DbConn) planRollback(migrations []*common.Migration, target string, minimal bool) ([]*common.Migration, error) {
	_, migrated, err := conn.verifyDatabaseState(migrations, true)
	if err != nil {
		return nil, err
	}

	log.Info("planning migration rollback")
	resMigs := make([]*common.Migration, 0, len(migrations))

	matchMigFound := false

	g, err := MakeRollbackGraph(migrations)

	// Limit output to successful migrations
	if minimal {
		m, err := g.PredecessorMap()
		if err != nil {
			return nil, err
		}
		ordering := map[string]int{}
		curOrder := 0
		getOrdering(m, ordering, &curOrder, target)

		// Limit output to non-migrated migrations in minimal tree
		for _, mig := range migrations {
			if _, ok := migrated[mig.Name]; ok {
				if _, ok := ordering[mig.Name]; ok {
					resMigs = append(resMigs, mig)
				}
			}
			if mig.Name == target {
				matchMigFound = true
			}
		}
	} else {
		// Limit output to migrated migrations
		for _, mig := range migrations {
			if _, ok := migrated[mig.Name]; ok {
				resMigs = append(resMigs, mig)
			}
			if mig.Name == target {
				matchMigFound = true
			}
		}
	}

	if target != "" && !matchMigFound {
		return nil, fmt.Errorf("Cannot plan rollback. Invalid target! Migration %#v does not exist!", target)
	}

	if len(resMigs) == 0 {
		return resMigs, nil
	}

	order, err := graph.StableTopologicalSort(g, func(a, b string) bool {
		return a < b
	})

	if err != nil {
		return nil, fmt.Errorf("Cannot plan rollback. %v", err)
	}

	orderIndex := make(map[string]int)
	for i, o := range order {
		orderIndex[o] = i
	}

	slices.SortFunc(resMigs, func(a, b *common.Migration) int {
		aO := orderIndex[a.Name]
		bO := orderIndex[b.Name]
		return aO - bO
	})

	end := slices.IndexFunc(resMigs, func(m *common.Migration) bool {
		return m.Name == target
	})

	if end >= 0 {
		return resMigs[:(end + 1)], nil
	} else {
		return resMigs, nil
	}
}

// Plans how migrations would be rolled back against the database
func (conn *DbConn) PlanRollback(migrations []*common.Migration, target string, minimal bool) (string, error) {
	if err := conn.EnsureSetup(); err != nil {
		return "", fmt.Errorf("Cannot plan rollback. %v", err)
	}
	plans, err := conn.planRollback(migrations, target, minimal)
	if err != nil {
		return "", err
	}
	builder := strings.Builder{}

	builder.WriteString("Rollback Plan:\n")
	for _, pl := range plans {
		builder.WriteString(pl.Name)
		builder.WriteString("\n")
	}

	return builder.String(), nil
}

// Rolls back migrations against the database
func (conn *DbConn) RollbackMigrations(migrations []*common.Migration, target string, minimal bool) (string, error) {
	if err := conn.EnsureSetup(); err != nil {
		return "", fmt.Errorf("Cannot perform rollback. %v", err)
	}

	builder := strings.Builder{}
	builder.WriteString("Migrations Rolled Back:\n")

	ordered, err := conn.planRollback(migrations, target, minimal)
	if err != nil {
		return "", fmt.Errorf("Cannot perform rollback. %v", err)
	}

	log.Info("Creating migration session")
	sessionId, err := conn.actions.CreateSession(true, conn.DB, conn.conf)
	if err != nil {
		return "", fmt.Errorf("Unable to create migration session. %v", err)
	}
	log.Infof("Started migration session %v", sessionId)

	log.Info("Running rollback of migrations")
	for _, mig := range ordered {
		cmds, err := mig.Down(conn.conf)
		if err != nil {
			log.Errorf("Failed to get commands for rollback of migration %v. Error %v", mig.Name, err)
			return builder.String(), err
		}

		if len(cmds) == 0 {
			log.Warnf("Rollback of migration %+v is a no-op!", mig.Name)
		}

		if err := conn.actions.RollbackMigration(mig, cmds, conn.DB, conn.conf); err != nil {
			return builder.String(), fmt.Errorf("Rollback failure detected. %v", err)
		} else if err := conn.actions.SaveMigrationToSession(sessionId, mig, conn.DB, conn.conf); err != nil {
			// We record this first since we did do database changes
			return builder.String(), fmt.Errorf("Unable to record that rollback of migration %v ran as part of session %v", mig.Name, sessionId)
		} else {
			builder.WriteString(mig.Name)
			builder.WriteString("\n")
		}
	}

	return builder.String(), nil
}

func (conn *DbConn) GetLastSessionInfo() (*common.SessionInfo, error) {
	return conn.actions.GetLastSessionInfo(conn.DB, conn.conf)
}

func (conn *DbConn) GetSessionInfo(sessionId int64) (*common.SessionInfo, error) {
	return conn.actions.GetSessionInfo(sessionId, conn.DB, conn.conf)
}

func (conn *DbConn) planSessionRevert(g graph.Graph[string, string], session *common.SessionInfo, migrated map[string]struct{}, migrations []*common.Migration) ([]*common.Migration, error) {
	_, migrated, err := conn.verifyDatabaseState(migrations, true)
	if err != nil {
		return nil, err
	}

	log.Info("Planning session revert")
	resMigs := make([]*common.Migration, 0, len(migrations))

	m, err := g.PredecessorMap()
	if err != nil {
		return nil, err
	}
	ordering := map[string]int{}
	curOrder := 0

	for _, target := range session.Migrations {
		getOrdering(m, ordering, &curOrder, target)
	}

	if session.IsRollback {
		// Limit output to non-migrated migrations for reverting a rollback
		for _, mig := range migrations {
			if _, ok := migrated[mig.Name]; !ok {
				if _, ok := ordering[mig.Name]; ok {
					resMigs = append(resMigs, mig)
				}
			}
		}
	} else {
		// Limit output to migrated migrations for reverting a forward migration
		for _, mig := range migrations {
			if _, ok := migrated[mig.Name]; ok {
				if _, ok := ordering[mig.Name]; ok {
					resMigs = append(resMigs, mig)
				}
			}
		}
	}

	// If there's no work to do, return
	if len(resMigs) == 0 {
		return resMigs, nil
	}

	order, err := graph.StableTopologicalSort(g, func(a, b string) bool {
		return a < b
	})

	if err != nil {
		return nil, fmt.Errorf("Cannot create plan. %v", err)
	}

	orderIndex := make(map[string]int)
	for i, o := range order {
		orderIndex[o] = i
	}

	slices.SortFunc(resMigs, func(a, b *common.Migration) int {
		aO := orderIndex[a.Name]
		bO := orderIndex[b.Name]
		return aO - bO
	})

	return resMigs, nil
}

func (conn *DbConn) RevertSession(session *common.SessionInfo, dryRun bool, migrations []*common.Migration) (string, error) {
	isRollback := !session.IsRollback

	log.Info("Building dependency tree")
	migratedList, err := conn.getMigrated()
	if err != nil {
		return "", fmt.Errorf("Cannot verify database state. Unable to get migration data. %v", err)
	}

	migrated := map[string]struct{}{}
	for _, m := range migratedList {
		migrated[m] = struct{}{}
	}

	var depGraph graph.Graph[string, string]

	if isRollback {
		log.Infof("Rolling back session %v", session.Id)
		depGraph, err = MakeRollbackGraph(migrations)
	} else {
		log.Infof("Migrating forward session %v", session.Id)
		depGraph, err = MakeMigrationGraph(migrations)
	}

	if err != nil {
		return "", err
	}

	order, err := conn.planSessionRevert(depGraph, session, migrated, migrations)
	if err != nil {
		return "", fmt.Errorf("Failed to plan session revert! %v", err)
	}

	if dryRun {
		res := strings.Builder{}
		if isRollback {
			res.WriteString("Migration plan:\n")
		} else {
			res.WriteString("Rollback plan:\n")
		}

		for _, m := range order {
			res.WriteString("- ")
			res.WriteString(m.Name)
			res.WriteString("\n")
		}
		return res.String(), nil
	}

	log.Info("Creating migration session")
	sessionId, err := conn.actions.CreateSession(isRollback, conn.DB, conn.conf)
	if err != nil {
		return "", fmt.Errorf("Unable to create migration session. %v", err)
	}
	log.Infof("Started migration session %v", sessionId)

	res := strings.Builder{}
	if isRollback {
		res.WriteString("Migrations Rolled Back:\n")
	} else {
		res.WriteString("Migrations Ran:\n")
	}

	for _, mig := range order {
		if isRollback {
			cmds, err := mig.Down(conn.conf)
			if err != nil {
				log.Errorf("Failed to get commands for rollback of migration %v. Error %v", mig.Name, err)
				return res.String(), err
			}

			if len(cmds) == 0 {
				log.Warnf("Rollback of migration %+v is a no-op!", mig.Name)
			}

			if err := conn.actions.RollbackMigration(mig, cmds, conn.DB, conn.conf); err != nil {
				return res.String(), fmt.Errorf("Rollback failure detected. %v", err)
			} else if err := conn.actions.SaveMigrationToSession(sessionId, mig, conn.DB, conn.conf); err != nil {
				// We record this first since we did do database changes
				return res.String(), fmt.Errorf("Unable to record that rollback of migration %v ran as part of session %v", mig.Name, sessionId)
			} else {
				res.WriteString(mig.Name)
				res.WriteString("\n")
			}
		} else {
			cmds, err := mig.Up(conn.conf)
			if err != nil {
				log.Errorf("Failed to get commands for migration %v. Error %v", mig.Name, err)
				return res.String(), err
			}

			if len(cmds) == 0 {
				log.Warnf("Migration %+v is a no-op!", mig.Name)
			}

			if err := conn.actions.RunMigration(mig, cmds, conn.DB, conn.conf); err != nil {
				log.Error(err)
				return res.String(), fmt.Errorf("Migration failure detected. %v", err)
			} else if err := conn.actions.SaveMigrationToSession(sessionId, mig, conn.DB, conn.conf); err != nil {
				// We record this first since we did do database changes
				// We then run assertions since assertions could mark it as a failure, but the changes persist
				return res.String(), fmt.Errorf("Unable to record that migration %v ran as part of session %v", mig.Name, sessionId)
			} else if err := conn.RunAssertions(mig); err != nil {
				log.Error(err)
				return res.String(), fmt.Errorf("%v. Once the issues is resolved, mark the migration as either success (1) to skip or unknown (0) to retry.", err)
			} else {
				res.WriteString(mig.Name)
				res.WriteString("\n")
			}
		}
	}

	return res.String(), nil
}
