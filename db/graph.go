package db

import (
	"errors"
	"fmt"
	"rove_migrate/common"

	"github.com/dominikbraun/graph"
)

// Makes a dependency graph geared towards rollbacks
func MakeRollbackGraph(migrations []*common.Migration) (graph.Graph[string, string], error) {
	g := graph.New(graph.StringHash, graph.Directed(), graph.PreventCycles())

	// Add vertexes
	for _, mig := range migrations {
		err := g.AddVertex(mig.Name)
		if err != nil {
			if errors.Is(err, graph.ErrVertexAlreadyExists) {
				return nil, fmt.Errorf("Duplicate migration id found! ID: %#v", mig.Name)
			}
			return nil, fmt.Errorf("Cannot build dependency graph. %v", err)
		}
	}

	// Add edges
	for _, mig := range migrations {
		for _, dep := range mig.Deps {
			// Edges are backwards from migration plan since we're "deconstructing" rather than "building"
			err := g.AddEdge(mig.Name, string(dep))
			if err != nil {
				switch {
				case errors.Is(err, graph.ErrVertexNotFound):
					return nil, fmt.Errorf("Invalid dependency graph. Migration %#v not found but depened on by %#v", dep, mig.Name)
				case errors.Is(err, graph.ErrEdgeCreatesCycle):
					return nil, fmt.Errorf("Invalid dependency graph. Cyclic dependency found with %#v and %#v!", dep, mig.Name)
				default:
					return nil, fmt.Errorf("Cannog build dependency graph. %v", err)
				}
			}
		}
	}

	return g, nil
}

// Makes a dependency graph geared towards forward migrations
func MakeMigrationGraph(migrations []*common.Migration) (graph.Graph[string, string], error) {
	g := graph.New(graph.StringHash, graph.Directed(), graph.PreventCycles())

	// Add vertexes
	for _, mig := range migrations {
		err := g.AddVertex(mig.Name)
		if err != nil {
			if errors.Is(err, graph.ErrVertexAlreadyExists) {
				return nil, fmt.Errorf("Duplicate migration id found! ID: %#v", mig.Name)
			}
			return nil, fmt.Errorf("Cannot build dependency graph. %v", err)
		}
	}

	// Add edges
	for _, mig := range migrations {
		for _, dep := range mig.Deps {
			err := g.AddEdge(string(dep), mig.Name)
			if err != nil {
				switch {
				case errors.Is(err, graph.ErrVertexNotFound):
					return nil, fmt.Errorf("Invalid dependency graph. Migration %#v not found but depened on by %#v", dep, mig.Name)
				case errors.Is(err, graph.ErrEdgeCreatesCycle):
					return nil, fmt.Errorf("Invalid dependency graph. Cyclic dependency found with %#v and %#v!", dep, mig.Name)
				default:
					return nil, fmt.Errorf("Cannog build dependency graph. %v", err)
				}
			}
		}
	}

	return g, nil
}
